import com.remsdaq.osapplication.entities.mait.XmlToMait;
import com.remsdaq.osapplication.entities.mait.entities.messages.IncidentCreationMait;
import com.remsdaq.osapplication.entities.mait.entities.messages.Mait;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class MainTest {
	
	public MainTest() throws JAXBException, IOException {
		
		// xml filepath
		String filePath = "C:\\Users\\cpt\\Documents\\Codes\\Testing\\CreateIncidentTest.XML";
		
		// Get xml as stream of strings
		List<String> strings = Files.readAllLines(Paths.get(filePath));
		String validXml = strings.stream().collect(Collectors.joining());
		
		Mait mait = new XmlToMait().getMait(validXml);
		
		IncidentCreationMait incidentCreationMait = (IncidentCreationMait) mait;
		
		
		System.out.println(incidentCreationMait.getSummary());
		
		System.out.println(mait.getXml());
		
		System.out.println(incidentCreationMait.getMessageControl().getSummary());
		System.out.println(incidentCreationMait.getCallerDetails().getSummary());
		System.out.println(incidentCreationMait.getC4Site().getSummary());
		
		// Create XmlToMait object
		XmlToMait xmlToMait = new XmlToMait();
		
		// get mait from xml
		Object maitObject = xmlToMait.getMait(validXml);
		
		// downcast?
		IncidentCreationMait icm = (IncidentCreationMait) maitObject;
		System.out.println(icm.getSummary());
		
		//        Email email = new Email();
		//        email.setValue("chris.p.topping@gmail.com");
		//        email.setPriority(1);
		//        System.out.println(email + "\n");
		//
		//        Contact mobile = new Mobile("07913241957", 2);
		//        System.out.println(mobile);
		//
		//        Contact pager = new Pager("", 3);
		
		//        PolygonGeometry pg = new PolygonGeometry();
		//        ArrayList<SimplePolygon> cp = new ArrayList<>();
		//        SimplePolygon sp = new SimplePolygon();
		//
		//
		//        // Using corners of anfield stadium because it's small and square and has a known area
		//        LatLng northWest = new LatLng(-2.960704, 53.431399); // NW corner
		//        LatLng northEast = new LatLng(-2.959901, 53.431020); // NE corner
		//        LatLng southEast = new LatLng(-2.960929, 53.430296); // SE corner
		//        LatLng southWest = new LatLng(-2.961695, 53.430694); // SW corner
		//
		//        sp.addVertex(northWest);
		//        sp.addVertex(northEast);
		//        sp.addVertex(southEast);
		//        sp.addVertex(southWest);
		//
		//        cp.add(sp);
		//        pg.addComplexPolygon(cp);
		//
		//        System.out.println("Compiled mystery area: " + pg.calculateAreaInMeters(sp.area()));
		//
		//        System.out.println("Area in km: " + pg.geometryArea("kilometre"));
		//        System.out.println("Area in metres: " + pg.geometryArea("metre"));
		
	}
	
	public static void main(String[] args) throws IOException, JAXBException, ClassNotFoundException, WellKnownTextException {
//		MainTest test = new MainTest();
		testGridSquare();
	}
	
	public static void testGridSquare() {
		String[][] strGridCodes = new String[][]{
				{"SV", "SQ", "SL", "SF", "SA", "NV", "NQ", "NL", "NF", "NA", "HV", "HQ"},
				{"SW", "SR", "SM", "SG", "SB", "NW", "NR", "NM", "NG", "NB", "HW", "HR"},
				{"SX", "SS", "SN", "SH", "SC", "NX", "NS", "NN", "NH", "NC", "HX", "HS"},
				{"SY", "ST", "SO", "SJ", "SD", "NY", "NT", "NO", "NJ", "ND", "HY", "HT"},
				{"SZ", "SU", "SP", "SK", "SE", "NZ", "NU", "NP", "NK", "NE", "HZ", "HU"},
				{"TV", "TQ", "TL", "TF", "TA", "OV", "OQ", "OL", "OF", "OA", "JV", "JQ"},
				{"TW", "TR", "TM", "TG", "TB", "OW", "OR", "OM", "OG", "OB", "JW", "JR"}
		};
		
		String[][] INVERTED_ALPHABET_SQUARE_ARRAY = new String[][] {
				{"V","W","X","Y","Z"},
				{"Q","R","S","T","U"},
				{"L","M","N","O","P"},
				{"F","G","H","J","K"},
				{"A","B","C","D","E"}
		};
		
		int alphabetArrayLength = INVERTED_ALPHABET_SQUARE_ARRAY.length;
		int elementsPerGrid = (int) Math.pow(alphabetArrayLength, 2);
		String[][] codeGridArray = new String[elementsPerGrid*alphabetArrayLength][elementsPerGrid*alphabetArrayLength];
		
		for (int i = 0; i < alphabetArrayLength; i++) {
			for (int j = 0; j < alphabetArrayLength; j++) {
				for (int k = 0; k < alphabetArrayLength; k++) {
					for (int m = 0; m < alphabetArrayLength; m++) {
						codeGridArray[i*elementsPerGrid + k][j*elementsPerGrid + m] = INVERTED_ALPHABET_SQUARE_ARRAY[i][j] + INVERTED_ALPHABET_SQUARE_ARRAY[k][m];
						System.out.println("i: " + i + " j: " + j + " k: " + k + " m: " + m);
						System.out.println(codeGridArray[i*elementsPerGrid + k][j*elementsPerGrid + m]);
					}
				}
			}
		}
		
		int eastOriginMajor = 2;
		int northOriginMajor = 1;
		int eastOriginMinor = 0;
		int northOriginMinor = 0;
		int eastRangeMajor = 1;
		int northRangeMajor = 2;
		int eastRangeMinor = 2;
		int northRangeMinor = 2;
		
//		String[][] gridSquareSubArray = Arrays.copyOfRange(codeGridArray, )
		
		System.out.println("EQUAL: " + codeGridArray.equals(strGridCodes));
	}
	
	
	
}