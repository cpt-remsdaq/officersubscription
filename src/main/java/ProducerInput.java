
import com.remsdaq.osapplication.entities.events.Producer;
import com.remsdaq.osapplication.configs.Config;
import java.util.Scanner;



/**
 *
 * @author cpt
 */
public class ProducerInput {

    public static void main(String[] args) {
        Config config = new Config();
        
        while (true) {
            
            System.out.println("Enter text to be sent through activeMQ:");
            Scanner scanner = new Scanner(System.in);
            String text = scanner.nextLine();

            Producer producer = new Producer();
            Producer.setQueueName(config.getProperty("event_queue_name"));
            Producer.setConnectionAddress(config.getProperty("event_queue_address"));
            producer.setText(text);
            producer.run();
        
        }
   }
}
