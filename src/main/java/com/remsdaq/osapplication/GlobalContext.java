package com.remsdaq.osapplication;

import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.DatabaseConnection;
import com.remsdaq.osapplication.entities.events.Event;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import javafx.stage.Stage;

import java.util.ArrayList;

public class GlobalContext {
	
	// Singleton instance
	private static GlobalContext instance = null;
	
	private boolean testing = false;
	
	// Backend variables
	private Object wrapper;
	//    private Object controller;
	private Stage stage;
	private DatabaseConnection databaseConnection;
	private Config config;
	
	// Entity arrays
	private ArrayList<Subscriber> subscribers;
	private ArrayList<Polygon> polygons;
	private ArrayList<Event> events;
	private ArrayList<PolygonTag> polygonTags;
	
	// Entities
	private Subscriber subscriber;
	private Polygon polygon;
	
	// Singleton instance
	public static synchronized GlobalContext getInstance() {
		if (instance == null) {
			System.out.println("Instantiating Global Context");
			instance = new GlobalContext();
		}
		return instance;
	}
	
	public Object getWrapper() {
		return wrapper;
	}
	
	public void setWrapper(Object wrapper) {
		this.wrapper = wrapper;
	}
	
	// Backend variables
	public Stage getStage() {
		return this.stage;
	}
	
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	public DatabaseConnection getDatabaseConnection() {
		return this.databaseConnection;
	}
	
	public void setDatabaseConnection(DatabaseConnection databaseConnection) {
		this.databaseConnection = databaseConnection;
	}
	
	public Config getConfig() {
		return this.config;
	}
	
	public void setConfig(Config config) {
		this.config = config;
	}
	
	//    public Object getController() {
	//        return controller;
	//    }
	//
	//    public void setController(Object controller) {
	//        this.controller = controller;
	//    }
	
	// Entity arrays
	public ArrayList<Subscriber> getSubscribers() {
		return this.subscribers;
	}
	
	public void setSubscribers(ArrayList<Subscriber> subscribers) {
		this.subscribers = subscribers;
	}
	
	public ArrayList<Polygon> getPolygons() {
		return this.polygons;
	}
	
	public void setPolygons(ArrayList<Polygon> polygons) {
		this.polygons = polygons;
	}
	
	public ArrayList<Event> getEvents() {
		return this.events;
	}
	
	public void setEvents(ArrayList<Event> events) {
		this.events = events;
	}
	
	public ArrayList<PolygonTag> getPolygonTags() {
		return this.polygonTags;
	}
	
	public void setPolygonTags(ArrayList<PolygonTag> polygonTags) {
		this.polygonTags = polygonTags;
	}
	
	// Entities
	public Subscriber getSubscriber() {
		return this.subscriber;
	}
	
	public void setSubscriber(Subscriber subscriber) {
		this.subscriber = subscriber;
	}
	
	public Polygon getPolygon() {
		return this.polygon;
	}
	
	public void setPolygon(Polygon polygon) {
		this.polygon = polygon;
	}
	
	public boolean isTesting() {
		return testing;
	}
	
	public void setTesting(boolean testing) {
		this.testing = testing;
	}
}
