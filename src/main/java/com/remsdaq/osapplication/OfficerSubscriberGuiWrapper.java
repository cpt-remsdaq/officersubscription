package com.remsdaq.osapplication;

import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.mock.MockDataConnection;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

/**
 *
 * @author cpt
 */
public class OfficerSubscriberGuiWrapper extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws IOException {

        try {
            // Set up global context variables
            GlobalContext.getInstance().setWrapper(this);
            GlobalContext.getInstance().setStage(primaryStage);
//            GlobalContext.getInstance().setDatabaseConnection(new Hibernate());
//            GlobalContext.getInstance().setDatabaseConnection(new XmlDataConnection());
            GlobalContext.getInstance().setDatabaseConnection(new MockDataConnection());
            GlobalContext.getInstance().setConfig(new Config());

            // Load main app window
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/OSApplication.fxml"));
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Main.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);

            // If main app window is closed, quit process
            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    quit();
                }
            });

            // Show main app window
            primaryStage.show();
        } catch (IOException e) {
            System.out.print("\n" + e);
            throw new IOException("Oh no! ", e);
        }
    }

    public void quit() {
        System.exit(0);
    }

    public void kill() {
        System.exit(9); // Let console user know they're a murderer
    }

    public void exitWithsignal(int signal) {
        if (signal > 0 && signal+128 < 255) {
            System.exit(128 + signal);
        } else {
            System.exit(255); // signal out of range
        }
    }
}