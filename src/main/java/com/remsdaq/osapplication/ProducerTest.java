package com.remsdaq.osapplication;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;

/**
 *
 * @author cpt
 */
class ProducerTest {
 
    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) {
        HelloWorldProducer producer = new HelloWorldProducer();
 
        Thread threadProducer = new Thread(producer);
        threadProducer.start();
    }
 
    /**
     *
     */
    static class HelloWorldProducer implements Runnable {
        public void run() {
            try {
                // Create a ConnectionFactory
                ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                        "tcp://cpt:61616");
 
                // Create a Connection
                Connection connection = connectionFactory.createConnection();
                connection.start();
 
                // Create a Session
                Session session = connection.createSession(false,
                        Session.AUTO_ACKNOWLEDGE);
 
                // Create the destination (Topic or Queue)
                Destination destination = session.createQueue("EventQueue");
 
                // Create a MessageProducer from the Session to the Topic or
                // Queue
                MessageProducer producer = session.createProducer(destination);
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
                
                
                String exampleText = "FIRE001::Fire::53.298410,-3.129580";
                TextMessage example = session.createTextMessage(exampleText);
                producer.send(example);
                
                while (1==1) {
                    // Create a messages
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                    Date now = new Date();
                    String strDate = sdf.format(now);
                    String text = "The time is: " + strDate;
                    TextMessage message = session.createTextMessage(text);
                    Thread.sleep(1000);
                    producer.send(message);
                }

                //session.close();
                //connection.close();
            } catch (Exception e) {
                System.out.println("Caught: " + e);
                e.printStackTrace();
            }
        }
    }
}
