/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.configs;

import java.io.*;
import java.util.Properties;

/**
 * @author cpt
 */
public class Config {
	
	String configFilePath = "configs/config.properties";
	Properties configFile;
	ConfigDefault configDefault;
	
	public Config(String configFilePath) {
		this.configFilePath = configFilePath;
		loadConfigFromPath();
		getDefaults();
	}
	
	public Config() {
		loadConfigFromPath();
		getDefaults();
	}
	
	public String getConfigFilePath() {
		return configFilePath;
	}
	
	private void loadConfigFromPath() {
		configFile = new Properties();
		try (InputStream in = new FileInputStream(
				this.getClass().getClassLoader().getResource(configFilePath).getFile())) {
			configFile.load(in);
			//            configFile.load(this.getClass().getClassLoader().getResourceAsStream(configFilePath));
		} catch (IOException eta) {
		}
	}
	
	private void getDefaults() {
		configDefault = new ConfigDefault();
		configDefault.getConfigDefault().forEach((kvPair) -> {
			configFile.putIfAbsent(kvPair.getKey(), kvPair.getValue());
		});
	}
	
	public static final String getProperty(String key, String overloadString) {
		String value = new Config().configFile.getProperty(key);
		return value;
	}
	
	public String getProperty(String key) {
		String value = this.configFile.getProperty(key);
		return value;
	}
	
	public void setProperty(String key, String value) {
		configFile.setProperty(key, value);
	}
	
	public void writeProperty(String key, String value) throws IOException {
		configFile.setProperty(key, value);
		this.saveProperties();
	}
	
	public void saveProperties() throws IOException {
		try (OutputStream out = new FileOutputStream(
				this.getClass().getClassLoader().getResource(configFilePath).getFile())) {
			configFile.store(out, "Officer Subscription Properties");
		}
	}
}
