/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.configs;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cpt
 */
class ConfigDefault {
	
	private List<ConfigPair> configDefault = new ArrayList<>();
	
	public ConfigDefault() {
		// Timing
		configDefault.add(new ConfigPair("info_label_timer", "3000")); // milliseconds to show info label
		
		// seconds between checking databases
		// if they have not been updated by user
		configDefault.add(new ConfigPair("database_check_frequency", "3600"));
		
		configDefault.add(new ConfigPair("database_username", "postgres"));
		configDefault.add(new ConfigPair("database_password", "postgres"));
		configDefault.add(new ConfigPair("database_path", "jdbc:postgresql://localhost:5432"));
		configDefault.add(new ConfigPair("database_name", "postgres"));
		configDefault.add(new ConfigPair("hibernate_config_xml", "hibernate/static-hibernate.cfg.xml"));
		
		//        configDefault.add(new ConfigPair("xml_data_folder_path","xmlData/default"));
		configDefault.add(new ConfigPair("subscriber_collection_xml_data_path", "xmlData/default/subscriber-collection.xml"));
		configDefault.add(new ConfigPair("subscriber_xml_data_path", "xmlData/default/subscriber.xml"));
		
		configDefault.add(new ConfigPair("polygon_collection_xml_data_path", "xmlData/default/polygon-collection.xml"));
		configDefault.add(new ConfigPair("polygon_xml_data_path", "xmlData/default/polygon.xml"));
		
		configDefault.add(new ConfigPair("polygon_tag_collection_xml_data_path", "xmlData/default/polygon-tag-collection.xml"));
		configDefault.add(new ConfigPair("polygon_tag_xml_data_path", "xmlData/default/polygon-tag.xml"));
		
		// Field length limits
		configDefault.add(new ConfigPair("subscribers_ID_limit", "24"));
		configDefault.add(new ConfigPair("subscribers_first_name_limit", "48"));
		configDefault.add(new ConfigPair("subscribers_second_name_limit", "48"));
		configDefault.add(new ConfigPair("subscribers_email_limit", "512"));
		configDefault.add(new ConfigPair("subscribers_pager_limit", "13"));
		configDefault.add(new ConfigPair("subscribers_mobile_limit", "13"));
		
		configDefault.add(new ConfigPair("polygon_ID_limit", "24"));
		configDefault.add(new ConfigPair("polygons_name_limit", "48"));
		configDefault.add(new ConfigPair("polygons_description_limit", "512"));
		
		// File extensions to be viewed when importing/exporting polygon files
		configDefault.add(new ConfigPair("polygon_file_filters", "*.wkt;*.polygon;*.txt,*.wkt,*.polygon,*.txt"));
		
		//screen is set to maximum on startup
		configDefault.add(new ConfigPair("screen_maximised", "true"));
		
		// Require confirmation before actions are...actioned?
		configDefault.add(new ConfigPair("require_confirmation_boolean", "false")); // Require for all
		configDefault.add(new ConfigPair("require_edit_confirmation_boolean", "true")); // Confirm on edit
		configDefault.add(new ConfigPair("require_save_confirmation_boolean", "true")); // Confirm on save
		configDefault.add(new ConfigPair("require_cancel_confirmation_boolean", "true")); // Confirm on cancel
		configDefault.add(new ConfigPair("require_delete_confirmation_boolean", "true")); // Confirm on delete
		
		// Polygon map properties
		configDefault.add(new ConfigPair("route_colour", "#0500bd"));
		configDefault.add(new ConfigPair("route_line_width", "5"));
		configDefault.add(new ConfigPair("route_line_opacity", "1.0"));
		configDefault.add(new ConfigPair("route_line_style", "solid"));
		configDefault.add(new ConfigPair("route_colour_glow", "#54fff9"));
		configDefault.add(new ConfigPair("route_glow_line_width", "9"));
		configDefault.add(new ConfigPair("route_fill_colour", "#0500bd"));
		configDefault.add(new ConfigPair("route_fill_opacity", "0.3"));
		
		
		// Hole map properties
		configDefault.add(new ConfigPair("hole_colour", "#2F9E28"));
		configDefault.add(new ConfigPair("hole_line_width", "3"));
		configDefault.add(new ConfigPair("hole_line_opacity", "1.0"));
		configDefault.add(new ConfigPair("hole_line_style", "solid"));
		configDefault.add(new ConfigPair("hole_colour_glow", "#67ff54"));
		configDefault.add(new ConfigPair("hole_glow_line_width", "5"));
		configDefault.add(new ConfigPair("hole_fill_colour", "#ffffff"));
		configDefault.add(new ConfigPair("hole_fill_opacity", "0.0"));
		
		// Total number of points to be shown
		// If the polygon vertex number is greater than this
		// the polygon will be simplified
		configDefault.add(new ConfigPair("maximum_points_shown_on_map", "1000"));
		// Used for polygon simplification
		configDefault.add(new ConfigPair("distance_threshold", "0.0001"));
		
		// Greenwich
		configDefault.add(new ConfigPair("default_map_location_latitude", "51.4826"));
		configDefault.add(new ConfigPair("default_map_location_longitude", "0.0077"));
		
		// Default zoom
		configDefault.add(new ConfigPair("default_map_zoom", "12"));
		configDefault.add(new ConfigPair("area_units", "mile"));
		
		// Events stream defaults
		configDefault.add(new ConfigPair("check_events", "true"));
		configDefault.add(new ConfigPair("event_queue_name", "events_queue"));
		
		// Number of integer priorities from 1 to N, 1 being highest
		configDefault.add(new ConfigPair("number_of_priorities", "3"));
	}
	
	public List<ConfigPair> getConfigDefault() {
		return this.configDefault;
	}
}
