/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.configs;

/**
 *
 * @author cpt
 */
public class ConfigPair {
    private String key;
    private String value;
    
    public ConfigPair(String key, String value) {
        this.key = key;
        this.value = value;
    }
    
    public String getKey() {
        return key;
    }
    
    public ConfigPair setKey(String key) {
        this.key = key;
        return this;
    }
    
    public String getValue() {
        return value;
    }
    
    public ConfigPair setValue(String value) {
        this.value = value;
        return this;
    }
}
