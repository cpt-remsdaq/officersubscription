package com.remsdaq.osapplication.database;

import java.util.Collection;

public interface DatabaseConnection {

    Boolean delete(Object toDelete);
    Boolean update(Object toUpdate);
    Object select(Object toSelect, String identifier);
    Boolean insert(Object toInsert);
    Collection selectAll(Object object);
}
