package com.remsdaq.osapplication.database;

import java.util.Collection;

public class DatabaseUtilities {

    public static Boolean insertSingle(DatabaseConnection databaseConnection, Object insertObject) {
        databaseConnection.insert(insertObject);
        return true;
    }

    public static Boolean insertMultiple(DatabaseConnection databaseConnection, Collection insertObjects) {
        for (Object insertObject : insertObjects) {
            databaseConnection.insert(insertObject);
        }
        return true;
    }

    public static Boolean deleteSingle(DatabaseConnection databaseConnection, Object deleteObject) {
        return databaseConnection.delete(deleteObject);
    }

    public static Boolean deleteMultiple(DatabaseConnection databaseConnection, Collection deleteObjects) {
        for (Object deleteObject : deleteObjects) {
            databaseConnection.delete(deleteObject);
        }
        return true;
    }

    public static Boolean updateSingle(DatabaseConnection databaseConnection, Object updateObject) {
        return databaseConnection.update(updateObject);
    }

    public static Boolean updateMultiple(DatabaseConnection databaseConnection, Collection updateObjects) {
        for (Object updateObject : updateObjects) {
            databaseConnection.update(updateObject);
        }
        return true;
    }

    public static Object selectSingle(DatabaseConnection databaseConnection, Object toSelect, String identifier) {
        return databaseConnection.select(toSelect, identifier);
    }

    public static Collection selectAll(DatabaseConnection databaseConnection, Object object) {
        return databaseConnection.selectAll(object);
    }
}
