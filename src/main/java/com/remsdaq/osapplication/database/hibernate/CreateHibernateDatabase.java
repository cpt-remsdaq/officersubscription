package com.remsdaq.osapplication.database.hibernate;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.sql.*;

public class CreateHibernateDatabase {

    static Connection connection;

    static void create(Config config) throws SQLException {

        // Get database setup values form config
        String username = config.getProperty("database_username");
        String password = config.getProperty("database_password");
        String path = config.getProperty("database_path");
        String name = config.getProperty("database_name");

        // Make connection post postgres
        connection = DriverManager.getConnection(path + "postgres", username, password);
        Statement statement = connection.createStatement();

        // Create database
        statement.execute("CREATE DATABASE " + name);

    }

}
