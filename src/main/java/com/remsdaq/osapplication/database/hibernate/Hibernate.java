/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.database.hibernate;

import com.remsdaq.osapplication.database.DatabaseConnection;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author cpt
 */
public final class Hibernate implements DatabaseConnection {
    
    private final Configuration CONFIG = new Configuration();
    private SessionFactory factory;
    private Session session;
    private Transaction transaction;
    private Boolean testing = false;
    
    public Hibernate() {
        System.out.println("##### PREPARING HIBERNATE CONNECTION #####");
        this.factory = CONFIG.configure().buildSessionFactory();
        this.openFactory();
    }

    @Override
    public Boolean delete(Object toDelete) {
        Boolean success = false;
        Object toDeleteCast = toDelete.getClass().cast(toDelete);

        try {
            openSession();
            Session session = getSession();
            Transaction transaction = session.beginTransaction();
            // Delete object
            session.delete(toDeleteCast);
            transaction.commit();

            System.out.println("Successfully deleted");
            success = true;
        } catch (HibernateException e) {
            throw new HibernateException("Unable to delete object " + e);
        } finally {
            closeSession();
            return success;
        }
    }

    @Override
    public Boolean update(Object toUpdate) {
        Boolean success = false;

        Object toUpdateCast = toUpdate.getClass().cast(toUpdate);

        try {
            openSession();
            Session session = getSession();
            Transaction transaction = session.beginTransaction();
            // Update object
            session.update(toUpdateCast);
            transaction.commit();

            System.out.println("Successfully updated");
            success = true;

        } catch (HibernateException e) {
            throw new HibernateException("Unable to update object " + e );
        } finally {
            closeSession();
            return success;
        }

    }

    @Override
    public Object select(Object toSelect, String identifier) {
        Object selection = null;

        try {
            selection = session.load(toSelect.getClass(),identifier);
        } catch (HibernateException e) {
            throw new HibernateException("Unable to select subscriber: " + e);
        } finally {
            closeSession();
        }
        return selection;
    }

    @Override
    public Collection selectAll(Object object) {
        Collection selection = new ArrayList();

        try {
            openSession();
            Session session = getSession();
            Query query = session.createQuery("SELECT p FROM " + object.getClass().getName() + " p");
            List<Object> selectionList = query.getResultList();
            selection.addAll(selectionList);
        } catch (HibernateException e) {
            throw new HibernateException("Unable to select subscriber: " + e);
        } finally {
            closeSession();
        }
        return selection;
    }

    @Override
    public Boolean insert(Object toInsert) {
        Boolean success = false;

        Object toInsertCast = toInsert.getClass().cast(toInsert);

        try {
            openSession();
            Session session = getSession();
            Transaction transaction = session.beginTransaction();
            // Insert object
            session.save(toInsertCast);
            transaction.commit();

            System.out.println("Successfully Saved");
            success =  true;

        } catch (HibernateException e) {
            throw new HibernateException("Unable to insert object: " + e);
        } finally {
            closeSession();
            return success;
        }
    }

    private SessionFactory getFactory() {
        return this.factory;
    }

    private Session getSession() {
        return this.session;
    }

    private void openAll() {
        this.factory = this.CONFIG.buildSessionFactory();
        this.session = this.factory.openSession();
    }

    private void openSession() {
        this.session = this.factory.openSession();
    }

    private void openFactory() {
        this.factory = this.CONFIG.buildSessionFactory();
    }

    private void closeAll() {
        this.session.close();
        this.factory.close();
    }
    
    private void closeSession() {
        this.session.close();
    }

    private void closeFactory() {
        this.factory.close();
    }
}
