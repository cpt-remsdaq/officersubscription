package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import java.util.List;

public final class MockData {
	
	private static MockData instance = null;
	
	private List<Subscriber> subscribers;
	private List<Polygon> polygons;
	private List<PolygonTag> polygonTags;
	
	private MockData() {
		subscribers = MockSubscriberSet.getSubscriberSet();
		polygons = MockPolygonSet.getPolygonSet();
		polygonTags = MockPolygonTagSet.getPolygonTagSet();
	}
	
	public static synchronized MockData getInstance() {
		if (instance == null) {
			instance = new MockData();
		}
		return instance;
	}
	
	public List<Subscriber> getSubscribers() {
		return subscribers;
	}
	
	public void setSubscribers(List<Subscriber> subscribers) {
		this.subscribers = subscribers;
	}
	
	public List<Polygon> getPolygons() {
		return polygons;
	}
	
	public void setPolygons(List<Polygon> polygons) {
		this.polygons = polygons;
	}
	
	public List<PolygonTag> getPolygonTags() {
		return polygonTags;
	}
	
	public void setPolygonTags(List<PolygonTag> polygonTags) {
		this.polygonTags = polygonTags;
	}
}
