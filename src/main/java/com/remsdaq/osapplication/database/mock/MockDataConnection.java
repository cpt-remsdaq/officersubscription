package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.database.DatabaseConnection;
import com.remsdaq.osapplication.entities.Entity;

import java.util.Collection;

public class MockDataConnection implements DatabaseConnection {
	
	@Override
	public Boolean delete(Object toDelete) {
		MockDataConnection connection = MockDataConnectionFactory.getConnection((Entity) toDelete);
		return connection.delete(toDelete);
	}
	
	@Override
	public Boolean update(Object toUpdate) {
		MockDataConnection connection = MockDataConnectionFactory.getConnection((Entity) toUpdate);
		return connection.update(toUpdate);
	}
	
	@Override
	public Object select(Object toSelect, String identifier) {
		MockDataConnection connection = MockDataConnectionFactory.getConnection((Entity) toSelect);
		return connection.select(toSelect, identifier);
	}
	
	@Override
	public Boolean insert(Object toInsert) {
		MockDataConnection connection = MockDataConnectionFactory.getConnection((Entity) toInsert);
		return connection.insert(toInsert);
	}
	
	@Override
	public Collection selectAll(Object object) {
		MockDataConnection connection = MockDataConnectionFactory.getConnection((Entity) object);
		return connection.selectAll(object);
	}
}
