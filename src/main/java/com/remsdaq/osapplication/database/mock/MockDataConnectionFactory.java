package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.entities.Entity;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

public class MockDataConnectionFactory {
	
	public static MockDataConnection getConnection(Entity entity) {
	
		if (entity instanceof Subscriber) {
			return new MockSubscriberConnection();
		} else if (entity instanceof Polygon) {
			return new MockPolygonConnection();
		} else if (entity instanceof PolygonTag) {
			return new MockPolygonTagConnection();
		} else {
			throw new RuntimeException("The entity has no mock connection");
		}
	}
	
}
