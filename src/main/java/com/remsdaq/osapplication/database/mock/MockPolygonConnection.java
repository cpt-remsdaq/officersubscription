package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.entities.polygons.Polygon;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class MockPolygonConnection extends MockDataConnection {

	private static final List<Polygon> polSet = MockData.getInstance().getPolygons();
	Polygon operablePol;
	
	@Override
	public Boolean delete(Object toDelete) {
		operablePol = (Polygon) toDelete;
		Iterator iter = polSet.iterator();
		
		while(iter.hasNext()) {
			Polygon pol = (Polygon) iter.next();
			if (pol.equals(operablePol.getID())) {
				iter.remove();
				setPols(polSet);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Boolean update(Object toUpdate) {
		operablePol = (Polygon) toUpdate;
		boolean deleteSuccessful = delete(toUpdate);
		if (deleteSuccessful) {
			polSet.add(operablePol);
			setPols(polSet);
		}
		return deleteSuccessful;
	}
	
	@Override
	public Boolean insert(Object toInsert) {
		operablePol = (Polygon) toInsert;
		boolean isObjectNotInData = true;
		for (Polygon pol : polSet) {
			if (pol.equals(operablePol.getID())) {
				isObjectNotInData = false;
			}
		}
		if (isObjectNotInData) {
			polSet.add(operablePol);
			setPols(polSet);
		}
		return isObjectNotInData;
	}
	
	@Override
	public Object select(Object toSelect, String identifier) {
		for (Polygon pol : polSet) {
			if (pol.getID().equals(identifier)) {
				return (Object) pol;
			}
		}
		return null;
	}
	
	@Override
	public Collection selectAll(Object toSelect) {
		return (Collection) polSet;
	}
	
	private void setPols(List<Polygon> polSet) {
		MockData.getInstance().setPolygons(polSet);
	}
	
}
