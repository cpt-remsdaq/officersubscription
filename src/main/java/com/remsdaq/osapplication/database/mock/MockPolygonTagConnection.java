package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class MockPolygonTagConnection extends MockDataConnection {
	
	private static final List<PolygonTag> tagSet = MockData.getInstance().getPolygonTags();
	PolygonTag operableTag;
	
	@Override
	public Boolean delete(Object toDelete) {
		operableTag = (PolygonTag) toDelete;
		Iterator iter = tagSet.iterator();
		while (iter.hasNext()) {
			PolygonTag tag = (PolygonTag) iter.next();
			if (tag.equals(operableTag.getID())) {
				iter.remove();
				setTags(tagSet);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Boolean update(Object toUpdate) {
		operableTag = (PolygonTag) toUpdate;
		boolean deleteSuccessful = delete(toUpdate);
		if (deleteSuccessful) {
			tagSet.add(operableTag);
			setTags(tagSet);
		}
		return deleteSuccessful;
	}
	
	@Override
	public Boolean insert(Object toInsert) {
		operableTag = (PolygonTag) toInsert;
		boolean isObjectNotInData = true;
		for (PolygonTag tag : tagSet) {
			if (tag.getID().equals(operableTag.getID())) {
				isObjectNotInData = false;
			}
		}
		if (isObjectNotInData) {
			tagSet.add(operableTag);
			setTags(tagSet);
		}
		return isObjectNotInData;
	}
	
	@Override
	public Object select(Object toUpdate, String identifier) {
		for (PolygonTag tag : tagSet) {
			if (tag.getID().equals(identifier)) {
				return (Object) tag;
			}
		}
		return null;
	}
	
	@Override
	public Collection selectAll(Object toUpdate) {
		return (Collection) tagSet;
	}
	
	private void setTags(List<PolygonTag> tagSet) {
		MockData.getInstance().setPolygonTags(tagSet);
	}
}
