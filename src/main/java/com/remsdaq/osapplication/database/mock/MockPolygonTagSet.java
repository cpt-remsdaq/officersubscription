package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;

import java.util.ArrayList;
import java.util.List;

public class MockPolygonTagSet {
	
	private static List<PolygonTag> tagSet = new ArrayList<>();
	
	private static final PolygonTag TEST = new PolygonTag("TEST");
	private static final PolygonTag COUNTY = new PolygonTag("COUNTY");
	private static final PolygonTag BOROUGH = new PolygonTag("BOROUGH");
	private static final PolygonTag URBAN = new PolygonTag("URBAN");
	
	public static List<PolygonTag> getPolygonTagSet() {
		tagSet.add(TEST);
		tagSet.add(COUNTY);
		tagSet.add(BOROUGH);
		tagSet.add(URBAN);
		
		return tagSet;
		
	}
	
}
