package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class MockSubscriberConnection extends MockDataConnection {
	
	private static final List<Subscriber> subSet = MockData.getInstance().getSubscribers();
	Subscriber operableSub;
	
	@Override
	public Boolean delete(Object toDelete) {
		operableSub = (Subscriber) toDelete;
		Iterator iter = subSet.iterator();
		
		while(iter.hasNext()) {
			Subscriber sub = (Subscriber) iter.next();
			if (sub.equals(operableSub.getID())) {
				iter.remove();
				setSubs(subSet);
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Boolean update(Object toUpdate) {
		operableSub = (Subscriber) toUpdate;
		boolean deleteSuccessful = delete(toUpdate);
		if (deleteSuccessful) {
			subSet.add(operableSub);
			setSubs(subSet);
		}
		return deleteSuccessful;
	}
	
	@Override
	public Boolean insert(Object toInsert) {
		operableSub = (Subscriber) toInsert;
		boolean isObjectNotInData = true;
		for (Subscriber sub : subSet) {
			if (sub.equals(operableSub.getID())) {
				isObjectNotInData = false;
			}
		}
		if (isObjectNotInData) {
			subSet.add(operableSub);
			setSubs(subSet);
		}
		return isObjectNotInData;
	}
	
	@Override
	public Object select(Object toSelect, String identifier) {
		for (Subscriber sub : subSet) {
			if (sub.getID().equals(identifier)) {
				return (Object) sub;
			}
		}
		return null;
	}
	
	@Override
	
	public Collection selectAll(Object toSelect) {
		return (Collection) subSet;
	}
	
	private void setSubs(List<Subscriber> subSet) {
		MockData.getInstance().setSubscribers(subSet);
	}
}
