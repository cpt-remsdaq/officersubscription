package com.remsdaq.osapplication.database.mock;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.entities.subscribers.SubscriberBuilder;

import java.util.ArrayList;
import java.util.List;

public class MockSubscriberSet {
	
	private static final Subscriber jHendrix = new SubscriberBuilder("JHENDRIX")
			.firstName("James")
			.lastName("Hendrix")
			.email("allalongthewatchtower@gmail.co.uk", 1)
			.build();
	
	private static final Subscriber dGrohl = new SubscriberBuilder("DGROHL")
			.firstName("David")
			.lastName("Grohl")
			.mobile("01234567890", 1)
			.email("onexone@gmail.com", 2)
			.build();
	
	private static final Subscriber rMarley = new SubscriberBuilder("RMARLEY")
			.firstName("Robert")
			.lastName("Marley")
			.pager("420", 1)
			.email("redemptions@ngs.com", 1)
			.build();
	
	private static final Subscriber jCash = new SubscriberBuilder("JCASH")
			.firstName("Jonathan")
			.lastName("Cash")
			.email("aboynamedsue@yahoo.us", 3)
			.build();
	
	
	private static final Subscriber dBowie = new SubscriberBuilder("DBOWIE")
			.firstName("David")
			.lastName("Bowie")
			.email("lifeonmars@hotmail.co.uk", 1)
			.build();
	
	private static final Subscriber fMercury = new SubscriberBuilder("FMERCURY")
			.firstName("Fredrick")
			.lastName("Mercury")
			.email("sevenseasofrye@gmail.com", 1)
			.pager("1", 1)
			.mobile("2", 2)
			.build();
	
	private static final Subscriber eSmith = new SubscriberBuilder("ESMITH")
			.firstName("Elliot")
			.lastName("Smith")
			.email("waltznumb@two.org", 2)
			.build();
	
	private static List<Subscriber> subscriberSet = new ArrayList<>();
	
	public static List<Subscriber> getSubscriberSet() {
		subscriberSet.add(jHendrix);
		subscriberSet.add(dGrohl);
		subscriberSet.add(rMarley);
		subscriberSet.add(jCash);
		subscriberSet.add(dBowie);
		subscriberSet.add(fMercury);
		subscriberSet.add(eSmith);
		
		return subscriberSet;
	}
	
}
