package com.remsdaq.osapplication.database.xml;

import com.remsdaq.osapplication.entities.XmlDataEntity;

import java.util.Collection;

public interface XmlDataCollectionEntity extends XmlDataEntity {
	Collection getCollection();
	void removeFromCollection(XmlDataEntity xmlDataEntity);
	void addToCollection(XmlDataEntity xmlDataEntity);
	void updateInCollection(XmlDataEntity xmlDataEntity);
}
