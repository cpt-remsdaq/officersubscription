package com.remsdaq.osapplication.database.xml;

import com.remsdaq.osapplication.database.DatabaseConnection;
import com.remsdaq.osapplication.database.xml.entities.polygonTags.PolygonTagCollection;
import com.remsdaq.osapplication.database.xml.entities.polygonTags.PolygonTagXmlAdapter;
import com.remsdaq.osapplication.database.xml.entities.polygons.PolygonCollection;
import com.remsdaq.osapplication.database.xml.entities.polygons.PolygonXmlAdapter;
import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberCollection;
import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberXmlAdapter;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.Collection;

public class XmlDataConnection implements DatabaseConnection {
	
	@Override
	public Boolean delete(Object toDelete) {
		XmlDataEntity toDeleteCast = castToXmlDataEntity(toDelete);
		XmlDataCollectionEntity collectionEntity = selectAllAndReturnEntityWithRelationships(toDeleteCast);
		collectionEntity.removeFromCollection(toDeleteCast);
		marshalToFile(collectionEntity);
		return true;
	}
	
	@Override
	public Boolean update(Object toUpdate) {
		XmlDataEntity toUpdateCast = castToXmlDataEntity(toUpdate);
		XmlDataCollectionEntity collectionEntity = selectAllAndReturnEntityWithRelationships(toUpdateCast);
		collectionEntity.updateInCollection(toUpdateCast);
		marshalToFile(collectionEntity);
		return true;
	}
	
	@Override
	public Object select(Object toSelect, String identifier) {
		return returnEntityFromCollection(toSelect, identifier);
	}
	
	@Override
	public Boolean insert(Object toInsert) {
		XmlDataEntity toInsertCast = castToXmlDataEntity(toInsert);
		XmlDataCollectionEntity collectionEntity = selectAllAndReturnEntityWithRelationships(toInsertCast);
		collectionEntity.addToCollection(toInsertCast);
		marshalToFile(collectionEntity);
		return true;
	}
	
	@Override
	public Collection selectAll(Object toSelect) {
		XmlDataEntity toSelectCast = castToXmlDataEntity(toSelect);
		XmlDataCollectionEntity collectionEntity = selectAllAndReturnEntityWithRelationships(toSelectCast);
		return collectionEntity.getCollection();
	}
	
	private XmlDataEntity castToXmlDataEntity(Object toDelete) {
		return (XmlDataEntity) toDelete;
	}
	
	private XmlDataCollectionEntity selectAllAndReturnEntity(XmlDataEntity toSelectCast) {
		XmlDataCollectionEntity collectionEntity = null;
		try {
			Unmarshaller jaxbUnmarshaller = createJAXBUnmarshaller(toSelectCast.makeCollection());
			collectionEntity = (XmlDataCollectionEntity) jaxbUnmarshaller.unmarshal(
					new File(toSelectCast.getXmlDataCollectionPath()));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return collectionEntity;
	}
	
	private XmlDataCollectionEntity selectAllAndReturnEntityWithRelationships(XmlDataEntity toSelectCast) {
		
		XmlDataCollectionEntity collectionEntity = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(SubscriberCollection.class, PolygonCollection.class, PolygonTagCollection.class,
			                                                  Subscriber.class, Polygon.class, PolygonTag.class);
			
			Unmarshaller jaxbUnmarshaller = createJAXBUnmarshaller(toSelectCast.makeCollection());
			
			SubscriberXmlAdapter subAdapter = populateSubscriberAdapter();
			PolygonXmlAdapter polAdapter = populatePolygonAdapter();
			PolygonTagXmlAdapter tagAdapter = populatePolygonTagAdapter();
			
			jaxbUnmarshaller.setAdapter(SubscriberXmlAdapter.class, subAdapter);
			jaxbUnmarshaller.setAdapter(PolygonXmlAdapter.class, polAdapter);
			jaxbUnmarshaller.setAdapter(PolygonTagXmlAdapter.class, tagAdapter);
			
			collectionEntity = (XmlDataCollectionEntity) jaxbUnmarshaller.unmarshal(
					new File(toSelectCast.getXmlDataCollectionPath()));
			
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return collectionEntity;
	}
	
	private PolygonTagXmlAdapter populatePolygonTagAdapter() {
		PolygonTagCollection tagCollection = (PolygonTagCollection) selectAllAndReturnEntity(new PolygonTag());
		PolygonTagXmlAdapter tagAdapter = new PolygonTagXmlAdapter();
		for (PolygonTag tag : tagCollection.getCollection()) {
			tagAdapter.getPolygonTags().put(tag.getID(), tag);
		}
		return tagAdapter;
	}
	
	private PolygonXmlAdapter populatePolygonAdapter() {
		PolygonCollection polCollection = (PolygonCollection) selectAllAndReturnEntity(new Polygon());
		PolygonXmlAdapter polAdapter = new PolygonXmlAdapter();
		for (Polygon pol : polCollection.getCollection()) {
			polAdapter.getPolygons().put(pol.getID(), pol);
		}
		return polAdapter;
	}
	
	private SubscriberXmlAdapter populateSubscriberAdapter() {
		SubscriberCollection subCollection = (SubscriberCollection) selectAllAndReturnEntity(new Subscriber());
		SubscriberXmlAdapter subAdapter = new SubscriberXmlAdapter();
		for (Subscriber sub : subCollection.getCollection()) {
			subAdapter.getSubscribers().put(sub.getID(), sub);
		}
		return subAdapter;
	}
	
	private void marshalToFile(XmlDataEntity toMarshal) {
		try {
			Marshaller jaxbMarshaller = createJAXBMarshaller(toMarshal);
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.marshal(toMarshal, new File(toMarshal.getXmlDataPath()));
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	
	private Unmarshaller createJAXBUnmarshaller(XmlDataEntity xmlDataEntity) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(xmlDataEntity.getClass());
		return jaxbContext.createUnmarshaller();
	}
	
	private Marshaller createJAXBMarshaller(XmlDataEntity xmlDataEntity) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(xmlDataEntity.getClass());
		return jaxbContext.createMarshaller();
	}
	
	private XmlDataEntity returnEntityFromCollection(Object toSelect, String identifier) {
		Collection<XmlDataEntity> collection = selectAll(toSelect);
		for (XmlDataEntity entity : collection) {
			if (entity.getID().equals(identifier)) {
				return entity;
			}
		}
		return null;
	}
	
	private XmlDataEntity unmarshalFromFile(XmlDataEntity xmlDataEntity, File xmlFile) {
		try {
			Unmarshaller jaxbUnmarshaller = createJAXBUnmarshaller(xmlDataEntity);
			return (XmlDataEntity) jaxbUnmarshaller.unmarshal(xmlFile);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}
}
