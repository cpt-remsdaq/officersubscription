package com.remsdaq.osapplication.database.xml.entities.polygonTags;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.database.xml.XmlDataCollectionEntity;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;

import javax.persistence.Transient;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "PolygonTags")
public class PolygonTagCollection implements XmlDataCollectionEntity {
	
	@XmlTransient
	private List<PolygonTag> collection;
	
	public PolygonTagCollection() {
		collection = new ArrayList<>();
	}
	
	@XmlElement(name = "PolygonTag")
	public List<PolygonTag> getTags() {
		return this.collection;
	}
	
	@Override
	@Transient
	@XmlTransient
	public Collection<PolygonTag> getCollection() {
		return this.collection;
	}
	
	public void setCollection(List<PolygonTag> collection) {
		this.collection = collection;
	}
	
	@Override
	public void removeFromCollection(XmlDataEntity xmlDataEntity) {
		removeFromCollection(xmlDataEntity.getID());
	}
	
	@Override
	public void addToCollection(XmlDataEntity xmlDataEntity) {
		this.collection.add((PolygonTag) xmlDataEntity);
	}
	
	@Override
	public void updateInCollection(XmlDataEntity updateEntity) {
		PolygonTag updateTag = (PolygonTag) updateEntity;
		
		Boolean hasBeenRemoved = false;
		
		for (Iterator<PolygonTag> iterator = collection.iterator(); iterator.hasNext(); ) {
			PolygonTag tag = iterator.next();
			if (tag.getID().equals(updateTag.getID())) {
				iterator.remove();
				hasBeenRemoved = true;
				break;
			}
		}
		if (hasBeenRemoved) {
			collection.add(updateTag);
		}
	}
	
	public void removeFromCollection(String tagID) {
		for (Iterator<PolygonTag> iterator = collection.iterator(); iterator.hasNext(); ) {
			PolygonTag tag = iterator.next();
			if (tag.getID().equals(tagID)) {
				iterator.remove();
				return;
			}
		}
	}
	
	public PolygonTag selectFromCollection(PolygonTag tag) {
		return selectFromCollection(tag.getID());
	}
	
	public PolygonTag selectFromCollection(String tagID) {
		for (Iterator<PolygonTag> iterator = collection.iterator(); iterator.hasNext(); ) {
			PolygonTag tag = iterator.next();
			if (tag.getID().equals(tagID)) {
				return tag;
			}
		}
		return null;
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataPath() {
		return null;
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataCollectionPath() {
		return GlobalContext.getInstance().getConfig().getProperty("polygons_tag_collection_xml_data_path");
	}
	
	@Override
	public XmlDataCollectionEntity makeCollection() {
		return this;
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getID() {
		return null;
	}
	
	@Override
	@XmlTransient
	public EntityType getEntityType() {
		return EntityType.POLYGON_TAG;
	}
}
