package com.remsdaq.osapplication.database.xml.entities.polygonTags;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;

public class PolygonTagToXml {
	
	public static void convertAndOutputToStream(PolygonTag tag, OutputStream os) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		jaxbMarshaller.marshal(tag, os);
	}
	
	private static Marshaller createFormattedMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(PolygonTag.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		return jaxbMarshaller;
	}
	
	public static String convertCollectionToString(PolygonTagCollection toConvert) throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(toConvert.getClass());
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		
		jaxbMarshaller.marshal(toConvert, sw);
		return sw.toString();
		
	}
	
	public static void convertToFile(PolygonTag tag, String filePath) throws JAXBException,
	                                                                         FileNotFoundException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		OutputStream os = new FileOutputStream(filePath);
		jaxbMarshaller.marshal(tag, os);
	}
	
	public static String convertToString(PolygonTag tag) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(tag, sw);
		return sw.toString();
	}
	
	private static Marshaller createFormattedCollectionMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(PolygonTagCollection.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		return jaxbMarshaller;
	}
}

