package com.remsdaq.osapplication.database.xml.entities.polygonTags;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.HashMap;
import java.util.Map;

public class PolygonTagXmlAdapter extends XmlAdapter<String, PolygonTag> {
	private Map<String, PolygonTag> polygonTags = new HashMap<String, PolygonTag>();
	
	public Map<String, PolygonTag> getPolygonTags() {
		return polygonTags;
	}
	
	@Override
	public PolygonTag unmarshal(String v) {
		return polygonTags.get(v);
	}
	
	@Override
	public String marshal(PolygonTag v) {
		return v.getName();
	}


}
