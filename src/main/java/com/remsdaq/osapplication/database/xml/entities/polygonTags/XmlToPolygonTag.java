package com.remsdaq.osapplication.database.xml.entities.polygonTags;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class XmlToPolygonTag {
	
	public static PolygonTag convertFromString(String xml) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(PolygonTag.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		PolygonTag tag = (PolygonTag) jaxbUnmarshaller.unmarshal(
				new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8.name())));
		return tag;
	}
	
}
