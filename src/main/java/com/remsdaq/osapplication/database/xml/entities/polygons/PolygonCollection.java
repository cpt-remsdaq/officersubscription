package com.remsdaq.osapplication.database.xml.entities.polygons;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.database.xml.XmlDataCollectionEntity;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.polygons.Polygon;

import javax.xml.bind.annotation.*;
import java.beans.Transient;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Polygons")
public class PolygonCollection implements XmlDataCollectionEntity {
	
	@XmlTransient
	private List<Polygon> collection;
	
	public PolygonCollection() {
		collection = new ArrayList<>();
	}
	
	@XmlElement(name = "Polygon")
	public List<Polygon> getPolygons() {
		return this.collection;
	}
	
	@XmlTransient
	public Collection<Polygon> getCollection() {
		return this.collection;
	}
	
	public void setCollection(List<Polygon> collection) {
		this.collection = collection;
	}
	
	@Override
	public void removeFromCollection(XmlDataEntity xmlDataEntity) {
		removeFromCollection(xmlDataEntity.getID());
	}
	
	public void removeFromCollection(String polID) {
		for (Iterator<Polygon> iterator = collection.iterator(); iterator.hasNext(); ) {
			Polygon pol = iterator.next();
			if (pol.getID().equals(polID)) {
				iterator.remove();
				return;
			}
		}
	}
	
	@Override
	public void addToCollection(XmlDataEntity xmlDataEntity) {
		this.collection.add((Polygon) xmlDataEntity);
	}
	
	public Polygon selectFromCollection(Polygon pol) {
		return selectFromCollection(pol.getID());
	}
	
	public Polygon selectFromCollection(String polID) {
		for (Iterator<Polygon> iterator = collection.iterator(); iterator.hasNext(); ) {
			Polygon pol = iterator.next();
			if (pol.getID().equals(polID)) {
				return pol;
			}
		}
		return null;
	}
	
	@Override
	public void updateInCollection(XmlDataEntity updateEntity) {
		Polygon updatePol = (Polygon) updateEntity;
		
		Boolean hasBeenRemoved = false;
		
		for (Iterator<Polygon> iterator = collection.iterator(); iterator.hasNext();) {
			Polygon pol = iterator.next();
			if (pol.getID().equals(updatePol.getID())) {
				iterator.remove();
				hasBeenRemoved = true;
				break;
			}
		}
		if (hasBeenRemoved) {
			collection.add(updatePol);
		}
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataPath() {
		return GlobalContext.getInstance().getConfig().getProperty("polygon_collection_xml_data_path");
	}
	
	@Override
	public String getXmlDataCollectionPath() {
		return GlobalContext.getInstance().getConfig().getProperty("polygon_collection_xml_data_path");
	}
	
	@Override
	public XmlDataCollectionEntity makeCollection() {
		return this;
	}
	
	@Override
	@javax.persistence.Transient
	@XmlTransient
	public String getID() {
		return null;
	}
	
	@Override
	@XmlTransient
	public EntityType getEntityType() {
		return EntityType.POLYGON;
	}
}

