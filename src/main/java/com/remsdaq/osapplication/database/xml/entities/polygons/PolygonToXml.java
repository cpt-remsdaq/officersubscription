package com.remsdaq.osapplication.database.xml.entities.polygons;


import com.remsdaq.osapplication.entities.polygons.Polygon;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;

public class PolygonToXml {
	
	public static void convertAndOutputToStream(Polygon polygon, OutputStream os) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		jaxbMarshaller.marshal(polygon, os);
	}
	
	private static Marshaller createFormattedMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Polygon.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		return jaxbMarshaller;
	}
	
	public static void convertToFile(Polygon polygon, String filePath) throws JAXBException, FileNotFoundException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		OutputStream os = new FileOutputStream(filePath);
		jaxbMarshaller.marshal(polygon, os);
	}
	
	public static String convertToString(Polygon polygon) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(polygon, sw);
		return sw.toString();
	}
	
}
