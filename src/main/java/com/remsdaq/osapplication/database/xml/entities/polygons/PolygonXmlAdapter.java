package com.remsdaq.osapplication.database.xml.entities.polygons;

import com.remsdaq.osapplication.entities.polygons.Polygon;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.HashMap;
import java.util.Map;

public class PolygonXmlAdapter extends XmlAdapter<String, Polygon> {
	private Map<String, Polygon> polygons = new HashMap<String, Polygon>();
	
	public Map<String, Polygon> getPolygons() {
		return polygons;
	}
	
	@Override
	public Polygon unmarshal(String v) {
		return polygons.get(v);
	}
	
	@Override
	public String marshal(Polygon v) {
		return v.getID();
	}
}