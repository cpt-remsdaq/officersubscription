package com.remsdaq.osapplication.database.xml.entities.polygons;

import com.remsdaq.osapplication.database.xml.entities.polygonTags.PolygonTagCollection;
import com.remsdaq.osapplication.database.xml.entities.polygonTags.PolygonTagXmlAdapter;
import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberCollection;
import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberXmlAdapter;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class XmlToPolygon {

	public static Polygon convertFromString(String xml) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Polygon.class, Subscriber.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Polygon pol = (Polygon) jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8.name())));
		return pol;
	}
	
	public static Polygon convertFromStringWitihSubscribers(String polXml, String subXml) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(SubscriberCollection.class, Polygon.class);
		
		Unmarshaller subUnmarshaller = jaxbContext.createUnmarshaller();
		SubscriberCollection subscriberCollection = (SubscriberCollection) subUnmarshaller.unmarshal(new ByteArrayInputStream(subXml.getBytes(StandardCharsets.UTF_8.name())));
		
		Unmarshaller polUnmarshaller = jaxbContext.createUnmarshaller();
		SubscriberXmlAdapter subAdapter = new SubscriberXmlAdapter();
		for (Subscriber sub : subscriberCollection.getCollection()) {
			subAdapter.getSubscribers().put(sub.getID(), sub);
		}
		polUnmarshaller.setAdapter(SubscriberXmlAdapter.class, subAdapter);
		
		return (Polygon) polUnmarshaller.unmarshal(new ByteArrayInputStream(polXml.getBytes(StandardCharsets.UTF_8.name())));
	}
	
	public static Polygon convertFromStringWithPolygonTags(String polXml, String tagXml) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(PolygonTagCollection.class, Polygon.class);
		
		Unmarshaller tagUnmarshaller = jaxbContext.createUnmarshaller();
		PolygonTagCollection tagCollection = (PolygonTagCollection) tagUnmarshaller.unmarshal(new ByteArrayInputStream(tagXml.getBytes(StandardCharsets.UTF_8.name())));
		
		Unmarshaller polUnmarshaller = jaxbContext.createUnmarshaller();
		PolygonTagXmlAdapter tagAdapter = new PolygonTagXmlAdapter();
		for (PolygonTag tag : tagCollection.getCollection()) {
			tagAdapter.getPolygonTags().put(tag.getName(),tag);
		}
		polUnmarshaller.setAdapter(tagAdapter);
		
		return (Polygon) polUnmarshaller.unmarshal(new ByteArrayInputStream(polXml.getBytes(StandardCharsets.UTF_8.name())));
	}
	
	public static Polygon convertFromStringWithSubscribersAndPolygonTags(String polXml, String subXml, String tagXml) throws JAXBException, UnsupportedEncodingException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(SubscriberCollection.class, PolygonTagCollection.class, Polygon.class);
		
		Unmarshaller subUnmarshaller = jaxbContext.createUnmarshaller();
		SubscriberCollection subscriberCollection = (SubscriberCollection) subUnmarshaller.unmarshal(new ByteArrayInputStream(subXml.getBytes(StandardCharsets.UTF_8.name())));
		
		Unmarshaller tagUnmarshaller = jaxbContext.createUnmarshaller();
		PolygonTagCollection tagCollection = (PolygonTagCollection) tagUnmarshaller.unmarshal(new ByteArrayInputStream(tagXml.getBytes(StandardCharsets.UTF_8.name())));
		
		Unmarshaller polUnmarshaller = jaxbContext.createUnmarshaller();
		SubscriberXmlAdapter subAdapter = new SubscriberXmlAdapter();
		for (Subscriber sub : subscriberCollection.getCollection()) {
			subAdapter.getSubscribers().put(sub.getID(), sub);
		}
		
		PolygonTagXmlAdapter tagAdapter = new PolygonTagXmlAdapter();
		for (PolygonTag tag : tagCollection.getCollection()) {
			tagAdapter.getPolygonTags().put(tag.getName(),tag);
			System.out.println(tag.getName());
		}
		
		polUnmarshaller.setAdapter(SubscriberXmlAdapter.class, subAdapter);
		polUnmarshaller.setAdapter(PolygonTagXmlAdapter.class, tagAdapter);
		
		return (Polygon) polUnmarshaller.unmarshal(new ByteArrayInputStream(polXml.getBytes(StandardCharsets.UTF_8.name())));
		
	}

}
