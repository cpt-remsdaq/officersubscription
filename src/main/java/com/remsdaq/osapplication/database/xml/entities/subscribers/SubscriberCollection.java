package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.database.xml.XmlDataCollectionEntity;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import javax.persistence.Transient;
import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Subscribers")
public class SubscriberCollection implements XmlDataCollectionEntity {
	
	@XmlTransient
	private List<Subscriber> collection;
	
	public SubscriberCollection() {
		collection = new ArrayList<>();
	}
	
	public String getID() {
		return null;
	}
	
	@Override
	@XmlTransient
	public EntityType getEntityType() {
		return EntityType.SUBSCRIBER;
	}
	
	@XmlElement(name = "Subscriber")
	public List<Subscriber> getSubscribers() {
		return this.collection;
	}
	
	@XmlTransient
	public Collection<Subscriber> getCollection() {
		return this.collection;
	}
	
	@Override
	public void removeFromCollection(XmlDataEntity xmlDataEntity) {
		removeFromCollection(xmlDataEntity.getID());
	}
	
	public void removeFromCollection(String subID) {
		for (Iterator<Subscriber> iterator = collection.iterator(); iterator.hasNext(); ) {
			Subscriber sub = iterator.next();
			if (sub.getID().equals(subID)) {
				iterator.remove();
				return;
			}
		}
	}
	
	public void setCollection(List<Subscriber> collection) {
		this.collection = collection;
	}
	
	@Override
	public void addToCollection(XmlDataEntity xmlDataEntity) {
		this.collection.add((Subscriber) xmlDataEntity);
	}
	
	public Subscriber selectFromCollection(Subscriber sub) {
		return selectFromCollection(sub.getID());
	}
	
	public Subscriber selectFromCollection(String subID) {
		for (Iterator<Subscriber> iterator = collection.iterator(); iterator.hasNext(); ) {
			Subscriber sub = iterator.next();
			if (sub.getID().equals(subID)) {
				return sub;
			}
		}
		return null;
	}
	
	@Override
	public void updateInCollection(XmlDataEntity updateEntity) {
		Subscriber updateSub = (Subscriber) updateEntity;
		
		Boolean hasBeenRemoved = false;
		
		for (Iterator<Subscriber> iterator = collection.iterator(); iterator.hasNext();) {
			Subscriber sub = iterator.next();
			if (sub.getID().equals(updateSub.getID())) {
				iterator.remove();
				hasBeenRemoved = true;
				break;
			}
		}
		if (hasBeenRemoved) {
			collection.add(updateSub);
		}
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataPath() {
		return GlobalContext.getInstance().getConfig().getProperty("subscriber_collection_xml_data_path");
	}
	
	@Override
	public String getXmlDataCollectionPath() {
		return GlobalContext.getInstance().getConfig().getProperty("subscriber_collection_xml_data_path");
	}
	
	@Override
	public XmlDataCollectionEntity makeCollection() {
		return this;
	}
}
