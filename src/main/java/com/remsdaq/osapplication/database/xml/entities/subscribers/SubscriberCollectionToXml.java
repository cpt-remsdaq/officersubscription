package com.remsdaq.osapplication.database.xml.entities.subscribers;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;

public class SubscriberCollectionToXml {
	
	public static void convertAndOutputToStream(SubscriberCollection subscriberCollection, OutputStream os) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		jaxbMarshaller.marshal(subscriberCollection, os);
	}
	
	private static Marshaller createFormattedMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(SubscriberCollection.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		return jaxbMarshaller;
	}
	
	public static void convertToFile(SubscriberCollection subscriberCollection,
	                                 String filePath) throws JAXBException, FileNotFoundException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		OutputStream os = new FileOutputStream(filePath);
		jaxbMarshaller.marshal(subscriberCollection, os);
	}
	
	public static String convertToString(SubscriberCollection subscriberCollection) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(subscriberCollection, sw);
		return sw.toString();
	}
	
	private static Marshaller createFormattedCollectionMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(SubscriberCollection.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		return jaxbMarshaller;
	}
	
}
