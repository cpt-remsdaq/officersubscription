package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.StringWriter;

public class SubscriberToXml {
	
	public static void convertAndOutputToStream(Subscriber subscriber, OutputStream os) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		jaxbMarshaller.marshal(subscriber, os);
	}
	
	private static Marshaller createFormattedMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Subscriber.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		return jaxbMarshaller;
	}
	
	public static void convertToFile(Subscriber subscriber, String filePath) throws JAXBException,
	                                                                                FileNotFoundException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		OutputStream os = new FileOutputStream(filePath);
		jaxbMarshaller.marshal(subscriber, os);
	}
	
	public static String convertToString(Subscriber subscriber) throws JAXBException {
		Marshaller jaxbMarshaller = createFormattedMarshaller();
		
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(subscriber, sw);
		return sw.toString();
	}
	
	private static Marshaller createFormattedCollectionMarshaller() throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(SubscriberCollection.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		return jaxbMarshaller;
	}
	
	public static String convertCollectionToString(SubscriberCollection toConvert) throws JAXBException {
		
		JAXBContext jaxbContext = JAXBContext.newInstance(toConvert.getClass());
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		StringWriter sw = new StringWriter();
		
		jaxbMarshaller.marshal(toConvert, sw);
		return sw.toString();
	
	}
}
