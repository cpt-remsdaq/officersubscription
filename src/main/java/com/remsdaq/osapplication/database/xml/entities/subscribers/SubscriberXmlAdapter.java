package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.HashMap;
import java.util.Map;

public class SubscriberXmlAdapter extends XmlAdapter<String, Subscriber> {
	
	private Map<String,Subscriber> subscribers = new HashMap<String,Subscriber>();
	
	public Map<String, Subscriber> getSubscribers() {
		return subscribers;
	}
	
	@Override
	public Subscriber unmarshal(String v) {
		return subscribers.get(v);
	}
	
	@Override
	public String marshal(Subscriber v) {
		return v.getID();
	}
}
