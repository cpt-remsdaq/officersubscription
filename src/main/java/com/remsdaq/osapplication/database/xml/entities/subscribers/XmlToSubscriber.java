package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

public class XmlToSubscriber {

	public static Subscriber convertFromString(String xml) throws JAXBException, UnsupportedEncodingException {
		JAXBContext jaxbContext = JAXBContext.newInstance(Subscriber.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Subscriber sub = (Subscriber) jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8.name())));
		return sub;
	}

	public static Subscriber convertFromFile(File xmlFile) {
		return new Subscriber();
	}

}
