package com.remsdaq.osapplication.entities;

public interface Entity {
	
	enum EntityType {
		SUBSCRIBER,
		SUBSCRIBER_TAG,
		POLYGON,
		POLYGON_TAG,
		INCIDENT,
		RESOURCE,
		PERSON,
		VEHICLE,
		SITE
	}
	
	EntityType entityType = null;
	
	String getID();
	EntityType getEntityType();
}
