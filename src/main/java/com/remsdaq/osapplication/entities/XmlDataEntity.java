package com.remsdaq.osapplication.entities;

import com.remsdaq.osapplication.database.xml.XmlDataCollectionEntity;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({Subscriber.class, Polygon.class})
public interface XmlDataEntity extends Entity{
	String getXmlDataPath();
	String getXmlDataCollectionPath();
	XmlDataCollectionEntity makeCollection();
}
