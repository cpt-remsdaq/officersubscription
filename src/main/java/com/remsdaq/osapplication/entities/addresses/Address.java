package com.remsdaq.osapplication.entities.addresses;

import com.remsdaq.osapplication.entities.coordinates.LatLng;

public class Address {
    private LatLng latLng;
    private String business;
    private String property;
    private String subProperty;
    private String street;
    private String place;
    private String district;
    private String county;
    private String postcode;
    private String mapref;
    private String eastingNorthing;
    private String uprn;
    private String usrn;

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getSubProperty() {
        return subProperty;
    }

    public void setSubProperty(String subProperty) {
        this.subProperty = subProperty;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getMapref() {
        return mapref;
    }

    public void setMapref(String mapref) {
        this.mapref = mapref;
    }

    public String getEastingNorthing() {
        return eastingNorthing;
    }

    public void setEastingNorthing(String eastingNorthing) {
        this.eastingNorthing = eastingNorthing;
    }

    public String getUprn() {
        return uprn;
    }

    public void setUprn(String uprn) {
        this.uprn = uprn;
    }

    public String getUsrn() {
        return usrn;
    }

    public void setUsrn(String usrn) {
        this.usrn = usrn;
    }

    public String getSummary() {
        return new StringBuilder()
                .append("LatLng: " + latLng + "\n")
                .append("business: " + business + "\n")
                .append("property: "  + property + "\n")
                .append("subProperty: " + subProperty + "\n")
                .append("street: " + street + "\n")
                .append("place: " + place + "\n")
                .append("district: " + district + "\n")
                .append("county: " + county + "\n")
                .append("postcode: "  + postcode + "\n")
                .append("mapref: " + mapref + "\n")
                .append("eastingNorthing: " + eastingNorthing + "\n")
                .append("uprn: " + uprn + "\n")
                .append("usrn: " + usrn + "\n")
                .toString();
    }
}
