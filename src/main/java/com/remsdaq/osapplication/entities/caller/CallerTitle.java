package com.remsdaq.osapplication.entities.caller;

// I went too far
public enum CallerTitle {

    ADVOCATE("Advocate"),
    AMBASSADOR("Ambassador"),
    BARON("Baron"),
    TBARON("The Baron"),
    TBARONOF("The Baron of"),
    BARONESS("Baroness"),
    TBARONESS("The Baroness"),
    TBARONESSOF("The Baroness of"),
    BRIGADIER("Brigadier"),
    CANON("Canon"),
    CAPTAIN("Captain"),
    CHANCELLOR("Chancellor"),
    CHIEF("Chief"),
    CLLR("Cllr"),
    COL("Col"),
    COMMODORE("Commodore"),
    COUNT("Count"),
    TCOUNT("The Count"),
    TCOUNTOF("The Count of"),
    COUNTESS("Countess"),
    TCOUNTESS("The Countess"),
    TCOUNTESSOF("The Countess of"),
    DAME("Dame"),
    DR("Dr"),
    DUKE("Duke"),
    TDUKE("The Duke"),
    TDUKEOF("The Duke of"),
    DUKEOF("Duke of"),
    DUCHESS("Duchess"),
    DUCHESSOF("Duchess of"),
    EARL("Earl"),
    EARLOF("Earl of"),
    FATHER("Father"),
    GENERAL("General"),
    GPCAPTAIN("Group Captain"),
    HRH("H R H"),
    HRHDTO("H R H the Dutchess of"),
    HRHDKO("H R H the Duke of"),
    HRHPCO("H R H the Prince"),
    HRHPSO("H R H the Princess"),
    HEMR("HE Mr"),
    HESEN("HE Senora"),
    HEFEM("HE The French Ambassador M"),
    HIGHNESS("His Highness"),
    HISHON("His Hon"),
    HISHOLYNESS("His Holyness"),
    HONJUDGE("His Hon Judge"),
    HON("Hon"),
    HONAMB("Hon Ambassador"),
    HONDR("Hon Dr"),
    HONLADY("Hon Lady"),
    HONMRS("Hon Mrs"),
    JUDGE("Judge"),
    KING("King"),
    LADY("Lady"),
    LORD("Lord"),
    LORDJUST("Lord Justice"),
    LTCDR("Lt Cdr"),
    LTCOL("Lt Col"),
    MADAM("Madam"),
    MADAME("Madame"),
    MAJ("Maj"),
    MAJGEN("Maj Gen"),
    MARQUESS("Marquess"),
    MARQUESSOF("Marquess of"),
    MARQUIS("Marquis"),
    MARQUISOF("Marquis of"),
    MASTER("Master"),
    MEP("MEP"),
    MINISTER("Minister"),
    MISS("Miss"),
    MP("MP"),
    MR("Mr"),
    MRS("Mrs"),
    MS("Ms"),
    MTREVD("The Most Revd"),
    MX("Mx"),
    OTHER("Other"),
    PRESIDENT("President"),
    PRIMEMINISTER("Prime Minister"),
    PRINCE("Prince"),
    PRINCESS("Princess"),
    PROF("Prof"),
    PROFEMERITUS("Prof Emeritus"),
    PROFDAME("Prof Dame"),
    QUEEN("Queen"),
    RABBI("Rabbi"),
    REPRESENTATIVE("Representative"),
    REVD("The Revd"),
    REVEREND("Reverend"),
    RTHONLORD("Rt Hon Lord"),
    RTREVD("The Rt Revd"),
    SENATOR("Senator"),
    SIR("Sir"),
    SISTER("Sister"),
    TLORD("The Lord"),
    VEN("The Ven"),
    VISCOUNT("Viscount"),
    VISCOUNTESS("Viscountess"),
    VYREVD("The Very Revd"),
    ;

    private String value;

    CallerTitle(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public String getValue() {
        return value;
    }

    public static final CallerTitle getTitleByValue(String value) {
        CallerTitle result = null;
        for (CallerTitle callerTitle : values()) {
            if (callerTitle.getValue().equals(value)) {
                result = callerTitle;
                break;
            }
        }
        return result;
    }

}
