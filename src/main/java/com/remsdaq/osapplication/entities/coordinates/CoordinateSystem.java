package com.remsdaq.osapplication.entities.coordinates;

public enum CoordinateSystem {

    OSGR,
    OSGB36,
    WGS84
}
