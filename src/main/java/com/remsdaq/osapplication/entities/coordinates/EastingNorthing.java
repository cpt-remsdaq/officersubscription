package com.remsdaq.osapplication.entities.coordinates;

import uk.me.jstott.jcoord.OSRef;

public class EastingNorthing {

	private Long east;
	private Long north;
	
	public EastingNorthing(Long east, Long north) {
		this.east = east;
		this.north = north;
	}
	
	public Long getEast() {
		return east;
	}
	
	public Long getNorth() {
		return north;
	}
	
	public LatLng toLatLng() {
		
		OSRef os = new OSRef(east,north);
		double lat = os.toLatLng().getLatitude();
		double lng = os.toLatLng().getLongitude();
		return new LatLng(lng,lat);
	}
	
	@Override
	public String toString() {
		return east + " " + north;
	}
}
