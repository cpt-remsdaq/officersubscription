package com.remsdaq.osapplication.entities.coordinates;

import math.geom2d.Point2D;
import uk.me.jstott.jcoord.OSRef;

import static java.lang.Math.abs;
import static java.lang.Math.sqrt;

public class LatLng {
    private double lat;
    private double lng;

    public LatLng(double lng, double lat) {
        this.lat = lat;
        this.lng = lng;
    }
    
    public void setLatLng(LatLng latLng) {
        this.lng = latLng.getLng();
        this.lat = latLng.getLat();
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
    
    public void setLat(double lat) {
        this.lat = lat;
    }
    
    public void setLng(double lng) {
        this.lng = lng;
    }
    
    @Override
    public String toString() {
        return (lng + " " + lat);
    }

    public double getSignedDistance(LatLng other) {
        double latDiff = other.getLat() - this.getLat();
        double longDiff = other.getLng() - this.getLng();
        return sqrt(Math.pow(latDiff,2) + Math.pow(longDiff,2));
    }

    public double getAbsoluteDistance(LatLng other) {
        return abs(getSignedDistance(other));
    }

    public Point2D getPoint2D() {
        return new Point2D(this.lng, this.lat);
    }
    
    public EastingNorthing toEastingNorthing() {
        uk.me.jstott.jcoord.LatLng latLng = new uk.me.jstott.jcoord.LatLng(lat,lng);
        OSRef ref  = latLng.toOSRef();
        Long east = Math.round(ref.getEasting());
        Long north = Math.round(ref.getNorthing());
        return new EastingNorthing(east,north);
    }
}