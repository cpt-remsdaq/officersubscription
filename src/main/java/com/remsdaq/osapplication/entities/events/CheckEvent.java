package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

/**
 *
 * @author cpt
 */
abstract class CheckEvent {
    
    /**
     *
     */
    private static Map<Event, Polygon> containedEvents = new HashMap();

    /**
     * Check if ArrayList of events are contained in ArrayList of polygons
     *
     * @param events
     * @param polygons
     * 
     */
     public static Map<Event, Polygon> isEventInPolygon(ArrayList<Event> events, ArrayList<Polygon> polygons) {
        
        events.stream().forEach((event) -> {
            polygons.stream().forEach((poly) -> {
               checkEventIsContained(event,poly);
            });
        });
        return containedEvents;
    }
     
    /**
     * Check if event is contained in ArrayList of polygons
     * 
     * @param event
     * @param polygons
     * 
     */
    public static Map<Event, Polygon> isEventInPolygon(Event event, ArrayList<Polygon> polygons) {
        polygons.stream().forEach((poly) -> {
            checkEventIsContained(event,poly);
        });
        return containedEvents;
    }
    
    /**
     * Check if event is contained in polygon
     *
     * @param event
     * @param poly
     * 
     */
    public static Map<Event, Polygon> isEventInPolygon(Event event, Polygon poly) {
        checkEventIsContained(event,poly);
        return containedEvents;
    }
    /**
     *
     * @param event
     * @param poly
     */
    private static void checkEventIsContained(Event event, Polygon poly) {

//        functionally the same as poly.containsEvent slower for some reason? Should only be passing a reference.
//        boolean bool = event.isContained(poly); 
//        boolean bool = poly.containsEvent(event);
//        if (bool == true) {
//            containedEvents.put(event, poly);
//        }
        
    }
}