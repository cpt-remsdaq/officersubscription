/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.gui.controllers.MainController;
import javafx.application.Platform;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 *
 * @author cpt
 */
public class Consumer implements Runnable, ExceptionListener {
        
    private static String connectionAddress;
    private static String queueName;
    private volatile static Boolean shutdown = false;
	private MainController controller;

	/** Shut down the consumer thread
     *
     * @param sShutdown
     */
    public void setShutdown(Boolean sShutdown) {
        //shutdown = sShutdown;
        System.out.println("Shutting thread down");
    }

    public void setController(MainController controller) {
    	this.controller = controller;
	}

    /**
     *
     * @param sConnectionAddress
     */
    public static void setConnectionAddress(String sConnectionAddress) {
        connectionAddress = sConnectionAddress;
    }
    
    /**
     *
     * @param sQueueName
     */
    public static void setQueueName(String sQueueName) {
        queueName = sQueueName;
    }

    @Override
    public void run() {
        System.out.println("Consumer started...");
        try {
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                    connectionAddress);

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();

            connection.setExceptionListener(this);

            // Create a Session
            Session session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);

            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue(queueName);

            // Create a MessageConsumer from the Session to the Topic or
            // Queue
            MessageConsumer consumer = session.createConsumer(destination);

            while (!shutdown) {
                // Wait for a message
                Message message = consumer.receive(1000);

                if (message instanceof TextMessage) {
                    TextMessage textMessage = (TextMessage) message;
                    String text = textMessage.getText();
                    System.out.println(text);
                    if (text!=null) {

                        System.out.println("Received: " + text);
                            Platform.runLater(new Runnable () {
                                @Override
                                public void run() {

									Event event = ParseEvent.parse(text);
//									controller.addEventToTableView(event);
                                }
                            });

//                        Platform.runLater(new Runnable(){
//                            public void run() {
//                                mainController.setInfoLabel(text,5000);
//                            }
//                        });
//                        
//                        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue(1024);
//                        
//                        QueueProducer queueProducer = new QueueProducer(blockingQueue);
//                            
//                        queueProducer.setText(text);
//                        shutdown = true;
                    } else {
                        System.out.println("Received nothing.");
                    }
                } else {
                    // No message
                }
            }
            consumer.close();
            session.close();
            connection.close();
        } catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
        }

    }

    /**
     *
     * @param ex
     */
    @Override
    public void onException(JMSException ex) {
        System.out.println("JMS Exception occured.  Shutting down client.");
    }
}
