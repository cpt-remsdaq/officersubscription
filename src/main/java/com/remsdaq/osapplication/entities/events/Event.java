package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.resources.Resource;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;

/**
 * @author cpt
 */
public class Event {
	
	public static final int STATUS_CLOSED = -1;
	public static final int STATUS_OPEN = 0;
	public static final int STATUS_MOBILISED = 1;
	public static final int STATUS_BATCH_MOBILISED = 2;
	
	private String incidentNumber;
	private List<IncidentType> types;
	private String postcode;
	private String addressString;
	private LocalDateTime dateTime;
	private LatLng latLng;
	private String description;
	private Set<Polygon> polygons;
	private Set<Resource> resources;
	private Integer status = 0;
	
	////////////////
	//Constructors//
	////////////////
	
	Event() {
		this.incidentNumber = "";
		this.dateTime = null;
		this.types = null;
		this.latLng = null;
		this.polygons = new HashSet<>();
		this.description = "";
	}
	
	Event(String incidentNumber, LocalDateTime date, List<IncidentType> types, String postcode, LatLng latLng,
	      Set<Polygon> polygons, Set<Resource> resources, String description) {
		this.incidentNumber = incidentNumber;
		this.dateTime = date;
		this.types = types;
		this.postcode = postcode;
		this.latLng = latLng;
		this.polygons = polygons;
		this.resources = resources;
		this.description = description;
	}
	
	// Check to see if contained within a closed <Path2D> polygon
	
	/**
	 * @param poly
	 */
	public boolean isContained(Polygon poly) {
		boolean isContainedBool = poly.getGeometry().geometryContains(this.latLng);
		return isContainedBool;
	}
	
	////////////
	// Getters//
	////////////
	
	public String getIncidentNumber() {
		return this.incidentNumber;
	}
	
	public void setIncidentNumber(String incidentNumber) {
		this.incidentNumber = incidentNumber;
	}
	
	public LocalDateTime getDateTime() {
		return this.dateTime;
	}
	
	public void setDateTime(LocalDateTime date) {
		this.dateTime = date;
	}
	
	public String getTimeString() {
		return this.getTime().toString();
	}
	
	private LocalTime getTime() {
		return this.dateTime.toLocalTime();
	}
	
	public String getDateString() {
		return this.getDate().toString();
	}
	
	private LocalDate getDate() {
		return this.dateTime.toLocalDate();
	}
	
	public String getDateTimeString() {
		return this.dateTime.toString();
	}
	
	public String getDayOfWeekString() {
		return this.getDate().getDayOfWeek().toString();
	}
	
	public String getTodayOrYesterday() {
		int day = this.getDate().getDayOfYear();
		int today = LocalDateTime.now().getDayOfYear();
		if (day == today) {
			return "today";
		} else if (day == today - 1) {
			return "yesterday";
		} else {
			Integer dayOfMonth = this.getDate().getDayOfMonth();
			return dayOfMonth.toString();
		}
	}
	
	public List<IncidentType> getTypes() {
		return this.types;
	}
	
	public void setTypes(List<IncidentType> types) {
		this.types = types;
	}
	
	public LatLng getLatLng() {
		return this.latLng;
	}
	
	public void setLatLng(LatLng latLng) {
		this.latLng = latLng;
	}
	
	////////////
	// Setters//
	////////////
	
	public String getLatLngString() {
		return this.latLng.toString();
	}
	
	public String getPolygonsString() {
		StringJoiner eventJoiner = new StringJoiner(", ");
		this.getPolygons().forEach((poly) -> {
			eventJoiner.add(poly.getID());
		});
		
		return eventJoiner.toString();
	}
	
	public Set<Polygon> getPolygons() {
		return this.polygons;
	}
	
	public void setPolygons(Set<Polygon> polygons) {
		this.polygons = polygons;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	public Set<Resource> getResources() {
		return resources;
	}
	
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	
	public String getPostcode() {
		return postcode;
	}
	
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	public String getAddressString() {
		return addressString;
	}
	
	public void setAddressString(String addressString) {
		this.addressString = addressString;
	}
}