/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.gui.controllers.MainController;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

/**
 * @author cpt
 */
public class EventConsumer implements ExceptionListener {
	
	static volatile private Boolean shutdown = false;
	static private MainController controller;
	private static Config config = new Config();
	
	public static String consume() {
		
		String connectionAddress = config.getProperty("event_queue_address");
		String queueName = config.getProperty("event_queue_name");
		String text = "";
		
		try {
			// Create a ConnectionFactory
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(connectionAddress);
			
			// Create a Connection
			Connection connection = connectionFactory.createConnection();
			connection.start();
			
			//            connection.setExceptionListener(this);
			
			// Create a Session
			Session session = connection.createSession(false,
			                                           Session.AUTO_ACKNOWLEDGE);
			
			// Create the destination (Topic or Queue)
			Destination destination = session.createQueue(queueName);
			
			// Create a MessageConsumer from the Session to the Topic or
			// Queue
			MessageConsumer consumer = session.createConsumer(destination);
			
			while (!shutdown) {
				
				// Wait for a message
				Message message = consumer.receive(1000);
				
				if (message instanceof TextMessage) {
					TextMessage textMessage = (TextMessage) message;
					text = textMessage.getText();
					if (text != null) {
						System.out.println("Received: " + text);
						shutdown = true;
					} else {
						System.out.println("Received nothing.");
					}
				} else {
					// No message
				}
			}
			consumer.close();
			session.close();
			connection.close();
		} catch (Exception e) {
			System.out.println("Caught: " + e);
			e.printStackTrace();
		}
		return text;
	}
	
	/**
	 * @param ex
	 */
	@Override
	public void onException(JMSException ex) {
		System.out.println("JMS Exception occured.  Shutting down client.");
	}
}
