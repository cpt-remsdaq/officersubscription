/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.gui.controllers.MainController;

/**
 *
 * @author cpt
 */
public class EventListener {

    private Config config = new Config();
    
    volatile private Boolean shutdown = false;
	private MainController controller;
	//static Boolean shutdown = false;
    
    /**
     *
     * @throws InterruptedException
     */
    public void listen() {
//        consumer.setQueueName(config.getProperty("event_queue_name"));
//        consumer.setConnectionAddress(config.getProperty("event_queue_address"));
//        listenThread.start();
//        Task<Void> task = new Task<Void>() {
//            @Override protected Void call() throws Exception {
//                    consumer.run();
//                return null;
//            }
//        };

		Consumer consumer = new Consumer();
		Consumer.setQueueName(config.getProperty("event_queue_name"));
		Consumer.setConnectionAddress(config.getProperty("event_queue_address"));
		Runnable task = new Runnable() {
            public void run() {
				Consumer consumer = new Consumer();
				Consumer.setQueueName(config.getProperty("event_queue_name"));
				Consumer.setConnectionAddress(config.getProperty("event_queue_address"));
				consumer.setController(controller);
                consumer.run();
            }
        };
		Thread backgroundThread = new Thread(task);
        backgroundThread.setDaemon(true);
        backgroundThread.start();
    }

    public void shutdown() {
//        listenThread.stop();
        System.out.println("Stopped listening");
    }
    
    public void setController(MainController controller) {
		this.controller = controller;
    }
}
