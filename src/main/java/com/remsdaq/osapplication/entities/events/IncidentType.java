package com.remsdaq.osapplication.entities.events;

public class IncidentType {
    private Long typeIdentifier;
    private String level1Description;
    private String level2Description;
    private String description;
    private String shortCode;
    private String priority;
    private String externalCode;
    private Boolean hidden;
    private String getLevel1DescriptionCode;

    public IncidentType(Long typeIdentifier, String level1Description, String level2Description) {
        this.typeIdentifier = typeIdentifier;
        this.level1Description = level1Description;
        this.level2Description = level2Description;
    }

    public IncidentType(Long typeIdentifier, String level1Description, String level2Description, String description, String priority) {
        this.typeIdentifier = typeIdentifier;
        this.level1Description = level1Description;
        this.level2Description = level2Description;
        this.description = description;
        this.priority = priority;
    }

    public IncidentType(Long typeIdentifier, String level1Description, String level2Description, String description, String priority, String shortCode, String externalCode) {
        this(typeIdentifier, level1Description, level2Description, description, priority);
        this.shortCode = shortCode;
        this.externalCode = externalCode;
    }

    public Long getTypeIdentifier() {
        return typeIdentifier;
    }

    public void setTypeIdentifier(Long typeIdentifier) {
        this.typeIdentifier = typeIdentifier;
    }

    public String getLevel1Description() {
        return level1Description;
    }

    public void setLevel1Description(String level1Description) {
        this.level1Description = level1Description;
    }

    public String getLevel2Description() {
        return level2Description;
    }

    public void setLevel2Description(String level2Description) {
        this.level2Description = level2Description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getGetLevel1DescriptionCode() {
        return getLevel1DescriptionCode;
    }

    public void setGetLevel1DescriptionCode(String getLevel1DescriptionCode) {
        this.getLevel1DescriptionCode = getLevel1DescriptionCode;
    }
}
