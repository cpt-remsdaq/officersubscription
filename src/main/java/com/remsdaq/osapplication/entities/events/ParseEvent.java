package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.entities.coordinates.LatLng;

import java.time.LocalDateTime;

/**
 *
 * @author cpt
 */
abstract class ParseEvent {
    
    /**
     *
     * @param eventString
     * 
     */
    public static Event parse(String eventString) {
		Event event = new Event();
		String[] eventStringSplit = eventString.split(";");

		LocalDateTime ldt = LocalDateTime.of(2012, 6, 30, 12, 00);

		event.setDateTime(ldt);
		event.setIncidentNumber(eventStringSplit[1]);
		event.setTypes(null);
		event.setDescription(eventStringSplit[3]);
		event.setLatLng(new LatLng(Double.parseDouble(eventStringSplit[4].split(",")[0]), Double.parseDouble(eventStringSplit[4].split(",")[1])));
//	    event.setPolygons();

		return event;
	}

    public static void handle(Event event) {
        System.out.println("The event ID is: " + event.getIncidentNumber());


    }
}