/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.entities.events;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Producer implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(Producer.class);
    
    private String text = "Message string not set!";
    
    private Connection connection;
    private Session session;

    private static String connectionAddress;
    private static String queueName;
    volatile static Boolean shutdown = false;

    /** Shut down the consumer thread
     *
     * @param sShutdown
     */
    public void setShutdown(Boolean sShutdown) {
        //shutdown = sShutdown;
        System.out.println("Shutting thread down");
    }
    
    public void setText(String string) {
        text = string;
    }
    
    /**
     *
     * @param sConnectionAddress
     */
    public static void setConnectionAddress(String sConnectionAddress) {
        connectionAddress = sConnectionAddress;
    }
    
    /**
     *
     * @param sQueueName
     */
    public static void setQueueName(String sQueueName) {
        queueName = sQueueName;
    }
    
    @Override
    public void run() {

        try {
            // Create a ConnectionFactory
            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://cpt:61616");
//            ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(connectionAddress);

            // Create a Connection
            Connection connection = connectionFactory.createConnection();
            connection.start();

            // Create a Session
            Session session = connection.createSession(false,
                    Session.AUTO_ACKNOWLEDGE);
            
            // Create the destination (Topic or Queue)
            Destination destination = session.createQueue("EventQueue");
//            Destination destination = session.createQueue(queueName);

            // create a MessageProducer for sending messages
            MessageProducer messageProducer = session.createProducer(destination);
            messageProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
            
            TextMessage message = session.createTextMessage(text);
            
            messageProducer.send(message);
            
        } catch (Exception e) {
            System.out.println("Caught: " + e);
            e.printStackTrace();
        }
    }
}
