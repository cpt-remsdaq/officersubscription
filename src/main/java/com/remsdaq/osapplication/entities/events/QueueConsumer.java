package com.remsdaq.osapplication.entities.events;

import java.util.concurrent.BlockingQueue;
 
class QueueConsumer implements Runnable {
    private final BlockingQueue<String> queue;
 
    QueueConsumer(BlockingQueue<String> q) { queue = q; }
    private String text = "";
    public void run() {
        try {
            while (!queue.take().equals("")) {
                text = queue.take();
                System.out.println(text);
            }
        } catch (Exception e) {
            System.out.println
            (Thread.currentThread().getName() + " " + e.getMessage());
        }
    }
}