package com.remsdaq.osapplication.entities.events;

import java.util.concurrent.*;
 
class QueueProducer implements Runnable{
    private final BlockingQueue<String> queue;
    private String text = "No text set!";
 
    public void setText(String text) {
        this.text = text;
    }
  
    QueueProducer(BlockingQueue<String> q) {
        queue = q;
    }
 
    public void run() {
        try {
            queue.put(text);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}