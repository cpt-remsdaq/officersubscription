package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.entities.events.Event;
import java.util.ArrayList;

/**
 *
 * @author cpt
 */
abstract class ReadEventStream {
    
    // Read the events stream into string array

    /**
     *
     * 
     * @throws InterruptedException
     */
    public static ArrayList<Event> readEvents() {
        
        EventListener eventListener = new EventListener();
        eventListener.listen();
        System.out.println("\nReading event stream");
        //eventListener.shutdown();

//        List<String> lines = Obsolete_ReadFile.read(eventFile);
        ArrayList<Event> eventArray = new ArrayList<Event>();
//        
//        for (String line : lines) {
//            Event event = ParseEvent.parse(line);
//            eventArray.add(event);
//        }

        return eventArray;

    }
    
    public static void killReader() {
        
    }
}