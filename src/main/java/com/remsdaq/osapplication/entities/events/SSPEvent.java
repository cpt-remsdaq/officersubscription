package com.remsdaq.osapplication.entities.events;

import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import javafx.beans.property.SimpleStringProperty;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author cpt
 */
public class SSPEvent{

	private SimpleStringProperty eventDateTime;
	private SimpleStringProperty eventID;
	private SimpleStringProperty eventCode;
	private SimpleStringProperty eventLatLng;
	private SimpleStringProperty eventDescription;
	private SimpleStringProperty eventPolygons;

	/**
	 *
	 * @param event
	 */
	public SSPEvent (Event event) {

		SSPPolygon(event.getDateTimeString() ,event.getIncidentNumber(), event.getTypes().toString(), event.getLatLngString(), event.getDescription(), event.getPolygonsString());
	}

	/**
	 *
	 * @param eventDateTime
	 * @param eventID
	 * @param eventCode
	 * @param eventLatLng
	 * @param eventPolygons
	 */
	private void SSPPolygon(String eventDateTime, String eventID, String eventCode, String eventLatLng, String eventDescription, String eventPolygons) {
		this.eventDateTime = new SimpleStringProperty(eventDateTime);
		this.eventID = new SimpleStringProperty(eventID);
		this.eventCode = new SimpleStringProperty(eventCode);
		this.eventLatLng = new SimpleStringProperty(eventLatLng);
		this.eventDescription = new SimpleStringProperty(eventDescription);
		this.eventPolygons = new SimpleStringProperty(eventPolygons);
	}

	public String getEventDateTime() {
		return eventDateTime.get();
	}

	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime.set(eventDateTime);
	}

	public String getEventID() {
		return eventID.get();
	}

	public void setEventID(String eventID) {
		this.eventID.set(eventID);
	}

	public String getEventCode() {
		return eventCode.get();
	}

	public void setEventCode(String eventCode) {
		this.eventCode.set(eventCode);
	}

	public String getEventLatLng() {
		return eventLatLng.get();
	}

	public void setEventLatLng(String eventLatLng) {
		this.eventLatLng.set(eventLatLng);
	}

	public String getEventDescription() {
		return eventDescription.get();
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription.set(eventDescription);
	}

	public String getEventPolygons() {
		return eventPolygons.get();
	}

	public void setEventPolygons(String eventPolygons) {
		this.eventPolygons.set(eventPolygons);
	}

	public Event returnEvent() {
		Event event = new Event();

		event.setDateTime(LocalDateTime.parse(eventDateTime.get()));
		event.setIncidentNumber(eventID.get());
		event.setTypes(null);

		String[] latLngSplit = eventLatLng.get().split(" ");
		event.setLatLng(new LatLng(Double.parseDouble(latLngSplit[0]), Double.parseDouble(latLngSplit[1])));

		event.setDescription(eventDescription.get());

		Set<Polygon> polySet  = new HashSet<>();
		for (String polyString : eventPolygons.get().split(", ")) {
			Polygon sacraPoly = new Polygon();
			sacraPoly.setID(polyString);
			polySet.add(sacraPoly);
		}

		event.setPolygons(polySet);

		return event;
	}

	@Override
	public String toString() {
		return(eventDateTime.get() + ";" +  eventID.get()+ ";" + eventCode.get() + ";" + eventLatLng.get() + ";" + eventDescription.get() +  ";" + eventPolygons.get());
	}

}
