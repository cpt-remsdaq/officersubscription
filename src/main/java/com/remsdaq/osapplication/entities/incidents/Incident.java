package com.remsdaq.osapplication.entities.incidents;

import com.remsdaq.osapplication.entities.Entity;
import com.remsdaq.osapplication.entities.addresses.Address;
import com.remsdaq.osapplication.entities.coordinates.EastingNorthing;
import com.remsdaq.osapplication.entities.resources.Attribute;
import com.remsdaq.osapplication.entities.resources.Person;
import com.remsdaq.osapplication.entities.resources.Resource;
import com.remsdaq.osapplication.entities.resources.Vehicle;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Incident implements Entity {
	
	private Long id;
	private String description;
	private List<IncidentType> types;
	private LocalDateTime dateTime;
	private LocalDateTime stopTime;
	private List<Resource> resources;
	private Long priority;
	private Integer numberOfCalls;
	private Long batchId;
	private List<Attribute> attributes;
	private Address transientAddress;
	private EastingNorthing eastingNorthing;
	private String addressString;
	private Status status;
	private Person commandPerson;
	private Vehicle commandVehicle;
	private Boolean fireOfSpecialInterest;
	private Boolean makeUp;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public List<IncidentType> getTypes() {
		return types;
	}
	
	public void setTypes(IncidentType type) {
		List<IncidentType> typeWrapper = new ArrayList<>();
		typeWrapper.add(type);
		this.types = typeWrapper;
	}
	
	public void setTypes(List<IncidentType> types) {
		this.types = types;
	}
	
	public LocalDateTime getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(LocalDateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	public LocalDateTime getStopTime() {
		return stopTime;
	}
	
	public void setStopTime(LocalDateTime stopTime) {
		this.stopTime = stopTime;
	}
	
	public List<Resource> getResources() {
		return resources;
	}
	
	public void setResources(List<Resource> resources) {
		this.resources = resources;
	}
	
	public Long getPriority() {
		return priority;
	}
	
	public void setPriority(Long priority) {
		this.priority = priority;
	}
	
	public Integer getNumberOfCalls() {
		return numberOfCalls;
	}
	
	public void setNumberOfCalls(Integer numberOfCalls) {
		this.numberOfCalls = numberOfCalls;
	}
	
	public Long getBatchId() {
		return batchId;
	}
	
	public void setBatchId(Long batchId) {
		this.batchId = batchId;
	}
	
	public List<Attribute> getAttributes() {
		return attributes;
	}
	
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	public Address getTransientAddress() {
		return transientAddress;
	}
	
	public void setTransientAddress(Address transientAddress) {
		this.transientAddress = transientAddress;
	}

	public String getAddressString() {
		return addressString;
	}

	public void setAddressString(String addressString) {
		this.addressString = addressString;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Vehicle getCommandVehicle() {
		return commandVehicle;
	}

	public void setCommandVehicle(Vehicle commandVehicle) {
		this.commandVehicle = commandVehicle;
	}

	public Person getCommandPerson() {
		return commandPerson;
	}

	public void setCommandPerson(Person commandPerson) {
		this.commandPerson = commandPerson;
	}

	public Boolean getFireOfSpecialInterest() {
		return fireOfSpecialInterest;
	}

	public void setFireOfSpecialInterest(Boolean fireOfSpecialInterest) {
		this.fireOfSpecialInterest = fireOfSpecialInterest;
	}

	public EastingNorthing getEastingNorthing() {
		return eastingNorthing;
	}

	public void setEastingNorthing(EastingNorthing eastingNorthing) {
		this.eastingNorthing = eastingNorthing;
	}

	@Override
	public String getID() {
		return null;
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.INCIDENT;
	}

	public boolean equals(Incident other) {
		Boolean areEqual = (
				other.getId() == id
				&& other.getDescription() == description
				&& other.getDateTime() == dateTime
				&& other.getStopTime() == stopTime
				&& other.getPriority() == priority
				&& other.getNumberOfCalls() == numberOfCalls
				&& other.getBatchId() == batchId
				&& other.getTransientAddress() == transientAddress
				&& other.getEastingNorthing() == eastingNorthing
				&& other.getAddressString() == addressString
				&& other.getStatus() == status
				&& other.getCommandVehicle() == commandVehicle
				&& other.getCommandPerson() == commandPerson
				&& other.getFireOfSpecialInterest() == fireOfSpecialInterest
		);
		return areEqual;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(233, 311)
				.append(id)
				.append(description)
				.append(dateTime)
				.append(stopTime)
				.append(priority)
				.append(numberOfCalls)
				.append(batchId)
				.append(transientAddress)
				.append(eastingNorthing)
				.append(addressString)
				.append(status)
				.append(commandVehicle)
				.append(commandPerson)
				.append(fireOfSpecialInterest)
				.hashCode();
	}
	
	public Boolean getMakeUp() {
		return makeUp;
	}
	
	public void setMakeUp(Boolean makeUp) {
		this.makeUp = makeUp;
	}
	
	public enum Status {
		STATUS_CLOSED(-1),
		STATUS_OPEN(0),
		STATUS_MOBILISED(1),
		STATUS_BATCH_MOBILISED(2);
		
		private final int statusValue;
		
		Status(int statusValue) {
			this.statusValue = statusValue;
		}
		
		public int getStatusValue() {
			return this.statusValue;
		}
	}
}
