package com.remsdaq.osapplication.entities.incidents;

import com.remsdaq.osapplication.entities.addresses.Address;
import com.remsdaq.osapplication.entities.coordinates.EastingNorthing;
import com.remsdaq.osapplication.entities.resources.Attribute;
import com.remsdaq.osapplication.entities.resources.Person;
import com.remsdaq.osapplication.entities.resources.Resource;
import com.remsdaq.osapplication.entities.resources.Vehicle;

import java.time.LocalDateTime;
import java.util.List;

public class IncidentBuilder {
	
	private Incident incident = new Incident();
	
	public IncidentBuilder(Long id) {
		incident.setId(id);
	}
	
	public IncidentBuilder types(List<IncidentType> types) {
		incident.setTypes(types);
		return this;
	}
	
	public IncidentBuilder type(IncidentType type) {
		incident.setTypes(type);
		return this;
	}
	
	public IncidentBuilder description(String description) {
		incident.setDescription(description);
		return this;
	}
	
	public IncidentBuilder dateTime(LocalDateTime dateTime) {
		incident.setDateTime(dateTime);
		return this;
	}
	
	public IncidentBuilder stopTime(LocalDateTime stopTime) {
		incident.setStopTime(stopTime);
		return this;
	}
	
	public IncidentBuilder resources(List<Resource> resources) {
		incident.setResources(resources);
		return this;
	}
	
	public IncidentBuilder priority(Long priority) {
		incident.setPriority(priority);
		return this;
	}
	
	public IncidentBuilder numberOfCalls(int numberOfCalls) {
		incident.setNumberOfCalls(numberOfCalls);
		return this;
	}
	
	public IncidentBuilder batchId(Long batchId) {
		incident.setBatchId(batchId);
		return this;
	}
	
	public IncidentBuilder attributes(List<Attribute> attributes) {
		incident.setAttributes(attributes);
		return this;
	}
	
	public IncidentBuilder transientAddress(Address transientAddress) {
		incident.setTransientAddress(transientAddress);
		return this;
	}
	
	public IncidentBuilder addressString(String addressString) {
		incident.setAddressString(addressString);
		return this;
	}
	
	public IncidentBuilder mapRef(EastingNorthing eastingNorthing) {
		incident.setEastingNorthing(eastingNorthing);
		return this;
	}
	
	public IncidentBuilder status(Incident.Status status) {
		incident.setStatus(status);
		return this;
	}
	
	public IncidentBuilder commandVehicle(Vehicle commandVehicle) {
		incident.setCommandVehicle(commandVehicle);
		return this;
	}
	
	public IncidentBuilder commandPerson(Person commandPerson) {
		incident.setCommandPerson(commandPerson);
		return this;
	}
	
	public IncidentBuilder fireOfSpecialInterest(Boolean fireOfSpecialInterest) {
		incident.setFireOfSpecialInterest(fireOfSpecialInterest);
		return this;
	}
	
	public IncidentBuilder makeUp(Boolean makeUp) {
		incident.setMakeUp(makeUp);
		return this;
	}
	
	public Incident build() {
		return this.incident;
	}
}
