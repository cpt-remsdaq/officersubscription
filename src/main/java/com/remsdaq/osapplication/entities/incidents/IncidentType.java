package com.remsdaq.osapplication.entities.incidents;

public class IncidentType {
    private Long id;
    private String level1Description;
    private String level2Description;
    private String description;
    private String shortCode;
    private Priority priority;
    private String externalCode;
    private Boolean hidden;
    private String level1DescriptionCode;
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getLevel1Description() {
        return level1Description;
    }
    
    public void setLevel1Description(String level1Description) {
        this.level1Description = level1Description;
    }
    
    public String getLevel2Description() {
        return level2Description;
    }
    
    public void setLevel2Description(String level2Description) {
        this.level2Description = level2Description;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getShortCode() {
        return shortCode;
    }
    
    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }
    
    public Priority getPriority() {
        return priority;
    }
    
    public void setPriority(Priority priority) {
        this.priority = priority;
    }
    
    public String getExternalCode() {
        return externalCode;
    }
    
    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }
    
    public Boolean getHidden() {
        return hidden;
    }
    
    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
    
    public String getLevel1DescriptionCode() {
        return level1DescriptionCode;
    }
    
    public void setLevel1DescriptionCode(String level1DescriptionCode) {
        this.level1DescriptionCode = level1DescriptionCode;
    }
    
    public String toString() {
    	return new StringBuilder()
			    .append(getLevel1Description())
			    .append(" - ")
			    .append(getLevel2Description())
			    .toString();
    }
    
}
