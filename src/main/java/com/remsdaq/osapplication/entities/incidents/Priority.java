package com.remsdaq.osapplication.entities.incidents;

public class Priority {
    private Long priority;
    
    public Priority(Long priority) {
        this.priority = priority;
    }
    
    public Long getPriority() {
        return priority;
    }
}
