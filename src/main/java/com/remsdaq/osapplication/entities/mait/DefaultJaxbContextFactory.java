package com.remsdaq.osapplication.entities.mait;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public class DefaultJaxbContextFactory implements JaxbContextFactory {
    @Override
    public JAXBContext build(Class type) throws JAXBException {
        return JAXBContext.newInstance(type);
    }
}
