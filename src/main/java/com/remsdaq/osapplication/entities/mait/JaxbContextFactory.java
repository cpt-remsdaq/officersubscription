package com.remsdaq.osapplication.entities.mait;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

public interface JaxbContextFactory {
    JAXBContext build(Class type) throws JAXBException;
}
