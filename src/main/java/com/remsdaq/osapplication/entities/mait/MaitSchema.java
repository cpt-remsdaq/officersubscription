package com.remsdaq.osapplication.entities.mait;

public enum MaitSchema {

    MAIT_SCHEMA_VERSION_FIVE("1dc5"),
    MAIT_SCHEMA_VERSION_SIX("1dc6"),
    MAIT_SCHEMA_VERSION_SEVEN("1dc7");

    private final String version;

    MaitSchema(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public static MaitSchema getMaitSchema(String version) {
        for (MaitSchema maitSchema : MaitSchema.values()) {
            if (maitSchema.getVersion().equals(version)) {
                return maitSchema;
            }
        }
        return null;
    }


}
