package com.remsdaq.osapplication.entities.mait;

import com.remsdaq.osapplication.utilities.FileUtilities;
import com.remsdaq.osapplication.utilities.MaitUtilities;
import org.xml.sax.SAXException;

import java.util.HashMap;
import java.util.Map;

public class MaitSchemaFactory {

    private Map<MaitSchema, XmlValidator> maitSchemaToXmlValidator;
    private boolean isInitialised = false;

    public void initialiseMaitSchemaToXmlValidator() throws Exception {
        maitSchemaToXmlValidator = new HashMap<>();
        try {
            maitSchemaToXmlValidator.put(MaitSchema.MAIT_SCHEMA_VERSION_SEVEN, new XmlValidator(FileUtilities.getResourceAsUrl("MAIT-1dc7-NoSig.xsd")));
            isInitialised = true;
        } catch (SAXException ex) {
            throw new Exception(ex);
        }
    }

    public XmlValidator getXmlValidator(MaitSchema maitSchema) throws Exception {
        if (maitSchemaToXmlValidator.containsKey(maitSchema)) {
            return maitSchemaToXmlValidator.get(maitSchema);
        } else {
            throw new Exception(String.valueOf(maitSchema));
        }
    }

    public XmlValidator getXmlValidator(String xml) throws Exception {
        return getXmlValidator(MaitUtilities.getMaitSchema(xml));
    }

    public boolean isInitialised() {
        return isInitialised;
    }

}