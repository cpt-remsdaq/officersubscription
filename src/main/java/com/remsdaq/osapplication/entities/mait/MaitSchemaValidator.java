package com.remsdaq.osapplication.entities.mait;

public class MaitSchemaValidator {

    private MaitSchemaFactory maitSchemaFactory;

    private String xml;

    public MaitSchemaValidator() {
        maitSchemaFactory = new MaitSchemaFactory();
    }

    public MaitSchemaValidator setXml(String xml) {
        this.xml = xml;
        return this;
    }

    public String getXml() {
        return xml;
    }

    public boolean isValid(String xml) throws Exception {
        setXml(xml);
        if (!maitSchemaFactory.isInitialised()) {
            maitSchemaFactory.initialiseMaitSchemaToXmlValidator();
        }
        if (xml != null) {
            return maitSchemaFactory.getXmlValidator(xml).isXmlValid(xml);
        }
        return false;
    }

}
