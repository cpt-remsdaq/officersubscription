package com.remsdaq.osapplication.entities.mait;

import com.remsdaq.osapplication.entities.mait.entities.messages.Mait;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MaitToXml {

    private static final String DEFAULT_CHARACTER_ENCODING = "UTF-8";
    private static final String DEFAULT_XML_VERSION = "1.0";

    private Marshaller jaxbMarshaller;
    private XMLOutputFactory xmlOutputFactory;
    private String characterEncoding = DEFAULT_CHARACTER_ENCODING;
    private String xmlVersion = DEFAULT_XML_VERSION;

    public MaitToXml() {
        initialiseJaxbMarshaller();
        xmlOutputFactory = XMLOutputFactory.newInstance();
    }

    public String getCharacterEncoding() {
        return characterEncoding;
    }

    public void setCharacterEncoding(String characterEncoding) {
        this.characterEncoding = characterEncoding;
    }

    public String getXmlVersion() {
        return xmlVersion;
    }

    public void setXmlVersion(String xmlVersion) {
        this.xmlVersion = xmlVersion;
    }

    private void initialiseJaxbMarshaller() {
        try {
            jaxbMarshaller = JAXBContext.newInstance(Mait.class).createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, true);
        } catch (JAXBException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "", ex);
        }
    }

    public String getXml(Mait mait) {
        String xml = null;
        if (mait != null) {
            try {
                final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                final XMLStreamWriter xmlStreamWriter = xmlOutputFactory.createXMLStreamWriter(baos, characterEncoding);
                xmlStreamWriter.writeStartDocument(characterEncoding, xmlVersion);
                jaxbMarshaller.marshal(mait, xmlStreamWriter);
                xmlStreamWriter.writeEndDocument();
                xmlStreamWriter.close();
                xml = new String(baos.toByteArray(), characterEncoding);
            } catch (XMLStreamException | JAXBException | UnsupportedEncodingException ex) {
                Logger.getLogger(getClass().getName()).log(Level.SEVERE, "", ex);
            }
        }
        return xml;
    }

}
