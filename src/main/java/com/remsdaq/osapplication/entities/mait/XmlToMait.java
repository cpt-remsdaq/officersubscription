package com.remsdaq.osapplication.entities.mait;

import com.remsdaq.osapplication.entities.mait.entities.messages.Mait;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

public class XmlToMait {

    private static final String DEFAULT_CHARACTER_ENCODING = "UTF-8";
    private static final Logger LOG = Logger.getLogger(XmlToMait.class.getName());

    protected static JaxbContextFactory contextFactory = new DefaultJaxbContextFactory();

    private JAXBContext jaxbContext;

    public XmlToMait() {
        try {
            initialiseJaxbContext();
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "", e);
        }
    }

    private void initialiseJaxbContext() throws JAXBException {
        jaxbContext = contextFactory.build(Mait.class);
    }

    public Mait getMait(String xml) {
        try {
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            return (Mait) unmarshaller.unmarshal(new ByteArrayInputStream(xml.getBytes(DEFAULT_CHARACTER_ENCODING)));
        } catch (JAXBException | UnsupportedEncodingException | NullPointerException ex) {
            LOG.log(SEVERE, "", ex);
        }
        return null;
    }

}