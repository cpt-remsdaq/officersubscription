package com.remsdaq.osapplication.entities.mait;

import com.remsdaq.osapplication.entities.mait.entities.messages.Mait;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class XmlValidator {

    private static final SchemaFactory SCHEMA_FACTORY = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

    private Validator validator;

    public XmlValidator(Schema schema) {
        validator = schema.newValidator();
    }

    public XmlValidator(String xsd) throws SAXException {
        this(SCHEMA_FACTORY.newSchema(new StreamSource(new StringReader(xsd))));
    }

    public XmlValidator(File xsd) throws SAXException {
        this(SCHEMA_FACTORY.newSchema(xsd));
    }

    public XmlValidator(URL xsd) throws SAXException {
        this(SCHEMA_FACTORY.newSchema(xsd));
    }

    public boolean isXmlValid(String xml) {
        boolean isValid = false;
        try {
            validator.validate(new StreamSource(new StringReader(xml)));
            isValid = true;
        } catch (SAXException | IOException ex) {
            Logger.getLogger(Mait.class.getName()).log(Level.SEVERE, "", ex);
        }
        return isValid;
    }

}
