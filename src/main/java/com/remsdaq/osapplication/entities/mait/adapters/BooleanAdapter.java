package com.remsdaq.osapplication.entities.mait.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Arrays;
import java.util.List;

public class BooleanAdapter extends XmlAdapter<String, Boolean> {

    private static final List<String> DEFAULT_BOOLEAN_TRUE_STRINGS = Arrays.asList("Y", "y", "Yes");
    private static final List<String> DEFAULT_BOOLEAN_FALSE_STRINGS = Arrays.asList("N", "n", "No");

    @Override
    public Boolean unmarshal(String boolString) {
        Boolean bool = null;
        if (boolString != null) {
            if (DEFAULT_BOOLEAN_TRUE_STRINGS.contains(boolString)) {
                bool = Boolean.TRUE;
            } else if (DEFAULT_BOOLEAN_FALSE_STRINGS.contains(boolString)) {
                bool = Boolean.FALSE;
            }
        }
        return bool;
    }

    @Override
    public String marshal(Boolean bool) {
        String boolString = null;
        if (bool != null) {
            if (bool) {
                boolString = DEFAULT_BOOLEAN_TRUE_STRINGS.get(0);
            } else {
                boolString = DEFAULT_BOOLEAN_FALSE_STRINGS.get(0);
            }
        }
        return boolString;
    }

}