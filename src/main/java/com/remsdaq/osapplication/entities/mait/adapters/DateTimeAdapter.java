package com.remsdaq.osapplication.entities.mait.adapters;

import com.remsdaq.osapplication.utilities.DateTimeUtilities;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

public class DateTimeAdapter extends XmlAdapter<String, Date> {

    private static final Logger LOG = Logger.getLogger(DateTimeAdapter.class.getName());

    private static final String GMT = "GMT";

    private static final DateFormat UTC_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss'Z'");
    private static final DateFormat ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS = new SimpleDateFormat("HH:mm:ssX");
    private static final DateFormat ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS_AND_MINUTES = new SimpleDateFormat("HH:mm:ssZ");
    private static final DateFormat DEFAULT_MARSHALL_DATE_FORMAT = UTC_DATE_FORMAT;
    private static final List<DateFormat> DEFAULT_UNMARSHALL_FORMATS = Arrays.asList(
            UTC_DATE_FORMAT,
            ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS,
            ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS_AND_MINUTES);

    static {
        UTC_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(GMT));
        ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS.setTimeZone(TimeZone.getTimeZone(GMT));
        ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS_AND_MINUTES.setTimeZone(TimeZone.getTimeZone(GMT));
    }

    @Override
    public String marshal(Date date) {
        return DateTimeUtilities.marshal(date, DEFAULT_MARSHALL_DATE_FORMAT, LOG);
    }

    @Override
    public Date unmarshal(String dateString) {
        return DateTimeUtilities.unmarshal(dateString, DEFAULT_UNMARSHALL_FORMATS, LOG);
    }

}