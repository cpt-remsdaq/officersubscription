package com.remsdaq.osapplication.entities.mait.adapters;

import com.remsdaq.osapplication.utilities.DateTimeUtilities;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    private static final Logger LOG = Logger.getLogger(DateTimeAdapter.class.getName());

    private static final String GMT = "GMT";

    private static final DateTimeFormatter UTC_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter UTC_DATE_FORMAT_WITH_T = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'");
    private static final DateTimeFormatter UTC_DATE_FORMAT_WITH_PERIODS = DateTimeFormatter.ofPattern("yyyy.MM.dd");

    private static final DateTimeFormatter DEFAULT_MARSHALL_DATE_FORMAT = UTC_DATE_FORMAT;

    private static final List<DateTimeFormatter> DEFAULT_UNMARSHALL_FORMATS = Arrays.asList(
            UTC_DATE_FORMAT,
            UTC_DATE_FORMAT_WITH_T,
            UTC_DATE_FORMAT_WITH_PERIODS
    );

    @Override
    public String marshal(LocalDate date) {
        return DateTimeUtilities.marshalLocalDate(date, DEFAULT_MARSHALL_DATE_FORMAT, LOG);
    }

    @Override
    public LocalDate unmarshal(String dateString) {
        return DateTimeUtilities.unmarshalLocalDate(dateString, DEFAULT_UNMARSHALL_FORMATS, LOG);
    }

}