package com.remsdaq.osapplication.entities.mait.adapters;

import com.remsdaq.osapplication.utilities.DateTimeUtilities;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Logger;

public class LocalTimeAdapter extends XmlAdapter<String, LocalTime> {

    private static final Logger LOG = Logger.getLogger(DateTimeAdapter.class.getName());

    private static final String GMT = "GMT";

    private static final DateTimeFormatter UTC_DATE_FORMAT = DateTimeFormatter.ofPattern("HH:mm:ss:'Z'");
    private static final DateTimeFormatter ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS = DateTimeFormatter.ofPattern("HH:mm:ssX");
    private static final DateTimeFormatter ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS_AND_MINUTES = DateTimeFormatter.ofPattern("HH:mm:ssZ");
    private static final DateTimeFormatter ISO_8601_DATE_FORMAT_TIMEZONE_IN_TEXT = DateTimeFormatter.ofPattern("HH:mm:ssz");

    private static final DateTimeFormatter DEFAULT_MARSHALL_DATE_FORMAT = UTC_DATE_FORMAT;

    private static final List<DateTimeFormatter> DEFAULT_UNMARSHALL_FORMATS = Arrays.asList(
            UTC_DATE_FORMAT,
            ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS,
            ISO_8601_DATE_FORMAT_TIMEZONE_WITH_HOURS_AND_MINUTES,
            ISO_8601_DATE_FORMAT_TIMEZONE_IN_TEXT
    );

    @Override
    public String marshal(LocalTime localTime) {
        return DateTimeUtilities.marshalLocalTime(localTime, DEFAULT_MARSHALL_DATE_FORMAT, LOG);
    }

    @Override
    public LocalTime unmarshal(String dateString) {
        return DateTimeUtilities.unmarshallLocalTime(dateString, DEFAULT_UNMARSHALL_FORMATS, LOG);
    }

}