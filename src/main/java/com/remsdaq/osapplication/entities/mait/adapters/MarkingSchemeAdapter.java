package com.remsdaq.osapplication.entities.mait.adapters;

import com.remsdaq.osapplication.entities.mait.entities.messageControl.MarkingScheme;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class MarkingSchemeAdapter extends XmlAdapter<String, MarkingScheme> {
	
	
	@Override
	public MarkingScheme unmarshal(String v) throws Exception {
		return MarkingScheme.getMarkingSchemeByValue(v);
	}
	
	@Override
	public String marshal(MarkingScheme v) throws Exception {
		return (v != null) ? v.name() : null;
	}
}
