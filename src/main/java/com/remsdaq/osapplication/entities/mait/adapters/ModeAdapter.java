package com.remsdaq.osapplication.entities.mait.adapters;

import com.remsdaq.osapplication.entities.mait.entities.Mode;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ModeAdapter extends XmlAdapter<String, Mode> {

    @Override
    public Mode unmarshal(String modeString) {
        return Mode.getModeByValue(modeString);
    }

    @Override
    public String marshal(Mode mode) {
        return (mode != null) ? mode.getValue() : null;
    }

}