package com.remsdaq.osapplication.entities.mait.adapters;

import com.remsdaq.osapplication.entities.mait.entities.messageControl.SecurityLevel;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class SecurityLevelAdapter extends XmlAdapter<String, SecurityLevel>{
	
	
	@Override
	public SecurityLevel unmarshal(String v) throws Exception {
		return SecurityLevel.getLevelByValue(v);
	}
	
	@Override
	public String marshal(SecurityLevel v) throws Exception {
		return (v != null) ? v.name() : null;
	}
}
