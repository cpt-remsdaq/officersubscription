package com.remsdaq.osapplication.entities.mait.adapters;

import com.remsdaq.osapplication.entities.caller.CallerTitle;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class TitleAdapter extends XmlAdapter<String, CallerTitle> {

    @Override
    public CallerTitle unmarshal(String titleString) {
        return CallerTitle.getTitleByValue(titleString);
    }


    @Override
    public String marshal(CallerTitle callerTitle) {
        return (callerTitle != null) ? callerTitle.getValue() : null;
    }
}
