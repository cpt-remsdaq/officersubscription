package com.remsdaq.osapplication.entities.mait.entities;

import java.time.LocalDate;
import java.time.LocalTime;

public class MessageComment {

    // [N]  :   Boolean     :   Flag to denote that the contents of this message or log entry requireurgent attension/action of the receiving organisation
    private Boolean operationallyUrgent;

    // [Y]  :   Boolean     :   Flag to indicate if this message originated from a human action
    private Boolean manualAcknowledgement;

    // [Y]  :   String(512) :   commentDescription
    private String commentDescription;

    // [Y]  :   Date        :   Date the log entry was created on the C&C system from which the update comes
    private LocalDate commentDate;

    // [Y]  :   Time        :   Time the log entry was created on the C&C system from which the update comes
    private LocalTime commentTime;

    // [N]  :   Integer     :   Priority of the log entry in the C&C system from which the update comes
    private int commentPriority;

    // [N]  :   String(512) :   Type of log entry in the C&C system from which the update comes
    private String commentType;

    // [N]  :   String(512) :   Id of user that created the log entry on the C&C system from which the update comes
    private String commentOwner;

    // [N]  :   Integer     :   A sequential number allocated to each log entry in an incident
    private int commentLineNo;

    public Boolean getOperationallyUrgent() {
        return operationallyUrgent;
    }

    public void setOperationallyUrgent(Boolean operationallyUrgent) {
        this.operationallyUrgent = operationallyUrgent;
    }


    public Boolean getManualAcknowledgement() {
        return manualAcknowledgement;
    }

    public void setManualAcknowledgement(Boolean manualAcknowledgement) {
        this.manualAcknowledgement = manualAcknowledgement;
    }

    public String getCommentDescription() {
        return commentDescription;
    }

    public void setCommentDescription(String commentDescription) {
        this.commentDescription = commentDescription;
    }

    public LocalDate getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(LocalDate commentDate) {
        this.commentDate = commentDate;
    }

    public LocalTime getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(LocalTime commentTime) {
        this.commentTime = commentTime;
    }

    public int getCommentPriority() {
        return commentPriority;
    }

    public void setCommentPriority(int commentPriority) {
        this.commentPriority = commentPriority;
    }

    public String getCommentType() {
        return commentType;
    }

    public void setCommentType(String commentType) {
        this.commentType = commentType;
    }

    public String getCommentOwner() {
        return commentOwner;
    }

    public void setCommentOwner(String commentOwner) {
        this.commentOwner = commentOwner;
    }

    public int getCommentLineNo() {
        return commentLineNo;
    }

    public void setCommentLineNo(int commentLineNo) {
        this.commentLineNo = commentLineNo;
    }
}
