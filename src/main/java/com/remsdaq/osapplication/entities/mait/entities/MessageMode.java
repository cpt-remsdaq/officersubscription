package com.remsdaq.osapplication.entities.mait.entities;

public enum MessageMode {
    CREATE("Create"),
    CHANGE("Change"),
    DELETE("Delete"),
    CLOSE("Close")
    ;

    private final String mode;

    MessageMode(String mode) {
        this.mode = mode;
    }
}
