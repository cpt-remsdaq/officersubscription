package com.remsdaq.osapplication.entities.mait.entities;

public enum Mode {

    CREATE("create"),
    CHANGE("change"),
    DELETE("delete"),
    CLOSE("close");

    private String value;

    Mode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return getValue();
    }

    public static final Mode getModeByValue(String value) {
        for (Mode mode : values()) {
            if (mode.getValue().equals(value)) {
                return mode;
            }
        }
        return null;
    }

}