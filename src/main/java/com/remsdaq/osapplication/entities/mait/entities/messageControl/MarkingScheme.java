package com.remsdaq.osapplication.entities.mait.entities.messageControl;

public enum MarkingScheme {
	
	BIL,
	GPMS,
	GSC;
	
	public static MarkingScheme getMarkingSchemeByValue(String v) {
		for (MarkingScheme markingScheme : values()) {
			if (markingScheme.name().equals(v.toUpperCase())) {
				return markingScheme;
			}
		}
		return null;
	}
}
