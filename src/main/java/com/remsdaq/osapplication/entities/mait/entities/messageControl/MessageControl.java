package com.remsdaq.osapplication.entities.mait.entities.messageControl;

import com.remsdaq.osapplication.entities.mait.adapters.*;
import com.remsdaq.osapplication.entities.mait.entities.Mode;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Entity
@XmlRootElement(name = "MessageControl")
@XmlAccessorType(XmlAccessType.FIELD)
public class MessageControl implements Serializable {
	
	private static final Mode DEFAULT_MODE = Mode.CREATE;
	
	// MessageControl
	
	// String   => maximum length should be 512
	// Date     => conforms to ISO 8601 (YYYY-MM-DD)
	// Time     => conforms to ISO 8601 (hh:mm:ss+hh[:mm] or hh:mm:ssZ
	// Integer  => minmax values +- 32768
	// Boolean  => Y or N
	
	// Mandatory? : format : description : notes
	
	@NotNull
	// [Y]  :   String(18)  :   Unique ID of the message
	@XmlElement(name = "MessageId")
	private String messageId;
	
	// [N]  :   Enum        :   The operating mode of the message   :   [default: Create]
	@XmlAttribute(name = "Mode")
	@XmlJavaTypeAdapter(value = ModeAdapter.class, type = Mode.class)
	private Mode mode = DEFAULT_MODE;
	
	// [N]  :   String(6)   :   Identification of the marking scheme used to code the incident  :   Must only be present if securityLevel is present
	@XmlElement(name = "MarkingScheme")
	@XmlJavaTypeAdapter(value = MarkingSchemeAdapter.class, type = MarkingScheme.class)
	private MarkingScheme markingScheme;
	
	// [N]  :   String(24)  :   Security level of the message:   Must only be present if markingScheme is present
	@XmlElement(name = "SecurityLevel")
	@XmlJavaTypeAdapter(value = SecurityLevelAdapter.class, type = SecurityLevel.class)
	private SecurityLevel securityLevel;
	
	@NotNull
	// [Y]  :   String(512) :   Code for the recipient organisation
	@XmlElement(name = "DestinOrganisation")
	private String destinOrganisation;
	
	@NotNull
	// [Y]  :   String(512) :   Code for the originating organisation
	@XmlElement(name = "OrigOrganisation")
	private String origOrganisation;
	
	@NotNull
	// [Y]  :   String(24)  :   Unique Reference Number of the incident in the originating organisation
	@XmlElement(name = "OrigIncidentURN")
	private String origIncidentURN;
	
	@NotNull
	// [Y]  :   String(24)  :   Number of incident in originating organisation as known by the users within this organisation
	@XmlElement(name = "OrigIncidentNum")
	private String origIncidentNum;
	
	// [N]  :   Date        :   Creation date of incident in originating organisation
	@XmlElement(name = "OrigIncidentDate")
	@XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)
	private LocalDate origIncidentDate;
	
	// [N]  :   Time        :   Creation time of incident in originating organisation
	@XmlElement(name = "OrigIncidentTime")
	@XmlJavaTypeAdapter(value = LocalTimeAdapter.class, type = LocalTime.class)
	private LocalTime origIncidentTime;
	
	public String getMessageId() {
		return messageId;
	}
	
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	
	public Mode getMode() {
		return mode;
	}
	
	public void setMode(Mode mode) {
		this.mode = mode;
	}
	
	public MarkingScheme getMarkingScheme() {
		return markingScheme;
	}
	
	public void setMarkingScheme(MarkingScheme markingScheme) {
		this.markingScheme = markingScheme;
	}
	
	public SecurityLevel getSecurityLevel() {
		return securityLevel;
	}
	
	public void setSecurityLevel(SecurityLevel securityLevel) {
		this.securityLevel = securityLevel;
	}
	
	public String getDestinOrganisation() {
		return destinOrganisation;
	}
	
	public void setDestinOrganisation(String destinOrganisation) {
		this.destinOrganisation = destinOrganisation;
	}
	
	public String getOrigOrganisation() {
		return origOrganisation;
	}
	
	public void setOrigOrganisation(String origOrganisation) {
		this.origOrganisation = origOrganisation;
	}
	
	public String getOrigIncidentURN() {
		return origIncidentURN;
	}
	
	public void setOrigIncidentURN(String origIncidentURN) {
		this.origIncidentURN = origIncidentURN;
	}
	
	public String getOrigIncidentNum() {
		return origIncidentNum;
	}
	
	public void setOrigIncidentNum(String origIncidentNum) {
		this.origIncidentNum = origIncidentNum;
	}
	
	public LocalDate getOrigIncidentDate() {
		return origIncidentDate;
	}
	
	public void setOrigIncidentDate(LocalDate origIncidentDate) {
		this.origIncidentDate = origIncidentDate;
	}
	
	public LocalTime getOrigIncidentTime() {
		return origIncidentTime;
	}
	
	public void setOrigIncidentTime(LocalTime origIncidentTime) {
		this.origIncidentTime = origIncidentTime;
	}
	
	public LocalDateTime getOrigIncidentLocalDateTime() {
		return LocalDateTime.of(getOrigIncidentDate(), getOrigIncidentTime());
	}
	
	public String getUserFriendlyOrigIncidentDate() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		return getOrigIncidentDate().format(formatter);
		
	}
	
	public String getUserFriendlyOrigIncidentTime() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
		return getOrigIncidentTime().format(formatter);
	}
	
	public String getSummary() {
		
		return new StringBuilder()
				.append("Message ID: " + this.messageId + "\n")
				.append("Mode: " + this.mode + "\n")
				.append("Marking Scheme: " + this.markingScheme + "\n")
				.append("Security Level: " + this.securityLevel + "\n")
				.append("Destination Organisation: " + this.destinOrganisation + "\n")
				.append("Originating Organisation: " + this.origOrganisation + "\n")
				.append("Originating Incident Unique Reference Number: " + this.origIncidentURN + "\n")
				.append("Originating Incident Number: " + this.origIncidentNum + "\n")
				.append("Originating Incident Date: " + this.origIncidentDate + "\n")
				.append("Originating Incident Time: " + this.origIncidentTime + "\n")
				.toString();
	}
}
