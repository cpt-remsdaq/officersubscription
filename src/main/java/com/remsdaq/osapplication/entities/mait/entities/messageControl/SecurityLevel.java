package com.remsdaq.osapplication.entities.mait.entities.messageControl;

public enum SecurityLevel {

	IL0,
	IL1,
	IL2,
	IL3,
	NPM,
	PROTECT,
	RESTRICTED,
	OFFICIAL,
	OFFICIAL_SENSITIVE;
	
	public static SecurityLevel getLevelByValue(String v) {
		for (SecurityLevel securityLevel : values()) {
			if (securityLevel.name().equals(v.toUpperCase())) {
				return securityLevel;
			}
		}
		return null;
	}
}
