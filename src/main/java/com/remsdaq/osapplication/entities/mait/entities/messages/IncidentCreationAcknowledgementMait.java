package com.remsdaq.osapplication.entities.mait.entities.messages;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.time.LocalDate;

//@Entity
//@XmlRootElement(name = "IncidentCreationAcknowledgement")
//@XmlAccessorType(XmlAccessType.FIELD)
public class IncidentCreationAcknowledgementMait implements Serializable  {

    // [Y]  :   String(18)  :   Unique ID of the incidentCreation message being acknowledge
    private String  messageId;

    // [Y]  :   String(512) :   Code of the organisation sending the acknowledgement
    private String origOrganisation;

    // [Y]  :   String(24)  :   Unique reference number of the incident created as a result of receiving the incidentCreation message
    private String origIncidentURN;

    // [N]  :   String(24)  :   Number of the incident created as a result of receiving the incident creation message
    private String origIncidentNum;

    // [N]  :   Date        :   Creation date of the incident created as a result of receiving the IncidentCreationMait message
    private LocalDate origIncidentDate;

    // [Y]  :   String(512) :   Code for the recipient organisation
    private String destinOrganisation;

    // [Y]  :   String(24)  :   Unique reference number of the incident in the C&C system which sent the original IncidentCreationMait message
    private String destinIncidentURN;

    // [Y]  :   Boolean     :   Flag to indicate whether or not the incident creation was successful
    private Boolean successful;

    // [N]  :   String(512) :   Text description of the error in the event of a failure
    private String errorDescription;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getOrigOrganisation() {
        return origOrganisation;
    }

    public void setOrigOrganisation(String origOrganisation) {
        this.origOrganisation = origOrganisation;
    }

    public String getOrigIncidentURN() {
        return origIncidentURN;
    }

    public void setOrigIncidentURN(String origIncidentURN) {
        this.origIncidentURN = origIncidentURN;
    }

    public String getOrigIncidentNum() {
        return origIncidentNum;
    }

    public void setOrigIncidentNum(String origIncidentNum) {
        this.origIncidentNum = origIncidentNum;
    }

    public LocalDate getOrigIncidentDate() {
        return origIncidentDate;
    }

    public void setOrigIncidentDate(LocalDate origIncidentDate) {
        this.origIncidentDate = origIncidentDate;
    }

    public String getDestinOrganisation() {
        return destinOrganisation;
    }

    public void setDestinOrganisation(String destinOrganisation) {
        this.destinOrganisation = destinOrganisation;
    }

    public String getDestinIncidentURN() {
        return destinIncidentURN;
    }

    public void setDestinIncidentURN(String destinIncidentURN) {
        this.destinIncidentURN = destinIncidentURN;
    }

    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
