package com.remsdaq.osapplication.entities.mait.entities.messages;

import com.remsdaq.osapplication.entities.mait.adapters.BooleanAdapter;
import com.remsdaq.osapplication.entities.mait.adapters.DateTimeAdapter;
import com.remsdaq.osapplication.entities.mait.adapters.ModeAdapter;
import com.remsdaq.osapplication.entities.mait.entities.Mode;
import com.remsdaq.osapplication.entities.mait.entities.messageControl.MessageControl;
import com.remsdaq.osapplication.entities.mait.entities.persons.CallerDetails;
import com.remsdaq.osapplication.entities.mait.entities.persons.NonResourcePerson;
import com.remsdaq.osapplication.entities.mait.entities.sites.C4Site;
import com.remsdaq.osapplication.entities.mait.entities.vehicles.VehicleInvolved;

import javax.validation.constraints.NotNull;

import javax.persistence.Entity;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;

@Entity
@XmlRootElement(name = "IncidentCreation")
@XmlAccessorType(XmlAccessType.FIELD)
public class IncidentCreationMait extends Mait implements Serializable {
	
	@XmlAttribute(name = "schema")
	private String schema;
	
	@XmlAttribute(name = "mode")
	@XmlJavaTypeAdapter(value = ModeAdapter.class, type = Mode.class)
	private Mode mode = Mode.CREATE;
	
	@NotNull
	@XmlElement(name = "MessageControl")
	private MessageControl messageControl;
	
	@XmlElement(name = "CallerDetails")
	private CallerDetails callerDetails;
	
	@NotNull
	// [Y]  :   String(16)  :   Origin of original call :   e.g. kiosk, mobile, private sub, officer, e-call etc. Not for source of incident
	@XmlElement(name = "CallOrigin")
	private String callOrigin; // check
	
	// [N]  :   String(20)  :   Priority of incident    :   this is also known as the Grade Code. This is the original organisations local value - use <AttendanceRequested> and <NotigicationReason> for National Inter Agency Priority
	//    private Priority priority; // create class
	@XmlElement(name = "Priority")
	private String priority; // create class
	
	// [N]  :   String(512) :   Description of the incident
	@XmlElement(name = "Description")
	private String description;
	
	@NotNull
	// [Y]  :   String(512) :   Detailed location as stored in the originating C&C system   :   needed for when no addressable location can be matched
	@XmlElement(name = "Location")
	private String location; // create class
	
	// [N]  :   String(24)  :   How to interpret the supplied X/Y   :   e.g OSGR, OSGB36, WGS84 etc.
	@XmlElement(name = "CoordinateSystem")
	private String coordinateSystem; // create enum
	
	// [N]  :   String(512) :   The X coordinate in the defined coordinate scheme
	@XmlElement(name = "X")
	private Double x;
	
	// [N]  :   String(512) :   The Y coordinate in the defined coordinate scheme
	@XmlElement(name = "Y")
	private Double y;
	
	// [N]  :   Integer     :   Reference Type of the gazeteer  :   Must only be present if IncidentGazRef is present
	@XmlElement(name = "IncidentGazType")
	private String incidentGazType; // check
	
	// [N]  :   String(512) :   Reference in the gazetteer  :   Must only be present if IncidentGazType is present
	@XmlElement(name = "IncidentGazRef")
	private String incidentGazRef; // check
	
	// [N]  :   Boolean     :   Indication that sending agency has risk information associated for the incident's location or vicinity
	@XmlElement(name = "HazardFlag")
	@XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
	private Boolean hazardFlag; // check array, check
	
	// [N]  :   Boolean     :   Warning that incident scene is known to be unsafe   :   absence must not be false, must be null
	@XmlElement(name = "KnownSceneSafetyIssue")
	@XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
	private Boolean knownSceneSafetyIssues; // check array, check
	//    @XmlElementRef
	@XmlElement(name = "C4Site")
	private C4Site c4Site;
	
	@NotNull
	// [Y]  :   String(512) :   Type of incident    :    Code for the type of the incident - this is the original
	// organisation code and could be used between similar organisations such as fire to exchange richer data if they both conform to the national fire incident type usage
	@XmlElement(name = "IncidentType")
	private String type;
	
	// [N]  :   String(512) :   Sub type of incident    :    Code for the sub-type of the incident
	//    private IncidentSubType subType;
	@XmlElement(name = "IncidentSubType")
	private String subType;
	
	// [N]  :   Boolean     :   To differentiate between those occasions where the originating organisation is informing
	// the destination organisation of their attendance and where a mobilisation of resources is required
	//      :   However this doesn't not mean that the receiving organisation is required to respond, just that the sender
	// requests it. Equally an operator may independently choose to attend, based on received information and local
	// intelligence.
	@XmlElement(name = "AttendanceRequested")
	@XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
	private Boolean attendanceRequested;
	
	// [N]  :   String(512) :   To enable the originating organisation to specify wy they are requesting resources from the destination organisation
	@XmlElement(name = "NotificationReason")
	private String notificationReason; // create enum?
	
	//  [N] :   Time        :   Time to attend incident from receiving organisation :   Omission indicates no ETA, (rather than already there, indicated by 00:00:00)
	@XmlElement(name = "ResourceETA")
	@XmlJavaTypeAdapter(value = DateTimeAdapter.class, type = Date.class)
	private Date resourceETA;
	
	//  [N] :   String(512) :   Identifying information for the resource
	@XmlElement(name = "ResourceID")
	private String resourceID;
	
	@XmlElementRef
	@XmlElementWrapper(name = "VehiclesInvolved")
	private ArrayList<VehicleInvolved> vehiclesInvolved;
	
	@XmlElement(name = "Person")
	@XmlElementWrapper(name = "PersonsInvolved")
	private ArrayList<NonResourcePerson> personsInvolved;
	
	public IncidentCreationMait() {
		super();
	}
	
	public String getSchema() {
		return schema;
	}
	
	public void setSchema(String schema) {
		this.schema = schema;
	}
	
	public Mode getMode() {
		return mode;
	}
	
	public void setMode(Mode mode) {
		this.mode = mode;
	}
	
	public MessageControl getMessageControl() {
		return messageControl;
	}
	
	public void setMessageControl(MessageControl messageControl) {
		this.messageControl = messageControl;
	}
	
	public CallerDetails getCallerDetails() {
		return callerDetails;
	}
	
	public void setCallerDetails(CallerDetails callerDetails) {
		this.callerDetails = callerDetails;
	}
	
	public String getCallOrigin() {
		return callOrigin;
	}
	
	public void setCallOrigin(String callOrigin) {
		this.callOrigin = callOrigin;
	}
	
	public String getPriority() {
		return priority;
	}
	
	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getCoordinateSystem() {
		return coordinateSystem;
	}
	
	public void setCoordinateSystem(String coordinateSystem) {
		this.coordinateSystem = coordinateSystem;
	}
	
	public Double getX() {
		return x;
	}
	
	public void setX(Double x) {
		this.x = x;
	}
	
	public Double getY() {
		return y;
	}
	
	public void setY(Double y) {
		this.y = y;
	}
	
	public String getIncidentGazType() {
		return incidentGazType;
	}
	
	public void setIncidentGazType(String incidentGazType) {
		this.incidentGazType = incidentGazType;
	}
	
	public String getIncidentGazRef() {
		return incidentGazRef;
	}
	
	public void setIncidentGazRef(String incidentGazRef) {
		this.incidentGazRef = incidentGazRef;
	}
	
	public Boolean getHazardFlag() {
		return hazardFlag;
	}
	
	public void setHazardFlag(Boolean hazardFlag) {
		this.hazardFlag = hazardFlag;
	}
	
	public Boolean getKnownSceneSafetyIssues() {
		return knownSceneSafetyIssues;
	}
	
	public void setKnownSceneSafetyIssues(Boolean knownSceneSafetyIssues) {
		this.knownSceneSafetyIssues = knownSceneSafetyIssues;
	}
	
	public C4Site getC4Site() {
		return c4Site;
	}
	
	public void setC4Site(C4Site c4Site) {
		this.c4Site = c4Site;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getSubType() {
		return subType;
	}
	
	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	public Boolean getAttendanceRequested() {
		return attendanceRequested;
	}
	
	public void setAttendanceRequested(Boolean attendanceRequested) {
		this.attendanceRequested = attendanceRequested;
	}
	
	public String getNotificationReason() {
		return notificationReason;
	}
	
	public void setNotificationReason(String notificationReason) {
		this.notificationReason = notificationReason;
	}
	
	public Date getResourceETA() {
		return resourceETA;
	}
	
	public void setResourceETA(Date resourceETA) {
		this.resourceETA = resourceETA;
	}
	
	public String getResourceID() {
		return resourceID;
	}
	
	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}
	
	public ArrayList<VehicleInvolved> getVehiclesInvolved() {
		return vehiclesInvolved;
	}
	
	public void setVehiclesInvolved(ArrayList<VehicleInvolved> vehiclesInvolved) {
		this.vehiclesInvolved = vehiclesInvolved;
	}
	
	public ArrayList<NonResourcePerson> getPersonsInvolved() {
		return personsInvolved;
	}
	
	public void setPersonsInvolved(ArrayList<NonResourcePerson> personsInvolved) {
		this.personsInvolved = personsInvolved;
	}
	
	public LocalTime getLocalResourceETA() {
		Instant resourceETAInstant = this.getResourceETA().toInstant();
		ZonedDateTime zonedResourceETAInstant = resourceETAInstant.atZone(ZoneId.of("GMT"));
		return zonedResourceETAInstant.toLocalTime();
	}
	
	public String getResourceETAString() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
		LocalTime localTime = getLocalResourceETA();
		return localTime.format(formatter);
	}
	
	@Override
	public String getSummary() {
		
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder
				//                .append("Schema: " + this.schema + "\n")
				//                .append("Modes: " + this.mode + "\n")
				.append("Message Control:\n")
				.append(this.messageControl.getSummary())
				.append("Caller Details:\n")
				.append(this.callerDetails.getSummary())
				.append("Call Origin: " + this.callOrigin + "\n")
				.append("Priority: " + this.priority + "\n")
				.append("Description: " + this.description + "\n")
				.append("Location: " + this.location + "\n")
				.append("Coordinate System: " + this.coordinateSystem + "\n")
				.append("Incident Gazetteer Type: " + this.incidentGazType + "\n")
				.append("Incident Gazeteer Reference: " + this.incidentGazRef + "\n")
				.append("Hazard Flag: " + this.hazardFlag + "\n")
				.append("Known Scene Safety Issue: " + this.knownSceneSafetyIssues + "\n")
				.append("C4Site:\n")
				.append(this.c4Site.getSummary())
				.append("Type: " + this.type + "\n")
				.append("Sub Type: " + this.subType + "\n")
				.append("Attendance Requested: " + this.attendanceRequested + "\n")
				.append("Notification Reason: " + this.notificationReason + "\n")
				.append("Resource ETA: " + this.getResourceETAString() + "\n")
				.append("Resource ID: " + this.resourceID + "\n")
				.append("Vehicles Involved:\n");
		
		for (VehicleInvolved vehicle : this.getVehiclesInvolved()) {
			stringBuilder.append(vehicle.getSummary());
		}
		
		stringBuilder
				.append("Persons Involved:\n");
		//                .append(this.personsInvolved.getSummary() + "\n")
		
		for (NonResourcePerson person : this.personsInvolved) {
			stringBuilder.append(person.getSummary());
		}
		
		return stringBuilder.toString();
	}
}
