package com.remsdaq.osapplication.entities.mait.entities.messages;

import java.io.Serializable;
import java.time.LocalDate;

//@Entity
//@XmlRootElement(name = "IncidentUpdateAcknowledgement")
//@XmlAccessorType(XmlAccessType.FIELD)
public class IncidentUpdateAcknowledgementMait extends Mait implements Serializable {

    // [Y]  :   String(16)  :   Unique ID of the message acknowledged
    private String messageID;

    // [Y]  :   String(512) :   Code of the organisation sending the acknowledgement
    private String origOrganisation;

    // [N]  :   String(24)  :   Unique reference number of the incident in the C&C system which sends the acknowledgement
    private String origIncidentURN;

    // [N]  :   String(24)  :   Number of the incident in the C&C system which sends teh acknowledgement
    private String origIncidentNum;

    // [N]  :   Date        :   Creation date of the incident in the C&C system which sends the acknowledgement
    private LocalDate origIncidentDate;

    // [Y]  :   String(512) :   Code for the recipient organisation
    private String destinOrganisation;

    // [Y]  :   String(24)  :   Unique reference number of the incident in the C&C system which receives the acknowledgement
    private String destinIncidentURN;

    // [Y]  :   Boolean     :   Flag to indicate whether or not the incident udpate was successful
    private Boolean successful;

    // [Y]  :   String(512) :   Text description of the error in the event of a failure
    private String errorDescription;

    public String getMessageID() {
        return messageID;
    }

    public void setMessageID(String messageID) {
        this.messageID = messageID;
    }

    public String getOrigOrganisation() {
        return origOrganisation;
    }

    public void setOrigOrganisation(String origOrganisation) {
        this.origOrganisation = origOrganisation;
    }

    public String getOrigIncidentURN() {
        return origIncidentURN;
    }

    public void setOrigIncidentURN(String origIncidentURN) {
        this.origIncidentURN = origIncidentURN;
    }

    public String getOrigIncidentNum() {
        return origIncidentNum;
    }

    public void setOrigIncidentNum(String origIncidentNum) {
        this.origIncidentNum = origIncidentNum;
    }

    public LocalDate getOrigIncidentDate() {
        return origIncidentDate;
    }

    public void setOrigIncidentDate(LocalDate origIncidentDate) {
        this.origIncidentDate = origIncidentDate;
    }

    public String getDestinOrganisation() {
        return destinOrganisation;
    }

    public void setDestinOrganisation(String destinOrganisation) {
        this.destinOrganisation = destinOrganisation;
    }

    public String getDestinIncidentURN() {
        return destinIncidentURN;
    }

    public void setDestinIncidentURN(String destinIncidentURN) {
        this.destinIncidentURN = destinIncidentURN;
    }

    public Boolean getSuccessful() {
        return successful;
    }

    public void setSuccessful(Boolean successful) {
        this.successful = successful;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
