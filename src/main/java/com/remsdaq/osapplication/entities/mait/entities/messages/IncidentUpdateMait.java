package com.remsdaq.osapplication.entities.mait.entities.messages;

import com.remsdaq.osapplication.entities.mait.entities.MessageComment;

import java.io.Serializable;
import java.util.List;

//@Entity
//@XmlRootElement(name = "IncidentUpdate")
//@XmlAccessorType(XmlAccessType.FIELD)
public class IncidentUpdateMait extends Mait implements Serializable {

    // [Y]  :   String(18)  :   Unique ID of the message
    private String messageId;

    // [Y]  :   String(512) :   Code of the organisation sending the incident update message
    private String origOrganisation;

    // [N]  :   String(24)  :   Unique reference number of the incident in the C&C system which sends the incident update
    private String origIncidentURN;

    // [Y]  :   String(512) :   Code for the recipient organisation
    private String destinOrganisation;

    // [Y]  :   String(24)  :   Unique reference number of the incident in the C&C system which receives the incident update
    private String destinIncidentURN;

    // [Y]  :   String      :   Log entry being sent    :   Maximum of 100 comments per message
    private List<MessageComment> messageComments;

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getOrigOrganisation() {
        return origOrganisation;
    }

    public void setOrigOrganisation(String origOrganisation) {
        this.origOrganisation = origOrganisation;
    }

    public String getOrigIncidentURN() {
        return origIncidentURN;
    }

    public void setOrigIncidentURN(String origIncidentURN) {
        this.origIncidentURN = origIncidentURN;
    }

    public String getDestinOrganisation() {
        return destinOrganisation;
    }

    public void setDestinOrganisation(String destinOrganisation) {
        this.destinOrganisation = destinOrganisation;
    }

    public String getDestinIncidentURN() {
        return destinIncidentURN;
    }

    public void setDestinIncidentURN(String destinIncidentURN) {
        this.destinIncidentURN = destinIncidentURN;
    }

    public List<MessageComment> getMessageComments() {
        return messageComments;
    }

    public void setMessageComments(List<MessageComment> messageComments) {
        this.messageComments = messageComments;
    }
    
    @Override
    public String getSummary() {
        return null;
    }
    
    //

}
