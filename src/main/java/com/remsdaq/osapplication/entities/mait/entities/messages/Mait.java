package com.remsdaq.osapplication.entities.mait.entities.messages;

import com.remsdaq.osapplication.entities.mait.MaitToXml;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.io.Serializable;

@Entity
@XmlRootElement(name = "root")
@XmlSeeAlso({IncidentCreationMait.class, IncidentCreationAcknowledgementMait.class, IncidentUpdateMait.class, IncidentUpdateAcknowledgementMait.class})
@XmlAccessorType(XmlAccessType.FIELD)
public class Mait implements Serializable {

    public String getXml() {
        return new MaitToXml().getXml(this);
    }

    public String getSummary() {
        return "";
    }
}
