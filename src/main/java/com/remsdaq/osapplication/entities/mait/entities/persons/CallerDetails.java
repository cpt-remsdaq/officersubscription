package com.remsdaq.osapplication.entities.mait.entities.persons;


import javax.persistence.Entity;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.*;
import java.time.format.DateTimeFormatter;

@Entity
@XmlRootElement(name = "CallerDetails")
@XmlAccessorType(XmlAccessType.FIELD)
public class CallerDetails implements Serializable{

    // [N]  :   String(512) :   Title of the caller
     @XmlElement(name="CallerTitle") // In the case that non enumerated values are required.
     private String callerTitle;
//    @XmlElement(name = "CallerTitle")
//    private CallerTitle callerTitle;

    // [N]  :   String(512) :   Forename of the caller
    @XmlElement(name = "CallerForename")
    private String callerForename;

    // [N]  :   String(512) :   Surname of the caller
    @XmlElement(name = "CallerSurname")
    private String callerSurname;

    // [N]  :   String(512) :   Phone number of the caller
    @XmlElement(name = "CallerNumber")
    private String callerNumber;

    // [N]  :   String(512) :   Complete address of the caller
    @XmlElement(name = "CallerAddress")
    private String callerAddress;

    // [N]  :   String(512) :   EISEC data provided by the mobile supplier bia BT
    @XmlElement(name = "CallerMobile")
    private String callerMobile;

    // [N]  :   Integer     :   Reference Type of Gazeteer  :    Must only be present if CallerGazRef is present
    @XmlElement(name = "CallerGazType")
    private int callerGazType;

    // [N]  : String(512)   :   The reference in the Gazetteer  :   Content of agreed primary key field for the selected gazeteer. Must only be included if CallerGazType is present;
    @XmlElement(name = "CallerGazRef")
    private String callerGazRef;

    public String getCallerTitle() {
        return callerTitle;
    }

    public void setCallerTitle(String callerTitle) {
        this.callerTitle = callerTitle;
    }

    public String getCallerForename() {
        return callerForename;
    }

    public void setCallerForename(String callerForename) {
        this.callerForename = callerForename;
    }

    public String getCallerSurname() {
        return callerSurname;
    }

    public void setCallerSurname(String callerSurname) {
        this.callerSurname = callerSurname;
    }

    public String getCallerNumber() {
        return callerNumber;
    }

    public void setCallerNumber(String callerNumber) {
        this.callerNumber = callerNumber;
    }

    public String getCallerAddress() {
        return callerAddress;
    }

    public void setCallerAddress(String callerAddress) {
        this.callerAddress = callerAddress;
    }

    public String getCallerMobile() {
        return callerMobile;
    }

    public void setCallerMobile(String callerMobile) {
        this.callerMobile = callerMobile;
    }

    public int getCallerGazType() {
        return callerGazType;
    }

    public void setCallerGazType(int callerGazType) {
        this.callerGazType = callerGazType;
    }

    public String getCallerGazRef() {
        return callerGazRef;
    }

    public void setCallerGazRef(String callerGazRef) {
        this.callerGazRef = callerGazRef;
    }

    public String getSummary() {
        return new StringBuilder()
                .append("Caller Title: " + callerTitle + "\n")
                .append("Caller Forename: " + callerForename + "\n")
                .append("Caller Surname: " + callerSurname + "\n")
                .append("Caller Number: " + callerNumber + "\n")
                .append("Caller Address: " + callerAddress + "\n")
                .append("Caller Mobile: " + callerMobile + "\n")
                .append("Caller Gazetter Type: " + callerGazType + "\n")
                .append("Caller Gazetter Reference: " + callerGazRef + "\n")
                .toString();
    }
}
