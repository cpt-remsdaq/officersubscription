package com.remsdaq.osapplication.entities.mait.entities.persons;

import com.remsdaq.osapplication.entities.addresses.Address;
import com.remsdaq.osapplication.entities.mait.adapters.BooleanAdapter;
import com.remsdaq.osapplication.entities.mait.adapters.LocalDateAdapter;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@XmlRootElement(name = "Person")
@XmlAccessorType(XmlAccessType.FIELD)
public class NonResourcePerson implements Serializable{

    // [N]  :   String(512) :   Forename of the person involved
    @XmlElement(name = "PersonForename")
    private String personForename;
    @XmlElement(name = "PersonForename2")
    private String personForename2;
    @XmlElement(name = "PersonForename3")
    private String personForename3;

    // [N]  :   String(512) :   Surname of the person involved
    @XmlElement(name = "PersonSurname")
    private String personSurname;

    // [N]  :   String(1)   :   Gender (code) of the person involved    :   M, F, N (not specified), D (described)
    @XmlElement(name = "PersonSex")
    private String personSex;

    // [N]  :   String(512) :   Gender (description) of the person involved
    @XmlElement(name = "PersonSexDescription")
    private String personSexDescription;

    // [N]  :   String(512) :   Address of the person involved  :   If postal address is entered then each line
    // SHOULD end with a comma (except the last line) and commas SHOULD NOT be used within a line
    @XmlElement(name = "PersonAddress")
    private String personAddress;

    // [N]  :   String(512) :   Phone number of the person involved
    @XmlElement(name = "PersonNumber")
    private String personNumber;

    // [N]  :    Date       :   Date of birth of the person involved
    @XmlElement(name = "PersonDateOfBirth")
    @XmlJavaTypeAdapter(value = LocalDateAdapter.class, type = LocalDate.class)
    private LocalDate personDateOfBirth;

    // [N]  :   Integer     :   Age of person if no DoB is proveded :    It may not be pssible to obtain a DoB at the time. Unit is years (duh?)
    @XmlElement(name = "PersonAge")
    private int personAge;

    // [N]  :   String(8)   :   Provides an indication of the consent given by the person to use personal information
    //  :   To meet information sharing protocols - values:
        // IMPLIED (emergency)
        // EXPLICIT (subsequent consent)
        // DENIED (subsequent consent denial)
    @XmlElement(name = "PersonConsent")
    private String personConsent;   // Create an adaptor for the PersonConsent class (and rename it Consent cause ya kno).

    // [N]  :   String(512) :   Involvement of the person
    @XmlElement(name = "PersonInvolvement")
    private String personInvolvement;

    // [N]  :   Boolean     :   To indicate that the person has been arrested/is being arrested
    @XmlElement(name = "PersonArrestInd")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personArrestInd;

    // [N]  :   Boolean     :    To indicate that the person is a casualty in connection with the incident
    @XmlElement(name = "PersonCasualtyInd")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personCasualtyInd;

    // [N]  :   Boolean     :   To indicate that the person related to the incident is conscious
    @XmlElement(name = "PersonConscious")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personConscious;

    // [N]  :   Boolean     :   To indicate that the person related to the incident is breathing
    @XmlElement(name = "PersonBreathing")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personBreathing;

    // [N]  :   Boolean     :   To indicate that the person related to the incident is bleeding severely
    @XmlElement(name = "PersonSeriousBleeding")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personSeriousBleeding;

    // [N]  :   Boolean     :   To indicate that the person is a suspect in connection with the incident
    @XmlElement(name = "PersonSuspectInd")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personSuspectInd;

    // [N]  :   Boolean     :   To indicate that the person is a victim in connection with the incident
    @XmlElement(name = "PersonVictimInd")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personVictimInd;

    // [N]  :   Boolean     :   To indicate that the person is a witness in connection with the incident
    @XmlElement(name = "PersonWitnessInd")
    @XmlJavaTypeAdapter(value = BooleanAdapter.class, type = Boolean.class)
    private Boolean personWitnessInd;

    // [N]  :   String(512) :   Additional comments about the person involved
    @XmlElement(name = "PersonComment")
    private String personComment;

    // [N]  :   Integer     :   A sequential number allocated to each person in an incident
    @XmlElement(name = "PersonSeqNo")
    private int personSeqNo;

    public String getPersonForename() {
        return personForename;
    }

    public void setPersonForename(String personForename) {
        this.personForename = personForename;
    }

    public String getPersonForename2() {
        return personForename2;
    }

    public void setPersonForename2(String personForename2) {
        this.personForename2 = personForename2;
    }

    public String getPersonForename3() {
        return personForename3;
    }

    public void setPersonForename3(String personForename3) {
        this.personForename3 = personForename3;
    }

    public String getPersonSurname() {
        return personSurname;
    }

    public void setPersonSurname(String personSurname) {
        this.personSurname = personSurname;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String getPersonSexDescription() {
        return personSexDescription;
    }

    public void setPersonSexDescription(String personSexDescription) {
        this.personSexDescription = personSexDescription;
    }

    public String getPersonAddress() {
        return personAddress;
    }

    public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }

    public String getPersonNumber() {
        return personNumber;
    }

    public void setPersonNumber(String personNumber) {
        this.personNumber = personNumber;
    }

    public LocalDate getPersonDateOfBirth() {
        return personDateOfBirth;
    }

    public void setPersonDateOfBirth(LocalDate personDateOfBirth) {
        this.personDateOfBirth = personDateOfBirth;
    }

    public int getPersonAge() {
        return personAge;
    }

    public void setPersonAge(int personAge) {
        this.personAge = personAge;
    }

    public String getPersonConsent() {
        return personConsent;
    }

    public void setPersonConsent(String personConsent) {
        this.personConsent = personConsent;
    }

    public String getPersonInvolvement() {
        return personInvolvement;
    }

    public void setPersonInvolvement(String personInvolvement) {
        this.personInvolvement = personInvolvement;
    }

    public Boolean getPersonArrestInd() {
        return personArrestInd;
    }

    public void setPersonArrestInd(Boolean personArrestInd) {
        this.personArrestInd = personArrestInd;
    }

    public Boolean getPersonCasualtyInd() {
        return personCasualtyInd;
    }

    public void setPersonCasualtyInd(Boolean personCasualtyInd) {
        this.personCasualtyInd = personCasualtyInd;
    }

    public Boolean getPersonConscious() {
        return personConscious;
    }

    public void setPersonConscious(Boolean personConscious) {
        this.personConscious = personConscious;
    }

    public Boolean getPersonBreathing() {
        return personBreathing;
    }

    public void setPersonBreathing(Boolean personBreathing) {
        this.personBreathing = personBreathing;
    }

    public Boolean getPersonSeriousBleeding() {
        return personSeriousBleeding;
    }

    public void setPersonSeriousBleeding(Boolean personSeriousBleeding) {
        this.personSeriousBleeding = personSeriousBleeding;
    }

    public Boolean getPersonSuspectInd() {
        return personSuspectInd;
    }

    public void setPersonSuspectInd(Boolean personSuspectInd) {
        this.personSuspectInd = personSuspectInd;
    }

    public Boolean getPersonVictimInd() {
        return personVictimInd;
    }

    public void setPersonVictimInd(Boolean personVictimInd) {
        this.personVictimInd = personVictimInd;
    }

    public Boolean getPersonWitnessInd() {
        return personWitnessInd;
    }

    public void setPersonWitnessInd(Boolean personWitnessInd) {
        this.personWitnessInd = personWitnessInd;
    }

    public String getPersonComment() {
        return personComment;
    }

    public void setPersonComment(String personComment) {
        this.personComment = personComment;
    }

    public int getPersonSeqNo() {
        return personSeqNo;
    }

    public void setPersonSeqNo(int personSeqNo) {
        this.personSeqNo = personSeqNo;
    }

    public String getSummary() {
        return new StringBuilder()
                .append("Person Forename 1: " + personForename + "\n")
                .append("Person Forename 2: " + personForename2 + "\n")
                .append("Person Forename 3: " + personForename3 + "\n")
                .append("Person Surname: " + personSurname + "\n")
                .append("Person Sex: " + personSex + "\n")
                .append("Person Sex Description: " + personSexDescription + "\n")
                .append("Person Address: " + personAddress + "\n")
                .append("Person Number: " + personNumber + "\n")
                .append("Person Date Of Birth: " + personDateOfBirth + "\n")
                .append("Person Age: " + personAge + "\n")
                .append("Person Consent: " + personConsent + "\n")
                .append("Person Involvement: " + personInvolvement + "\n")
                .append("Person Arrest Indicator: " + personArrestInd + "\n")
                .append("Person Casualty Indicator: " + personCasualtyInd + "\n")
                .append("Person Concious: " + personConscious + "\n")
                .append("Person Breathing: " + personBreathing + "\n")
                .append("Person Seriously Bleeding: " + personSeriousBleeding + "\n")
                .append("Person Suspect Indicator: " + personSuspectInd + "\n")
                .append("Person Victim Indicator: " + personVictimInd + "\n")
                .append("Person Witness Indicator: " + personWitnessInd + "\n")
                .append("Person Comment: " + personComment + "\n")
                .append("Person Sequential Number: " + personSeqNo + "\n")
                .toString();
    }
}
