package com.remsdaq.osapplication.entities.mait.entities.persons;

public enum PersonConsent {

    IMPLIED,
    EXPlICIT,
    DENIED
}
