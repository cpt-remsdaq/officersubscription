package com.remsdaq.osapplication.entities.mait.entities.sites;

import com.remsdaq.osapplication.entities.addresses.Address;
import com.remsdaq.osapplication.entities.coordinates.CoordinateSystem;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import javax.persistence.Entity;

// Command, Control, Coordination, and Communication
// This is the general version of the resource cite
@Entity
@XmlRootElement(name = "C4Site")
@XmlAccessorType(XmlAccessType.FIELD)
public class C4Site implements Serializable{

    // [N]  :   String(24)  :   The type of site being specified
    @XmlElement(name = "C4SType")
    private String c4SType;

    // [N]  :   Integer     :   Reference type of gazetteer
    @XmlElement(name = "C4SGazType")
    private String c4SGazType;

    // [N]  :   String(512) :   The reference in the Gazetteer
    @XmlElement(name = "C4SGazRef")
    private String c4SGazRef;

    // [N]  :   String(512) :   Textual description of C4 site
    @XmlElement(name = "C4SAddress")
    private String c4SAddress;

    // [N]  :   String(24)  :   How to interpret the supplied X/Y
    @XmlElement(name = "C4SCoordinateSystem")
    private String c4SCoordinateSystem;

    // [N]  :   String(512) :   The X coordinate in the defined coordinate scheme
    @XmlElement(name = "C4SX")
    private double c4SX;

    // [N]  :   String(512) :   The Y coordinate in the defined coordinate scheme
    @XmlElement(name = "C4SY")
    private double c4SY;

    // [N]  :   Integer     :   A sequential number allocated to each C4Site in an incident :    This allows multiple RC4S instances. This is only unique when pertaining to the originOrganisation
    @XmlElement(name = "C4SSeqNo")
    private String c4SSeqNo;

    public String getC4SType() {
        return c4SType;
    }

    public void setC4SType(String c4SType) {
        this.c4SType = c4SType;
    }

    public String getC4SGazType() {
        return c4SGazType;
    }

    public void setC4SGazType(String c4SGazType) {
        this.c4SGazType = c4SGazType;
    }

    public String getC4SGazRef() {
        return c4SGazRef;
    }

    public void setC4SGazRef(String c4SGazRef) {
        this.c4SGazRef = c4SGazRef;
    }

    public String getC4SAddress() {
        return c4SAddress;
    }

    public void setC4SAddress(String c4SAddress) {
        this.c4SAddress = c4SAddress;
    }

    public String getC4SCoordinateSystem() {
        return c4SCoordinateSystem;
    }

    public void setC4SCoordinateSystem(String c4SCoordinateSystem) {
        this.c4SCoordinateSystem = c4SCoordinateSystem;
    }

    public double getC4SX() {
        return c4SX;
    }

    public void setC4SX(double c4SX) {
        this.c4SX = c4SX;
    }

    public double getC4SY() {
        return c4SY;
    }

    public void setC4SY(double c4SY) {
        this.c4SY = c4SY;
    }

    public String getC4SSeqNo() {
        return c4SSeqNo;
    }

    public void setC4SSeqNo(String c4SSeqNo) {
        this.c4SSeqNo = c4SSeqNo;
    }

    public String getSummary() {
        return new StringBuilder()
                .append("C4 Site Type: " + c4SType + "\n")
                .append("C4 Site Gazetteer Type: " + c4SGazType + "\n")
                .append("C4 Site Gazetteer Reference: " + c4SGazRef + "\n")
                .append("C4 Site Address: " + c4SAddress + "\n")
                .append("C4 Site Coordinate System: " + c4SCoordinateSystem + "\n")
                .append("C4 Site X: " + c4SX + "\n")
                .append("C4 Site Y: " + c4SY + "\n")
                .append("C4 Site Sequential Number: " + c4SSeqNo + "\n")
                .toString();
    }

}
