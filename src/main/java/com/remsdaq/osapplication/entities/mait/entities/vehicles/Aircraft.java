package com.remsdaq.osapplication.entities.mait.entities.vehicles;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@XmlRootElement(name = "Aircraft")
@XmlAccessorType(XmlAccessType.FIELD)
public class Aircraft extends VehicleInvolved implements Serializable {

    private String description;

    Aircraft() {
        this.setType(Type.AIRCRAFT);
    }

}
