package com.remsdaq.osapplication.entities.mait.entities.vehicles;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@XmlRootElement(name = "Train")
@XmlAccessorType(XmlAccessType.FIELD)
public class Train extends VehicleInvolved implements Serializable {

    Train() {
        this.setType(Type.TRAIN);
    }



}
