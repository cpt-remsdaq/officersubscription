package com.remsdaq.osapplication.entities.mait.entities.vehicles;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@XmlRootElement(name = "Vehicle")
@XmlAccessorType(XmlAccessType.FIELD)
public class Vehicle extends VehicleInvolved implements Serializable{

    Vehicle() {
        this.setType(Type.VEHICLE);
    }

    // [N]  :   String(11)  :   Vehicle Registration Mark
    @XmlElement(name = "VRM")
    private String VRM;

    // [N]  :   String(512) :   Vehicle Identification Number
    @XmlElement(name = "VIN")
    private String VIN;

    // [N]  :   String(512) :   Make of the vehicle
    @XmlElement(name = "VehicleMake")
    private String vehicleMake;

    // [N]  :   String(512) :   Model of the vehicle
    @XmlElement(name = "VehicleModel")
    private String vehicleModel;

    // [N]  :   String(512) :   To record, in investigative and intelligence systems, incomplete information obtained from any source about a motor vehicle model
    @XmlElement(name = "VehicleVariant")
    private String vehicleVariant;

    // [N]  :   String(512) :   Colour of the vehicle
    @XmlElement(name = "VehicleColour")
    private String vehicleColour;

    // [N]  :   String(512) :   Involvement of the vehicle
    @XmlElement(name = "VehicleInvolvement")
    private String vehicleInvolvement;

    // [N]  :   String(512) :   Additional comments about the vehicle involved
    @XmlElement(name = "VehicleComment")
    private String vehicleComment;

    // [N]  :   Integer     :   A sequential number allocated to each vehicle in an incident
    @XmlElement(name = "VehicleSeqNo")
    private String vehicleSeqNo;

    public String getVRM() {
        return VRM;
    }

    public void setVRM(String VRM) {
        this.VRM = VRM;
    }

    public String getVIN() {
        return VIN;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleVariant() {
        return vehicleVariant;
    }

    public void setVehicleVariant(String vehicleVariant) {
        this.vehicleVariant = vehicleVariant;
    }

    public String getVehicleColour() {
        return vehicleColour;
    }

    public void setVehicleColour(String vehicleColour) {
        this.vehicleColour = vehicleColour;
    }

    public String getVehicleInvolvement() {
        return vehicleInvolvement;
    }

    public void setVehicleInvolvement(String vehicleInvolvement) {
        this.vehicleInvolvement = vehicleInvolvement;
    }

    public String getVehicleComment() {
        return vehicleComment;
    }

    public void setVehicleComment(String vehicleComment) {
        this.vehicleComment = vehicleComment;
    }

    public String getVehicleSeqNo() {
        return vehicleSeqNo;
    }

    public void setVehicleSeqNo(String vehicleSeqNo) {
        this.vehicleSeqNo = vehicleSeqNo;
    }

    @Override
    public void setDescription(String description) {
        throw new RuntimeException("Cannot set a value on a complex type!");
    }

    @Override
    public String getSummary() {
        return new StringBuilder()
                .append("Vehicle Type: " + this.getType() + "\n")
                .append("Vehicle Registration Mark: " + this.VRM + "\n")
                .append("Vehicle Identification Number: " + this.VIN + "\n")
                .append("Vechicle Make: " + this.vehicleMake + "\n")
                .append("Vehicle Model: " + this.vehicleModel + "\n")
                .append("Vehicle Variant: " + this.vehicleVariant + "\n")
                .append("Vehicle Colour: " + this.vehicleColour + "\n")
                .append("Vehicle Involvement: " + this.vehicleInvolvement + "\n")
                .append("Vehicle Comment: " + this.vehicleComment + "\n")
                .append("Vehicle Sequential Number: " + this.vehicleSeqNo + "\n")
                .toString();
    }


}
