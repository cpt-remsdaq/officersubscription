package com.remsdaq.osapplication.entities.mait.entities.vehicles;

import javax.persistence.Entity;
import javax.xml.bind.annotation.*;
import java.io.Serializable;

@Entity
@XmlRootElement(name = "VehicleInvolved")
@XmlSeeAlso({Vehicle.class, Aircraft.class, Train.class, Vessel.class})
@XmlAccessorType(XmlAccessType.FIELD)
public class VehicleInvolved implements Serializable {

    @XmlValue
    private String description;

    @XmlTransient
    private Type type = Type.UNKNOWN;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    public enum Type {
        VEHICLE,
        VESSEL,
        AIRCRAFT,
        TRAIN,
        UNKNOWN
    }

    public String getSummary() {
        return new StringBuilder()
                .append("Vehicle Type: " + this.getType() + "\n")
                .append("Description: " + this.getDescription() + "\n")
                .toString();
    }

}
