package com.remsdaq.osapplication.entities.mait.entities.vehicles;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@Entity
@XmlRootElement(name = "Vessel")
@XmlAccessorType(XmlAccessType.FIELD)
public class Vessel extends VehicleInvolved implements Serializable{

    Vessel() {
        this.setType(Type.VESSEL);
    }

    // [N]  :   String(512) :   The human readable name
    @XmlElement(name = "VesselName")
    private String vesselName;

    // [N] :    String(512) :   Mobile Maritime Service Identity    :    This is a unique 9 digit number given to vessels
    // but can change if a vessel owner flags the vessel to a different country. This can apply to both commercial and
    // leisure vessels as it is normally associated with communications equipment
    @XmlElement(name = "VesselMMSI")
    private String vesselMMSI;

    // [N]  :   String(512) :   International Maritime Organisation Hull Number :   For commercial vessels this is a unique
    // number given on construction and remains the same for the duration that the vessel exists regardless of ownership etc.
    @XmlElement(name = "VesselIMO")
    private String vesselIMO;

    // [N]  :   Integer     :   A sequential number allocated to each vessel in an incident
    @XmlElement(name = "VesselSeqNo")
    private int vesselSeqNo;

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getVesselMMSI() {
        return vesselMMSI;
    }

    public void setVesselMMSI(String vesselMMSI) {
        this.vesselMMSI = vesselMMSI;
    }

    public String getVesselIMO() {
        return vesselIMO;
    }

    public void setVesselIMO(String vesselIMO) {
        this.vesselIMO = vesselIMO;
    }

    public int getVesselSeqNo() {
        return vesselSeqNo;
    }

    public void setVesselSeqNo(int vesselSeqNo) {
        this.vesselSeqNo = vesselSeqNo;
    }

    @Override
    public void setDescription(String description) {
        throw new RuntimeException("Cannot set a value on a complex type!");
    }

    @Override
    public String getSummary() {
        return new StringBuilder()
                .append("Vehicle Type: " + this.getType() + "\n")
                .append("Vessel Name: " + this.vesselName + "\n")
                .append("Vessel Maritime Mobile Service Identity: " + this.vesselMMSI + "\n")
                .append("Vessel International Maritime Organization Number: " + this.vesselIMO + "\n")
                .append("Vessel Sequential Number: " + this.vesselSeqNo + "\n")
                .toString();
    }
}
