@XmlSchema(
        namespace = "http://www.mait.org/mait/1dc7",
        elementFormDefault = XmlNsForm.QUALIFIED,
        attributeFormDefault = XmlNsForm.QUALIFIED,
        xmlns = {
                @XmlNs(prefix = "mait", namespaceURI = "http://www.mait.org/mait/1dc7")
        })
package com.remsdaq.osapplication.entities.mait.entities.vehicles;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;