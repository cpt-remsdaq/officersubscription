package com.remsdaq.osapplication.entities.messages;

import com.remsdaq.osapplication.entities.incidents.Incident;
import com.remsdaq.osapplication.entities.incidents.IncidentType;
import com.remsdaq.osapplication.entities.resources.Person;
import com.remsdaq.osapplication.entities.resources.Resource;
import com.remsdaq.osapplication.entities.resources.Vehicle;

import java.util.Collections;

public class IncidentMessage implements Message {
	
	private Incident incident;
	
	public IncidentMessage(Incident incident) {
		this.incident = incident;
	}
	
	@Override
	public String buildMessage() {
		StringBuilder builder = new StringBuilder()
				.append(buildIncidentNoString());
		
		if (incident.getTypes() != null) {
			for (IncidentType type : incident.getTypes()) {
				builder.append(buildIncidentTypeString(type));
			}
		}
		
		builder.append(buildPostcodeString())
		       .append(buildMapRefString());
		
		if (incident.getResources() != null) {
			for (Resource res : incident.getResources()) {
				builder.append(buildResourceString(res));
			}
		}
		
		return builder.toString();
	}
	
	@Override
	public String buildMessageVerbose() {
		StringBuilder builder = new StringBuilder()
				.append(buildIncidentNoString());
		
		if (incident.getTypes() != null) {
			for (IncidentType type : incident.getTypes()) {
				builder.append(buildIncidentTypeString(type));
			}
		}
		
		builder.append(buildPostcodeString())
		       .append(buildMapRefString());
		
		if (incident.getResources() != null) {
			for (Resource res : incident.getResources()) {
				builder.append(buildResourceString(res));
			}
		}
		
		Person commandPerson = null;
		Vehicle commandVehicle = null;
		for (Resource res : incident.getResources()) {
			if (res.getId().equals(incident.getCommandPerson())) {
				commandPerson = (Person) res;
			} else if (res.getId().equals(incident.getCommandVehicle())); {
				commandVehicle = (Vehicle) res;
			}
		}
		
		builder.append(buildIncidentStatusString())
		       .append(buildPriorityString())
		       .append(buildNoOfCallsString())
		       .append(buildAddressString())
		       .append(buildCommandPersonString(commandPerson))
		       .append(buildCommandVehicleString(commandVehicle));
		
		builder.append(buildDescriptionString());
		return builder.toString();
	}
	
	private String buildIncidentTypeString(IncidentType type) {
		return getNonNullMessageString("Incident Type", type, 6);
	}
	
	private String buildMapRefString() {
//		LatLng latLng = new LatLng(incident.getLon(),incident.getLat());
//		EastingNorthing eastNorth = latLng.toEastingNorthing();
//		return getNonNullMessageString("Map Ref", eastNorth.toString(), 12);

        return null;
	}
	
	private String buildPostcodeString() {
//		if (incident.getTempTransientAddress() != null) {
//			return getNonNullMessageString("Postcode", incident.getTempTransientAddress().getPostcode(), 11);
//		}
		return "";
	}
	
	private String buildDescriptionString() {
		return getNonNullMessageString("Description", incident.getDescription(), 8);
	}
	
	private String buildResourceString(Resource res) {
		return getNonNullMessageString("Resource", res.getCallSign(), 11);
	}
	
	private String buildCommandPersonString(Person commandPerson) {
//		return getNonNullMessageString("Command Person", commandPerson.getCallSign() + ", " + commandPerson.getName(), 5);

        return null;
	}
	
	private String buildCommandVehicleString(Vehicle commandVehicle) {
		return getNonNullMessageString("Command Vehicle", commandVehicle.getCallSign(), 4);
	}
	
	private String buildAddressString() {
		return getNonNullMessageString("Address", incident.getAddressString().replace("\n", ", "), 12);
	}
	
	private String buildNoOfCallsString() {
		return getNonNullMessageString("No Of Calls", incident.getNumberOfCalls(), 8);
	}
	
	private String buildPriorityString() {
		if (incident.getPriority() != null) {
//			return getNonNullMessageString("Incident Priority", incident.getPriority().getId(), 2);
		}
		return "";
	}
	
	private String buildIncidentStatusString() {
		return getNonNullMessageString("Incident Status", incident.getStatus(), 4);
	}
	
	private String buildIncidentNoString() {
		return getNonNullMessageString("Incident No", incident.getId(), 8);
	}
	
	private String getNonNullMessageString(String header, Object value, int spaces) {
		return (value != null) ? header + ":" + stringOfSpaces(spaces) + value + "\n" : "";
	}
	
	private String stringOfSpaces(int spaces) {
		return String.join("", Collections.nCopies(spaces, " "));
		
	}
}
