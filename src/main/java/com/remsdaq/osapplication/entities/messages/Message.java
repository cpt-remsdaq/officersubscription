package com.remsdaq.osapplication.entities.messages;

public interface Message {
	
	public String buildMessage();
	public String buildMessageVerbose();
	
}
