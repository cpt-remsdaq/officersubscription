package com.remsdaq.osapplication.entities.messages.incidentSubscription;

//import com.remsdaq.resqueentities.entities.incident.Incident;

import com.remsdaq.osapplication.entities.incidents.Incident;
import com.remsdaq.osapplication.entities.messages.IncidentMessage;
import com.remsdaq.osapplication.entities.messages.Message;

public class BeginIncidentSubscriptionMessage implements Message {

	private Incident incident;
	
	public BeginIncidentSubscriptionMessage(Incident incident) {
		this.incident = incident;
	}
	
	
	@Override
	public String buildMessage() {
		return new StringBuilder()
				.append("You are now receiving incident updates for...\n\n")
				.append(new IncidentMessage(incident).buildMessage())
				.toString();
	}
	
	@Override
	public String buildMessageVerbose() {
		return new StringBuilder()
				.append("You are now receiving incident updates for...\n\n")
				.append(new IncidentMessage(incident).buildMessageVerbose())
				.toString();
	}
}
