package com.remsdaq.osapplication.entities.messages.polygonSubscription;

import com.remsdaq.osapplication.entities.messages.Message;
import com.remsdaq.osapplication.entities.polygons.Polygon;

public class BeginPolygonSubscriptionMessage implements Message {

	private Polygon polygon;
	
	public BeginPolygonSubscriptionMessage(Polygon polygon) {
		this.polygon = polygon;
	}
	
	@Override
	public String buildMessage() {
		return String.format("You are now receiving updates from polygon area '%s'",polygon.getName());
	}
	
	@Override
	public String buildMessageVerbose() {
		return buildMessage();
	}
	
}
