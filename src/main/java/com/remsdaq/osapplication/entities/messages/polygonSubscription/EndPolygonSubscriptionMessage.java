package com.remsdaq.osapplication.entities.messages.polygonSubscription;

import com.remsdaq.osapplication.entities.messages.Message;
import com.remsdaq.osapplication.entities.polygons.Polygon;

public class EndPolygonSubscriptionMessage implements Message {
	
	private Polygon polygon;
	
	public EndPolygonSubscriptionMessage(Polygon polygon) {
		this.polygon = polygon;
	}
	
	@Override
	public String buildMessage() {
		return String.format("You are NO LONGER receiving updates from polygon area '%s'",polygon.getName());
	}
	
	@Override
	public String buildMessageVerbose() {
		return buildMessage();
	}
	
}
