package com.remsdaq.osapplication.entities.polygonTags;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.database.xml.XmlDataCollectionEntity;
import com.remsdaq.osapplication.database.xml.entities.polygonTags.PolygonTagCollection;
import com.remsdaq.osapplication.database.xml.entities.polygons.PolygonXmlAdapter;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.EntityIdComparator;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.Set;

@Table(name = "polygon_tags")
@Entity
// JAXB
@XmlRootElement(name = "PolygonTag")
@XmlAccessorType(XmlAccessType.FIELD)
public class PolygonTag implements Serializable, XmlDataEntity {
	
	@XmlTransient private String name = "";
	@XmlTransient private Set<Polygon> polygons;
	
	public PolygonTag() {
	}
	
	public PolygonTag(String name) {
		this.name = name.toUpperCase();
	}
	
	@Id
	@Column(name = "polygon_tag_name")
	@XmlAttribute(name = "Name")
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name.toUpperCase();
	}
	
	@ManyToMany(cascade = {CascadeType.PERSIST,
	                       CascadeType.MERGE}, targetEntity = Polygon.class, fetch = FetchType.EAGER)
	@JoinTable(name = "polygon_tag_polygon", joinColumns = @JoinColumn(name = "polygon_tag_name"),
			inverseJoinColumns = @JoinColumn(name = "polygon_ID"))
	@XmlJavaTypeAdapter(PolygonXmlAdapter.class)
	@XmlElementWrapper(name = "Polygons")
	@XmlElement(name = "Polygon")
	public Set<Polygon> getPolygons() {
		return this.polygons;
	}
	
	public void setPolygons(Set<Polygon> polygons) {
		this.polygons = polygons;
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataPath() {
		return GlobalContext.getInstance().getConfig().getProperty("polygon_tag_xml_data_path");
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataCollectionPath() {
		return GlobalContext.getInstance().getConfig().getProperty("polygon_tag_collection_xml_data_path");
	}
	
	@Override
	public XmlDataCollectionEntity makeCollection() {
		PolygonTagCollection collectionEntity = new PolygonTagCollection();
		collectionEntity.addToCollection(this);
		return collectionEntity;
	}
	
	@Override
	//	@Id
	//	@Column(name = "ID")
	//	@XmlAttribute(name = "ID")
	@Transient
	public String getID() {
		return this.name;
	}
	
	public void setID(String ID) {
		this.name = ID;
	}
	
	@Override
	@Transient
	@XmlTransient
	public EntityType getEntityType() {
		return EntityType.POLYGON_TAG;
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder(2315, 549)
				.append(name)
				.toHashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof Subscriber) {
			Polygon comparison = (Polygon) o;
			EntityIdComparator comparator = new EntityIdComparator();
			return this.name.equals(comparison.getName());
		} else if (o instanceof String) {
			String name = (String) o;
			return this.name.equals(name);
		}
		return false;
	}
	
}
