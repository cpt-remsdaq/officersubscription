/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.entities.polygons;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author cpt
 */
public class ImportPolygonGeometry {
    
    private File file;
    private String line;
    
    public ImportPolygonGeometry(File file) throws IOException {
        this.file = file;
        importPolygon();
    }
    
    private void importPolygon() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            line = br.readLine();
        }
    }
    
    public String getGeometry() {
        return line;
    }
    
}
