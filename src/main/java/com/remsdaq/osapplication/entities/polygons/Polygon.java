package com.remsdaq.osapplication.entities.polygons;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.database.xml.XmlDataCollectionEntity;
import com.remsdaq.osapplication.database.xml.entities.polygonTags.PolygonTagXmlAdapter;
import com.remsdaq.osapplication.database.xml.entities.polygons.PolygonCollection;
import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberXmlAdapter;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.subscribers.EntityIdComparator;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.utilities.WKTUtilities;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;

// Hibernate
@Table(name = "polygons")
@Entity
// JAXB
@XmlRootElement(name = "Polygon")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"ID", "name", "description", "WKTGeometry", "subscribers", "polygonTags"})
public class Polygon implements Serializable, XmlDataEntity {
	
	@XmlTransient private String ID;
	@XmlTransient private String name;
	@XmlTransient private Set<Subscriber> subscribers;
	@XmlTransient private String description;
	@XmlTransient private PolygonGeometry geometry;
	@XmlTransient private Set<PolygonTag> polygonTags;
	
	public void Polygon() {
		this.ID = "";
		this.name = "";
		this.description = "";
		this.subscribers = new HashSet();
		this.polygonTags = new HashSet();
		this.geometry = new PolygonGeometry();
		this.polygonTags.add(new PolygonTag("test"));
	}
	
	@Override
	@Id
	@Column(name = "ID")
	@XmlAttribute(name = "ID")
	public String getID() {
		return this.ID;
	}
	
	public void setID(String ID) {
		this.ID = ID;
	}
	
	@Column(name = "name")
	@XmlElement(name = "Name")
	public String getName() {
		return this.name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "description", columnDefinition = "TEXT")
	@XmlElement(name = "Description")
	public String getDescription() {
		return this.description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	//	@XmlTransient
	@Column(name = "wktgeometry", columnDefinition = "TEXT")
	@XmlElement(name = "WellKnownText")
	public String getWKTGeometry() {
		if (this.geometry != null)
			return WKTUtilities.encode(geometry);
		else
			return null;
	}
	
	public void setWKTGeometry(String WKTGeometry) {
		try {
			this.geometry = WKTUtilities.decode(WKTGeometry);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@ManyToMany(
			cascade = {CascadeType.PERSIST, CascadeType.MERGE},
			targetEntity = Subscriber.class,
			fetch = FetchType.EAGER)
	@JoinTable(
			name = "subscriber_polygon",
			joinColumns = @JoinColumn(name = "polygon_ID"),
			inverseJoinColumns = @JoinColumn(name = "subscriber_ID"))
	@XmlJavaTypeAdapter(SubscriberXmlAdapter.class)
	@XmlElementWrapper(name = "Subscribers")
	@XmlElement(name = "Subscriber")
	public Set<Subscriber> getSubscribers() {
		return this.subscribers;
	}
	
	public void setSubscribers(Set<Subscriber> subscribers) {
		this.subscribers = subscribers;
	}
	
	@ManyToMany(
			cascade = {CascadeType.PERSIST, CascadeType.MERGE},
			targetEntity = PolygonTag.class,
			fetch = FetchType.EAGER)
	@JoinTable(
			name = "polygon_tag_polygon",
			joinColumns = @JoinColumn(name = "polygon_ID"),
			inverseJoinColumns = @JoinColumn(name = "polygon_tag_name"))
	@XmlJavaTypeAdapter(PolygonTagXmlAdapter.class)
	@XmlElementWrapper(name = "Tags")
	@XmlElement(name = "Tag")
	public Set<PolygonTag> getPolygonTags() {
		return this.polygonTags;
	}
	
	public void setPolygonTags(Set<PolygonTag> polygonTags) {
		this.polygonTags = polygonTags;
	}
	
	@Transient
	@XmlTransient
	public PolygonGeometry getGeometry() {
		return this.geometry;
	}
	
	public void setGeometry(PolygonGeometry geometry) {
		this.geometry = geometry;
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataPath() {
		return GlobalContext.getInstance().getConfig().getProperty("polygon_xml_data_path");
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataCollectionPath() {
		return GlobalContext.getInstance().getConfig().getProperty("polygon_collection_xml_data_path");
	}
	
	@Override
	public XmlDataCollectionEntity makeCollection() {
		PolygonCollection collectionEntity = new PolygonCollection();
		collectionEntity.addToCollection(this);
		return collectionEntity;
	}
	
	public double calculateArea() {
		return this.geometry.geometryArea();
	}
	
	@Override
	@Transient
	@XmlTransient
	public EntityType getEntityType() {
		return EntityType.POLYGON;
	}
	
	@Transient
	@XmlTransient
	public String getSubscriberString() {
		if (this.getSubscribers() != null) {
			StringJoiner joiner = new StringJoiner(", ");
			this.getSubscribers().forEach(sub -> {
				if (sub != null) {
					joiner.add(sub.getID());
				}
			});
			return joiner.toString();
		}
		return null;
	}
	
	@Transient
	@XmlTransient
	public String[] getSubscriberStringArray() {
		if (this.getSubscribers() != null) {
			Object[] subArray = subscribers.toArray();
			String[] subIdArray = new String[this.getSubscribers().size()];
			int i = 0;
			for (Object obj : subArray) {
				Subscriber sub = (Subscriber) obj;
				subIdArray[i] = sub.getID();
				i++;
			}
			return subIdArray;
		}
		return null;
	}
	
	@Transient
	@XmlTransient
	public String getPolygonTagString() {
		if (this.getPolygonTags() != null) {
			StringJoiner joiner = new StringJoiner(", ");
			this.getPolygonTags().forEach(tag -> {
				if (tag != null) {
					joiner.add(tag.getID());
				}
			});
			return joiner.toString();
		}
		return null;
	}
	
	@Override
	public final boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof Subscriber) {
			Polygon comparison = (Polygon) o;
			return areAllFieldsEqual(comparison);
		} else if (o instanceof String) {
			String ID = (String) o;
			return this.ID.equals(ID);
		}
		return false;
	}
	
	// WARNING: currently does not take into account subscribers and tags.
	// This will make any hashtable operations regarding Polygon slower.
	@Override
	public final int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder(113, 47)
				.append(ID)
				.append(name)
				.append(description)
				.append(getWKTGeometry());
		return builder.toHashCode();
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("ID: " + this.ID + "\n")
				.append("Name: " + this.name + "\n")
				.append("Description: " + this.description + "\n")
				.append("WKT: " + this.getWKTGeometry() + "\n")
				.append("Subscribers: " + this.getSubscriberString() + "\n")
				.append("Tags: " + this.getPolygonTagString() + "\n")
				.toString();
	}
	
	private final boolean areAllFieldsEqual(Polygon comparison) {
		EntityIdComparator comparator = new EntityIdComparator();
		return this.ID.equals(comparison.getID())
		       && this.name.equals(comparison.getName())
		       && this.description.equals(comparison.getDescription())
		       && this.getWKTGeometry().equals(comparison.getWKTGeometry())
		       && comparator.areEntitySetsEqual(this.subscribers, comparison.getSubscribers())
		       && comparator.areEntitySetsEqual(this.polygonTags, comparison.getPolygonTags());
	}
}