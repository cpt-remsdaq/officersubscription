package com.remsdaq.osapplication.entities.polygons;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import java.util.Set;

public class PolygonBuilder {
	
	private Polygon polygon;
	
	public PolygonBuilder(String id) {
		polygon = new Polygon();
		polygon.setID(id);
	}
	
	public PolygonBuilder name(String name) {
		polygon.setName(name);
		return this;
	}
	
	public PolygonBuilder description(String description) {
		polygon.setDescription(description);
		return this;
	}
	
	public PolygonBuilder wkt(String wkt) {
		polygon.setWKTGeometry(wkt);
		return this;
	}
	
	public PolygonBuilder polygons(Set<Subscriber> subscribers) {
		polygon.setSubscribers(subscribers);
		return this;
	}
	
	public PolygonBuilder tags(Set<PolygonTag> tags) {
		polygon.setPolygonTags(tags);
		return this;
	}
	
	public Polygon build() {
		return polygon;
	}
	
}
