package com.remsdaq.osapplication.entities.polygons;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import java.util.Set;
import java.util.function.Predicate;

public class PolygonComparitor {
	
	private Set<Subscriber> subscribers;
	private Set<Subscriber> comparisonSubscribers;
	
	private Set<PolygonTag> polygonTags;
	private Set<PolygonTag> comparisonTags;
	
	Predicate<Subscriber> subscriberIdMatcher;
	Predicate<PolygonTag> tagIdMatcher;
	
	public boolean areSubscriberSetsEqual(Set<Subscriber> subscribers, Set<Subscriber> comparisonSubscribers) {
		this.subscribers = subscribers;
		this.comparisonSubscribers = comparisonSubscribers;
		return areAllSubscribersInComparison() && areAllComparisonInSubscribers();
	}
	
	public boolean arePolygonTagSetsEqual(Set<PolygonTag> tags, Set<PolygonTag> comparisonTags) {
		this.polygonTags = tags;
		this.comparisonTags = comparisonTags;
		return areAllTagsInComparison() && areAllComparisonInTags();
	}
	
	private Boolean areAllSubscribersInComparison() {
		subscriberIdMatcher = isSubscriberInSet(comparisonSubscribers);
		if (this.subscribers.size() == comparisonSubscribers.size()) {
			return subscribers.stream().allMatch(subscriberIdMatcher);
		}
		return false;
	}
	
	private Boolean areAllTagsInComparison() {
		tagIdMatcher = isTagInSet(comparisonTags);
		if (this.polygonTags.size() == comparisonTags.size()) {
			return polygonTags.stream().allMatch(tagIdMatcher);
		}
		return false;
	}
	
	private Boolean areAllComparisonInSubscribers() {
		Predicate<Subscriber> matchPolygonID = isSubscriberInSet(this.subscribers);
		if (this.subscribers.size() == comparisonSubscribers.size()) {
			return comparisonSubscribers.stream().allMatch(matchPolygonID);
		}
		return false;
	}
	
	private Boolean areAllComparisonInTags() {
		Predicate<PolygonTag> matchTagID = isTagInSet(this.polygonTags);
		if (this.polygonTags.size() == comparisonTags.size()) {
			return comparisonTags.stream().allMatch(matchTagID);
		}
		return false;
	}
	
	private Predicate<Subscriber> isSubscriberInSet(Set<Subscriber> comparisonSubscribers) {
		return sub -> {
			for (Subscriber comparisonSubscriber : comparisonSubscribers) {
				if (sub.getID().equals(comparisonSubscriber.getID())) {
					return true;
				}
			}
			return false;
		};
	}
	
	private Predicate<PolygonTag> isTagInSet(Set<PolygonTag> comparisonTags) {
		return tag -> {
			for (PolygonTag comparisonTag : comparisonTags) {
				if (tag.getID().equals(comparisonTag.getID())) {
					return true;
				}
			}
			return false;
		};
	}
	
}
