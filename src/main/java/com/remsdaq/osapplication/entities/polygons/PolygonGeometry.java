/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.entities.polygons;

import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.utilities.gui.MapUtilities;
import com.remsdaq.osapplication.utilities.UnitsUtilities;

import java.util.ArrayList;


/**
 *
 * @author cpt
 */
public class PolygonGeometry{

    Config config = new Config();
    String defaultUnit = config.getProperty("area_units");
    
    ArrayList<ArrayList<SimplePolygon>> geometry;

    public PolygonGeometry() {
        this.geometry = new ArrayList<>();
    }

    public PolygonGeometry(ArrayList<ArrayList<SimplePolygon>> geometry) {
        this.geometry = geometry;
    }
    
    // Geometry
    public ArrayList<ArrayList<SimplePolygon>> getGeometry() {
        return this.geometry;
    }
    
    public void setGeometry(ArrayList<ArrayList<SimplePolygon>> geometry) {
        this.geometry = geometry;
    }
    
    public int totalPolygonCount() {
        int count = 0;
        for (ArrayList<SimplePolygon> complexPolygon : geometry) {
            for (SimplePolygon simplePolygon : complexPolygon) {
                count++;
            }
        }
        return count;
    }
    
    public int totalVertexCount() {
        int count = 0;
        for (ArrayList<SimplePolygon> complexPolygon : geometry) {
            for (SimplePolygon simplePolygon : complexPolygon) {
                count += simplePolygon.vertexNumber();
            }
        }
        return count;
    }
    
    public void clearGeometry() {
        this.geometry.clear();
    }
    
    // Complex Polygons
    public ArrayList<SimplePolygon> getComplexPolygon(int complexPolygonNum) {
        return this.geometry.get(complexPolygonNum);
    }
    
    public void setComplexPolygon(int complexPolygonNum, ArrayList<SimplePolygon> complexPolygon) {
        this.geometry.set(complexPolygonNum, complexPolygon);
    }
    
    public void addComplexPolygon(ArrayList<SimplePolygon> complexPolygon) {
        this.geometry.add(complexPolygon);
    }
    
    public void removeComplexPolygon(int complexPolygon) {
        this.geometry.remove(complexPolygon);
    }
    
    public void removeLastComplexPolygon() {
        this.geometry.remove(geometry.size()-1);
    }
    
    public void removeFirstComplexPolygon() {
        this.geometry.remove(0);
    }
    
    public int complexPolygonCount() {
        return this.geometry.size();
    }
    
    public int complexPolygonVertexNumber(int complexPolygon) {
        int count = 0;
        for (SimplePolygon simplePolygon : this.geometry.get(complexPolygon)) {
            count += simplePolygon.vertexNumber();
        }
        return count;
    }
    
    // Simple Polygons
    public SimplePolygon getSimplePolygon(int complexPolygonNum, int simplePolygonNum) {
        return this.geometry.get(complexPolygonNum).get(simplePolygonNum);
    }
    
    public void setSimplePolygon(int complexPolygonNum, int simplePolygonNum, SimplePolygon simplePolygon) {
        this.geometry.get(complexPolygonNum).set(simplePolygonNum, simplePolygon);
    }
    
    public void addSimplePolygon (int complexPolygonNum, SimplePolygon simplePolygon) {
        this.geometry.get(complexPolygonNum).add(simplePolygon);
    }
    
    public void addSimplePolygon(SimplePolygon simplePolygon) {
        if (this.complexPolygonCount() > 0) {
            this.geometry.get(0).add(simplePolygon);
        } else {
            this.geometry.add(new ArrayList<>());
            this.geometry.get(0).add(simplePolygon);
        }
    }
    
    public void removeSimplePolygon(int complexPolygonNum, int simplePolygonNum) {
        this.geometry.get(complexPolygonNum).remove(simplePolygonNum);
    }
    
    public void removeLastSimplePolygon(int complexPolygonNum) {
        this.geometry.get(complexPolygonNum).remove(simplePolygonCount(complexPolygonNum));
    }
    
    public void removeFirstSimplePolygon(int complexPolygonNum) {
        this.geometry.get(complexPolygonNum).remove(0);
    }
    
    public int simplePolygonCount(int complexPolygonNum) {
        return this.geometry.get(complexPolygonNum).size();
    }
    
    public int simplePolygonVertexCount(int complexPolygon, int simplePolygon) {
        return this.geometry.get(complexPolygon).get(simplePolygon).vertexNumber();
    }
    
    // Vertices
    
    public LatLng getVertex(int complexPolygon, int simplePolygon, int vertex) {
        return this.geometry.get(complexPolygon).get(simplePolygon).getLatLng(vertex);
    }
    
    public LatLng getFirstVertex(int complexPolygon, int simplePolygon) {
        return this.geometry.get(complexPolygon).get(simplePolygon).getLatLng(0);
    }
    
    public LatLng getLastVertex(int complexPolygon, int simplePolygon) {
        return this.getVertex(complexPolygon,simplePolygon,simplePolygonVertexCount(complexPolygon,simplePolygon)-1);
    }

    public LatLng removeLastVertex(int complexPolygon, int simplePolygon) {
        LatLng latLng = getLastVertex(complexPolygon,simplePolygon);

        this.geometry.get(complexPolygon).get(simplePolygon).removeVertex(simplePolygonVertexCount(complexPolygon,simplePolygon) -1 );

        return latLng;
    }
    
    // Area
    // NB: this becomes less accurate as the area increases
    
    public double geometryArea() {
        
        return geometryArea(defaultUnit);
    }
    
    public double geometryArea(String unit) {
        double area = 0;
        
        for (int i = 0; i < this.complexPolygonCount(); i++) {
            area += complexPolygonArea(i, unit);
        }
        
        return area;
    }
    
    public double complexPolygonArea(int complexPolygonNum) {
        return complexPolygonArea(complexPolygonNum, defaultUnit);
    }
    
    public double complexPolygonArea(int complexPolygonNum, String unit) {
        double area = simplePolygonArea(complexPolygonNum, 0, unit);
        
        for (int i = 1; i < this.simplePolygonCount(complexPolygonNum); i++) {
            area -= simplePolygonArea(complexPolygonNum, i, unit);
            System.out.println("Remove hole area");
        }
        
        return area;
    }
    
    
    public double simplePolygonArea(int complexPolygonNum, int simplePolygonNum) {
        return simplePolygonArea(complexPolygonNum, simplePolygonNum, defaultUnit);
    }
    
    public double simplePolygonArea(int complexPolygonNum, int simplePolygonNum, String unit) {
        // cheap Fourier transform
        // needed for conversion from projection area to area in meters
        
        double area = 0;
        SimplePolygon simplePolygon = this.getSimplePolygon(complexPolygonNum, simplePolygonNum);
        
        // MATHS!
        for (int i = 0; i < simplePolygon.vertexNumber()-1; i++) {
            LatLng p1 = simplePolygon.getLatLng(i);
            LatLng p2 = simplePolygon.getLatLng(i+1);
            area += calculateAreaSection(p1, p2);
        }

        LatLng p1 = simplePolygon.getLatLng(simplePolygon.vertexNumber()-1);
        LatLng p2 = simplePolygon.getLatLng(0);
        area += calculateAreaSection(p1,p2);

        area = calculateAreaInMeters(area);

        area = convertAreaUnits(unit, area);
        return area;
    }

    private double calculateAreaSection(LatLng p1, LatLng p2) {
        // A = sum[(x_i+1 - x_i) * (2 + sin(y_i) + sin(y_i+1
        return Math.toRadians(p2.getLng() - p1.getLng())
                * (2 + Math.sin(Math.toRadians(p1.getLat()) + Math.toRadians(p2.getLat())));
    }

    public double calculateAreaInMeters(double area) {
//        area = Math.abs(area * MapUtilities.EARTH_EQUATORIAL_RADIUS * MapUtilities.EARTH_POLAR_RADIUS);

        // This is technically the correct formula for WGS84 projections (i.e. with the factor of 1/2 at the end)
        // but produces incorrect value for magic reason
        area = Math.abs(area * MapUtilities.EARTH_EQUATORIAL_RADIUS * MapUtilities.EARTH_POLAR_RADIUS / 2);
        return area;
    }

    private double convertAreaUnits(String unit, double area) {
        double unitConverter;
        switch (unit) {
            case "metre":
                unitConverter = UnitsUtilities.Unit.METRE_SQUARED.getValue();
                break;
            case "kilometre":
                unitConverter = UnitsUtilities.Unit.KILOMETER_SQUARED.getValue();
                break;
            case "mile":
                unitConverter = UnitsUtilities.Unit.MILE_SQUARED.getValue();
                break;
            case "acre":
                unitConverter = UnitsUtilities.Unit.ACRE.getValue();
                break;
            case "hectare":
                unitConverter = UnitsUtilities.Unit.HECTARE.getValue();
                break;
            default:
                unitConverter = 1.0;
                System.out.println("Using default units");
                break;
        }

        area *= unitConverter;
        return area;
    }

    // Contains
    public boolean geometryContains(LatLng latLng) {
        // if multigon contains point
        boolean contains = false;
        
        for (int i = 0; i < complexPolygonCount(); i++) {
            if (complexPolygonContains(i, latLng)) {
                contains = true;
            }
        }
        return contains;
    }
    
    public boolean complexPolygonContains(int complexPolygon, LatLng latLng) {
        boolean contains = simplePolygonContains(complexPolygon, 0, latLng);
        if (contains) {
            for (int i = 1; i < simplePolygonCount(complexPolygon); i++) {
                if (simplePolygonContains(complexPolygon, i, latLng)) {
                    contains = false;
                }
            }
        }
        return contains;
    }
    
    public boolean simplePolygonContains(int complexPolygon, int simplePolygon, LatLng latLng) {
        boolean contains = this.geometry.get(complexPolygon).get(simplePolygon).containsLatLng(latLng);
        return contains;
    }
    
    // Centroid
    public LatLng simplePolygonCentroid(int complexPolygon, int simplePolygon) {
        return this.geometry.get(complexPolygon).get(simplePolygon).centroid();
    }

    public LatLng getCentroid() {
        double latAverage = 0;
        double lngAverage = 0;
        int polyCount = 0;
        for (ArrayList<SimplePolygon> complexPolygon : geometry) {
                LatLng centroid = complexPolygon.get(0).centroid();
                lngAverage += centroid.getLng();
                latAverage += centroid.getLat();
                polyCount++;
        }

        return new LatLng(lngAverage/polyCount, latAverage/polyCount);
    }
    
    // Type
    public boolean isSimple() {
        if (this.geometry == null || this.geometry.isEmpty()) {
            return true;
        } else {
            boolean simple = true;

            for (ArrayList<SimplePolygon> complexPolygon : this.geometry) {
                if (complexPolygon.size() > 1) {
                    simple = false;
                }
            }
            return simple;
        }
    }
    
    public boolean isSingle() {
        
        if (this.geometry == null || this.geometry.isEmpty()) {
            return true;
        } else {
            return (this.geometry.size() == 1);
        }
    }
    
    public boolean isSimple(int complexPolygon) {   
        return (this.geometry.get(complexPolygon).size() == 1);
    }
    
    // :(
    @Override
    public String toString() {
        String string = "";
        
        if (this.isSingle()) {
            string += "POLYGON ";
        } else {
            string += "MULTIPOLYGON (";
        }
        
        for (int i = 0; i < this.geometry.size() -1; i++) {
            string += "(";
            
            for (int j = 0; j < this.geometry.get(i).size() -1; j++) {
                string += "(";
                string += this.geometry.get(i).get(j).toString();
                string += "), ";
            }
            string += "(";
            string += this.geometry.get(i).get(this.geometry.get(i).size()-1).toString();
            string += ")";
            
            
            string += "), ";
        }
        
        string += "(";
            
            for (int j = 0; j < this.geometry.get(this.complexPolygonCount()-1).size() -1; j++) {
                string += "(";
                string += this.geometry.get(this.complexPolygonCount()-1).get(j).toString();
                string += "), ";
            }
            string += "(";
            string += this.geometry.get(this.complexPolygonCount()-1).get(this.geometry.get(this.complexPolygonCount()-1).size()-1).toString();
            string += ")";
            
            string += ")";
        
        if (!this.isSingle()) {
            string += ")";
        }
        
        return string;
    }
}
