/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.entities.polygons;

import java.awt.geom.Path2D;
import java.util.ArrayList;

import com.remsdaq.osapplication.entities.coordinates.LatLng;
import math.geom2d.Point2D;
import math.geom2d.line.LinearShape2D;
import math.geom2d.polygon.LinearRing2D;

/**
 *
 * @author cpt
 */
public class SimplePolygon extends LinearRing2D {
    
    public LatLng centroid() {
        double latSum = 0;
        double lngSum = 0;
        
        for (Point2D point : this.vertices()) {
            lngSum += point.x();
            latSum += point.y();
        }

        Double lngAverage = lngSum/this.vertexNumber();
        Double latAverage = latSum/this.vertexNumber();
        
        return (vertexNumber() > 0)  ? new LatLng(lngAverage, latAverage) : null;
    }
    
    public LatLng getLatLng(int vertex) {
        return new LatLng(this.vertex(vertex).x(), this.vertex(vertex).y());
    }
    
    public ArrayList<LatLng> getLatLngs() {
        ArrayList latLngs = new ArrayList<>();
        for (Point2D point2D : this.vertices()) {
            latLngs.add(new LatLng(point2D.x(), point2D.y()));
        }
        return latLngs;
    }
    
    public void addVertex(LatLng latLng) {
        addVertex(latLng.getPoint2D());
    }
    
    // Checks if the polygon contains a LatLng
    public Boolean containsLatLng(LatLng latLng) {
        
        Path2D path = new Path2D.Double();
        
        path.moveTo(this.firstPoint().x(), this.firstPoint().y());
        
        for (int i = 1; i < this.vertexNumber(); i++) {
            path.lineTo(this.vertex(i).x(), this.vertex(i).y());
        }
        
        path.closePath();
        
        Boolean contains = path.contains(latLng.getLng(), latLng.getLat());
        return contains;
    }
    
    // Checks if the polygon is intersected by a line
    public Boolean isIntersectedBy(LinearShape2D line) {
        Boolean isIntersectedBy = (this.intersections(line) != null);
        return isIntersectedBy;
    }
    
    @Override
    public String toString() {
        String string = "";
        ArrayList<LatLng> latLngs = getLatLngs();
        for (int i = 0; i < this.getLatLngs().size()-1; i++) {
            string += latLngs.get(i).toString() +  ", ";
        }
        string += latLngs.get(latLngs.size()-1).toString();
        return string;
    }
}
