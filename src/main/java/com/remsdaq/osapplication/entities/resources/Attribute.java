package com.remsdaq.osapplication.entities.resources;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.remsdaq.osapplication.entities.resources.AttributeType.ATTRDATATYPE.ATTR_NULLVALUE;

public class Attribute {

    private Long id;
    private AttributeType type;
    private Integer ival;
    private Double dval;
    private String sval;
    private LocalDate dateval;
    private Boolean bval;

    public Attribute() {

    }

    public Attribute(AttributeType type, Integer ival) {
        this.type = type;
        this.ival = ival;
    }

    public Attribute(AttributeType type, Double dval) {
        this.type = type;
        this.dval = dval;
    }

    public Attribute(AttributeType type, String sval) {
        this.type = type;
        this.sval = sval;
    }

    public Attribute(AttributeType type, LocalDate dateval) {
        this.type = type;
        this.dateval = dateval;
    }

    public Attribute(AttributeType type, Boolean bval) {
        this.type = type;
        this.bval = bval;
    }

    public Object getValue() {
        if ((type != null) && (type.getDataType() != null)) {
            switch (type.getDataType()) {
                case ATTR_BOOLEAN:
                    return this.bval;
                case ATTR_DATE:
                    return this.dateval;
                case ATTR_DOUBLE:
                    return this.dval;
                case ATTR_INTEGER:
                    return this.ival;
                case ATTR_NULLVALUE:
                    return ATTR_NULLVALUE;
                default:
                    return null;
            }
        }
        return null;
    }

    public void setValue(Object pVal) throws Exception {


        if (pVal != null) {
            switch (this.getType().getDataType()) {
                case ATTR_STRING:
                    attributeIsString(pVal);
                    break;
                case ATTR_BOOLEAN:
                    attributeIsBoolean(pVal);
                    break;
                case ATTR_INTEGER:
                    attributeIsInteger(pVal);
                    break;
                case ATTR_DOUBLE:
                    attributeIsDouble(pVal);
                    break;
                case ATTR_DATE:
                    attributeIsLocaldate(pVal);
                    break;
            }

        }

    }
    
    void attributeIsLocaldate(Object pVal) throws Exception {
        if (pVal instanceof LocalDate) {
            this.setDateval((LocalDate) pVal);
        } else if (pVal instanceof String) {
            try {
                this.setDateval((LocalDate.parse((CharSequence) pVal, DateTimeFormatter.ISO_DATE)));
            } catch (Exception e) {
                throw new Exception("value not valid date");
            }
        }
    }
    
    void attributeIsDouble(Object pVal) throws Exception {
        if (pVal instanceof Double) {
            this.setDval((Double) pVal);
        } else if (pVal instanceof String) {
            try {
                this.setDval(Double.parseDouble((String) pVal));
            } catch (Exception e) {
                throw new Exception("value not valid double");
            }
        }
    }
    
    void attributeIsInteger(Object pVal) throws Exception {
        if (pVal instanceof Integer) {
            this.setIval((Integer) pVal);
        } else if (pVal instanceof String) {
            try {
                this.setIval(Integer.parseInt((String) pVal));
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "", e);
                throw new Exception("value not valid integer");

            }
        }
    }
    
    void attributeIsString(Object pVal) {
        if (pVal instanceof String) {
            this.setSval((String) pVal);
        }
    }
    
    void attributeIsBoolean(Object pVal) throws Exception {
        if (pVal instanceof Boolean) {
            this.setBval((Boolean) pVal);
        } else if (pVal instanceof String) {
            try {
                this.setBval(Boolean.parseBoolean((String) pVal));
            } catch (Exception e) {
                throw new Exception("value not valid boolean");
            }
        }
    }
    
    public String getUnits() {
        String units = "";
        if ((this.getType() != null) && (this.getType().getUnits() != null) && (this.getType().getUnits() != AttributeType.ATTRUNITS.UNDEFINED)
                && (this.getType().getUnits().name() != null) && !this.getType().getUnits().name().isEmpty()) {
            units = this.getType().getUnits().name();
        }
        return units;
    }

    public AttributeType getType() {
        return this.type;
    }

    public void setType(AttributeType type) {
        this.type = type;
    }

    public Integer getIval() {
        return ival;
    }

    public void setIval(Integer ival) {
        this.ival = ival;
    }

    public Double getDval() {
        return dval;
    }

    public void setDval(Double dval) {
        this.dval = dval;
    }

    public String getSval() {
        return sval;
    }

    public void setSval(String sval) {
        this.sval = sval;
    }

    public LocalDate getDateval() {
        return dateval;
    }

    public void setDateval(LocalDate dateval) {
        this.dateval = dateval;
    }

    public Boolean getBval() {
        return bval;
    }

    public void setBval(Boolean bval) {
        this.bval = bval;
    }
}
