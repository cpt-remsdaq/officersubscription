package com.remsdaq.osapplication.entities.resources;

public class AttributeType {

    private Long id;
    private String name;
    private String description;
    private ATTRDATATYPE dataType;
    private ATTRUNITS units;

    public AttributeType() {
        name = null;
        description = null;
        dataType = ATTRDATATYPE.ATTR_UNDEFINED;
        units = ATTRUNITS.UNDEFINED;
    }

    public AttributeType(ATTRTYPE attrtype) {
        id = attrtype.typeId;
        name = attrtype.type;
        description = attrtype.description;
        dataType = (attrtype.datatype == null ? ATTRDATATYPE.ATTR_UNDEFINED : attrtype.datatype);
        units = (attrtype.dataunits == null ? ATTRUNITS.UNDEFINED : attrtype.dataunits);
    }

    public AttributeType(String aname, String adescription, AttributeType.ATTRDATATYPE atype, AttributeType.ATTRUNITS aunits) {
        name = aname;
        description = adescription;
        dataType = AttributeType.ATTRDATATYPE.ATTR_UNDEFINED;
        if (atype != null) {
            dataType = atype;
        }
        units = ATTRUNITS.UNDEFINED;
        if (aunits != null) {
            units = aunits;
        }
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the dataType
     */
    public ATTRDATATYPE getDataType() {
        return dataType;
    }

    /**
     * @param dataType the dataType to set
     */
    public void setDataType(ATTRDATATYPE dataType) {
        this.dataType = dataType;
    }

    /**
     * @return the units
     */
    public ATTRUNITS getUnits() {
        return units;
    }

    /**
     * @param units the units to set
     */
    public void setUnits(ATTRUNITS units) {
        this.units = units;
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder();
        if (this.getName() != null) {
            buff.append(getName());
            if (this.units != null && this.units != ATTRUNITS.UNDEFINED) {
                buff.append(" (").append(this.units).append(")");
            }
            if (getDescription() != null) {
                buff.append(" - ");
                buff.append(getDescription());
            }
        }
        return buff.toString();
    }

    //ENUMS
    public enum ATTRTYPE {
        // Mobilisation equipment attribute types...

        ATTR_TYPE_SONICS(1L, "Sounders", "Station Sounders", null, null),
        ATTR_TYPE_LIGHTS(2L, "Lights", "Station Lights", null, null),
        ATTR_TYPE_ALERTA(3L, "Alerter A", "Station Alerters A", null, null),
        ATTR_TYPE_ALERTB(4L, "Alerter B", "Station Alerters B", null, null),
        ATTR_TYPE_ALERTC(5L, "Alerter C", "Station Alerters C", null, null),
        ATTR_TYPE_ALERT_TEST(6L, "Alerter Test", "Station Test Alerters", null, null),
        ATTR_TYPE_BAYLAMP0(7L, "Baylamp 0", "Station Baylamp 0", null, null),
        ATTR_TYPE_BAYLAMP1(8L, "Baylamp 1", "Station Baylamp 1", null, null),
        ATTR_TYPE_BAYLAMP2(9L, "Baylamp 2", "Station Baylamp 2", null, null),
        ATTR_TYPE_BAYLAMP3(10L, "Baylamp 3", "Station Baylamp 3", null, null),
        ATTR_TYPE_BAYLAMP4(11L, "Baylamp 4", "Station Baylamp 4", null, null),
        ATTR_TYPE_BAYLAMP5(12L, "Baylamp 5", "Station Baylamp 5", null, null),
        ATTR_TYPE_BAYLAMP6(13L, "Baylamp 6", "Station Baylamp 6", null, null),
        // On-board equipment attribute types...
        ATTR_TYPE_WATER(14L, "Water", "Water Carrier", null, null),
        ATTR_TYPE_DRILL(15L, "Drill", "Heavy Duty Drill", null, null),
        ATTR_TYPE_HAZCHEM(16L, "HAZCHEM", "Hazardous Chemical Support", null, null),
        ATTR_TYPE_LAMPS(17L, "Lamps", "Lighting Equipment", null, null),
        ATTR_TYPE_RESUSCITATION(18L, "Resuscitator", "Resuscitation Equipment", null, null),
        ATTR_TYPE_BA(19L, "BA", "Breathing Apparatus", null, null),
        ATTR_TYPE_CUTTING_CLANLUCAS(20L, "Cutting Gear CL", "Clan Lucas Cutting Gear", null, null),
        ATTR_TYPE_CUTTING_HOLMATRO(21L, "Cutting Gear HOL", "Holmatro Cutting Gear", null, null),
        ATTR_TYPE_TIC(22L, "TIC", "Thermal Imaging Camera", null, null),
        ATTR_TYPE_MDT(23L, "MDT", "MDT", ATTRDATATYPE.ATTR_BOOLEAN, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_FIRSTAID(24L, "First Aid", "First Aid Kit", null, null),
        ATTR_TYPE_CAFS(25L, "CAFS", "Compressed Air Foam System", null, null),
        ATTR_TYPE_LADDER_SHORT(26L, "Short Ladder", "10.5m Ladder", null, null),
        ATTR_TYPE_LADDER_LONG(27L, "Long Ladder", "13.5m Ladder", null, null),
        ATTR_TYPE_LPP(28L, "LPP", "Low Power Pump", null, null),
        ATTR_TYPE_PPV(29L, "PPV", "Positive Pressure Valve", null, null),
        ATTR_TYPE_LINERESCUE(30L, "Line Rescue", "Line Rescue Equipment", null, null),
        // Officer Contact equipment attribute types...
        ATTR_TYPE_MOBILE(31L, "Mobile", "Mobile Phone", null, null),
        ATTR_TYPE_TELEPHONE(32L, "Telephone", "Telephone Land Line", null, null),
        ATTR_TYPE_MEGAPHONE(33L, "Megaphone", "Megaphone", null, null),
        ATTR_TYPE_PAGER(34L, "Pager", "Pager Device", null, null),
        // Officer skill attribute types...
        ATTR_TYPE_DRIVER(35L, "Driver", "Driver", null, null),
        ATTR_TYPE_PHOTOGRAPHER(36L, "Photographer", "Photographer", null, null),
        ATTR_TYPE_CHEMICALS(37L, "Chemical", "Hazardous Chemicals", null, null),
        ATTR_TYPE_BOAT(38L, "Boat", "Boats", null, null),
        ATTR_TYPE_MECHANIC(39L, "Mechanic", "Mechanic", null, null),
        ATTR_TYPE_AVIATION(40L, "Aviation", "Aviation Officer", null, null),
        ATTR_TYPE_CADRE(41L, "CADRE", "CADRE Officer", null, null),
        ATTR_TYPE_FIO(42L, "FIO/FSO", "FIO/Fire Safety Officer", null, null),
        ATTR_TYPE_ILO(43L, "ILO", "Incident Liason Officer", null, null),
        ATTR_TYPE_NEBOSH(44L, "NEBOSH", "Health and Safety Officer", null, null),
        ATTR_TYPE_SHIP(45L, "Ship", "Ship Fire Fighting", null, null),
        ATTR_TYPE_WATERSAFE(46L, "Water Safety", "Water Safety Officer", null, null),
        // Station types....
        ATTR_TYPE_STN_DAYMANNED(47L, "Daymanned", "Day Manned Station", null, null),
        ATTR_TYPE_STN_WHOLETIME(48L, "Wholetime", "Wholetime Station", null, null),
        ATTR_TYPE_STN_RETAINED(49L, "Retained", "Retained Station", null, null),
        // Station attributes....
        ATTR_TYPE_SITE_PUMP_THRESHOLD(50L, "Pump Threshold", "Station Pump Threshold", ATTRDATATYPE.ATTR_ENTITY, null),
        ATTR_TYPE_SITE_MOBACK_DELAY(51L, "Stn Acknowledge Delay", "Station Acknowledge Delay", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.SECONDS),
        ATTR_TYPE_RSRC_MOBILE_DELAY(52L, "Book Mobile Delay", "Book Mobile Delay", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.SECONDS),
        ATTR_TYPE_RSRC_ATTEND_DELAY(53L, "Book Attend Delay", "Book Attending Delay", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.SECONDS),
        ATTR_TYPE_RSRC_MOBILE_AV_TIMER(74L, "Mobile Available Timer", "Mobile Available Timeout", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.MINUTES),
        //Appliance types....
        ATTR_TYPE_RSRC_PUMP(54L, "Pump", "Pump Type", null, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERS_COUNT(55L, "Rider Count", "Rider Count", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERS_CLASS(56L, "Rider Class", "Rider Class", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERS_OIC(57L, "Rider IC", "Rider In Charge", ATTRDATATYPE.ATTR_STRING, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_WEIGHTING(58L, "Weighting", "Weighting", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.SECONDS),
        ATTR_TYPE_RSRC_STANDBY_WEIGHTING(59L, "Standby Weighting", "Standby Weighting", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.SECONDS),
        ATTR_TYPE_RSRC_ADDITIONAL_WEIGHTING(60L, "Additional Weighting", "Additional Weighting", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.SECONDS),
        ATTR_TYPE_INCIDENT_CLOSED(61L, "Incident Closed", "Incident Closed", ATTRDATATYPE.ATTR_BOOLEAN, null),
        ATTR_TYPE_INCIDENT_STOP(62L, "Stop Message", "Stop Message", ATTRDATATYPE.ATTR_BOOLEAN, null),
        //Nominated Resource....
        ATTR_TYPE_DUTY_OFFICER(63L, "Duty Officer", "Duty Officer", ATTRDATATYPE.ATTR_BOOLEAN, null),
        ATTR_TYPE_PCD_SERVER_ADDRESS(64L, "PCD_SERVER_ADDRESS", "PCD_SERVER_ADDRESS", ATTRDATATYPE.ATTR_STRING, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_TALKGROUP_ALLOCATED(65L, "Talkgroup", "Talkgroup", ATTRDATATYPE.ATTR_STRING, null),
        ATTR_TYPE_RSRC_RIDERS_REQUIRED(66L, "Riders Required", "Riders Required", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERS_RANK(67L, "Rider Rank", "Rider In Charge Rank", ATTRDATATYPE.ATTR_STRING, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERSBA_COUNT(68L, "Rider BA", "Riders With BA", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERSGT_COUNT(69L, "Rider GTS", "Riders With GTS", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERSBA_AVAILABLE(70L, "BA Available", "BA Availability", ATTRDATATYPE.ATTR_BOOLEAN, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_RIDERSGT_AVAILABLE(71L, "GTS Available", "GTS Availability", ATTRDATATYPE.ATTR_BOOLEAN, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_SYSTEM_WEIGHTING(72L, "System Weighting", "System Weighting", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.SECONDS),
        /**
         * Page One pager's units hold battery charge percentage.
         */
        ATTR_TYPE_RSRC_PAGEONE_PAGER(73L, "Page one pager", "Page one pager", ATTRDATATYPE.ATTR_INTEGER, ATTRUNITS.PERCENT),
        // 74L - entry defined in Station attributes above....
        ATTR_TYPE_PCD_USEFREQUENTIS(75L, "Frequentis", "Use Frequentis", ATTRDATATYPE.ATTR_BOOLEAN, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_EMAIL(76L, "Email", "Email", ATTRDATATYPE.ATTR_UNDEFINED, ATTRUNITS.UNDEFINED),
        // Psuedo resource aatributes
        ATTR_TYPE_RSRC_PSUEDO_VEHICLE(77L, "Psuedo Vehicle", "Dummy Vehicle", ATTRDATATYPE.ATTR_UNDEFINED, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_RSRC_PSUEDO_PERSON(78L, "Psuedo Person", "Dummy Person", ATTRDATATYPE.ATTR_UNDEFINED, ATTRUNITS.UNDEFINED),

        ATTR_TYPE_STN_BEARERS_ENABLED(250L, "Station Bearers Enabled", "Station Bearers Enabled", ATTRDATATYPE.ATTR_BOOLEAN, ATTRUNITS.UNDEFINED),
        ATTR_TYPE_ISSI(19794L, "ISSI", "ISSI", ATTRDATATYPE.ATTR_STRING, ATTRUNITS.UNDEFINED)
        ;

        private final Long typeId;
        private final String type;
        private final String description;
        private final ATTRDATATYPE datatype;
        private final ATTRUNITS dataunits;

        ATTRTYPE(Long id, String strtype, String strdesc, ATTRDATATYPE datatype, ATTRUNITS dataunits) {
            this.typeId = id;
            this.type = strtype;
            this.description = strdesc;
            this.datatype = datatype;
            this.dataunits = dataunits;
        }

        public Long typeId() {
            return typeId;
        }

        public String typename() {
            return type;
        }

        public String description() {
            return description;
        }

        public ATTRDATATYPE getDataType() {
            return datatype;
        }

        public ATTRUNITS getDataUnits() {
            return dataunits;
        }

        @Override
        public String toString() {
            return typename();
        }
    }

    public enum ATTRDATATYPE {

        ATTR_UNDEFINED("Undefined"),
        ATTR_BOOLEAN("Boolean"),
        ATTR_INTEGER("Integer"),
        ATTR_DOUBLE("Double"),
        ATTR_DATE("Date"),
        ATTR_STRING("String"),
        ATTR_ENTITY("Entity"),
        ATTR_NULLVALUE("Null");
        private final String type;

        ATTRDATATYPE(String strtype) {
            type = strtype;
        }

        private String type() {
            return type;
        }

        @Override
        public String toString() {
            return type();
        }
    }

    public enum ATTRUNITS {

        UNDEFINED("Undefined"),
        METRES("Metres"),
        KILOMETRES("Km"),
        FEET("Feet"),
        YARDS("Yards"),
        MILES("Miles"),
        SECONDS("Secs"),
        MINUTES("Mins"),
        HOURS("Hrs"),
        METRES_PER_SECOND("M/S"),
        KILOMETRES_PER_HOUR("KM/Hr"),
        FEET_PER_MINUTE("Feet/Min"),
        YARDS_PER_MINUTE("Yards/Min"),
        FEET_PER_HOUR("Feet/Hr"),
        YARDS_PER_HOUR("Yards/Hr"),
        MILES_PER_HOUR("MPH"),
        PERCENT("%");

        private final String units;

        ATTRUNITS(String strunits) {
            units = strunits;
        }

        private String units() {
            return units;
        }

        @Override
        public String toString() {
            return units();
        }
    }
//END OF ENUMS
}
