package com.remsdaq.osapplication.entities.resources;

public class Person extends Resource {
	
	public Person(Long id) {
		super(id);
	}
	
	@Override
	public EntityType getEntityType() {
		return EntityType.PERSON;
	}
}
