package com.remsdaq.osapplication.entities.resources;

import com.remsdaq.osapplication.entities.Entity;
import com.remsdaq.osapplication.entities.incidents.Incident;
import com.remsdaq.osapplication.entities.coordinates.LatLng;

import java.util.List;

public class Resource implements Entity {

//    // This is one of the following: site, vehicle, person
    private Long id;
    private String callSign;
    private LatLng latLng;
    private Status status;
    private ResourceType type;
    private List<Attribute> attributes;
    private String location;
    private Incident incident;
    private Site homeSite;
    private Site currentSite;
    private boolean locked;
    private String pagerNumber;
    private String firstName;
    private String lastName;

    public Resource(Long id){
        this.id = id;
    }
	
    public Long getId() {
    	return id;
    }
    
    public void setId(Long id) {
    	this.id = id;
    }
    
	@Override
	public String getID() {
		return id.toString();
	}
	
	@Override
	public EntityType getEntityType() {
		return EntityType.RESOURCE;
	}
	
	public String getCallSign() {
		return callSign;
	}
	
	public void setCallSign(String callSign) {
		this.callSign = callSign;
	}
	
	public LatLng getLatLng() {
		return latLng;
	}
	
	public void setLatLng(LatLng latLng) {
		this.latLng = latLng;
	}
	
	public Status getStatus() {
		return status;
	}
	
	public void setStatus(Status status) {
		this.status = status;
	}
	
	public ResourceType getType() {
		return type;
	}
	
	public void setType(ResourceType type) {
		this.type = type;
	}
	
	public List<Attribute> getAttributes() {
		return attributes;
	}
	
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public Incident getIncident() {
		return incident;
	}
	
	public void setIncident(Incident incident) {
		this.incident = incident;
	}
	
	public Site getHomeSite() {
		return homeSite;
	}
	
	public void setHomeSite(Site homeSite) {
		this.homeSite = homeSite;
	}
	
	public Site getCurrentSite() {
		return currentSite;
	}
	
	public void setCurrentSite(Site currentSite) {
		this.currentSite = currentSite;
	}
	
	public boolean isLocked() {
		return locked;
	}
	
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	
	public String getPagerNumber() {
		return pagerNumber;
	}
	
	public void setPagerNumber(String pagerNumber) {
		this.pagerNumber = pagerNumber;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
