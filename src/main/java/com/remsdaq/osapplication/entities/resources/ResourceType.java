package com.remsdaq.osapplication.entities.resources;

public class ResourceType {

    private Long id;
    private String code;
    private String description;
    private ResourceTypeClass typeClass;

    public enum ResourceTypeClass {
        PERSON,
        VEHICLE,
        SITE
    }
}
