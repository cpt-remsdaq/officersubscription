package com.remsdaq.osapplication.entities.resources;

import com.remsdaq.osapplication.entities.addresses.Address;

public class Site extends Resource {

    private Address address;
	
	
	public Site(Long id) {
		super(id);
	}
	
	@Override
	public EntityType getEntityType() {
		return EntityType.SITE;
	}
}
