package com.remsdaq.osapplication.entities.resources;

import java.util.List;

public class Status {

    private int id;
    private String statusCode;
    private String description;
    private List<StatusClass> statusClasses;

    public Status(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public Status(StatusClass status) {
        this.id = status.getId();
        this.description = status.getDescription();
    }

    public enum StatusClass {
        INC_RELATED(1,"Incident Related"),
        STBY_RELATED(2,"Standby Related"),
        GUEST_RELATED(3, "Guesting Related"),
        AVAILABLE(4, "Available"),
        NOT_AVAILABLE(5, "UnAvailable"),
        MOBILE(6, "Mobile"),
        STBY_ORDERED(7, "Ordered to Standby"),
        STBY_RADIO_ORDERED(8, "Radio Ordered to Standby"),
        STBY_PROCEED(9, "Proceeding to Standby"),
        STBY_PROCEED_IN_AREA(10, "Proceeding to Standby in Area"),
        STBY_STATION(11, "At Standby Station"),
        STBY_RETURNING(12, "Returning from Standby"),
        GUEST_ORDERED(13, "Ordered to Guesting"),
        GUEST_RADIO_ORDERED(14, "Radio Ordered to Guesting"),
        GUEST_PROCEED(15, "Proceeding to Guesting"),
        GUEST_PROCEED_IN_AREA(16, "Proceeding to Guesting in Area"),
        GUEST_STATION(17, "At Guesting Station"),
        GUEST_RETURNING(18, "Returning from Guesting"),
        INC_ORDERED(19, "Ordered to Incident"),
        INC_RADIO_ORDERED(20, "Radio Ordered to Incident"),
        INC_PROCEED(21, "Proceeding to Incident"),
        INC_ATTENDING(22, "In Attendance at Incident"),
        INC_RETURNING(23, "Returning from Incident"),
        OFF_RUN(24, "Off the Run"),
        CREW_DEFICIENT(25, "Crew Deficient"),
        LAST_ONE(26, "End"),
        PERSON(27, "Person status"),
        VEHICLE(28, "Vehicle status"),
        REDEPLOYABLE(29, "Re-Deployable status"),
        STBY_RELEASABLE(30, "Release from Standby"),
        CLEARS_RIDERS(31, "Clears riders"),
        RELIEF(32, "Relief status"),
        CAN_BE_MODIFIED_BY_CREWING(33, "Can be modified by crewing"),
        INC_ATTENDING_AT_ACCESSPOINT(34, "In Attendance at AccessPoint");

        int id;
        String description;

        StatusClass(int id, String description) {
            this.id = id;
            this.description = description;
        }

        StatusClass(StatusClass statusEnum) {
            this.id = statusEnum.getId();
            this.description = statusEnum.getDescription();
        }

        public int getId() {
            return this.id;
        }

        public String getDescription() {
            return this.description;
        }

    }
}
