package com.remsdaq.osapplication.entities.resources;

public class Vehicle extends Resource {
	
	public Vehicle(Long id) {
		super(id);
	}
	
	@Override
	public EntityType getEntityType() {
		return EntityType.VEHICLE;
	}
}
