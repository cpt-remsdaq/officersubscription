package com.remsdaq.osapplication.entities.subscribers;

import com.remsdaq.osapplication.entities.Entity;

import java.util.Collection;
import java.util.function.Predicate;

public class EntityIdComparator {
	
	Collection<Entity> ownerEntities;
	Collection<Entity> comparisonEntities;
	
	public boolean areEntitySetsEqual(Object ownerEntities, Object comparisonEntities) {
		this.ownerEntities = (Collection<Entity>) ownerEntities;
		this.comparisonEntities = (Collection<Entity>) comparisonEntities;
		return areAllOwnersInComparisonSet() && areAllComparisonsInOwnersSet();
	}
	
	private Boolean areAllOwnersInComparisonSet() {
		Predicate<Entity> matchPolygonID = isEntityInSet(comparisonEntities);
		if (neitherEntitySetsAreNull() && (entitySetsOfSameSize())) {
			return ownerEntities.stream().allMatch(matchPolygonID);
		}
		return false;
	}
	
	private Boolean areAllComparisonsInOwnersSet() {
		Predicate<Entity> matchPolygonID = isEntityInSet(ownerEntities);
		if (neitherEntitySetsAreNull() && entitySetsOfSameSize()) {
			return comparisonEntities.stream().allMatch(matchPolygonID);
		}
		return false;
	}
	
	private boolean entitySetsOfSameSize() {
		return ownerEntities.size() == comparisonEntities.size();
	}
	
	private boolean neitherEntitySetsAreNull() {
		return (ownerEntities != null) && (comparisonEntities != null);
	}
	
	private Predicate<Entity> isEntityInSet(Collection<Entity> comparisonEntities) {
		return entity -> {
			for (Entity comparisonEntity : comparisonEntities) {
				if (entity.getID().equals(comparisonEntity.getID())) {
					return true;
				}
			}
			return false;
		};
	}
	
}
