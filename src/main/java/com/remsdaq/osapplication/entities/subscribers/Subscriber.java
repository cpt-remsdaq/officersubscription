package com.remsdaq.osapplication.entities.subscribers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.database.xml.XmlDataCollectionEntity;
import com.remsdaq.osapplication.database.xml.entities.polygons.PolygonXmlAdapter;
import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberCollection;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.contacts.Contacts;
import com.remsdaq.osapplication.utilities.StringUtilities;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.IntStream;

// Hibernate
@Table(name = "subscribers")
@Entity
// JAXB
@XmlRootElement(name = "Subscriber")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = {"ID", "firstName", "lastName", "email", "emailPriority", "mobile", "mobilePriority", "pager", "pagerPriority", "polygons"})
public class Subscriber implements Serializable, com.remsdaq.osapplication.entities.Entity , XmlDataEntity {
	
	@XmlTransient private String ID = "";
	@XmlTransient private String firstName = "";
	@XmlTransient private String lastName = "";
	@XmlTransient private String email = "";
	@XmlTransient private Integer emailPriority = 0;
	@XmlTransient private String mobile = "";
	@XmlTransient private Integer mobilePriority = 0;
	@XmlTransient private String pager = "";
	@XmlTransient private Integer pagerPriority = 0;
	@XmlTransient private Set<Polygon> polygons = new HashSet();
	
	@XmlTransient private Contacts contacts;
	
	@XmlTransient private GlobalContext globalContext = GlobalContext.getInstance();
	
	public Subscriber() {
	}
	
	public Subscriber(String ID, String firstName, String lastName, Set<Polygon> polygons, String email, Integer emailPriority, String mobile,
	                  Integer mobilePriority, String pager, Integer pagerPriority) {
		this.ID = ID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.polygons = polygons;
		
		this.email = email;
		this.emailPriority = emailPriority;
		this.mobile = mobile;
		this.mobilePriority = mobilePriority;
		this.pager = pager;
		this.pagerPriority = pagerPriority;
	}
	
	@Transient
	@XmlTransient
	public String[] getPolygonStringArray() {
		if (this.getPolygons() != null) {
			Object[] polyArray = polygons.toArray();
			String[] polyIdArray = new String[this.getPolygons().size()];
			int i = 0;
			for (Object obj : polyArray) {
				Polygon poly = (Polygon) obj;
				polyIdArray[i] = poly.getID();
				i++;
			}
			return polyIdArray;
		}
		return new String[0];
	}
	
	@ManyToMany(
			cascade = {CascadeType.PERSIST, CascadeType.MERGE},
			//            mappedBy="subscribers",
			targetEntity = Polygon.class,
			fetch = FetchType.EAGER)
	@JoinTable(
			name = "subscriber_polygon",
			joinColumns = @JoinColumn(name = "subscriber_ID"),
			inverseJoinColumns = @JoinColumn(name = "polygon_ID"))
	@XmlJavaTypeAdapter(PolygonXmlAdapter.class)
	@XmlElementWrapper(name = "Polygons")
	@XmlElement(name = "Polygon")
	public Set<Polygon> getPolygons() {
		return this.polygons;
	}
	
	public void setPolygons(Set<Polygon> polygons) {
		this.polygons = polygons;
	}
	
	public void setPolygonStringArray(String[] stringArray) {
		Set<Polygon> polySet = new HashSet<>();
		for (String id : stringArray) {
			Polygon poly = new Polygon();
			poly.setID(id);
			polySet.add(poly);
		}
		setPolygons(polySet);
	}
	
	@Transient
	public int[] getPriorities() {
		return IntStream.range(1, Integer.parseInt(globalContext.getConfig().getProperty("number_of_priorities")) + 1).toArray();
	}
	
	@Transient
	@XmlTransient
	public Object[] getContactDetails() {
		Object[] out = new Object[]{"", -1, "", -1, "", -1};
		
		if (!StringUtilities.isEmptyNullOrBlank(this.getEmail())) {
			out[0] = this.getEmail();
			out[1] = this.getEmailPriority();
		}
		if (!StringUtilities.isEmptyNullOrBlank(this.getMobile())) {
			out[2] = this.getMobile();
			out[3] = this.getMobilePriority();
		}
		if (!StringUtilities.isEmptyNullOrBlank(this.getPager())) {
			out[4] = this.getPager();
			out[5] = this.getPagerPriority();
		}
		return out;
	}
	
	@Column(name = "email")
	@XmlElement(name = "Email")
	public String getEmail() {
		return this.email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name = "email_priority")
	@XmlElement(name = "EmailPriority")
	public Integer getEmailPriority() {
		return this.emailPriority;
	}
	
	public void setEmailPriority(Integer emailPriority) {
		this.emailPriority = emailPriority;
	}
	
	@Column(name = "mobile")
	@XmlElement(name = "Mobile")
	public String getMobile() {
		return this.mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	@Column(name = "mobile_priority")
	@XmlElement(name = "MobilePriority")
	public Integer getMobilePriority() {
		return this.mobilePriority;
	}
	
	public void setMobilePriority(Integer mobilePriority) {
		this.mobilePriority = mobilePriority;
	}
	
	@Column(name = "pager")
	@XmlElement(name = "Pager")
	public String getPager() {
		return this.pager;
	}
	
	public void setPager(String pager) {
		this.pager = pager;
	}
	
	@Column(name = "pager_priority")
	@XmlElement(name = "PagerPriority")
	public Integer getPagerPriority() {
		return this.pagerPriority;
	}
	
	public void setPagerPriority(Integer pagerPriority) {
		this.pagerPriority = pagerPriority;
	}
	
	public int numOfPolygons() {
		return polygons.size();
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataPath() {
		return globalContext.getConfig().getProperty("subscriber_xml_data_path");
	}
	
	@Override
	@Transient
	@XmlTransient
	public String getXmlDataCollectionPath() {
		return globalContext.getConfig().getProperty("subscriber_collection_xml_data_path");
	}
	
	@Override
	public XmlDataCollectionEntity makeCollection() {
		SubscriberCollection collectionEntity = new SubscriberCollection();
		collectionEntity.addToCollection(this);
		return collectionEntity;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else if (o instanceof Subscriber) {
			Subscriber comparison = (Subscriber) o;
			return areAllFieldsEqual(comparison);
		} else if (o instanceof String) {
			String ID = (String) o;
			return this.ID.equals(ID);
		}
		return false;
	}
	
	boolean areAllFieldsEqual(Subscriber comparison) {
		EntityIdComparator comparator = new EntityIdComparator();
		return this.ID.equals(comparison.getID())
		       && this.firstName.equals(comparison.getFirstName())
		       && this.lastName.equals(comparison.getLastName())
		       && this.email.equals(comparison.getEmail())
		       && this.mobile.equals(comparison.getMobile())
		       && this.pager.equals(comparison.getPager())
		       && (this.emailPriority == comparison.getEmailPriority())
		       && (this.mobilePriority == comparison.getMobilePriority())
		       && (this.pagerPriority == comparison.getPagerPriority())
		       && comparator.areEntitySetsEqual(this.polygons, comparison.getPolygons());
	}
	
	@Override
	public int hashCode() {
		HashCodeBuilder builder = new HashCodeBuilder(311, 73)
				.append(ID)
				.append(firstName)
				.append(lastName)
				.append(email)
				.append(emailPriority)
				.append(mobile)
				.append(mobilePriority)
				.append(pager)
				.append(pagerPriority);
		return builder.toHashCode();
	}
	
	@Override
	@Id
	@Column(name = "subscriber_ID")
	@XmlAttribute(name = "ID")
	public String getID() {
		return this.ID;
	}
	
	public void setID(String ID) {
		this.ID = ID;
	}
	
	@XmlElement(name = "FirstName")
	@Column(name = "firstName")
	public String getFirstName() {
		return this.firstName;
	}
	
	@XmlElement(name = "LastName")
	@Column(name = "lastName")
	public String getLastName() {
		return this.lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Override
	@Transient
	@XmlTransient
	public EntityType getEntityType() {
		return EntityType.SUBSCRIBER;
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("ID: " + this.ID + "\n")
				.append("First Name: " + this.firstName + "\n")
				.append("Last Name: " + this.lastName + "\n")
				.append("Email: " + this.email + "\n")
				.append("Email Priority: " + this.emailPriority + "\n")
				.append("Mobile: " + this.mobile + "\n")
				.append("Mobile Priority: " + this.mobilePriority + "\n")
				.append("Pager: " + this.pager + "\n")
				.append("Pager Priority: " + this.pagerPriority + "\n")
				.append("Polygons: " + getPolygonString() + "\n")
				.toString();
	}
	
	@Transient
	@XmlTransient
	public String getPolygonString() {
		if (this.getPolygons() != null) {
			StringJoiner joiner = new StringJoiner(", ");
			this.getPolygons().forEach(poly -> {
				if (poly != null) {
					joiner.add(poly.getID());
				}
			});
			return joiner.toString();
		}
		return null;
	}
	
}