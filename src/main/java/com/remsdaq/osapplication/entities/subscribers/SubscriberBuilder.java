package com.remsdaq.osapplication.entities.subscribers;

import com.remsdaq.osapplication.entities.polygons.Polygon;

import java.util.Set;

public class SubscriberBuilder {
	private Subscriber subscriber;
	
	public SubscriberBuilder(String id){
		subscriber = new Subscriber();
		subscriber.setID(id);
	}
	
	public SubscriberBuilder firstName(String firstName) {
		subscriber.setFirstName(firstName);
		return this;
	}
	
	public SubscriberBuilder lastName(String lastName) {
		subscriber.setLastName(lastName);
		return this;
	}
	
	public SubscriberBuilder email(String email, int emailPriority) {
		subscriber.setEmail(email);;
		subscriber.setEmailPriority(emailPriority);
		return this;
	}
	
	public SubscriberBuilder mobile(String mobile, int mobilePriority) {
		subscriber.setMobile(mobile);
		subscriber.setMobilePriority(mobilePriority);
		return this;
	}
	
	public SubscriberBuilder pager(String pager, int pagerPriority) {
		subscriber.setPager(pager);
		subscriber.setPagerPriority(pagerPriority);
		return this;
	}
	
	public SubscriberBuilder setPolygons(Set<Polygon> polygonSet) {
		subscriber.setPolygons(polygonSet);
		return this;
	}
	
	public Subscriber build() {
		return subscriber;
	}
}
