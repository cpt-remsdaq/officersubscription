package com.remsdaq.osapplication.entities.subscribers;

import com.remsdaq.osapplication.other.UserMessage;
import com.remsdaq.osapplication.utilities.RegexUtilities;
import com.remsdaq.osapplication.utilities.StringUtilities;

import javax.persistence.Transient;
import java.util.ArrayList;

public class SubscriberValidity {
	
	@Transient
	public Object[] isValid(Subscriber sub) {
		
		Boolean valid = false;
		ArrayList<String> invalidMessage = new ArrayList();
		
		if (StringUtilities.isEmptyNullOrBlank(sub.getID())) {
			invalidMessage.add(UserMessage.MISSING_SUBSCRIBER_ID);
		}
		
		if (StringUtilities.isEmptyNullOrBlank(sub.getFirstName())) {
			invalidMessage.add(UserMessage.MISSING_SUBSCRIBER_FIRST_NAME);
		}
		
		if (StringUtilities.isEmptyNullOrBlank(sub.getLastName())) {
			invalidMessage.add(UserMessage.MISSING_SUBSCRIBER_LAST_NAME);
		}
		
		if (StringUtilities.isAllEmptyNullOrBlank(new String[]{sub.getEmail(), sub.getMobile(), sub.getPager()})) {
			invalidMessage.add(UserMessage.MISSING_SUBSCRIBER_CONTACT_METHOD);
		}
		
		if (!StringUtilities.isEmptyNullOrBlank(sub.getEmail()) && !sub.getEmail().toUpperCase().matches(
				RegexUtilities.EMAIL_REGEX)) {
			invalidMessage.add(UserMessage.INVALID_SUBSCRIBER_EMAIL);
		}
		
//		if (!StringUtilities.isEmptyNullOrBlank(sub.getEmail()) && !ArrayUtils.contains(getPriorities(), sub.emailPriority)) {
//			invalidMessage.add(Message.INVALID_SUBSCRIBER_EMAIL_PRIORITY);
//		}
//
//		if (!StringUtilities.isEmptyNullOrBlank(sub.getMobile()) && !ArrayUtils.contains(getPriorities(), sub.mobilePriority)) {
//			invalidMessage.add(Message.INVALID_SUBSCRIBER_MOBILE_PRIORITY);
//		}
//
//		if (!StringUtilities.isEmptyNullOrBlank(sub.getPager()) && !ArrayUtils.contains(getPriorities(), sub.pagerPriority)) {
//			invalidMessage.add(Message.INVALID_SUBSCRIBER_PAGER_PRIORITY);
//		}
		
		valid = (invalidMessage.size() == 0);
		
		Object[] out = new Object[]{valid, invalidMessage};
		
		return out;
	}
	
}
