package com.remsdaq.osapplication.entities.subscribers.contacts;

import javax.xml.bind.annotation.XmlElement;

public class Contact {

	private Type type = Type.UNKNOWN;
	private String value = null;
	private int priority = 0;
	
	public Contact(String value) {
		this.setValue(value);
	}
	
	public Contact(String value, int priority) {
		this.setValue(value);
		this.setPriority(priority);
	}
	
	public void setContactType() {
		this.setType(Type.UNKNOWN);
	}
	
	@XmlElement
	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}
	
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	public int getContactPriority() {
		return priority;
	}
	
	public void setPriority(int priority) {
		this.priority = priority;
	}
	
	@Override
	public String toString() {
		return new StringBuilder()
				.append("Contact Type: " + this.type)
				.append("\n")
				.append("Contact Value: " + this.value)
				.append("\n")
				.append("Contact Priority: " + this.priority)
				.toString();
	}
	
	public enum Type {
		EMAIL,
		PHONE,
		PAGER,
		UNKNOWN
	}
	
}
