package com.remsdaq.osapplication.entities.subscribers.contacts;

public class EmailAddress extends Contact {
	
	public EmailAddress(String value) {
		super(value);
		setType(Type.EMAIL);
	}
	
	public EmailAddress(String value, int priority) {
		super(value, priority);
		setType(Type.EMAIL);
	}
}
