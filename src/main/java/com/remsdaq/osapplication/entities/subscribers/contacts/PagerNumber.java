package com.remsdaq.osapplication.entities.subscribers.contacts;

public class PagerNumber extends Contact {
	
	public PagerNumber(String value) {
		super(value);
		setType(Contact.Type.PAGER);
	}
	
	public PagerNumber(String value, int priority) {
		super(value, priority);
		setType(Contact.Type.PAGER);
	}
	
}
