package com.remsdaq.osapplication.entities.subscribers.contacts;

public class PhoneNumber extends Contact{
	
	public PhoneNumber(String value) {
		super(value);
		setType(Contact.Type.PHONE);
	}
	
	public PhoneNumber(String value, int priority) {
		super(value, priority);
		setType(Contact.Type.PHONE);
	}
	
}
