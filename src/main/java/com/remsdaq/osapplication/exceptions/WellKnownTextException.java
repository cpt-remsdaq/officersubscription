/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.exceptions;

/**
 *
 * @author cpt
 */
public class WellKnownTextException extends Exception {
    
    public WellKnownTextException() {
        super("Not valid WKT input.");
    }

    public WellKnownTextException(String message) {
        super("Not valid WKT input: " + message);
    }
    
}
