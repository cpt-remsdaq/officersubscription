package com.remsdaq.osapplication.gui;

import com.remsdaq.osapplication.gui.controllers.OSPolygonMapController;

public class JavaBridge {
    
    private Object controller;
    private String logText;
    
    public JavaBridge(Object controller) {
        if (controller.getClass() == OSPolygonMapController.class) {
            this.controller = controller;
        }
    }
    
    public void log(String text)
    {
        this.logText = text;
        // If the bridge is being created by OSPolygonMapController
//        System.out.println(text);
        if (controller.getClass() == OSPolygonMapController.class && this.logText.contains("MAP CLICKED")) {
            OSPolygonMapController ctrl = (OSPolygonMapController) controller;
            ctrl.getLatLongPair(false);
            ctrl.createPolygonLayer(false, false);
        }
    }
    public String getLog() {
        return logText;
    }
}