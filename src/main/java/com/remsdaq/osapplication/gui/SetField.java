/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.gui;

import com.remsdaq.osapplication.utilities.RegexUtilities;
import static com.sun.xml.internal.ws.util.StringUtils.capitalize;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;


/**
 *
 * @author cpt
 */
public class SetField {
    
    // RegexUtilities for input validation
    private static final String phoneRegex = RegexUtilities.PHONE_REGEX;
    private static final String emailRegex = RegexUtilities.EMAIL_REGEX;
    
    public static void setFieldUpper(TextField field) {
        field.textProperty().addListener((ov, oldValue, newValue) -> {
            field.setText(newValue.toUpperCase());
        });
    }
    
    public static void setFieldCapitalised(TextField field) {
        field.textProperty().addListener((ov, oldValue, newValue) -> {
           field.setText(capitalize(newValue));
        });
    }
    
    public static void setFieldLength(TextField field, int lengthLimit) {
        field.setTextFormatter(
                new TextFormatter<>(change -> change.getControlNewText().length() <= lengthLimit ? change : null));
    }
    
    public static void setFieldLength(TextArea area, int lengthLimit) {
        area.setTextFormatter(
                new TextFormatter<>(change -> change.getControlNewText().length() <= lengthLimit ? change : null));
    }
    
    public static void setFieldToPhoneNumber(TextField field) {
        setFieldRegex(field,phoneRegex);
    }
    
    public static void setFieldRegex(TextField field, String regex) {
        field.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            if (!newValue.matches(regex)) {
                field.setText(oldValue);
            }
        });
    }
    
}
