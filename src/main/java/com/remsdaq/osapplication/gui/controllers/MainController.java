package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.OfficerSubscriberGuiWrapper;
import com.remsdaq.osapplication.configs.Config;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

	// FXML fields

	@FXML public VBox outerVBox;
	@FXML public MenuBar menuBar;
	@FXML public Menu fileMenu;
	@FXML public MenuItem fileNewSubscriber;
	@FXML public MenuItem fileNewPolygon;
	@FXML public MenuItem fileClose;
	@FXML public Menu editMenu;
	@FXML public Menu helpMenu;
	@FXML public MenuItem helpHelp;
	@FXML public MenuItem helpAbout;
	@FXML public TabPane tabPane;
	@FXML public Tab subscribersTab;
	@FXML public Tab polygonsTab;
	@FXML public Tab eventsTab;
	@FXML public SplitPane eventsSplitPane;
	@FXML public Label infoLabel;
	@FXML public Button panelSplitButton;

	@FXML private AnchorPane subscribersTabPane;
	@FXML private AnchorPane polygonsTabPane;

	@FXML private SubscribersTabPaneController subscribersTabPaneController;
	@FXML private PolygonsTabPaneController polygonsTabPaneController;

	// other fields
	private Config config = GlobalContext.getInstance().getConfig();
	private final int INFO_LABEL_TIMER = Integer.parseInt(config.getProperty("info_label_timer"));
	private Stage thisStage = GlobalContext.getInstance().getStage();
	private Timeline infoLabelTimeline = new Timeline(new KeyFrame(
			Duration.millis(INFO_LABEL_TIMER),
			ae -> setInfoLabel("")));
	private String info;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		injectControllers();
		initializeControllers();
		startListeners();
		setStyles();
		setMaximisationPreference();
	}
	
	private void startListeners() {
		// Whenever tabs are changed
		tabPane.getSelectionModel().selectedItemProperty().addListener(
				new ChangeListener<Tab>() {
					@Override
					public void changed(ObservableValue<? extends Tab> ov, Tab oldTab, Tab newTab) {
						if (newTab == subscribersTab) {
							subscribersTabPaneController.refreshTab();
						} else if (newTab == polygonsTab) {
							polygonsTabPaneController.refreshTab();
						}
					}
				}
		);
	}
	
	private void injectControllers() {
		subscribersTabPaneController.injectMainController(this);
		polygonsTabPaneController.injectMainController(this);
	}

	private void initializeControllers() {
		subscribersTabPaneController.initialize();
		//		polygonsTabPaneController.initialize();
	}

	private void setStyles() {
		infoLabel.setStyle(
				"-fx-text-fill: white;" +
						"-fx-font-weight: bold;"
		);
	}

	private void setMaximisationPreference() {
		if (Boolean.parseBoolean(config.getProperty("screen_maximised")) == false) {
			thisStage.setMaximized(false);
		} else {
			thisStage.setMaximized(true);
		}
	}

	void setInfoLabelWithDefaultTimer(String info) {
		this.info = info;
		setInfoLabelWithTimer(info, INFO_LABEL_TIMER);
	}

	private void setInfoLabelWithTimer(String info, int milliseconds) {
		setInfoLabel(info);
		if (milliseconds > 0) {
			infoLabelTimeline.stop();
			infoLabelTimeline.setDelay(new Duration(milliseconds));
			infoLabelTimeline.play();
		}
	}

	private void setInfoLabel(String info) {
		infoLabel.setText(info);
	}

	void selectNextTab() {
		tabPane.getSelectionModel().selectNext();
	}

	void selectPreviousTab() {
		tabPane.getSelectionModel().selectPrevious();
	}

	void ensureSubscribersTabIsSelected() {
		if (tabPane.getSelectionModel().getSelectedItem() != subscribersTab) {
			tabPane.getSelectionModel().select(subscribersTab);
		}
	}

	void ensurePolygonsTabIsSelected() {
		if (tabPane.getSelectionModel().getSelectedItem() != polygonsTab) {
			tabPane.getSelectionModel().select(polygonsTab);
		}
	}

	@FXML private void newSubscriberPressed() {
		subscribersTabPaneController.newSubscriber();
	}

	@FXML private void newPolygonButtonPressed() throws Exception {
				polygonsTabPaneController.newPolygon();
	}

	@FXML private void fileClosePressed() {
		Scene scene = tabPane.getScene();
		if (scene != null) {
			javafx.stage.Window window = scene.getWindow();
			if (window != null) {
				window.hide();
				Object wrapper = GlobalContext.getInstance().getWrapper();

				if (wrapper.getClass() == OfficerSubscriberGuiWrapper.class) {
					OfficerSubscriberGuiWrapper wrppr = (OfficerSubscriberGuiWrapper) wrapper;
					wrppr.quit();
				}
			}
		}
	}

	@FXML private void panelSplitButtonPressed() {
	}
}
