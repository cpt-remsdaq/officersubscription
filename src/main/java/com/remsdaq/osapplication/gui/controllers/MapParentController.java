package com.remsdaq.osapplication.gui.controllers;

		import com.remsdaq.osapplication.entities.polygons.PolygonGeometry;

public abstract class MapParentController {

	public abstract void polygonDrawn(PolygonGeometry polygonGeometry);
}
