/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.polygons.PolygonGeometry;
import com.remsdaq.osapplication.entities.polygons.SimplePolygon;
import com.remsdaq.osapplication.gui.JavaBridge;
import com.remsdaq.osapplication.gui.polygon_map.PolygonMapUserAction;
import com.remsdaq.osapplication.utilities.gui.MapUtilities;
import com.remsdaq.osapplication.utilities.NodeUtilities;
import com.remsdaq.osapplication.utilities.gui.GuiUtilities;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;
import netscape.javascript.JSObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author cpt
 */
public class OSPolygonMapController implements Initializable {
	
	static public WebEngine engine;
	public PolygonGeometry multigon;
	public JavaBridge bridge;
	public MapParentController controller;
	Config config = new Config();
	public String mapPage = config.getProperty("polygons_map_page");
	private final int INFO_LABEL_TIMER = Integer.parseInt(config.getProperty("info_label_timer"));
	// Buttons
	@FXML
	private Button saveGeometryButton;
	@FXML
	private Button clearGeometryButton;
	@FXML
	private Button cancelButton;
	@FXML
	private Button undoButton;
	@FXML
	private Button redoButton;
	@FXML
	private Button newPolygonButton;
	@FXML
	private Button closePolygonButton;
	@FXML
	private Button cancelPolygonButton;
	@FXML
	private Button deletePolygonButton;
	@FXML
	private Button newHoleButton;
	@FXML
	private Button closeHoleButton;
	@FXML
	private Button cancelHoleButton;
	@FXML
	private Button deleteHoleButton;
	@FXML
	private WebView polygonWebView;
	@FXML
	private Label infoLabel;
	// Collection of user actions for undo-redo
	private List<PolygonMapUserAction> userActionList = new ArrayList<>();
	private List<PolygonMapUserAction> userActionUndoList = new ArrayList<>();
	private int currentComplexPolygon = -1;
	private int currentSimplePolygon = -1;
	private Boolean newPolygonBool = false;
	private Boolean newHoleBool = false;
	private Boolean deletePolygonBool = false;
	private Boolean deleteHoleBool = false;
	private int holeVertexNumber = -1;
	
	public OSPolygonMapController() {
		this.bridge = new JavaBridge(this);
	}
	
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		// Load map to webengine
		
		multigon = new PolygonGeometry();
		engine = polygonWebView.getEngine();
		engine.reload();
		engine.load(mapPage);
		
		startListeners();
		setFormatters();
	}
	
	public void startListeners() {
		
		// Check if the webview has loaded so that firebug can be removed
		engine.getLoadWorker().workDoneProperty().addListener((observable, oldValue, value) -> {
			System.out.println("Checking if firebug is loaded");
			//if 100% loaded
			if (value.intValue() == 100) {
				engine.executeScript("markers = new OpenLayers.Layer.Markers();\n"
				                     + "map.addLayer(markers);");
				removeFirebug();
				MapUtilities.zoomToDefault(engine);
				
				if (GlobalContext.getInstance().getPolygon().getGeometry().totalVertexCount() > 0) {
					multigon = GlobalContext.getInstance().getPolygon().getGeometry();
					createPolygonLayer(true, false);
				}
				
				//                // Get polygon geometry if it is being edited
				//                multigon = GlobalContext.getInstance().getPolygon().getGeometry();
				//                if (multigon.totalVertexCount() > 0) {
				//                    createPolygonLayer(true, false);
				//                }
			}
		});
		
		// Check if console changes
		engine.getLoadWorker().stateProperty().addListener((observable, oldValue, newValue) -> {
			JSObject window = (JSObject) engine.executeScript("window");
			window.setMember("java", bridge);
			engine.executeScript("console.log = function(message)\n" + "{\n" +
			                     "   java.log(message);\n" + "};");
		});
		
	} // Listeners
	
	public void setFormatters() {
		// Set polygon and hole buttons to the colour values of the polygons and holes on the map
		// To make it clearer which is polygon and which is hole
		GuiUtilities.setFontColourToContrastBackground(newPolygonButton, config.getProperty("route_colour"));
		GuiUtilities.setFontColourToContrastBackground(closePolygonButton, config.getProperty("route_colour"));
		GuiUtilities.setFontColourToContrastBackground(cancelPolygonButton, config.getProperty("route_colour"));
		GuiUtilities.setFontColourToContrastBackground(deletePolygonButton, config.getProperty("route_colour"));
		
		GuiUtilities.setFontColourToContrastBackground(newHoleButton, config.getProperty("hole_colour"));
		GuiUtilities.setFontColourToContrastBackground(cancelHoleButton, config.getProperty("hole_colour"));
		GuiUtilities.setFontColourToContrastBackground(closeHoleButton, config.getProperty("hole_colour"));
		GuiUtilities.setFontColourToContrastBackground(deleteHoleButton, config.getProperty("hole_colour"));
	}
	
	private void removeFirebug() {
		try {
			engine.executeScript(
					"var frame = document.getElementById('FirebugUI');frame.parentNode.removeChild(frame);\n");
		} catch (Exception e) {
			System.out.println("FirebugUI : " + e.getMessage());
		}
	}
	
	public void createPolygonLayer(Boolean closeBool, Boolean displayOnlyBool) {
		Polygon originalPoly = new Polygon();
		originalPoly.setGeometry(multigon);
		MapUtilities.drawGeometry(engine, originalPoly, closeBool, !closeBool, displayOnlyBool);
	}
	
	public void useMapClickedCoordinates(LatLng latLng, Boolean displayOnlyBool) {
		
		// If a polygon is being added to the map
		// and is contained within another polygon
		// say no no no
		if (newPolygonBool && !newHoleBool && !deletePolygonBool && !deleteHoleBool) {
			addNewPolygonPoint(latLng);
			
			// Check if hole is being added to the map
			// If hole has 0 points (from holeVertexNumber), check if vertex is contained within a polygon
			// If it is, add it to the polygon as a child simple polygon and holeVertexNumber += 1
		} else if (newHoleBool && !newPolygonBool && holeVertexNumber == 0 && !deletePolygonBool && !deleteHoleBool) {
			addFirstNewHolePoint(latLng);
			
			// Check if hole is being added to the map
			// If hole has more than 0 points, check if vertex is contained within the correct polygon
			// If it is, add it
		} else if (newHoleBool && !newPolygonBool && holeVertexNumber > 0) {
			addNewHolePoint(latLng);
			
			// Check if polygon is being deleted
		} else if (deletePolygonBool) {
			deletePolygon(latLng);
			createPolygonLayer(true, displayOnlyBool);
			// Check if hole is being deleted
		} else if (deleteHoleBool) {
			deleteHole(latLng, displayOnlyBool);
		}
		
	}
	
	public void setInfoLabel(String info) {
		infoLabel.setText(info);
	}
	
	public void setInfoLabel(String info, int milliseconds) {
		
		infoLabel.setText(info);
		if (milliseconds != 0) {
			Timeline timeline = new Timeline(new KeyFrame(
					Duration.millis(milliseconds),
					ae -> setInfoLabel("")));
			timeline.play();
		}
	}
	
	public void getLatLongPair(Boolean displayOnlyBool) {
		LatLng oldLatLng = new LatLng(-1000, -1000);
		
		if (bridge.getLog() != null && bridge.getLog().contains("MAP CLICKED")) {
			try {
				LatLng latLng = decodeBridge(bridge.getLog());
				int compVal = Double.compare(latLng.getLng(), oldLatLng.getLng());
				if (compVal != 0) {
					oldLatLng.setLatLng(latLng);
					useMapClickedCoordinates(latLng, displayOnlyBool);
				}
			} catch (Exception e) {
				System.out.println("Error getting JS log from bridge" + e);
			}
		}
	}
	
	public void createPolygonLayer() {
		createPolygonLayer(false, false);
	}
	
	public void setParentController(MapParentController controller) {
		this.controller = controller;
	}
	
	private LatLng decodeBridge(String logText) {
		// Split the console string
		String[] splitText = logText.split(":");
		String[] latAndLong = splitText[3].split(",");
		// Create lat and long from the split
		Double latCoord = Double.parseDouble(latAndLong[0].substring(1, latAndLong[0].length()));
		Double longCoord = Double.parseDouble(latAndLong[1].substring(0, latAndLong[1].length() - 2));
		System.out.println("Long: " + longCoord + " Lat: " + latCoord);
		return new LatLng(longCoord, latCoord);
	}
	
	private Boolean isLatLngContainedInPolygon(LatLng latLng) {
		for (int complexPolygonNumber = 0;
		     complexPolygonNumber < multigon.complexPolygonCount() - 1; complexPolygonNumber++) {
			if (multigon.simplePolygonContains(complexPolygonNumber, 0, latLng)) {
				return true;
			}
		}
		return false;
	}
	
	private void addNewPolygonPoint(LatLng latLng) {
		
		// Add to map if not contained in another polygon
		if (!isLatLngContainedInPolygon(latLng)) {
			multigon.getGeometry().get(currentComplexPolygon).get(currentSimplePolygon).addVertex(latLng);
			
			addActionToUserActionList(new PolygonMapUserAction(
					PolygonMapUserAction.Action.POINT_ADDED,
					currentComplexPolygon,
					currentSimplePolygon,
					multigon.simplePolygonVertexCount(currentComplexPolygon, currentSimplePolygon),
					multigon.getLastVertex(currentComplexPolygon, currentSimplePolygon)));
			
		} else {
			setInfoLabel("A polygon may not contain a vertex from another polygon.", INFO_LABEL_TIMER);
		}
	}
	
	private Boolean isLatLngContainedInHole(LatLng latLng, int complexPolygonNumber) {
		for (int simplePolygonNumber = 1; simplePolygonNumber < multigon.simplePolygonCount(
				complexPolygonNumber); simplePolygonNumber++) {
			if (multigon.simplePolygonContains(complexPolygonNumber, 0, latLng) && multigon.simplePolygonContains(
					complexPolygonNumber, simplePolygonNumber, latLng)) {
				return false;
			}
		}
		
		return true;
	}
	
	private void addFirstNewHolePoint(LatLng latLng) {
		Boolean containedBool = false; // If it is contained within ANY parent polygon
		String errorMessage = "";
		
		for (int complexPolygonNumber = 0;
		     complexPolygonNumber < multigon.complexPolygonCount(); complexPolygonNumber++) {
			if (multigon.simplePolygonContains(complexPolygonNumber, 0, latLng)) {
				containedBool = isLatLngContainedInHole(latLng, complexPolygonNumber);
			} else {
				errorMessage = "A hole must be contained within a polygon.";
			}
			if (containedBool) {
				currentComplexPolygon = complexPolygonNumber;
				currentSimplePolygon = multigon.simplePolygonCount(complexPolygonNumber);
				break;
			}
		}
		if (containedBool) {
			SimplePolygon hole = new SimplePolygon();
			hole.addVertex(latLng);
			multigon.addSimplePolygon(currentComplexPolygon, hole);
			holeVertexNumber += 1;
			addActionToUserActionList(
					new PolygonMapUserAction((PolygonMapUserAction.Action.NEW_HOLE), currentComplexPolygon,
					                         currentSimplePolygon));
			addActionToUserActionList(
					new PolygonMapUserAction(PolygonMapUserAction.Action.POINT_ADDED, currentComplexPolygon,
					                         currentSimplePolygon,
					                         multigon.simplePolygonVertexCount(currentComplexPolygon,
					                                                           currentSimplePolygon)));
		} else {
			setInfoLabel(errorMessage, INFO_LABEL_TIMER);
		}
	}
	
	private void addNewHolePoint(LatLng latLng) {
		Boolean containedBool = false;
		if (multigon.simplePolygonContains(currentComplexPolygon, 0, latLng)) {
			containedBool = true;
			for (int simplePolygonNumber = 1; simplePolygonNumber < multigon.simplePolygonCount(currentComplexPolygon)
			                                                        - 1; simplePolygonNumber++) {
				if (multigon.simplePolygonContains(currentComplexPolygon, simplePolygonNumber, latLng)) {
					containedBool = false;
				}
			}
		}
		if (containedBool) {
			multigon.getSimplePolygon(currentComplexPolygon, currentSimplePolygon).addVertex(latLng);
			holeVertexNumber += 1;
			
			addActionToUserActionList(
					new PolygonMapUserAction(PolygonMapUserAction.Action.POINT_ADDED, currentComplexPolygon,
					                         currentSimplePolygon,
					                         multigon.simplePolygonVertexCount(currentComplexPolygon,
					                                                           currentSimplePolygon), latLng));
		} else {
			setInfoLabel("A hole must not be contained by another hole.", INFO_LABEL_TIMER);
		}
	}
	
	private void deletePolygon(LatLng latLng) {
		deletePolygonBool = false;
		
		for (int complexPolygonNumber = 0;
		     complexPolygonNumber < multigon.complexPolygonCount(); complexPolygonNumber++) {
			System.out.println(multigon.simplePolygonContains(complexPolygonNumber, 0, latLng));
			if (multigon.simplePolygonContains(complexPolygonNumber, 0, latLng)) {
				
				addActionToUserActionList(new PolygonMapUserAction(PolygonMapUserAction.Action.DELETE_POLYGON,
				                                                   complexPolygonNumber,
				                                                   multigon.getGeometry().get(complexPolygonNumber)));
				
				multigon.removeComplexPolygon(complexPolygonNumber);
			}
		}
	}
	
	private void deleteHole(LatLng latLng, Boolean displayOnlyBool) {
		deleteHoleBool = false;
		
		for (int complexPolygonNumber = 0;
		     complexPolygonNumber < multigon.complexPolygonCount(); complexPolygonNumber++) {
			for (int simplePolygonNumber = 1; simplePolygonNumber < multigon.simplePolygonCount(
					complexPolygonNumber); simplePolygonNumber++) {
				if (multigon.simplePolygonContains(complexPolygonNumber, simplePolygonNumber, latLng)) {
					
					addActionToUserActionList(new PolygonMapUserAction(
							PolygonMapUserAction.Action.DELETE_HOLE,
							complexPolygonNumber,
							simplePolygonNumber,
							multigon.getGeometry().get(complexPolygonNumber).get(simplePolygonNumber)));
					
					multigon.removeSimplePolygon(complexPolygonNumber, simplePolygonNumber);
				}
			}
		}
		createPolygonLayer(true, displayOnlyBool);
	}
	
	private void undoClearGeometry(PolygonMapUserAction userAction) {
		multigon = userAction.getGeometry();
		createPolygonLayer();
	}
	
	private void undoNewPolygon(PolygonMapUserAction userAction) {
		
		if (userAction.getCurrentComplexPolygon() > 0) {
			multigon.getGeometry().get(userAction.getCurrentComplexPolygon()).clear();
			multigon.getGeometry().remove(userAction.getCurrentComplexPolygon());
		}
	}
	
	private void undoNewHole(PolygonMapUserAction userAction) {
		
		if (userAction.getCurrentSimplePolygon() > 0) {
			multigon.getGeometry().get(userAction.getCurrentComplexPolygon()).remove(
					userAction.getCurrentSimplePolygon());
		}
	}
	
	// UNDO  & REDO
	
	private void undoPointAdded(PolygonMapUserAction userAction) {
		
		System.out.println("!!!!! Current Complex Polygon: " + userAction.getCurrentComplexPolygon()
		                   + " \n!!!!! Curren Simple Polygon: " + userAction.getCurrentSimplePolygon()
		                   + "\n!!!!! Current Vertex: " + userAction.getCurrentVertex());
		
		if (multigon.simplePolygonVertexCount(userAction.getCurrentComplexPolygon(),
		                                      userAction.getCurrentSimplePolygon()) > 0) {
			multigon.removeLastVertex(userAction.getCurrentComplexPolygon(), userAction.getCurrentSimplePolygon());
		}
	}
	
	private void undoAction(PolygonMapUserAction userAction) {
		
		switch (userAction.getAction()) {
			
			case CLEAR_GEOMETRY:
				undoClearGeometry(userAction);
			
			case NEW_POLYGON:
				undoNewPolygon(userAction);
				break;
			
			case NEW_HOLE:
				undoNewHole(userAction);
				break;
			
			case POINT_ADDED:
				undoPointAdded(userAction);
				break;
		}
		
	}
	
	private void setButtonsToStatePriorToAddingPoint() {
		// Set buttons to where they would have been before action (not counting when vertices have been added to the geometry)
		for (int i = userActionList.size() - 1; i <= 0; i--) {
			{
				if (userActionList.get(i).getAction() != PolygonMapUserAction.Action.POINT_ADDED) {
					setButtons(userActionList.get(i).getAction());
					createPolygonLayer(false, false);
					return;
				}
			}
		}
		//        setButtons(PolygonMapUserAction.CLEAR_GEOMETRY);
		// Refresh the map
		createPolygonLayer(false, false);
	}
	
	private void undoLastActionOfMany() {
		// Undo action, remove from action list, add to undone action list
		
		Boolean isLastVertex = false;
		
		PolygonMapUserAction currentAction = userActionList.get(userActionList.size() - 1);
		
		
		if (currentAction.getAction() == PolygonMapUserAction.Action.POINT_ADDED
		    && currentAction.getCurrentVertex() == 0) {
			isLastVertex = true;
		} else if (currentAction.getAction() == PolygonMapUserAction.Action.NEW_HOLE) {
			isLastVertex = true;
		} else if (currentAction.getAction() == PolygonMapUserAction.Action.NEW_HOLE) {
			isLastVertex = true;
		}
		
		undoAction(currentAction);
		userActionUndoList.add(currentAction);
		userActionList.remove(userActionList.size() - 1);
		
		setButtonsToStatePriorToAddingPoint();
		
		if (isLastVertex) {
			undoLastAction();
		}
		
	}
	
	private void undoLastActionOfOne() {
		// Undo action, remove from action list, add to undone action list
		undoAction(userActionList.get(userActionList.size() - 1));
		userActionUndoList.add(userActionList.get(userActionList.size() - 1));
		userActionList.remove(userActionList.size() - 1);
		
		// Set buttons to where they would have been before action
		setButtons(PolygonMapUserAction.Action.CLEAR_GEOMETRY);
	}
	
	private void undoLastAction() {
		// If more than one action has been undone
		if (userActionList.size() > 1) {
			// DIAGNOSTICS
			System.out.println(userActionList.get(userActionList.size() - 1).toString());
			undoLastActionOfMany();
			
			// If only one action has been undone
		} else if (userActionList.size() == 1) {
			// DIAGNOSTICS
			System.out.println(userActionList.get(userActionList.size() - 1).toString());
			undoLastActionOfOne();
		} else {
			setButtons(PolygonMapUserAction.Action.CLEAR_GEOMETRY);
		}
	}
	
	@FXML
	private void saveGeometryButtonPressed(ActionEvent event) {
		setLastAction(PolygonMapUserAction.Action.SAVE_GEOMETRY);
		
		for (int i = 0; i < multigon.complexPolygonCount(); i++) {
			System.out.println("Complex polygon: " + i);
			for (int j = 0; j < multigon.simplePolygonCount(i); j++) {
				System.out.println("Complex polygon " + i + ", simple polygon " + j);
			}
		}
		controller.polygonDrawn(multigon);
		Stage stage = (Stage) saveGeometryButton.getScene().getWindow();
		stage.close();
	}
	
	private void setLastAction(PolygonMapUserAction.Action userAction) {
		setButtons(userAction);
		
		switch (userAction) {
			
			case CLEAR_GEOMETRY:
				addActionToUserActionList(new PolygonMapUserAction(userAction, multigon));
				break;
			
			case NEW_POLYGON:
				addActionToUserActionList(
						new PolygonMapUserAction(userAction, currentComplexPolygon, currentSimplePolygon));
				break;
			
			case CLOSE_POLYGON:
				addActionToUserActionList(new PolygonMapUserAction(userAction, currentComplexPolygon, 0));
				break;
			
			//            case CLOSE_HOLE:
			//                addActionToUserActionList(new PolygonMapUserAction(userAction, currentComplexPolygon,currentSimplePolygon));
			//                break;
		}
	}
	
	private void setButtons(PolygonMapUserAction.Action buttonPressed) {
		List disableList = new ArrayList<>();
		List enableList = new ArrayList<>();
		
		switch (buttonPressed) {
			case START:
			case SAVE_GEOMETRY:
				break;
			
			case CLEAR_GEOMETRY:
				disableList.add(saveGeometryButton);
				disableList.add(clearGeometryButton);
				disableList.add(closePolygonButton);
				disableList.add(deletePolygonButton);
				disableList.add(newHoleButton);
				disableList.add(closeHoleButton);
				disableList.add(deleteHoleButton);
				disableList.add(undoButton);
				
				enableList.add(cancelButton);
				enableList.add(newPolygonButton);
				break;
			
			case CANCEL:
				break;
			
			case UNDO:
				enableList.add(redoButton);
				break;
			
			case REDO:
				// Check here to see if redo is behind undo.
				break;
			
			case NEW_POLYGON:
				disableList.add(saveGeometryButton);
				disableList.add(newPolygonButton);
				disableList.add(deletePolygonButton);
				disableList.add(newHoleButton);
				disableList.add(closeHoleButton);
				disableList.add(deleteHoleButton);
				
				enableList.add(clearGeometryButton);
				enableList.add(cancelButton);
				enableList.add(undoButton);
				enableList.add(closePolygonButton);
				break;
			
			case CLOSE_POLYGON:
				disableList.add(closePolygonButton);
				disableList.add(closeHoleButton);
				disableList.add(undoButton);
				
				enableList.add(saveGeometryButton);
				enableList.add(clearGeometryButton);
				enableList.add(cancelButton);
				enableList.add(newPolygonButton);
				enableList.add(deletePolygonButton);
				enableList.add(newHoleButton);
				enableList.add(deleteHoleButton);
				break;
			
			case DELETE_POLYGON:
				disableList.add(closePolygonButton);
				disableList.add(closeHoleButton);
				disableList.add(undoButton);
				
				enableList.add(saveGeometryButton);
				enableList.add(clearGeometryButton);
				enableList.add(cancelButton);
				enableList.add(newPolygonButton);
				enableList.add(deletePolygonButton);
				enableList.add(newHoleButton);
				enableList.add(deleteHoleButton);
				break;
			
			case NEW_HOLE:
				disableList.add(saveGeometryButton);
				disableList.add(newPolygonButton);
				disableList.add(closePolygonButton);
				disableList.add(deletePolygonButton);
				disableList.add(newHoleButton);
				disableList.add(deleteHoleButton);
				
				enableList.add(clearGeometryButton);
				enableList.add(cancelButton);
				enableList.add(undoButton);
				enableList.add(closeHoleButton);
				break;
			
			case CLOSE_HOLE:
				disableList.add(closePolygonButton);
				disableList.add(closeHoleButton);
				disableList.add(undoButton);
				
				enableList.add(saveGeometryButton);
				enableList.add(clearGeometryButton);
				enableList.add(cancelButton);
				enableList.add(newPolygonButton);
				enableList.add(deletePolygonButton);
				enableList.add(newHoleButton);
				enableList.add(deleteHoleButton);
				break;
			
			case DELETE_HOLE:
				disableList.add(closePolygonButton);
				disableList.add(closeHoleButton);
				disableList.add(undoButton);
				
				enableList.add(saveGeometryButton);
				enableList.add(clearGeometryButton);
				enableList.add(cancelButton);
				enableList.add(newPolygonButton);
				enableList.add(deletePolygonButton);
				enableList.add(newHoleButton);
				enableList.add(deleteHoleButton);
				break;
			
			case MOUSE_CLICKED:
				break;
		}
		
		NodeUtilities.setAllNodesDisabled(disableList, true);
		NodeUtilities.setAllNodesDisabled(enableList, false);
	}
	
	private void addActionToUserActionList(PolygonMapUserAction userAction) {
		System.out.println(userAction.toString());
		userActionList.add(userAction);
	}
	
	@FXML
	private void cancelButtonPressed(ActionEvent event) {
		setLastAction(PolygonMapUserAction.Action.CANCEL);
		Stage stage = (Stage) cancelButton.getScene().getWindow();
		stage.close();
	}
	
	@FXML
	private void undoButtonPressed(ActionEvent event) {
		undoLastAction();
	}
	
	@FXML
	private void redoButtonPressed(ActionEvent event) {
		redoLastUndoneAction();
	}
	
	// FXML
	
	private void redoLastUndoneAction() {
		redoAction(userActionUndoList.get(userActionUndoList.size() - 1));
	}
	
	private void redoAction(PolygonMapUserAction userActionUndo) {
	
	}
	
	@FXML
	private void newPolygonButtonPressed(ActionEvent event) {
		//        removeFirebug();
		
		this.newPolygonBool = true;
		
		// Get current complex count of polygon
		currentComplexPolygon = getCurrentComplexPolygon();
		
		// set current simple polygon to 0 (first element is parent polygon)
		currentSimplePolygon = 0;
		
		//        System.out.println("currentComplexPolygon: " + currentComplexPolygon + " currentSimplePolygon: " + currentSimplePolygon);
		
		// Create the parent polygon
		multigon.getGeometry().add(new ArrayList<SimplePolygon>());
		multigon.getGeometry().get(currentComplexPolygon).add(new SimplePolygon());
		
		setLastAction(PolygonMapUserAction.Action.NEW_POLYGON);
	}
	
	private int getCurrentComplexPolygon() {
		if (multigon.getGeometry().isEmpty()) {
			currentComplexPolygon = 0;
		} else {
			currentComplexPolygon = multigon.complexPolygonCount();
		}
		return currentComplexPolygon;
	}
	
	@FXML
	private void closePolygonButtonPressed(ActionEvent event) {
		this.newPolygonBool = false;
		
		if (multigon.simplePolygonVertexCount(currentComplexPolygon, currentSimplePolygon) > 0) {
			createPolygonLayer(true, false);
			saveGeometryButton.setDisable(false);
		}
		
		setLastAction(PolygonMapUserAction.Action.CLOSE_POLYGON);
	}
	
	@FXML
	private void cancelPolygonButtonPressed(ActionEvent event) {
		setLastAction(PolygonMapUserAction.Action.CANCEL_POLYGON);
	}
	
	@FXML
	private void deletePolygonButtonPressed(ActionEvent event) {
		this.newPolygonBool = false;
		this.deletePolygonBool = true;
		
		setLastAction(PolygonMapUserAction.Action.DELETE_POLYGON);
	}
	
	@FXML
	private void newHoleButtonPressed(ActionEvent event) {
		this.newHoleBool = true;
		holeVertexNumber = 0;
		
		setLastAction(PolygonMapUserAction.Action.NEW_HOLE);
	}
	
	@FXML
	private void closeHoleButtonPressed(ActionEvent event) {
		this.newHoleBool = false;
		holeVertexNumber = -1;
		
		createPolygonLayer(true, false);
		setLastAction(PolygonMapUserAction.Action.CLOSE_HOLE);
	}
	
	@FXML
	private void cancelHoleButtonPressed(ActionEvent event) {
		this.deleteHoleBool = false;
		setLastAction(PolygonMapUserAction.Action.CANCEL_HOLE);
	}
	
	@FXML
	private void deleteHoleButtonPressed(ActionEvent event) {
		this.newHoleBool = false;
		this.deleteHoleBool = true;
		setLastAction(PolygonMapUserAction.Action.DELETE_HOLE);
		
	}
	
	@FXML
	private void clearGeometryButtonPressed(ActionEvent event) {
		clearMap();
		setLastAction(PolygonMapUserAction.Action.CLEAR_GEOMETRY);
	}
	
	void clearMap() {
		try {
			//            engine.executeScript("linkLayer.removeFeatures([shape]);");
			//            engine.executeScript("markers.clearMarkers();");
			MapUtilities.clearPolygonMap(engine);
		} catch (Exception e) {
			System.out.println("already empty :" + e.getMessage());
		}
		multigon.clearGeometry();
	}
	
	@FXML
	private void mouseClicked(MouseEvent event) {
		setLastAction(PolygonMapUserAction.Action.MOUSE_CLICKED);
	}
}