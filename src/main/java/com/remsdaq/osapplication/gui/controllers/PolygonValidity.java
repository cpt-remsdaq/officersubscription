package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.other.UserMessage;
import com.remsdaq.osapplication.utilities.StringUtilities;

import java.util.HashMap;
import java.util.Map;

public class PolygonValidity {
	
	Polygon polygon;
	
	
	private Map<String,String> invalidMap = new HashMap<>();
	
	public Map<String, String> getInvalidMap() {
		return invalidMap;
	}
	
	public Boolean isValid(Polygon polygon) {
		this.polygon = polygon;
		
		addIdMessageIfInvalid(polygon);
		addNameMessageIfInvalid(polygon);
		addGeometryMessageIfInvalid(polygon);
		
		return invalidMap.size() == 0;
	}
	
	private void addGeometryMessageIfInvalid(Polygon polygon) {
		if (polygon.getGeometry() == null || polygon.getGeometry().totalPolygonCount() == 0) {
			invalidMap.put("missingGeometry", UserMessage.MISSING_POLYGON_GEOMETRY);
		}
	}
	
	private void addNameMessageIfInvalid(Polygon polygon) {
		if (StringUtilities.isEmptyNullOrBlank(polygon.getName())) {
			invalidMap.put("missingName", UserMessage.MISSING_POLYGON_NAME);
		}
	}
	
	private void addIdMessageIfInvalid(Polygon polygon) {
		if (StringUtilities.isEmptyNullOrBlank(polygon.getID())) {
			invalidMap.put("missingID", UserMessage.MISSING_POLYGON_ID);
		}
	}
	
}
