package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.DatabaseUtilities;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.gui.SetField;
import com.remsdaq.osapplication.gui.entities.SSPSubscriber;
import com.remsdaq.osapplication.gui.main_application.SearchSubscribers;
import com.remsdaq.osapplication.utilities.NodeUtilities;
import com.remsdaq.osapplication.utilities.gui.CreateYesNoOptionDialog;
import com.remsdaq.osapplication.utilities.gui.GuiUtilities;
import com.sun.javafx.scene.control.behavior.TextAreaBehavior;
import com.sun.javafx.scene.control.skin.TextAreaSkin;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.*;

@SuppressWarnings("unchecked")
public class PolygonsFormController implements Initializable {
	
	@FXML private TableColumn<?, ?> polygonsSubscribersSelectedColumn;
	@FXML private TableView polygonsSubscribersTableView;
	@FXML private TextField polygonIDField;
	@FXML private TextField polygonsNameField;
	@FXML private TextArea polygonsDescriptionField;
	@FXML private TextField polygonsFormSearchField;
	@FXML private ComboBox polygonAddTagCombobox;
	@FXML private ListView polygonTagsListView;
	
	
	private PolygonsTabPaneController mainController;
	private GlobalContext globalContext = GlobalContext.getInstance();
	private Config config = GlobalContext.getInstance().getConfig();
	
	private final int POLYGON_ID_LENGTH = Integer.parseInt(config.getProperty("polygon_ID_limit"));
	private final int POLYGON_NAME_LENGTH = Integer.parseInt(config.getProperty("polygons_name_limit"));
	private final int POLYGON_DESCRIPTION_LENGTH = Integer.parseInt(config.getProperty("polygons_description_limit"));
	
	private final Boolean REQUIRE_CONFIRMATION = Boolean.parseBoolean(
			config.getProperty("require_confirmation_boolean"));
	private final Boolean REQUIRE_SAVE_CONFIRMATION = Boolean.parseBoolean(
			config.getProperty("require_save_confirmation_boolean"));
	private final Boolean REQUIRE_CANCEL_CONFIRMATION = Boolean.parseBoolean(
			config.getProperty("require_cancel_confirmation_boolean"));
	@FXML private GridPane polygonsFormGridPane;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		globalContext.setSubscribers(selectPolygonsSubscribersFromDatabase());
		globalContext.setPolygonTags(selectPolygonTagsFromDatabase());
		populatePolygonsSubscribersTable(GlobalContext.getInstance().getSubscribers());
		polygonsSubscribersSelectedColumn.setCellFactory(tc -> new CheckBoxTableCell<>());
		
		startListeners();
		setFormatters();
	}
	
	private ArrayList<Subscriber> selectPolygonsSubscribersFromDatabase() {
		return (ArrayList<Subscriber>) DatabaseUtilities.selectAll(globalContext.getDatabaseConnection(),
		                                                           new Subscriber());
	}
	
	private ArrayList<PolygonTag> selectPolygonTagsFromDatabase() {
		return (ArrayList<PolygonTag>) DatabaseUtilities.selectAll(globalContext.getDatabaseConnection(),
		                                                           new PolygonTag());
	}
	
	private void populatePolygonsSubscribersTable(ArrayList<Subscriber> subArray) {
		ObservableList polygonsSubscribersData = getPolygonSubscribersTableData(subArray);
		polygonsSubscribersTableView.getItems().setAll(polygonsSubscribersData);
	}
	
	private void startListeners() {
		polygonsSubscribersSearchChangeListener();
		polygonsAddTagChangeListener();
		tabInDescriptionListener();
	}
	
	private void setFormatters() {
		SetField.setFieldUpper(polygonIDField);
		SetField.setFieldUpper(polygonAddTagCombobox.getEditor());
		
		SetField.setFieldCapitalised(polygonsNameField);
		
		SetField.setFieldLength(polygonIDField, POLYGON_ID_LENGTH);
		SetField.setFieldLength(polygonsNameField, POLYGON_NAME_LENGTH);
		SetField.setFieldLength(polygonsDescriptionField, POLYGON_DESCRIPTION_LENGTH);
		
	}
	
	private ObservableList getPolygonSubscribersTableData(ArrayList<Subscriber> subArray) {
		List list = new ArrayList();
		subArray.forEach((sub) -> list.add(new SSPSubscriber(sub)));
		ObservableList data = FXCollections.observableList(list);
		return data;
	}
	
	private void polygonsSubscribersSearchChangeListener() {
		polygonsFormSearchField.textProperty().addListener(
				(observable, oldText, newText) -> polygonFormSearchChanged());
	}
	
	private void polygonsAddTagChangeListener() {
		polygonAddTagCombobox.getEditor().textProperty().addListener((observable, oldText, newText) -> {
			try {
				polygonAddTagComboboxChanged();
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}
	
	private void tabInDescriptionListener() {
		polygonsDescriptionField.addEventFilter(KeyEvent.KEY_PRESSED, (KeyEvent event) -> {
			if (event.getCode() == KeyCode.TAB) {
				TextAreaSkin skin = (TextAreaSkin) polygonsDescriptionField.getSkin();
				if (skin.getBehavior() instanceof TextAreaBehavior) {
					TextAreaBehavior behavior = skin.getBehavior();
					if (event.isControlDown()) {
						behavior.callAction("InsertTab");
					} else if (event.isShiftDown()) {
						behavior.callAction("TraversePrevious");
					} else {
						behavior.callAction("TraverseNext");
					}
					event.consume();
				}
			}
		});
	}
	
	@FXML
	private void polygonFormSearchChanged() {
		String searchText = polygonsFormSearchField.getText();
		mainController.setInfoLabelWithDefaultTimer("Searching subscribers: " + searchText);
		ArrayList<Subscriber> searchSubscribers = SearchSubscribers.search(searchText, GlobalContext.getInstance()
		                                                                                            .getSubscribers());
		populatePolygonsSubscribersTable(searchSubscribers);
		populatePolygonSubscribersChecks();
		polygonsSubscribersTableView.getSelectionModel().clearAndSelect(0);
	}
	
	@FXML
	private void polygonAddTagComboboxChanged() throws Exception {
		Object selected = polygonAddTagCombobox.getSelectionModel().getSelectedItem();
		String editorText = polygonAddTagCombobox.getEditor().getText();
		if (!editorText.equals("")) {
			Platform.runLater(() -> {
				try {
					clearPolygonAddTags();
				} catch (Exception e) {
					e.printStackTrace();
				}
				populatePolygonAddTags();
				if (selected == null) {
					ObservableList allTags = polygonAddTagCombobox.getItems();
					ObservableList tagsToAdd = FXCollections.observableArrayList();
					allTags.forEach((tagToAdd) -> {
						if (tagToAdd.toString().toUpperCase().startsWith(
								polygonAddTagCombobox.getEditor().getText().toUpperCase())) {
							tagsToAdd.add(tagToAdd);
						}
					});
					polygonAddTagCombobox.setItems(tagsToAdd);
					polygonAddTagCombobox.show();
				} else {
					polygonAddTagCombobox.getEditor().setText(selected.toString());
				}
			});
		} else {
			clearPolygonAddTags();
			populatePolygonAddTags();
		}
	}
	
	private void populatePolygonSubscribersChecks() {
		if (GlobalContext.getInstance().getPolygon().getSubscribers() != null) {
			for (Subscriber subscriber : GlobalContext.getInstance().getPolygon().getSubscribers()) {
				
				String subscriberID = subscriber.getID();
				subscriberID = subscriberID.replaceAll("\\s+", "");
				
				for (Object object : polygonsSubscribersTableView.getItems()) {
					SSPSubscriber tableRow = (SSPSubscriber) object;
					if (tableRow.getSubID().equals(subscriberID)) {
						tableRow.setSelected(true);
					}
				}
			}
		}
	}
	
	private void clearPolygonAddTags() {
		polygonAddTagCombobox.getItems().clear();
	}
	
	private void populatePolygonAddTags() {
		ObservableList<String> tagList = FXCollections.observableArrayList();
		
		for (PolygonTag polygonTag : GlobalContext.getInstance().getPolygonTags()) {
			tagList.add(polygonTag.getName());
		}
		
		if (!tagList.isEmpty() && tagList != null) {
			polygonAddTagCombobox.setItems(tagList);
		}
	}
	
	public void injectMainController(PolygonsTabPaneController mainController) {
		this.mainController = mainController;
	}
	
	public void focus() {
		polygonsSubscribersSelectedColumn.setEditable(true);
		polygonIDField.requestFocus();
	}
	
	void unfocus() {
		polygonsSubscribersSelectedColumn.setEditable(false);
		polygonIDField.setEditable(true);
	}
	
	void enableIDField() {
		polygonIDField.setDisable(false);
	}
	
	void disableIDField() {
		polygonIDField.setDisable(true);
	}
	
	@FXML
	private void polygonIDFieldChanged() {
		// soon
	}
	
	@FXML
	private void savePolygonButtonPressed() throws Exception {
		
		if (!mainController.isPolygonBeingEdited()) {
			if (userConfirmedSave()) {
				Boolean correctInfoSuccess = null;
				try {
					correctInfoSuccess = insertPolygonForm();
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (correctInfoSuccess == true) {
					populatePolygonForm();
					mainController.savePolygon();
				}
			}
		} else {
			if (userConfirmedSave()) {
				Boolean correctInfoSuccess = updatePolygonForm();
				if (correctInfoSuccess == true) {
					populatePolygonForm();
					mainController.savePolygon();
				}
			}
		}
	}
	
	private boolean userConfirmedSave() {
		if (REQUIRE_CONFIRMATION || REQUIRE_SAVE_CONFIRMATION) {
			return CreateYesNoOptionDialog.create("Are you sure you want to save this polygon?", "Confirmation");
		}
		return true;
	}
	
	private Boolean insertPolygonForm() {
		Polygon insertPoly = getPolygonFromForm();
		Boolean correctInfoSuccess = checkForCorrectPolygonInfo(insertPoly);
		if (correctInfoSuccess == true) {
			correctInfoSuccess = DatabaseUtilities.insertSingle(GlobalContext.getInstance().getDatabaseConnection(),
			                                                    insertPoly);
			GlobalContext.getInstance().setPolygon(insertPoly);
			if (correctInfoSuccess == false) {
				mainController.setInfoLabelWithDefaultTimer("This polygon ID already exists in the database."
				                                            + "\nPlease choose another polygon ID");
			}
		}
		return correctInfoSuccess;
	}
	
	void populatePolygonForm() throws Exception {
		polygonIDField.setText(globalContext.getPolygon().getID());
		polygonsNameField.setText(globalContext.getPolygon().getName());
		polygonsNameField.setText(globalContext.getPolygon().getName());
		polygonsDescriptionField.setText(globalContext.getPolygon().getDescription());
		
		clearPolygonSubscribersChecks();
		populatePolygonSubscribersChecks();
		
		clearPolygonAddTags();
		populatePolygonAddTags();
		
		clearPolygonTags();
		populatePolygonTags();
	}
	
	private Boolean updatePolygonForm() {
		Polygon updatePoly = getPolygonFromForm();
		Boolean success = checkForCorrectPolygonInfo(updatePoly);
		if (success == true) {
			//            success = UpdatePolygons.updateSingle(updatePoly);
			success = DatabaseUtilities.updateSingle(GlobalContext.getInstance().getDatabaseConnection(), updatePoly);
			if (success == false) {
				String errorString = "The polygon ID already exists on the database. Please enter a different polygon ID";
				mainController.setInfoLabelWithDefaultTimer(errorString);
			}
		}
		GlobalContext.getInstance().setPolygon(updatePoly);
		return success;
	}
	
	private Polygon getPolygonFromForm() {
		Polygon formPoly = new Polygon();
		
		formPoly.setID(polygonIDField.getText());
		formPoly.setName(polygonsNameField.getText());
		formPoly.setDescription(polygonsDescriptionField.getText());
		
		for (Object object : polygonsSubscribersTableView.getItems()) {
			SSPSubscriber tableRow = (SSPSubscriber) object;
			if (tableRow.returnSelected()) {
				if (formPoly.getSubscribers() != null) {
					formPoly.getSubscribers().add(tableRow.returnSubscriber());
				} else {
					Set<Subscriber> sacraSubSet = new HashSet<>();
					sacraSubSet.add(tableRow.returnSubscriber());
					formPoly.setSubscribers(sacraSubSet);
				}
			}
		}
		
		Set<PolygonTag> polygonTagList = new HashSet<>();
		for (Object polygonTagObject : polygonTagsListView.getItems()) {
			PolygonTag polygonTag = new PolygonTag();
			polygonTag.setName(polygonTagObject.toString());
			polygonTagList.add(polygonTag);
		}
		
		formPoly.setPolygonTags(polygonTagList);
		
		try {
			formPoly.setWKTGeometry(GlobalContext.getInstance().getPolygon().getWKTGeometry());
		} catch (Exception e) {
			System.out.println("Error decoding WKT String:" + e);
		}
		return formPoly;
	}
	
	private Boolean checkForCorrectPolygonInfo(Polygon poly) {
		
		PolygonValidity pv = new PolygonValidity();
		if (!pv.isValid(poly)) {
			Map<String, String> invalidMap = pv.getInvalidMap();
			
			for (Map.Entry<String, String> kv : invalidMap.entrySet()) {
				mainController.setInfoLabelWithDefaultTimer(kv.getValue());
				switch (kv.getKey()) {
					case "missingID":
						polygonIDField.requestFocus();
						break;
					case "missingName":
						polygonsNameField.requestFocus();
						break;
				}
				return false;
			}
		}
		return true;
	}
	
	private void clearPolygonSubscribersChecks() {
		for (Object object : polygonsSubscribersTableView.getItems()) {
			SSPSubscriber tableRow = (SSPSubscriber) object;
			tableRow.setSelected(false);
		}
	}
	
	private void clearPolygonTags() {
		polygonTagsListView.getItems().clear();
	}
	
	private void populatePolygonTags() {
		
		ObservableList<String> tagList = FXCollections.observableArrayList();
		
		if (!tagList.isEmpty() && tagList != null) {
			polygonTagsListView.setItems(tagList);
		}
		
		if (GlobalContext.getInstance().getPolygon().getPolygonTags() != null) {
			for (PolygonTag polygonTag : GlobalContext.getInstance().getPolygon().getPolygonTags()) {
				polygonTagsListView.getItems().add(polygonTag.getName());
			}
		}
	}
	
	@FXML
	private void cancelPolygonButtonPressed() throws Exception {
		if (userConfirmedCancel()) {
			mainController.cancelSubscriber();
			clearPolygonForm();
		}
	}
	
	private Boolean userConfirmedCancel() {
		if (REQUIRE_CONFIRMATION || REQUIRE_CANCEL_CONFIRMATION) {
			return CreateYesNoOptionDialog.create("Are you sure you want to cancel?", "Confirmation");
		}
		return true;
	}
	
	void clearPolygonForm() {
		NodeUtilities.clearAllNodes(polygonsFormGridPane);
		clearPolygonSubscribersChecks();
		clearPolygonAddTags();
		populatePolygonAddTags();
		clearPolygonTags();
	}
	
	@FXML
	private void polygonSubscriberTableKeyPressed(KeyEvent event) {
		KeyCode keyCode = event.getCode();
		if (event.isControlDown() && keyCode == KeyCode.PAGE_DOWN) {
			mainController.ctrlPageDownPressedOnTable();
		} else if (event.isControlDown() && keyCode == KeyCode.PAGE_UP) {
			mainController.ctrlPageUpPressedOnTable();
		} else {
			GuiUtilities.typeInFieldExternally(event, polygonsFormSearchField);
		}
	}
	
	@FXML
	private void polygonAddTagKeyPressed(KeyEvent event) throws Exception {
		if (event.isControlDown() && event.getCode().equals(KeyCode.ENTER)) {
			addPolygonTagButtonPressed();
		}
	}
	
	@FXML
	private void addPolygonTagButtonPressed() throws Exception {
		if (polygonAddTagCombobox.getValue() != null) {
			
			PolygonTag sacraTag = new PolygonTag();
			String tagName = polygonAddTagCombobox.getValue().toString();
			sacraTag.setName(tagName);
			
			try {
				DatabaseUtilities.insertSingle(GlobalContext.getInstance().getDatabaseConnection(), sacraTag);
			} catch (Exception e) {
			
			}
			ObservableList<String> tagList = FXCollections.observableArrayList();
			
			if (!polygonTagsListView.getItems().isEmpty() && polygonTagsListView.getItems() != null) {
				tagList = polygonTagsListView.getItems();
			}
			tagList.add(polygonAddTagCombobox.getSelectionModel().getSelectedItem().toString());
			
			if (!tagList.isEmpty() && tagList != null) {
				polygonTagsListView.setItems(tagList);
			}
			
			polygonAddTagCombobox.getEditor().setText("");
			polygonAddTagCombobox.hide();
			clearPolygonAddTags();
			populatePolygonAddTags();
		}
	}
	
	@FXML
	private void deletePolygonTagButtonPressed() {
		int index = polygonTagsListView.getSelectionModel().getSelectedIndex();
		String item = polygonTagsListView.getSelectionModel().getSelectedItem().toString();
		
		if (item != null) {
			polygonTagsListView.getItems().remove(index);
		}
	}
}