package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.entities.polygons.ImportPolygonGeometry;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.polygons.PolygonGeometry;
import com.remsdaq.osapplication.utilities.gui.MapUtilities;
import com.remsdaq.osapplication.utilities.gui.CreateFileChooser;
import com.remsdaq.osapplication.utilities.gui.CreateTextInputDialog;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PolygonsMapController extends MapParentController implements Initializable {

	private final GlobalContext globalContext = GlobalContext.getInstance();
	private final Config config = globalContext.getConfig();
	public final String MAP_PAGE = config.getProperty("polygons_map_page");
	public final Double DEFAULT_MAP_LATITUDE = Double.parseDouble(config.getProperty("default_map_location_latitude"));
	public final Double DEFAULT_MAP_LONGITUDE = Double.parseDouble(config.getProperty("default_map_location_longitude"));
	public final int DEFAULT_MAP_ZOOM = Integer.parseInt(config.getProperty("default_map_zoom"));
	private final String[] POLYGON_FILE_FILTERS = config.getProperty("polygon_file_filters").split(",");
	@FXML private WebView polygonsWebView;
	private WebEngine engine;
	private PolygonsTabPaneController mainController;

	@Override public void initialize(URL location, ResourceBundle resources) {
		engine = polygonsWebView.getEngine();
		engine.load(MAP_PAGE);

		startListeners();
	}

	private void startListeners() {
		fireBugLoadedListener();
	}

	private void fireBugLoadedListener() {
		engine.getLoadWorker().stateProperty().addListener((obs, oldState, newState) -> {
			if (newState == Worker.State.SUCCEEDED) {
				// new page has loaded, process:
				MapUtilities.removeFirebug(engine);
			}
		});
	}

	public void clearPolygonMap() {
		MapUtilities.clearPolygonMap(engine);
		MapUtilities.zoomOut(engine);
	}

	public void injectMainController(PolygonsTabPaneController mainController) {
		this.mainController = mainController;
	}

	public void polygonTableKeyPressed(KeyEvent keyEvent) {
	}

	@Override public void polygonDrawn(PolygonGeometry polygonGeometry) {
		if (!mainController.isPolygonBeingEdited()) {
			globalContext.setPolygon(new Polygon());
		}

		GlobalContext.getInstance().getPolygon().setGeometry(polygonGeometry);
		MapUtilities.drawGeometry(engine, globalContext.getPolygon(), true);
	}

	@FXML
	private void drawPolygonButtonPressed() {
		try {
			FXMLLoader mapLoader = new FXMLLoader(getClass().getResource("/fxml/_obsolete/OSPolygonMap.fxml"));
			Parent root = mapLoader.load();
			OSPolygonMapController controller = mapLoader.getController();
			controller.setParentController(this);

			Stage mapStage = new Stage();
			mapStage.initModality(Modality.APPLICATION_MODAL);
			mapStage.initStyle(StageStyle.UTILITY);
			mapStage.setTitle("Draw Polygon");
			Scene scene = new Scene(root);
			mapStage.setScene(scene);
			mapStage.show();
		} catch (IOException ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
		}
	}

	@FXML
	private void clearGeometryButtonPressed() {
		MapUtilities.clearPolygonMap(engine);
		GlobalContext.getInstance().getPolygon().setGeometry(new PolygonGeometry());
	}

	@FXML
	private void pasteWKTGeometryButtonPressed() {
		CreateTextInputDialog wktGeometryInputDialog = new CreateTextInputDialog(globalContext.getStage());
		wktGeometryInputDialog.setLarge(true);
		wktGeometryInputDialog.setTitle("Paste Geometry");
		wktGeometryInputDialog.setHeaderText("Paste any valid WKT (Well Known Text) POLYGON or MULTIPOLYGON geometry into the input box.");
		wktGeometryInputDialog.setText("WKT:");

		String wktGeometry = wktGeometryInputDialog.create();

		if (wktGeometry != null && !wktGeometry.equals("")) {
			GlobalContext.getInstance().getPolygon().setWKTGeometry(wktGeometry);
			drawGeometry();
		} else {
			MapUtilities.clearPolygonMap(engine);
			drawGeometry();
		}
	}

	void drawGeometry() {
		if (GlobalContext.getInstance().getPolygon().getGeometry().totalVertexCount() > 0) {
			MapUtilities.drawGeometry(engine, GlobalContext.getInstance().getPolygon(), true);
		}
	}

	@FXML
	private void importPolygonButtonPressed() throws IOException {
		CreateFileChooser polyFileChooser = new CreateFileChooser(globalContext.getStage());
		polyFileChooser.setTitle("Import PolygonFile");
		polyFileChooser.setInitialDirectory(config.getProperty("initialDirectory"));
		polyFileChooser.setExtensionFilter(POLYGON_FILE_FILTERS);

		File polygonFile = polyFileChooser.create();
		if (polygonFile != null) {
			ImportPolygonGeometry polygonGeometry = new ImportPolygonGeometry(polygonFile);
			GlobalContext.getInstance().getPolygon().setWKTGeometry(polygonGeometry.getGeometry());
			drawGeometry();
		}
	}

	@FXML
	private void exportPolygonButtonPressed() throws IOException {
		CreateFileChooser polyFileChooser = new CreateFileChooser(globalContext.getStage());
		polyFileChooser.setTitle("Select a directory to export the polygon");
		polyFileChooser.setInitialDirectory(config.getProperty("initialDirectory"));
		polyFileChooser.setExtensionFilter(POLYGON_FILE_FILTERS);
		polyFileChooser.setSave(true);

		File polygonFile = polyFileChooser.create();

		FileUtils.writeStringToFile(polygonFile, GlobalContext.getInstance().getPolygon().getWKTGeometry(), "UTF-8", false);
	}
}