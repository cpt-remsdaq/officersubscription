package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.utilities.NodeUtilities;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class PolygonsTabPaneController implements Initializable {

	private MainController mainController;
	@FXML private PolygonsTableController polygonsTablePaneController;
	@FXML private PolygonsFormController polygonsFormPaneController;
	@FXML private PolygonsMapController polygonsMapPaneController;

	@FXML private AnchorPane polygonsFormPane;
	@FXML private AnchorPane polygonsTablePane;
	@FXML private AnchorPane polygonsMapPane;

	private boolean isPolygonBeingEdited;

	@Override public void initialize(URL location, ResourceBundle resources) {
		injectControllers();
	}
	
	public void refreshTab() {
		polygonsTablePaneController.populatePolygonsTableFromDatabase();
		polygonsFormPaneController.clearPolygonForm();
	}
	
	private void injectControllers() {
		polygonsTablePaneController.injectMainController(this);
		polygonsFormPaneController.injectMainController(this);
		polygonsMapPaneController.injectMainController(this);
	}

	void injectMainController(MainController mainController) {
		this.mainController = mainController;
	}

	public void ctrlPageDownPressedOnTable() {
		mainController.selectNextTab();
	}

	public void ctrlPageUpPressedOnTable() {
		mainController.selectPreviousTab();
	}

	public void editPolygon() throws Exception {
		isPolygonBeingEdited = true;
		ensurePolygonsTabIsSelected();
		setInfoLabelWithDefaultTimer("Editing Polygon");
		populatePolygonForm();
		goToPolyRHS();
		polygonsFormPaneController.disableIDField();
	}

	private void ensurePolygonsTabIsSelected() {
		mainController.ensurePolygonsTabIsSelected();
	}

	void setInfoLabelWithDefaultTimer(String info) {
		mainController.setInfoLabelWithDefaultTimer(info);
	}

	private void goToPolyRHS() {
		polygonsTablePaneController.unfocus();
		NodeUtilities.switchPanes(polygonsFormPane, polygonsTablePane);
		NodeUtilities.switchPanes(polygonsMapPane, polygonsTablePane);
		polygonsFormPaneController.focus();
	}

	public void newPolygon() throws Exception {
		isPolygonBeingEdited = false;
		setInfoLabelWithDefaultTimer("Creating new subscriber.");
		ensurePolygonsTabIsSelected();
		goToPolyRHS();
		clearPolygonForm();
		polygonsFormPaneController.enableIDField();
	}

	public void clearPolygonForm() throws Exception {
		polygonsFormPaneController.clearPolygonForm();
	}

	public void clearPolygonMap() {
		polygonsMapPaneController.clearPolygonMap();
	}

	void populatePolygonForm() throws Exception {
		polygonsFormPaneController.populatePolygonForm();
	}

	public boolean isPolygonBeingEdited() {
		return isPolygonBeingEdited;
	}

	public void savePolygon() {
		goToPolyLHS();
		setInfoLabelWithDefaultTimer("Saving Polygon.");
		polygonsTablePaneController.populatePolygonsTableFromDatabase();
	}

	private void goToPolyLHS() {
		polygonsFormPaneController.unfocus();
		NodeUtilities.switchPanes(polygonsTablePane, polygonsFormPane);
		polygonsTablePaneController.focus();
	}

	public void cancelSubscriber() {
		setInfoLabelWithDefaultTimer("Cancelling.");
		goToPolyLHS();
	}

	void polyonTableViewChanged() throws Exception {
		clearPolygonForm();
		populatePolygonForm();
		polygonsMapPaneController.drawGeometry();
	}
}
