package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.DatabaseUtilities;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.gui.entities.TableViewPolygon;
import com.remsdaq.osapplication.gui.main_application.SearchPolygons;
import com.remsdaq.osapplication.utilities.gui.CreateYesNoOptionDialog;
import com.remsdaq.osapplication.utilities.gui.GuiUtilities;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javafx.collections.FXCollections.observableList;

public class PolygonsTableController implements Initializable {

	@FXML private TableView<?> polygonsTableView;
	@FXML private TextField polygonsTableSearchField;
	@FXML private AnchorPane polygonsTablePane;
	@FXML private Button polygonsEditPolygonsButton;
	@FXML private Button polygonsDeletePolygonsButton;

	private PolygonsTabPaneController mainController;
	private GlobalContext globalContext = GlobalContext.getInstance();
	private Config config = GlobalContext.getInstance().getConfig();

	private final Boolean REQUIRE_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_confirmation_boolean"));
	private final Boolean REQUIRE_EDIT_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_edit_confirmation_boolean"));
	private final Boolean REQUIRE_DELETE_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_delete_confirmation_boolean"));

	public void injectMainController(PolygonsTabPaneController mainController) {
		this.mainController = mainController;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		GlobalContext.getInstance().setPolygons((ArrayList<Polygon>) DatabaseUtilities.selectAll(GlobalContext.getInstance().getDatabaseConnection(), new Polygon()));
		populatePolygonsTable(GlobalContext.getInstance().getPolygons());
		startListeners();
	}

	private void populatePolygonsTable(ArrayList<Polygon> polyArray) {
		List polygonsData = getPolygonsTableData(polyArray);
		polygonsTableView.getItems().setAll(polygonsData);
	}

	private void startListeners() {
		tableViewChangeListener();
		tableSearchFieldChangeListener();
	}

	private ObservableList getPolygonsTableData(ArrayList<Polygon> polyArray) {
		List<TableViewPolygon> list = new ArrayList<>();
		polyArray.forEach((poly) -> list.add(new TableViewPolygon(poly)));
		return observableList(list);
	}

	private void tableViewChangeListener() {
		polygonsTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null && isPolygonSelected()) {
				try {
					polygonTableViewChanged();
				} catch (Exception ex) {
					Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
				}
			}
		});
	}

	private void tableSearchFieldChangeListener() {
		polygonsTableSearchField.textProperty().addListener((observable, oldText, newText) -> polygonTableSearchChanged());
	}

	private Boolean isPolygonSelected() {
		return !polygonsTableView.getSelectionModel().isEmpty();
	}

	@FXML private void polygonTableViewChanged() throws Exception {
		if (isPolygonSelected()) {
			polygonsEditPolygonsButton.disableProperty().set(false);
			polygonsDeletePolygonsButton.disableProperty().set(false);
			globalContext.setPolygon(getSelectedPolygon());
			mainController.polyonTableViewChanged();
		}
	}

	@FXML private void polygonTableSearchChanged() {
		String searchText = polygonsTableSearchField.getText();
		mainController.setInfoLabelWithDefaultTimer("Searching polygons: " + searchText);
		ArrayList<Polygon> searchPolygons = SearchPolygons.search(searchText, GlobalContext.getInstance().getPolygons());
		populatePolygonsTable(searchPolygons);
		polygonsTableView.getSelectionModel().clearAndSelect(0);
	}

	private Polygon getSelectedPolygon() throws WellKnownTextException {

		Object object = polygonsTableView.getSelectionModel().getSelectedItem();
		TableViewPolygon selectedRow = (TableViewPolygon) object;
		Polygon selectedPolygon = selectedRow.returnPolygon();
//		selectedPolygon.decodeWKTString();
		return selectedPolygon;
	}

	@FXML private void newPolygonButtonPressed() throws Exception {

		GlobalContext.getInstance().setPolygon(new Polygon());
		mainController.newPolygon();
	}

	@FXML
	private void editPolygonButtonPressed() throws Exception {
		if (isPolygonSelected()) {
			if (userConfirmedEdit()) {
				mainController.editPolygon();
			} else {
				mainController.setInfoLabelWithDefaultTimer("No subscriber selected)");
			}
		}
	}

	private boolean userConfirmedEdit() {
		if (REQUIRE_CONFIRMATION || REQUIRE_EDIT_CONFIRMATION) {
			return CreateYesNoOptionDialog.create("Are you sure you want to edit this polygon?", "Confirmation");
		}
		return true;
	}

	@FXML private void deletePolygonButtonPressed() throws Exception {
		if (isPolygonSelected()) {
			Boolean deleteBool = false;
			if (REQUIRE_CONFIRMATION || REQUIRE_DELETE_CONFIRMATION) {
				deleteBool = CreateYesNoOptionDialog.create("Are you sure you want to delete the selected polygon?", "Confirmation");
			} else {
				deleteBool = true;
			}
			if (deleteBool == true) {
				mainController.setInfoLabelWithDefaultTimer("Deleting polygon.");
				//                DeletePolygons.deleteSingle(GlobalContext.getInstance().getPolygon());
				DatabaseUtilities.deleteSingle(GlobalContext.getInstance().getDatabaseConnection(), GlobalContext.getInstance().getPolygon());
				populatePolygonsTableFromDatabase();
				mainController.clearPolygonForm();
			}
		}
	}

	void populatePolygonsTableFromDatabase() {
		ObservableList polygonsData = getInitialPolygonsTableData();
		polygonsTableView.getSelectionModel().select(0);
		polygonsTableView.getItems().setAll(polygonsData);
	}

	private ObservableList getInitialPolygonsTableData() {
		List<TableViewPolygon> list = new ArrayList<TableViewPolygon>();
		GlobalContext.getInstance().setPolygons((ArrayList<Polygon>) DatabaseUtilities.selectAll(GlobalContext.getInstance().getDatabaseConnection(), new Polygon()));
		GlobalContext.getInstance().getPolygons().forEach((poly) -> {
			list.add(new TableViewPolygon(poly));
		});
		return observableList(list);
	}

	//	@FXML private void polygonLeftSearchChanged() {
	//		String searchText = polygonsTableSearchField.getText();
	//		mainController.setInfoLabelWithDefaultTimer("Searching polygons: " + searchText);
	//		ArrayList<Polygon> searchPolygons = SearchPolygons.search(searchText, GlobalContext.getInstance().getPolygons());
	//		populatePolygonsTableFromDatabase(searchPolygons);
	//		polygonsTableView.getSelectionModel().clearAndSelect(0);
	//	}

	@FXML private void polygonTableKeyPressed(KeyEvent event) {

		KeyCode keyCode = event.getCode();
		if (event.isControlDown() && keyCode == KeyCode.PAGE_DOWN) {
			mainController.ctrlPageDownPressedOnTable();
		} else if (event.isControlDown() && keyCode == KeyCode.PAGE_UP) {
			mainController.ctrlPageUpPressedOnTable();
		} else {
			GuiUtilities.typeInFieldExternally(event, polygonsTableSearchField);
		}
	}

	public void focus() {
		polygonsTablePane.disableProperty().set(false);
		polygonsTableView.requestFocus();
	}

	public void unfocus() {
	}
}
