package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.DatabaseUtilities;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.SubscriberValidity;
import com.remsdaq.osapplication.gui.entities.TableViewPolygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.gui.SetField;
import com.remsdaq.osapplication.gui.main_application.SearchPolygons;
import com.remsdaq.osapplication.other.UserMessage;
import com.remsdaq.osapplication.utilities.NodeUtilities;
import com.remsdaq.osapplication.utilities.gui.CreateYesNoOptionDialog;
import com.remsdaq.osapplication.utilities.gui.GuiUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

import java.net.URL;
import java.util.*;

public class SubscribersFormController implements Initializable {

	GlobalContext globalContext = GlobalContext.getInstance();
	Config config = globalContext.getConfig();
	private final Boolean REQUIRE_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_confirmation_boolean"));
	private final Boolean REQUIRE_SAVE_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_save_confirmation_boolean"));
	private final Boolean REQUIRE_CANCEL_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_cancel_confirmation_boolean"));
	// TextField/TextArea length limits
	private final int SUBSCRIBERS_ID_LENGTH = Integer.parseInt(config.getProperty("subscribers_ID_limit"));
	private final int SUBSCRIBERS_FIRST_NAME_LENGTH = Integer.parseInt(config.getProperty("subscribers_first_name_limit"));
	private final int SUBSCRIBERS_SECOND_NAME_LENGTH = Integer.parseInt(config.getProperty("subscribers_second_name_limit"));
	private final int SUBSCRIBERS_EMAIL_LENGTH = Integer.parseInt(config.getProperty("subscribers_email_limit"));
	private final int SUBSCRIBERS_PAGER_LENGTH = Integer.parseInt(config.getProperty("subscribers_pager_limit"));
	private final int SUBSCRIBERS_MOBILE_LENGTH = Integer.parseInt(config.getProperty("subscribers_mobile_limit"));
	// FXML fields
	@FXML private AnchorPane subscribersFormPane;
	@FXML private GridPane subscribersFormGridPane;
	@FXML private TableColumn<?, ?> subscriberSubscriptionsSelectedColumn;
	@FXML private TableColumn<?, ?> subscribersSubscriptionsIDColumn;
	@FXML private TableColumn<?, ?> subscribersSubscriptionsNamecolumn;
	@FXML private TextField subscribersIDField;
	@FXML private TextField subscribersFirstNameField;
	@FXML private TextField subscribersSecondNameField;
	@FXML private TextField subscribersEmailField;
	@FXML private TextField subscribersMobileField;
	@FXML private TextField subscribersPagerField;
	@FXML private ChoiceBox emailPriorityChoiceBox;
	@FXML private ChoiceBox mobilePriorityChoiceBox;
	@FXML private ChoiceBox pagerPriorityChoiceBox;
	@FXML private TextField subscribersRightSearchField;
	@FXML private TableView<?> subscribersSubscriptionsTableView;
	// other fields
	private SubscribersTabPaneController mainController;

	public void injectMainController(SubscribersTabPaneController mainController) {
		this.mainController = mainController;
	}

	@Override public void initialize(URL location, ResourceBundle resources) {
		globalContext.setPolygons(selectSubscribersSubscriptionsFromDatabase());
		populateSubscribersSubscriptionsTable(GlobalContext.getInstance().getPolygons());
		subscriberSubscriptionsSelectedColumn.setCellFactory(tc -> new CheckBoxTableCell<>());
		startListeners();
		setFormatters();
	}

	private ArrayList<Polygon> selectSubscribersSubscriptionsFromDatabase() {
		return (ArrayList<Polygon>) DatabaseUtilities.selectAll(globalContext.getDatabaseConnection(), new Polygon());
	}

	private void populateSubscribersSubscriptionsTable(ArrayList<Polygon> polyArray) {
		ObservableList subscribersSubscriptionsData = getSubscribersSubscriptionsTableData(polyArray);
		subscribersSubscriptionsTableView.getItems().setAll(subscribersSubscriptionsData);
	}

	private void startListeners() {
		subscribersFormSearchFieldListener();
		subscribersFormEmailFieldListener();
		subscribersMobileFieldListener();
		subscribersPagerFieldListener();
	}
	
	private void subscribersPagerFieldListener() {
		subscribersPagerField.textProperty().addListener((observable, oldText, newText) -> {
			if (newText.length() == 1 && oldText.length() == 0) {
				pagerPriorityChoiceBox.getSelectionModel().selectFirst();
			} else if (newText.length() == 0) {
				pagerPriorityChoiceBox.getSelectionModel().clearSelection();
			}
		});
	}
	
	private void subscribersMobileFieldListener() {
		subscribersMobileField.textProperty().addListener((observable, oldText, newText) -> {
			if (newText.length() == 1) {
				mobilePriorityChoiceBox.getSelectionModel().selectFirst();
			} else if (newText.length() == 0) {
				mobilePriorityChoiceBox.getSelectionModel().clearSelection();
			}
		});
	}
	
	private void subscribersFormEmailFieldListener() {
		subscribersEmailField.textProperty().addListener((observable, oldText, newText) -> {
			if (newText.length() == 1) {
				emailPriorityChoiceBox.getSelectionModel().selectFirst();
			} else if (newText.length() == 0) {
				emailPriorityChoiceBox.getSelectionModel().clearSelection();
			}
		});
	}
	
	private void subscribersFormSearchFieldListener() {
		subscribersRightSearchField.textProperty().addListener((observable, oldText, newText) -> {
			subscriberRightSearchChanged();
		});
	}
	
	private void setFormatters() {
		setCapitalisation();
		
		setFieldLengths();
		
		setFieldRules();
	}
	
	private void setFieldRules() {
		SetField.setFieldToPhoneNumber(subscribersMobileField);
		SetField.setFieldToPhoneNumber(subscribersPagerField);
	}
	
	private void setFieldLengths() {
		SetField.setFieldLength(subscribersIDField, SUBSCRIBERS_ID_LENGTH);
		SetField.setFieldLength(subscribersFirstNameField, SUBSCRIBERS_FIRST_NAME_LENGTH);
		SetField.setFieldLength(subscribersSecondNameField, SUBSCRIBERS_SECOND_NAME_LENGTH);
		SetField.setFieldLength(subscribersEmailField, SUBSCRIBERS_EMAIL_LENGTH);
		SetField.setFieldLength(subscribersMobileField, SUBSCRIBERS_MOBILE_LENGTH);
		SetField.setFieldLength(subscribersPagerField, SUBSCRIBERS_PAGER_LENGTH);
	}
	
	private void setCapitalisation() {
		SetField.setFieldUpper(subscribersIDField);
		SetField.setFieldCapitalised(subscribersFirstNameField);
		SetField.setFieldCapitalised(subscribersSecondNameField);
	}
	
	private ObservableList getSubscribersSubscriptionsTableData(ArrayList<Polygon> polyArray) {
		List list = new ArrayList();
		polyArray.forEach((poly) -> {
			list.add(new TableViewPolygon(poly));
		});
		ObservableList data = FXCollections.observableList(list);
		return data;
	}

	@FXML private void subscriberRightSearchChanged() {
		String searchText = subscribersRightSearchField.getText(); // Search string
		mainController.setInfoLabelWithDefaultTimer("Searching polygons: " + searchText);
		ArrayList<Polygon> searchPolygons = SearchPolygons.search(searchText, GlobalContext.getInstance().getPolygons());
		populateSubscribersSubscriptionsTable(searchPolygons);
		populateSubscriberSubscriptionsChecks();
		subscribersSubscriptionsTableView.getSelectionModel().clearAndSelect(0);
	}

	private void populateSubscriberSubscriptionsChecks() {
		if (GlobalContext.getInstance().getSubscriber().numOfPolygons()> 0) {
			for (Polygon subscription : globalContext.getSubscriber().getPolygons()) {
				setCorrectSelectionStateOfSubscription(subscription);
			}
		}
	}
	
	private void setCorrectSelectionStateOfSubscription(Polygon subscription) {
		String subscriptionID = subscription.getID();
		subscriptionID = subscriptionID.replaceAll("\\s+", "");
		
		for (Object object : subscribersSubscriptionsTableView.getItems()) {
			setTablerowSelectedIfPolygonSelected(subscriptionID, (TableViewPolygon) object);
		}
	}
	
	private void setTablerowSelectedIfPolygonSelected(String subscriptionID, TableViewPolygon object) {
		TableViewPolygon tableRow = object;
		if (tableRow.getPolyID().equals(subscriptionID)) {
			tableRow.setSelected(true);
		}
	}
	
	void populateSubscriberForm() {
		subscribersIDField.setText(GlobalContext.getInstance().getSubscriber().getID());
		subscribersFirstNameField.setText(GlobalContext.getInstance().getSubscriber().getFirstName());
		subscribersSecondNameField.setText(GlobalContext.getInstance().getSubscriber().getLastName());
		Object[] contactDetails = GlobalContext.getInstance().getSubscriber().getContactDetails();
		subscribersEmailField.setText((String) contactDetails[0]);
		emailPriorityChoiceBox.setValue(((Integer) contactDetails[1] == 0) ? "" : contactDetails[1].toString());
		subscribersMobileField.setText((String) contactDetails[2]);
		mobilePriorityChoiceBox.setValue(((Integer) contactDetails[3] == 0) ? "" : contactDetails[3].toString());
		subscribersPagerField.setText((String) contactDetails[4]);
		pagerPriorityChoiceBox.setValue(((Integer) contactDetails[5] == 0) ? "" : contactDetails[5].toString());

		clearSubscriberSubscriptionsChecks();
		populateSubscriberSubscriptionsChecks();
	}

	void focus() {
		subscriberSubscriptionsSelectedColumn.setEditable(true);
		subscribersIDField.requestFocus();
	}

	void unfocus() {
		subscriberSubscriptionsSelectedColumn.setEditable(false);
		subscribersRightSearchField.setEditable(true);
	}

	void disableIDField() {
		subscribersIDField.setDisable(true);
	}

	void enableIDField() {
		subscribersIDField.setDisable(false);
	}

	private Subscriber getSubscriberFromForm() throws WellKnownTextException {
		Subscriber formSub = new Subscriber();
		formSub.setID(subscribersIDField.getText());
		formSub.setFirstName(subscribersFirstNameField.getText());
		formSub.setLastName(subscribersSecondNameField.getText());
		if (!subscribersEmailField.getText().isEmpty() && !subscribersEmailField.equals("")) {
			formSub.setEmail(subscribersEmailField.getText());
			formSub.setEmailPriority(Integer.parseInt(emailPriorityChoiceBox.getSelectionModel().getSelectedItem().toString()));
		}
		if (!subscribersMobileField.getText().isEmpty() && !subscribersMobileField.equals("")) {
			formSub.setMobile(subscribersMobileField.getText());
			formSub.setMobilePriority(Integer.parseInt(mobilePriorityChoiceBox.getSelectionModel().getSelectedItem().toString()));
		}
		if (!subscribersPagerField.getText().isEmpty() && !subscribersPagerField.equals("")) {
			formSub.setPager(subscribersPagerField.getText());
			formSub.setPagerPriority(Integer.parseInt(pagerPriorityChoiceBox.getSelectionModel().getSelectedItem().toString()));
		}
		formSub.setPolygons(getSubscribersSubscriptionsFromForm());
		return formSub;
	}

	private Set<Polygon> getSubscribersSubscriptionsFromForm() throws WellKnownTextException {
		Set<Polygon> polygons = new HashSet<>();
		for (Object object : subscribersSubscriptionsTableView.getItems()) {
			TableViewPolygon tableRow = (TableViewPolygon) object;
			if (tableRow.returnSelected()) {
				polygons.add(tableRow.returnPolygon());
			}
		}
		return polygons;
	}

	private void insertSubscriberForm() throws WellKnownTextException {
		Subscriber insertSub = getSubscriberFromForm();
		Boolean correctInfoSuccess = checkForCorrectSubscriberInfo(insertSub);

		if (correctInfoSuccess) {
			correctInfoSuccess = DatabaseUtilities.insertSingle(GlobalContext.getInstance().getDatabaseConnection(), insertSub);
			GlobalContext.getInstance().setSubscriber(insertSub);

			if (!correctInfoSuccess) {
				String errorString = ("This subscriber ID already exists in the database." +
						"\nPlease choose another subscriber ID.");
				mainController.setInfoLabelWithDefaultTimer(errorString);
			}
		}
	}

	private void updateSubscriberForm() throws WellKnownTextException {
		Subscriber updateSub = getSubscriberFromForm();
		Boolean correctInfoSuccess = checkForCorrectSubscriberInfo(updateSub);

		if (correctInfoSuccess == true) {
			Boolean success = DatabaseUtilities.updateSingle(GlobalContext.getInstance().getDatabaseConnection(), updateSub);
			GlobalContext.getInstance().setSubscriber(updateSub);

			if (success == false) {
				String errorString = "The subscriber failed to update.";
				mainController.setInfoLabelWithDefaultTimer(errorString);
				throw new RuntimeException("Database failed to update");
			}
		}
	}

	private Boolean checkForCorrectSubscriberInfo(Subscriber sub) {
		SubscriberValidity sv = new SubscriberValidity();
		Object[] validArray = sv.isValid(sub);
		boolean correctInfoSuccess = (Boolean) validArray[0];
		ArrayList<String> invalidMessage = (ArrayList<String>) validArray[1];

		if (!correctInfoSuccess) {
			// Inform user of error
			mainController.setInfoLabelWithDefaultTimer(invalidMessage.get(0));
			// Set focus to first field producing error
			switch (invalidMessage.get(0)) {
				case UserMessage.MISSING_SUBSCRIBER_ID:
					subscribersIDField.requestFocus();
					break;
				case UserMessage.MISSING_SUBSCRIBER_FIRST_NAME:
					subscribersFirstNameField.requestFocus();
					break;
				case UserMessage.MISSING_SUBSCRIBER_LAST_NAME:
					subscribersSecondNameField.requestFocus();
					break;
				case UserMessage.MISSING_SUBSCRIBER_CONTACT_METHOD:
					subscribersEmailField.requestFocus();
					break;
				case UserMessage.INVALID_SUBSCRIBER_EMAIL:
					subscribersEmailField.requestFocus();
					break;
			}
		}
		return correctInfoSuccess;
	}

	private boolean userConfirmedSave() {
		if (REQUIRE_CONFIRMATION || REQUIRE_SAVE_CONFIRMATION) {
			return CreateYesNoOptionDialog.create("Are you sure you want to save this subscriber?", "Confirmation");
		}
		return true;
	}

	@FXML public void saveSubscriberButtonPressed(ActionEvent actionEvent) {
		if (!mainController.isSubscriberBeingEdited()) {
			saveEditedSubscriber();
		} else  {
			saveNewSubscriber();
		}
	}
	
	private void saveNewSubscriber() {
		if (userConfirmedSave()) {
			try {
				updateSubscriberForm();
				populateSubscriberForm();
				mainController.saveSubscriber();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void saveEditedSubscriber() {
		if (userConfirmedSave()) {
			Boolean correctInfoSuccess = null;
			try {
				insertSubscriberForm();
				populateSubscriberForm();
				mainController.saveSubscriber();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	@FXML public void subscribersCancelButtonPressed(ActionEvent actionEvent) {
		if (userConfirmedCancel()) {
			mainController.cancelSubscriber();
			clearSubscriberForm();
		}
	}

	private boolean userConfirmedCancel() {
		if (REQUIRE_CONFIRMATION || REQUIRE_CANCEL_CONFIRMATION) {
			return CreateYesNoOptionDialog.create("Are you sure you want to cancel?", "Confirmation");
		}
		return true;
	}

	void clearSubscriberForm() {
		clearSubscriberSubscriptionsChecks();
		clearSubscriberPriorities();
		NodeUtilities.clearAllNodes(subscribersFormGridPane);
	}

	private void clearSubscriberSubscriptionsChecks() {
		for (Object object : subscribersSubscriptionsTableView.getItems()) {
			TableViewPolygon tableRow = (TableViewPolygon) object;
			tableRow.setSelected(false);
		}
	}

	private void clearSubscriberPriorities() {
		ObservableList<String> priorities = FXCollections.observableArrayList("1", "2", "3");
		emailPriorityChoiceBox.setItems(priorities);
		mobilePriorityChoiceBox.setItems(priorities);
		pagerPriorityChoiceBox.setItems(priorities);
	}

	@FXML public void subscribersIDFieldChanged(KeyEvent keyEvent) {
	}

	@FXML public void subscriberSubscriptionTableKeyPressed(KeyEvent event) {
		KeyCode keyCode = event.getCode();
		if (event.isControlDown() && keyCode == KeyCode.PAGE_DOWN) {
			mainController.ctrlPageDownPressedOnTable();
		} else if (event.isControlDown() && keyCode == KeyCode.PAGE_UP) {
			mainController.ctrlPageUpPressedOnTable();
		} else {
			GuiUtilities.typeInFieldExternally(event, subscribersRightSearchField);
		}
	}
}