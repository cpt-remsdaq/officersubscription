package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.utilities.NodeUtilities;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

public class SubscribersTabPaneController {
	
	@FXML public AnchorPane subscribersTablePane;
	@FXML public AnchorPane subscribersFormPane;
	MainController mainController;
	@FXML private SubscribersTableController subscribersTablePaneController;
	@FXML private SubscribersFormController subscribersFormPaneController;
	private Boolean isSubscriberBeingEdited = false;
	private Boolean isPolygonBeingEdited = false;
	
	public void initialize() {
		injectControllers();
		goToSubLHS();
	}
	
	public void refreshTab() {
		subscribersTablePaneController.populateSubscribersTableFromDatabase();
		subscribersFormPaneController.clearSubscriberForm();
	
	}
	
	public void subscriberTableViewChanged() {
		clearSubscriberForm();
		populateSubscriberForm();
	}
	
	public void ctrlPageDownPressedOnTable() {
		mainController.selectNextTab();
	}
	
	public void ctrlPageUpPressedOnTable() {
		mainController.selectPreviousTab();
	}
	
	void injectMainController(MainController mainController) {
		this.mainController = mainController;
	}
	
	Boolean isSubscriberBeingEdited() {
		return isSubscriberBeingEdited;
	}
	
	void newSubscriber() {
		isSubscriberBeingEdited = false;
		setInfoLabelWithDefaultTimer("Creating new subscriber.");
		ensureSubscribersTabIsSelected();
		goToSubRHS();
		clearSubscriberForm();
		subscribersFormPaneController.enableIDField();
	}
	
	void setInfoLabelWithDefaultTimer(String info) {
		mainController.setInfoLabelWithDefaultTimer(info);
	}
	
	void clearSubscriberForm() {
		subscribersFormPaneController.clearSubscriberForm();
	}
	
	void editSubscriber() {
		isSubscriberBeingEdited = true;
		ensureSubscribersTabIsSelected();
		setInfoLabelWithDefaultTimer("Editing subscriber.");
		populateSubscriberForm();
		goToSubRHS();
		subscribersFormPaneController.disableIDField();
	}
	
	void deleteSubscriber() {
		setInfoLabelWithDefaultTimer("Deleting subscriber.");
		clearSubscriberForm();
		populateSubscriberForm();
	}
	
	void populateSubscriberForm() {
		subscribersFormPaneController.populateSubscriberForm();
	}
	
	void saveSubscriber() {
		goToSubLHS();
		setInfoLabelWithDefaultTimer("Saving subscriber.");
		subscribersTablePaneController.populateSubscribersTableFromDatabase();
	}
	
	void cancelSubscriber() {
		setInfoLabelWithDefaultTimer("Cancelling.");
		goToSubLHS();
	}
	
	private void injectControllers() {
		subscribersTablePaneController.injectMainController(this);
		subscribersFormPaneController.injectMainController(this);
	}
	
	private void goToSubLHS() {
		subscribersFormPaneController.unfocus();
		NodeUtilities.switchPanes(subscribersTablePane, subscribersFormPane);
		subscribersTablePaneController.focus();
	}
	
	private void ensureSubscribersTabIsSelected() {
		mainController.ensureSubscribersTabIsSelected();
	}
	
	private void goToSubRHS() {
		subscribersTablePaneController.unfocus();
		NodeUtilities.switchPanes(subscribersFormPane, subscribersTablePane);
		subscribersFormPaneController.focus();
	}
}