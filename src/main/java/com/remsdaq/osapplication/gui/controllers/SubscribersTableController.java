package com.remsdaq.osapplication.gui.controllers;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.DatabaseUtilities;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.gui.entities.SSPSubscriber;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.gui.main_application.SearchSubscribers;
import com.remsdaq.osapplication.utilities.gui.CreateYesNoOptionDialog;
import com.remsdaq.osapplication.utilities.gui.GuiUtilities;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class SubscribersTableController implements Initializable {

	// FXML fields
	@FXML private Button subscribersNewSubscriberButton;
	@FXML private Button subscribersEditSubscriberButton;
	@FXML private Button subscribersDeleteSubscriberButton;
	@FXML private TextField subscribersTableSearchField;
	@FXML private TableView<?> subscribersTableView;
	@FXML private TableColumn<?, ?> subscribersIDColumn;
	@FXML private AnchorPane subscribersTablePane;

	// other fields
	private SubscribersTabPaneController mainController;
	private GlobalContext globalContext = GlobalContext.getInstance();
	private Config config = GlobalContext.getInstance().getConfig();

	//    private ArrayList<Subscriber> subscribers;

	private final Boolean REQUIRE_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_confirmation_boolean"));
	private final Boolean REQUIRE_EDIT_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_edit_confirmation_boolean"));
	private final Boolean REQUIRE_DELETE_CONFIRMATION = Boolean.parseBoolean(config.getProperty("require_delete_confirmation_boolean"));

	public void injectMainController(SubscribersTabPaneController mainController) {
		this.mainController = mainController;
	}

	@Override public void initialize(URL location, ResourceBundle resources) {
		globalContext.setSubscribers(selectSubscribersFromDatabase());
		populateSubscribersTable(globalContext.getSubscribers());
		startListeners();
	}

	private ArrayList<Subscriber> selectSubscribersFromDatabase() {
		return (ArrayList<Subscriber>) DatabaseUtilities.selectAll(globalContext.getDatabaseConnection(), new Subscriber());
	}

	private void populateSubscribersTable(ArrayList<Subscriber> subArray) {
		ObservableList subscribersData = getSubscribersTableData(subArray); // Changed from list to observablelist
		subscribersTableView.getItems().setAll(subscribersData);
	}

	private void startListeners() {
		// Subscribers tableview selection changed
		subscribersTableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null && isSubscriberSelected()) {
				subscriberTableViewChanged();
			}
		});

		// Subscribers searched
		subscribersTableSearchField.textProperty().addListener((observable, oldText, newText) -> {
			subscribersTableSearchChanged();
		});
	}

	private ObservableList getSubscribersTableData(ArrayList<Subscriber> subArray) {
		List list = new ArrayList();

		subArray.forEach((sub) -> {
			list.add(new SSPSubscriber(sub));
		});
		ObservableList data = FXCollections.observableList(list);
		return data;
	}

	private Boolean isSubscriberSelected() {
		return !subscribersTableView.getSelectionModel().isEmpty();
	}

	@FXML private void subscriberTableViewChanged() {
		if (isSubscriberSelected()) {
			subscribersEditSubscriberButton.disableProperty().set(false);
			subscribersDeleteSubscriberButton.disableProperty().set(false);
			globalContext.setSubscriber(getSelectedSubscriber());
			mainController.subscriberTableViewChanged();

		}
	}

	@FXML public void subscribersTableSearchChanged() {
		String searchText = subscribersTableSearchField.getText();
		ArrayList<Subscriber> searchSubscribers = SearchSubscribers.search(searchText, globalContext.getSubscribers());
		populateSubscribersTable(searchSubscribers);
		subscribersTableView.getSelectionModel().clearAndSelect(0);
	}

	private Subscriber getSelectedSubscriber() {
		String tableRow = subscribersTableView.getSelectionModel().getSelectedItem().toString();
		String selectedSubscriberID = tableRow.split(";")[0];

		for (Subscriber sub : GlobalContext.getInstance().getSubscribers()) {
			if (selectedSubscriberID.equals(sub.getID())) {
				GlobalContext.getInstance().setSubscriber(sub);
			}
		}
		return GlobalContext.getInstance().getSubscriber();
	}

	void focus() {
		subscribersTablePane.disableProperty().set(false);
		subscribersTableView.requestFocus();
	}

	void unfocus() {
		subscribersTablePane.disableProperty().set(true);
	}

	private ArrayList<Polygon> selectPolygonsFromDatabase() {
		return (ArrayList<Polygon>) DatabaseUtilities.selectAll(globalContext.getDatabaseConnection(), new Polygon());
	}

	@FXML public void newSubscriberPressed(ActionEvent actionEvent) {
		GlobalContext.getInstance().setSubscriber(new Subscriber());
		mainController.newSubscriber();
	}

	@FXML public void editSubscriberPressed(ActionEvent actionEvent) {

		if (isSubscriberSelected()) {
			if (userConfirmedEdit()) {
				mainController.editSubscriber();
			}
		} else {
			mainController.setInfoLabelWithDefaultTimer("No subscriber selected");
		}
	}

	private boolean userConfirmedEdit() {
		if (REQUIRE_CONFIRMATION || REQUIRE_EDIT_CONFIRMATION) {
			return CreateYesNoOptionDialog.create("Are you sure you want to edit this subscriber?", "Confirmation");
		}
		return true;
	}

	@FXML public void deleteSubscriberPressed(ActionEvent actionEvent) {
		if (isSubscriberSelected()) {
			if (userConfirmedDelete()) {
				DatabaseUtilities.deleteSingle(globalContext.getDatabaseConnection(), globalContext.getSubscriber());
				populateSubscribersTableFromDatabase();
				mainController.deleteSubscriber();
			}
		}
	}

	private boolean userConfirmedDelete() {
		if (REQUIRE_CONFIRMATION || REQUIRE_DELETE_CONFIRMATION) {
			return CreateYesNoOptionDialog.create("Are you sure you want to delete this subscriber?", "Confirmation");
		}
		return true;
	}

	void populateSubscribersTableFromDatabase() {
		globalContext.setSubscribers(selectSubscribersFromDatabase());
		populateSubscribersTable(globalContext.getSubscribers());
	}

	@FXML public void subscribersTableKeyPressed(KeyEvent event) {
		KeyCode keyCode = event.getCode();
		if (event.isControlDown() && keyCode == KeyCode.PAGE_DOWN) {
			mainController.ctrlPageDownPressedOnTable();
		} else if (event.isControlDown() && keyCode == KeyCode.PAGE_UP) {
			mainController.ctrlPageUpPressedOnTable();
		} else {
			GuiUtilities.typeInFieldExternally(event, subscribersTableSearchField);
		}
	}
}