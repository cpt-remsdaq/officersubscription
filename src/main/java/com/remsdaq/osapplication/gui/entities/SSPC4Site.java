package com.remsdaq.osapplication.gui.entities;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class SSPC4Site {

    private final SimpleStringProperty type;
    private final SimpleIntegerProperty gazType;
    private final SimpleStringProperty gazRef;
    private final SimpleStringProperty address;
    private final SimpleStringProperty coordinateSystem;
    private final SimpleStringProperty x;
    private final SimpleStringProperty y;
    private final SimpleStringProperty status;
    private final SimpleStringProperty seqNo;

    public SSPC4Site(SimpleStringProperty type, SimpleIntegerProperty gazType, SimpleStringProperty gazRef, SimpleStringProperty address, SimpleStringProperty coordinateSystem, SimpleStringProperty x, SimpleStringProperty y, SimpleStringProperty status, SimpleStringProperty seqNo) {
        this.type = type;
        this.gazType = gazType;
        this.gazRef = gazRef;
        this.address = address;
        this.coordinateSystem = coordinateSystem;
        this.x = x;
        this.y = y;
        this.status = status;
        this.seqNo = seqNo;
    }
}
