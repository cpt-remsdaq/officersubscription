package com.remsdaq.osapplication.gui.entities;

import javafx.beans.property.SimpleStringProperty;

public class SSPCallerDetails {

    private final SimpleStringProperty callerTitle;
    private final SimpleStringProperty callerForename;
    private final SimpleStringProperty callerSurname;
    private final SimpleStringProperty callerNumber;
    private final SimpleStringProperty callerAddress;
    private final SimpleStringProperty callerMobile;
    private final SimpleStringProperty callerGazType;
    private final SimpleStringProperty callerGazRef;

    public SSPCallerDetails(SimpleStringProperty callerTitle, SimpleStringProperty callerForename, SimpleStringProperty callerSurname, SimpleStringProperty callerNumber, SimpleStringProperty callerAddress, SimpleStringProperty callerMobile, SimpleStringProperty callerGazType, SimpleStringProperty callerGazRef) {
        this.callerTitle = callerTitle;
        this.callerForename = callerForename;
        this.callerSurname = callerSurname;
        this.callerNumber = callerNumber;
        this.callerAddress = callerAddress;
        this.callerMobile = callerMobile;
        this.callerGazType = callerGazType;
        this.callerGazRef = callerGazRef;
    }
}
