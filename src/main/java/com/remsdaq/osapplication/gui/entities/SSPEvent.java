package com.remsdaq.osapplication.gui.entities;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.ArrayList;

public class SSPEvent {

    private final SimpleStringProperty schema;
    private final SimpleStringProperty mode;

    private final SSPMessageControl messageControl;

    private final SSPCallerDetails callerDetails;

    private final SimpleStringProperty callOrigin;
    private final SimpleStringProperty priority;
    private final SimpleStringProperty description;
    private final SimpleStringProperty location;
    private final SimpleStringProperty coordinateSystem;
    private final SimpleStringProperty x;
    private final SimpleStringProperty y;
    private final SimpleStringProperty incidentGazType;
    private final SimpleStringProperty incidentGazRef;
    private final SimpleBooleanProperty hazardFlag;
    private final SimpleBooleanProperty knownSceneSafetyIssue;

    private final SSPC4Site c4Site;

    private final SimpleStringProperty incidentType;
    private final SimpleStringProperty incidentSubType;
    private final SimpleBooleanProperty attendanceRequested;
    private final SimpleStringProperty notificationReason;
    private final SimpleStringProperty resiyrceETA;
    private final SimpleStringProperty resourceID;

    private final ArrayList<SSPVehicleInvolved> vehiclesInvolved;

    private final ArrayList<SSPPersonInvolved> personsInvolved;




    public SSPEvent(SimpleStringProperty schema, SimpleStringProperty mode, SSPMessageControl messageControl, SSPCallerDetails callerDetails, SimpleStringProperty callOrigin, SimpleStringProperty priority, SimpleStringProperty description, SimpleStringProperty location, SimpleStringProperty coordinateSystem, SimpleStringProperty x, SimpleStringProperty y, SimpleStringProperty incidentGazType, SimpleStringProperty incidentGazRef, SimpleBooleanProperty hazardFlag, SimpleBooleanProperty knownSceneSafetyIssue, SSPC4Site c4Site, SimpleStringProperty incidentType, SimpleStringProperty incidentSubType, SimpleBooleanProperty attendanceRequested, SimpleStringProperty notificationReason, SimpleStringProperty resiyrceETA, SimpleStringProperty resourceID, ArrayList<SSPVehicleInvolved> vehiclesInvolved, ArrayList<SSPPersonInvolved> personsInvolved) {
        this.schema = schema;
        this.mode = mode;
        this.messageControl = messageControl;
        this.callerDetails = callerDetails;
        this.callOrigin = callOrigin;
        this.priority = priority;
        this.description = description;
        this.location = location;
        this.coordinateSystem = coordinateSystem;
        this.x = x;
        this.y = y;
        this.incidentGazType = incidentGazType;
        this.incidentGazRef = incidentGazRef;
        this.hazardFlag = hazardFlag;
        this.knownSceneSafetyIssue = knownSceneSafetyIssue;
        this.c4Site = c4Site;
        this.incidentType = incidentType;
        this.incidentSubType = incidentSubType;
        this.attendanceRequested = attendanceRequested;
        this.notificationReason = notificationReason;
        this.resiyrceETA = resiyrceETA;
        this.resourceID = resourceID;
        this.vehiclesInvolved = vehiclesInvolved;
        this.personsInvolved = personsInvolved;
    }
}
