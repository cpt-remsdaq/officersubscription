package com.remsdaq.osapplication.gui.entities;

import javafx.beans.property.SimpleStringProperty;

public class SSPMessageControl {

    private final SimpleStringProperty messageID;
    private final SimpleStringProperty markingScheme;
    private final SimpleStringProperty securityLevel;
    private final SimpleStringProperty destinOrganisation;
    private final SimpleStringProperty origOrganisation;
    private final SimpleStringProperty origIncidentURN;
    private final SimpleStringProperty origIncidentNum;
    private final SimpleStringProperty origIncidentDate;
    private final SimpleStringProperty origIncidentTime;

    public SSPMessageControl(SimpleStringProperty messageID, SimpleStringProperty markingScheme, SimpleStringProperty securityLevel, SimpleStringProperty destinOrganisation, SimpleStringProperty origOrganisation, SimpleStringProperty origIncidentURN, SimpleStringProperty origIncidentNum, SimpleStringProperty origIncidentDate, SimpleStringProperty origIncidentTime) {
        this.messageID = messageID;
        this.markingScheme = markingScheme;
        this.securityLevel = securityLevel;
        this.destinOrganisation = destinOrganisation;
        this.origOrganisation = origOrganisation;
        this.origIncidentURN = origIncidentURN;
        this.origIncidentNum = origIncidentNum;
        this.origIncidentDate = origIncidentDate;
        this.origIncidentTime = origIncidentTime;
    }
}
