package com.remsdaq.osapplication.gui.entities;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import java.util.HashSet;
import java.util.Set;

public class SSPSubscriber {

	private final SimpleStringProperty subID;
	private final SimpleStringProperty firstName;
	private final SimpleStringProperty lastName;
	private final SimpleStringProperty polygons;
	private final SimpleStringProperty email;
	private final SimpleIntegerProperty emailPriority;
	private final SimpleStringProperty mobile;
	private final SimpleIntegerProperty mobilePriority;
	private final SimpleStringProperty pager;
	private final SimpleIntegerProperty pagerPriority;
	private final SimpleBooleanProperty selected;

	public SSPSubscriber(Subscriber subscriber) {
		this(
				subscriber.getID(),
				subscriber.getFirstName(),
				subscriber.getLastName(),
				subscriber.getPolygonString(),
				subscriber.getEmail(),
				subscriber.getEmailPriority(),
				subscriber.getMobile(),
				subscriber.getMobilePriority(),
				subscriber.getPager(),
				subscriber.getPagerPriority(),
				false);
	}

 	public SSPSubscriber(String subID, String firstName, String lastName, String polygons, String email, Integer emailPriority, String mobile, Integer mobilePriority, String pager, Integer pagerPriority, Boolean selected) {
		this.subID = new SimpleStringProperty(subID);
		this.firstName = new SimpleStringProperty(firstName);
		this.lastName = new SimpleStringProperty(lastName);
		this.polygons = new SimpleStringProperty(polygons);
		this.email = new SimpleStringProperty(email);
		this.emailPriority = new SimpleIntegerProperty(emailPriority);
		this.mobile = new SimpleStringProperty(mobile);
		this.mobilePriority = new SimpleIntegerProperty(mobilePriority);
		this.pager = new SimpleStringProperty(pager);
		this.pagerPriority = new SimpleIntegerProperty(pagerPriority);
		this.selected = new SimpleBooleanProperty(selected);
	}

	public SSPSubscriber(Subscriber subscriber, Boolean selected) {
		this(
				subscriber.getID(),
				subscriber.getFirstName(),
				subscriber.getLastName(),
				subscriber.getPolygonString(),
				subscriber.getEmail(),
				subscriber.getEmailPriority(),
				subscriber.getMobile(),
				subscriber.getMobilePriority(),
				subscriber.getPager(),
				subscriber.getPagerPriority(),
				selected);
	}

	public String getSubID() {
		return this.subID.get();
	}

	/**
	 * @param subID
	 */
	public void setSubID(String subID) {
		this.subID.set(subID);
	}

	/**
	 *
	 *
	 */
	public String getFirstName() {
		return this.firstName.get();
	}

	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName.set(firstName);
	}

	/**
	 *
	 *
	 */
	public String getLastName() {
		return this.lastName.get();
	}

	/**
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName.set(lastName);
	}

	/**
	 *
	 *
	 */
	public String getPolygons() {
		return this.polygons.get();
	}

	/**
	 * @param polygons
	 */
	public void setPolygons(String polygons) {
		this.polygons.set(polygons);
	}

	/**
	 *
	 *
	 */
	public String getEmail() {
		return this.email.get();
	}

	/**
	 * @param email
	 */
	public void setEmail(String email) {
		this.email.set(email);
	}

	public Integer getEmailPriority() {
		return this.emailPriority.get();
	}

	public void setEmailPriority(Integer emailPriority) {
		this.emailPriority.set(emailPriority);
	}

	/**
	 *
	 *
	 */
	public String getMobile() {
		return this.mobile.get();
	}

	/**
	 * @param mobile
	 */
	public void setMobile(String mobile) {
		this.mobile.set(mobile);
	}

	public Integer getMobilePriority() {
		return this.mobilePriority.get();
	}

	public void setMobilePriority(Integer mobilePriority) {
		this.mobilePriority.set(mobilePriority);
	}

	/**
	 *
	 *
	 */
	public String getPager() {
		return this.pager.get();
	}

	/**
	 * @param s
	 */
	public void setPager(String pager) {
		this.pager.set(pager);
	}

	public Integer getPagerPriority() {
		return this.pagerPriority.get();
	}

	public void setPagerPriority(Integer pagerPriority) {
		this.pagerPriority.set(pagerPriority);
	}

	public SimpleBooleanProperty selectedProperty() {
		return this.selected;
	}

	public Boolean getSelected() {
		return selected.get();
	}

	public void setSelected(Boolean selected) {
		this.selected.set(selected);
	}

	/**
	 *
	 *
	 */
	public Subscriber returnSubscriber() {
		Subscriber sub = new Subscriber();
		sub.setID(subID.get());
		sub.setFirstName(firstName.get());
		sub.setLastName(lastName.get());

		sub.setEmail(email.get());
		sub.setMobile(mobile.get());
		sub.setPager(pager.get());

		sub.setEmailPriority(emailPriority.get());
		sub.setMobilePriority(mobilePriority.get());
		sub.setPagerPriority(pagerPriority.get());

		Set<Polygon> polySet = new HashSet<>();
		for (String polyString : polygons.get().split(", ")) {
			Polygon sacraPoly = new Polygon();
			sacraPoly.setID(polyString);
			polySet.add(sacraPoly);
		}

		sub.setPolygons(polySet);
		return sub;
	}

	public Boolean returnSelected() {
		return selected.get();
	}

	@Override
	public String toString() {
		return (subID.get() + ";" + firstName.get() + ";" + lastName.get() + ";" + polygons.get() + ";" + email.get() + ";" + mobile.get() + ";" + pager.get() + ";" + selected.get() + ";");
	}

}
