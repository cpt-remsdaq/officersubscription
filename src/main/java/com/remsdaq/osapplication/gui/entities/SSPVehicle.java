package com.remsdaq.osapplication.gui.entities;

import javafx.beans.property.SimpleStringProperty;

public class SSPVehicle {

    private final SimpleStringProperty VRM;
    private final SimpleStringProperty VIN;
    private final SimpleStringProperty vehicleMake;
    private final SimpleStringProperty vehicleModel;
    private final SimpleStringProperty vehicleVariant;
    private final SimpleStringProperty vecicleColour;
    private final SimpleStringProperty vehicleInvolvement;
    private final SimpleStringProperty vehicleComment;
    private final SimpleStringProperty vehicleSeqNo;

    public SSPVehicle(SimpleStringProperty vrm, SimpleStringProperty vin, SimpleStringProperty vehicleMake, SimpleStringProperty vehicleModel, SimpleStringProperty vehicleVariant, SimpleStringProperty vecicleColour, SimpleStringProperty vehicleInvolvement, SimpleStringProperty vehicleComment, SimpleStringProperty vehicleSeqNo) {
        VRM = vrm;
        VIN = vin;
        this.vehicleMake = vehicleMake;
        this.vehicleModel = vehicleModel;
        this.vehicleVariant = vehicleVariant;
        this.vecicleColour = vecicleColour;
        this.vehicleInvolvement = vehicleInvolvement;
        this.vehicleComment = vehicleComment;
        this.vehicleSeqNo = vehicleSeqNo;
    }
}
