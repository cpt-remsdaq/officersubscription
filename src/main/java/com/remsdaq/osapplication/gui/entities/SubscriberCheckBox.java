/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.gui.entities;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;

/**
 *
 * @author cpt
 */
class SubscriberCheckBox extends CheckBox{
    
    private Subscriber subscriber;
    
    public SubscriberCheckBox(Subscriber subscriber) {
        this.subscriber = subscriber;
        this.setText(subscriber.getID() + "\n" + subscriber.getLastName() + ", " + subscriber.getFirstName());
        this.setAlignment(Pos.TOP_LEFT);
        this.setMaxWidth(300.0);
        this.setContentDisplay(ContentDisplay.TOP);
        this.setMaxHeight(100.0);
        this.setWrapText(true);
    }
    
    private Subscriber getSubscriber() {
        return subscriber;
    }
    
    public String getID() {
        return getSubscriber().getID();
    }
}