package com.remsdaq.osapplication.gui.entities;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import javafx.beans.property.SimpleBooleanProperty;

public class TableViewPolygon extends Polygon implements TableViewSelected {

	private Polygon polygon;
	SimpleBooleanProperty selected = new SimpleBooleanProperty();

	public Boolean getSelected() {
		return this.selected.get();
	}

	public void setSelected(Boolean selected) {
		this.selected.set(selected);
	}

	public Boolean returnSelected() {
		return selected.get();
	}

	public SimpleBooleanProperty selectedProperty() {
		return this.selected;
	}

	public TableViewPolygon(Polygon polygon) {
		this.polygon = polygon;
	}

	public String getPolyID() {
		return polygon.getID();
	}

	public String getPolyName() {
		return polygon.getName();
	}

//	public String getPolyGeometry() {
//		return polygon.getWKTGeometry();
//	}

	public String getDescription() {
		return polygon.getDescription();
	}

	public Polygon returnPolygon() {
		return polygon;
	}

	//	@Override
	//	public String toString() {
	//		return (polygon.getID() + ";" + polygon.getName() + ";" + polygon.getWKTGeometry() + ";" + joinSubscriberIDs(polygon) + ";" + joinPolygonTags(polygon) + ";" + polygon.getDescription() + ";" + selected.get());
	//	}
}