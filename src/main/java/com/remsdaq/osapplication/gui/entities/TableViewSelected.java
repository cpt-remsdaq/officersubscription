package com.remsdaq.osapplication.gui.entities;

import javafx.beans.property.SimpleBooleanProperty;

public interface TableViewSelected {

	SimpleBooleanProperty selected = new SimpleBooleanProperty();

	Boolean getSelected();

	void setSelected(Boolean selected);

	Boolean returnSelected();

	SimpleBooleanProperty selectedProperty();
}
