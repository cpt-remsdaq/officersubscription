/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.gui.main_application;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import javafx.geometry.Pos;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;

/**
 *
 * @author cpt
 */
public class PolygonCheckBox extends CheckBox{

    private Polygon polygon;
    
    public PolygonCheckBox(Polygon polygon) {
        this.polygon = polygon;
        this.setText(polygon.getID() + "\n" + polygon.getName() + ": " + polygon.getDescription());
        this.setAlignment(Pos.TOP_LEFT);
        this.setContentDisplay(ContentDisplay.TOP);
        this.setMaxHeight(60.0);
        this.setWrapText(true);
        this.setMaxWidth(300.0);
    }
    
    public Polygon getPolygon() {
        return polygon;
    }
    
    public String getID() {
        return getPolygon().getID();
    }
    
}
