package com.remsdaq.osapplication.gui.main_application;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SearchPolygons{
    
    public static ArrayList<Polygon> search(String searchString, ArrayList<Polygon> searchPolygons) {
        
        ArrayList<Polygon> clearingPolyArray = new ArrayList();
        String[] split = searchString.split(" ");
        searchPolygons.forEach((poly) -> {
            int polySearchInt = 0;
            Boolean addToPolys = false;
            for (int i = 0; i<split.length; i++) {
                // If search substring matches ID, name, or description
                if (poly.getID().toUpperCase().contains(split[i].toUpperCase())
                        || poly.getName().toUpperCase().contains(split[i].toUpperCase())
                        || poly.getDescription().toUpperCase().contains(split[i].toUpperCase())
                        || poly.getPolygonTags().toString().toUpperCase().contains(split[i].toUpperCase())) {
                    addToPolys = true;
                }
                for (PolygonTag tag : poly.getPolygonTags()) {
                    if (tag.getName().toUpperCase().contains(split[i].toUpperCase())) {
                        addToPolys = true;
                    }
                }
                
                if (addToPolys) {
                    polySearchInt += 1;
                }
            }
            // Check that all substrings match a polygon parameter
            if (polySearchInt == split.length) {
                clearingPolyArray.add(poly);
            }
        });
        
        // Remove duplicates by adding to set and returning polygons to an array.
        // Maybe consider changing arraylist to set for polygon collection.
        Set<Polygon> clearingSet = new HashSet();
        clearingSet.addAll(clearingPolyArray);
        ArrayList<Polygon> polyArray = new ArrayList();
        polyArray.addAll(clearingSet);
        
        return polyArray;
    }
}