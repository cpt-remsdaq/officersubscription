package com.remsdaq.osapplication.gui.main_application;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SearchSubscribers {
    
    public static ArrayList<Subscriber> search(String searchString, ArrayList<Subscriber> searchSubscribers) {
        
        ArrayList<Subscriber> clearingSubArray = new ArrayList();
        
        String[] split = searchString.split(" ");
        searchSubscribers.forEach((sub) -> {
            int subSearchInt = 0;
            for (int i = 0; i<split.length; i++) {
                // If search substring matches ID, first name, or second name
                if (sub.getID().toUpperCase().contains(split[i].toUpperCase())
                        || sub.getFirstName().toUpperCase().contains(split[i].toUpperCase())
                        || sub.getLastName().toUpperCase().contains(split[i].toUpperCase())
                        || sub.getPolygonString().toUpperCase().contains(split[i].toUpperCase())) {
                    subSearchInt += 1;
                }
            }
            // Check that all substrings match a subscriber parameter
            if (subSearchInt == split.length) {
                clearingSubArray.add(sub);
            }
        });
        
        // Remove duplicates by adding to set and returning subscribers to an array.
        // Maybe consider changing arraylist to set for subscribers collection.
        Set<Subscriber> clearingSet = new HashSet();
        clearingSet.addAll(clearingSubArray);
        ArrayList<Subscriber> subArray = new ArrayList();
        subArray.addAll(clearingSet);
        
        return subArray;
    }
}