package com.remsdaq.osapplication.gui.main_application;

import com.remsdaq.osapplication.gui.main_application.PolygonCheckBox;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class SearchSubscriptions {
    
    public static ArrayList<PolygonCheckBox> search(String searchString, ArrayList<PolygonCheckBox> searchCheckBoxes) {
        
        ArrayList<PolygonCheckBox> clearingSubArray = new ArrayList();
        
        String[] split = searchString.split(" ");
        searchCheckBoxes.forEach((chk) -> {
            int subSearchInt = 0;
            for (int i = 0; i<split.length; i++) {
                // If search substring matches ID, first name, or second name
                if (chk.getID().toUpperCase().contains(split[i].toUpperCase())
                        || chk.getPolygon().getName().toUpperCase().contains(split[i].toUpperCase())
                        || chk.getPolygon().getDescription().toUpperCase().contains(split[i].toUpperCase())) {
                    subSearchInt += 1;
                }
            }
            // Check that all substrings match a subscriber parameter
            if (subSearchInt == split.length) {
                clearingSubArray.add(chk);
            }
        });
        
        // Remove duplicates by adding to set and returning subscribers to an array.
        // Maybe consider changing arraylist to set for subscribers collection.
        Set<PolygonCheckBox> clearingSet = new HashSet();
        clearingSet.addAll(clearingSubArray);
        ArrayList<PolygonCheckBox> subArray = new ArrayList();
        subArray.addAll(clearingSet);
        
        return subArray;
    }
}