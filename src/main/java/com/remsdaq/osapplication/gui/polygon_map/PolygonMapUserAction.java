package com.remsdaq.osapplication.gui.polygon_map;

import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.entities.polygons.PolygonGeometry;
import com.remsdaq.osapplication.entities.polygons.SimplePolygon;

import java.util.ArrayList;

public class PolygonMapUserAction {

    private Action action;
    private Integer currentComplexPolygon;
    private Integer currentSimplePolygon;
    private Integer currentVertex;
    private LatLng currentLatLng;
    private PolygonGeometry geometry;
    private SimplePolygon polygon;
    private ArrayList<SimplePolygon> polygons;

    public PolygonMapUserAction(Action action) {
        this.action = action;
    }

    public PolygonMapUserAction(Action action, PolygonGeometry geometry) {
        this.action = action;
        this.geometry = geometry;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, PolygonGeometry geometry) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.geometry = geometry;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, int currentSimplePolygon, PolygonGeometry geometry) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.currentSimplePolygon = currentSimplePolygon;
        this.geometry = geometry;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.currentSimplePolygon = 0;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, int currentSimplePolygonPolygon) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.currentSimplePolygon = currentSimplePolygonPolygon;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, int currentSimplePolygon, int currentVertex) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.currentSimplePolygon = currentSimplePolygon;
        this.currentVertex = currentVertex;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, int currentSimplePolygon, int currentVertex, LatLng latLng) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.currentSimplePolygon = currentSimplePolygon;
        this.currentVertex = currentVertex;
        this.currentLatLng = latLng;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, SimplePolygon polygon) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.polygon = polygon;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, int currentSimplePolygon, SimplePolygon polygon) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.currentSimplePolygon = currentSimplePolygon;
        this.polygon = polygon;
    }

    public PolygonMapUserAction(Action action, int currentComplexPolygon, ArrayList<SimplePolygon> polygons) {
        this.action = action;
        this.currentComplexPolygon = currentComplexPolygon;
        this.polygons = polygons;
    }

    public enum Action {
        SAVE_GEOMETRY,
        CLEAR_GEOMETRY,
        CANCEL,
        UNDO,
        REDO,
        NEW_POLYGON,
        CLOSE_POLYGON,
        CANCEL_POLYGON,
        DELETE_POLYGON,
        NEW_HOLE,
        CLOSE_HOLE,
        CANCEL_HOLE,
        DELETE_HOLE,
        START,
        MOUSE_CLICKED,
        POINT_ADDED
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public PolygonGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(PolygonGeometry geometry) {
        this.geometry = geometry;
    }

    public LatLng getCurrentLatLng() {
        return currentLatLng;
    }

    public void setCurrentLatLng(LatLng currentLatLng) {
        this.currentLatLng = currentLatLng;
    }

    public int getCurrentVertex() {
        return currentVertex;
    }

    public void setCurrentVertex(int currentVertex) {
        this.currentVertex = currentVertex;
    }

    public int getCurrentSimplePolygon() {
        return currentSimplePolygon;
    }

    public void setCurrentSimplePolygon(int currentSimplePolygon) {
        this.currentSimplePolygon = currentSimplePolygon;
    }

    public int getCurrentComplexPolygon() {
        return currentComplexPolygon;
    }

    public void setCurrentComplexPolygon(int currentComplexPolygon) {
        this.currentComplexPolygon = currentComplexPolygon;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append("Action: " + action)
                .append("\n")
                .append((currentComplexPolygon != null) ? "Current Complex Polygon: " + currentComplexPolygon : "")
                .append("\n")
                .append((currentSimplePolygon != null) ? "Current Simple Polygon: " + currentSimplePolygon : "")
                .append("\n")
                .append((currentVertex != null) ? "Current Vertex: " + currentVertex : "")
                .append("\n")
                .append((currentLatLng != null) ? "Current LatLng: " + currentLatLng.toString() : "")
                .append("\n")
                .toString();
    }
}



