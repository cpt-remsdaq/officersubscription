package com.remsdaq.osapplication.other;

public class UserMessage {

    ////////////////
    // Subscriber //
    ////////////////

    // Missing
    public static final String MISSING_SUBSCRIBER_ID = "Please enter the subscriber's ID";
    public static final String MISSING_SUBSCRIBER_FIRST_NAME = "Please enter the subscriber's first name";
    public static final String MISSING_SUBSCRIBER_LAST_NAME = "Please enter the subscriber's last name";
    public static final String MISSING_SUBSCRIBER_CONTACT_METHOD = "Please enter at least one valid contact method";

    // Invalid
    public static final String INVALID_SUBSCRIBER_EMAIL = "Please enter a valid email address";
    public static final String INVALID_SUBSCRIBER_EMAIL_PRIORITY = "Please select a valid email priority";
    public static final String INVALID_SUBSCRIBER_MOBILE_PRIORITY = "Please select a valid mobile priority";
    public static final String INVALID_SUBSCRIBER_PAGER_PRIORITY = "Please select a valid pager priority";

    /////////////
    // Polygon //
    /////////////

    // Missing
    public static final String MISSING_POLYGON_ID = "Please enter the polygon's ID";
    public static final String MISSING_POLYGON_NAME = "Please enter the polygon's name";
    public static final String MISSING_POLYGON_DESCRIPTION = "Please enter the polygon's description";
    public static final String MISSING_POLYGON_GEOMETRY = "Please draw or import a polygon geometry";
}


