package com.remsdaq.osapplication.rules;

import java.util.function.BiFunction;

import static com.remsdaq.osapplication.rules.ConditionalFunctionInstance.*;

public enum ConditionalFunction {

    INCIDENT_ID_EQUALS(1L, _INCIDENT_ID_EQUALS),
    INCIDENT_BATCH_ID_EQUALS(2L, _INCIDENT_BATCH_ID_EQUALS),

    INCIDENT_DESCRIPTION_CONTAINS(3L, _INCIDENT_DESCRIPTION_CONTAINS);

    private Long id;
    private BiFunction function;

    ConditionalFunction(Long id, BiFunction function) {
        this.id = id;
        this.function = function;
    }

    public BiFunction getFunction() {
        return function;
    }

}
