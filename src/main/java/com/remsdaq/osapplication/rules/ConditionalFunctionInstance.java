package com.remsdaq.osapplication.rules;

import com.remsdaq.osapplication.entities.incidents.Incident;
import com.remsdaq.osapplication.entities.incidents.IncidentType;
import com.remsdaq.osapplication.entities.resources.Resource;
import com.remsdaq.osapplication.utilities.RuleUtilities;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;

public class ConditionalFunctionInstance {

    public final static BiFunction<Incident, Long, Long> _INCIDENT_ID_EQUALS = (incident, id) -> {
        return (incident.getId() != null
                && id.equals(incident.getId()))
                ? id : null;
    };

    public final static BiFunction<Incident, Long, Long> _INCIDENT_BATCH_ID_EQUALS = (incident, batchId) -> {
        return (incident.getId() != null
                && batchId.equals(incident.getBatchId()))
                ? batchId : null;
    };

    public final static BiFunction<Incident, String[], List<String>> _INCIDENT_DESCRIPTION_CONTAINS = (incident, searches) -> {
        List<String> found = new ArrayList<>();
        for (String string : searches) {
            if (incident.getDescription() != null && incident.getDescription().contains(string)) {
                found.add(string);
            }
        }
        return (found.size() == 0) ? null : found;
    };

    public final static BiFunction<Incident, String, String> _INCIDENT_DESCRIPTION_CHANGED_FROM = (incident, description) -> {
        return (incident.getDescription() != null && !incident.getDescription().equals(description)) ? incident.getDescription() : null;
    };

    public final static BiFunction<Incident, Long, IncidentType> _INCIDENT_CONTAINS_TYPE_WITH_ID = (incident, typeId) -> {
        if (incident.getTypes() != null) {
            for (IncidentType type : incident.getTypes()) {
                if (incident.getTypes() != null && type.getId().equals(typeId)) {
                    return type;
                }
            }
        }
        return null;
    };

    public final static BiFunction<Incident, Long[], List<IncidentType>> _INCIDENT_CONTAINS_ALL_TYPES_WITH_ID = (incident, typeIds) -> {
        List<IncidentType> contained = new ArrayList<>();

        for (Long typeId : typeIds) {
            IncidentType type = _INCIDENT_CONTAINS_TYPE_WITH_ID.apply(incident, typeId);
            if (type == null) {
                return null;
            } else {
                contained.add(type);
            }
        }
        return contained;
    };

    public final static BiFunction<Incident, Long[], List<IncidentType>> _INCIDENT_CONTAINS_ANY_TYPE_WITH_ID = (incident, typeIds) -> {
        List<IncidentType> contained = new ArrayList<>();

        for (Long typeId : typeIds) {
            IncidentType type = _INCIDENT_CONTAINS_TYPE_WITH_ID.apply(incident, typeId);
            if (type != null) {
                contained.add(type);
            }
        }
        return (contained.size() == 0) ? null : contained;
    };

    public final static BiFunction<Incident, LocalDate, LocalDate> _INCIDENT_DATE_EQUALS = (incident, date) -> {
        return (incident.getDateTime() != null
                && incident.getDateTime().toLocalDate().isEqual(date))
                ? incident.getDateTime().toLocalDate() : null;
    };

    public final static BiFunction<Incident, LocalDate[], LocalDate> _INCIDENT_DATE_BETWEEN = (incident, dates) -> {
        return incident.getDateTime() != null
                && RuleUtilities.isBetween(incident.getDateTime().toLocalDate(),dates)
                ? incident.getDateTime().toLocalDate() : null;
    };

    public final static BiFunction<Incident, LocalDate, LocalDate> _INCIDENT_DATE_BEFORE = (incident, date) -> {
        return (incident.getDateTime() != null
                && (RuleUtilities.isSmallerThan(incident.getDateTime().toLocalDate(),date)))
                ? incident.getDateTime().toLocalDate() : null;
    };

    public final static BiFunction<Incident, LocalDate, LocalDate> _INCIDENT_DATE_AFTER = (incident, date) -> {
        return (incident.getDateTime() != null
                && (RuleUtilities.isGreaterThan(incident.getDateTime().toLocalDate(),date)))
                ? incident.getDateTime().toLocalDate() : null;
    };

    public final static BiFunction<Incident, LocalTime, LocalTime> _INCIDENT_TIME_EQUALS = (incident, time) -> {
        return (incident.getDateTime() != null
                && incident.getDateTime().toLocalTime().equals(time))
                ? incident.getDateTime().toLocalTime() : null;
    };

    public final static BiFunction<Incident, LocalTime[], LocalTime> _INCIDENT_TIME_BETWEEN = (incident, times) -> {
        return (incident.getDateTime() != null
                && RuleUtilities.isBetween(incident.getDateTime().toLocalTime(),times)
                ? incident.getDateTime().toLocalTime() : null);
    };

    public final static BiFunction<Incident, LocalTime, LocalTime> _INCIDENT_TIME_BEFORE = (incident, time) -> {
        return (incident.getDateTime() != null
                && RuleUtilities.isSmallerThan(incident.getDateTime().toLocalTime(),time)
                ? incident.getDateTime().toLocalTime() : null);
    };

    public final static BiFunction<Incident, LocalTime, LocalTime> _INCIDENT_TIME_AFTER = (incident, time) -> {
        return (incident.getDateTime() != null
                && RuleUtilities.isGreaterThan(incident.getDateTime().toLocalTime(),time)
                ? incident.getDateTime().toLocalTime() : null);
    };

    public final static BiFunction<Incident, LocalTime, LocalTime> _STOP_TIME_EQUALS = (incident, time) -> {
        return (incident.getStopTime() != null
                && incident.getStopTime().toLocalTime().equals(time))
                ? incident.getStopTime().toLocalTime() : null;
    };

    public final static BiFunction<Incident, LocalTime[], LocalTime> _STOP_TIME_BETWEEN = (incident, times) -> {
        return incident.getStopTime() != null
                && RuleUtilities.isBetween(incident.getStopTime().toLocalTime(),times)
                ? incident.getStopTime().toLocalTime() : null;
    };

    public final static BiFunction<Incident, Void, Boolean> _NO_RESOURCES_AT_INCIDENT = (incident, VOID) -> {
        return (incident.getResources() != null && !incident.getResources().isEmpty()) ? false : true;
    };

    public final static BiFunction<Incident, Long, Resource> _INCIDENT_CONTAINS_RESOURCE_WITH_ID = (incident, resourceId) -> {
        if (incident.getResources() != null) {
            for (Resource resource : incident.getResources()) {
                if (resource.getId().equals(resourceId)) {
                    return resource;
                }
            }
        }
        return null;
    };

    public final static BiFunction<Incident, Long[], List<Resource>> _INCIDENT_CONTAINS_ANY_RESOURCE_WITH_ID = (incident, resourceIds) -> {
        List<Resource> contained = new ArrayList<>();
        if (incident.getResources() != null) {
            for (Resource resource : incident.getResources()) {
                for (Long id : resourceIds) {
                    if (id.equals(resource.getId())) {
                        contained.add(resource);
                    }
                }
            }
        }
        return (incident.getResources() != null && !contained.isEmpty()) ? contained : null;
    };

    public final static BiFunction<Incident, Long[], List<Resource>> _INCIDENT_CONTAINS_ALL_RESOURCES_WITH_IDS = (incident, resourceIds) -> {
        for (Long resourceId : resourceIds) {
            Resource resource = _INCIDENT_CONTAINS_RESOURCE_WITH_ID.apply(incident, resourceId);
            if (resource == null) {
                return null;
            }
        }
        return (incident.getResources());
    };

    public final static BiFunction<Incident, Long, Long> _INCIDENT_PRIORITY_EQUALS = (incident, priority) -> {
        return (incident.getPriority() != null
                && incident.getPriority() == priority)
                ? priority : null;
    };

    public final static BiFunction<Incident, Long[], Long> _INCIDENT_PRIORITY_BETWEEN = (incident, priorities) -> {
        return (incident.getPriority() != null
                && RuleUtilities.isBetween(incident.getPriority(), priorities)
                ? incident.getPriority() : null);
    };

    public final static BiFunction<Incident, Long, Long> _INCIDENT_PRIORITY_GREATER_THAN = (incident, priority) -> {
        return (incident.getPriority() != null
                && RuleUtilities.isGreaterThan(incident.getPriority(),priority)
                ? incident.getPriority() : null);
    };

    public final static BiFunction<Incident, Long, Long> _INCIDENT_PRIORITY_SMALLER_THAN = (incident, priority) -> {
        return (incident.getPriority() != null
                && RuleUtilities.isSmallerThan(incident.getPriority(),priority)
                ? incident.getPriority() : null);
    };

    public final static BiFunction<Incident, Integer, Integer> _INCIDENT_NUMBER_OF_CALLS_EQUALS = (incident, numberOfCalls) -> {
        return (incident.getNumberOfCalls() != null
                && incident.getNumberOfCalls() == numberOfCalls)
                ? numberOfCalls : null;
    };

    public final static BiFunction<Incident, Integer[], Integer> _INCIDENT_NUMBER_OF_CALLS_BETWEEN = (incident, numberOfCalls) -> {
        return (incident.getNumberOfCalls() != null
                && RuleUtilities.isBetween(incident.getNumberOfCalls(), numberOfCalls))
                ? incident.getNumberOfCalls() : null;
    };

    public final static BiFunction<Incident, Integer, Integer> _INCIDENT_NUMBER_OF_CALLS_GREATER_THAN = (incident, numberOfCalls) -> {
        return (incident.getNumberOfCalls() != null
                && RuleUtilities.isGreaterThan(incident.getNumberOfCalls(),numberOfCalls))
                ? incident.getNumberOfCalls() : null;
    };

    public final static BiFunction<Incident, Integer, Integer> _INCIDENT_NUMBER_OF_CALLS_SMALLER_THAN = (incident, numberOfCalls) -> {
        return incident.getNumberOfCalls() != null
                && RuleUtilities.isSmallerThan(incident.getNumberOfCalls(),numberOfCalls)
                ? incident.getNumberOfCalls() : null;

    };

}
