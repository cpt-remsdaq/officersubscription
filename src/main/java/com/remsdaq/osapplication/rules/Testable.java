package com.remsdaq.osapplication.rules;

public interface Testable {

    public boolean test(Object object);
}
