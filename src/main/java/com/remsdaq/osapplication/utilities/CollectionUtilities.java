package com.remsdaq.osapplication.utilities;

import java.util.*;

public class CollectionUtilities {

    public static <T> Set<T> toSet(Collection<T> collection) {
        Set<T> result = null;
        if (collection != null) {
            result = new HashSet<>(collection);
        }
        return result;
    }

    public static <T> List<T> toList(Collection<T> collection) {
        List<T> result = null;
        if (collection != null) {
            result = new ArrayList<>(collection);
        }
        return result;
    }

}
