package com.remsdaq.osapplication.utilities;

import java.awt.*;

public class ColourUtilities {

    public static String getColourHex(Color colourValue) {
        String colourHex = "000000";
        if (colourValue != null) {
            colourHex = StringUtilities.intoHexString(colourValue.getRed(),2);
            colourHex += StringUtilities.intoHexString(colourValue.getGreen(),2);
            colourHex += StringUtilities.intoHexString(colourValue.getBlue(),2);
        }
        return colourHex;
    }

    public static int[] getColourDecimals(String colourHex) {
        if (colourHex.startsWith("#")) { // remove "#" if present
            colourHex = colourHex.substring(1,colourHex.length());
        } else if (colourHex.startsWith("0x")) { // remove "0x" if present
            colourHex = colourHex.substring(2,colourHex.length());
        }
        int red = (int) Long.parseLong(colourHex.substring(0,2),16);
        int green = (int) Long.parseLong(colourHex.substring(2,4),16);
        int blue = (int) Long.parseLong(colourHex.substring(4,6), 16);

        int[] colourDecimals = {red,green,blue};
        return colourDecimals;
    }

    public static Boolean isCloserToBlackThanWhite(String colourHex) {
        int[] colourDecimals = getColourDecimals(colourHex);
        double luminance = 0.2126*colourDecimals[0] + 0.7152*colourDecimals[1] + 0.0722*colourDecimals[2];
        return (luminance < 128);
    }

}
