package com.remsdaq.osapplication.utilities;

public class ConstantUtilities {

    // Earth ellipsoid radii
    public static final Double EQUATORIAL_RADIUS_M = 6378136.6;
    public static final Double POLAR_RADIUS_M = 6356751.9;
    public static final Double AVERAGE_RADIUS_M = 6371008.0;

    // EQUATORIAL_RADIUS/(EQUATORIAL_RADIUS - POLAR_RADIUS)
    // Used for... things
    public static final Double INVERSE_FLATTENING = 298.257;


}
