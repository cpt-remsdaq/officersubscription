/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities;

import com.sun.org.apache.xerces.internal.impl.dv.DTDDVFactory;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Date;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.toMap;

import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

/**
 *
 * @author cpt
 */
public abstract class DateTimeUtilities {
 
    
    private static Map<Long, String> DAYS_LOOKUP =IntStream.rangeClosed(1, 31).boxed().collect(toMap(Long::valueOf, i -> DateTimeUtilities.getOrdinal(i)));
    private static Map<Long, String> COMMIE_DAYS_LOOKUP =IntStream.rangeClosed(1, 31).boxed().collect(toMap(Long::valueOf, i -> DateTimeUtilities.getOrdinal(-i)));
    
    // Gets the ordinal for a particular day of the month
    public static String getOrdinal(int n) {
        
        if (n > 0) {            
            if (n >= 11 && n <= 13) {
                return n + "th";
            }
            switch (n % 10) {
                case 1:  return n + "st";
                case 2:  return n + "nd";
                case 3:  return n + "rd";
                default: return n + "th";
            }
        } else {
            // make n negative or 0 if you want the euro commie hippie hoop
            //(the boy one cause dates can't be girls)
            //(because of the glass ceiling)
            return -n + "º";
        }
    }
    
    // Check if the date is yesterday
    public static boolean isYesterday (LocalDateTime dateTime) {
        LocalDate yesterday = LocalDate.now().minusDays(1);
        return yesterday.equals(dateTime.toLocalDate());
    }
    
    // Check if the date is today
    public static boolean isToday (LocalDateTime dateTime) {
        LocalDate today = LocalDate.now();
        return today.equals(dateTime.toLocalDate());
    }
    
    // Get date as a user friendly string
    // If yesterday, returns "yesterday"
    // If today, returns "today"
    // Else returns dd_ordinal MMMM yy
    public static String getUserFriendlyDateString (LocalDateTime dateTime) {
        return getUserFriendlyDateString(dateTime, "d.MY");
    }
    
    // Get date as a user friendly string
    // If yesterday, returns "yesterday"
    // If today, returns "today"
    // Else returns date in one of user friendly formats
    public static String getUserFriendlyDateString (LocalDateTime dateTime, String formatType) {
        if (isToday(dateTime)) {
            return "today";
        } else if (isYesterday(dateTime)) {
            return "yesterday";
        } else {
            return getSlightlyLessUserFriendlyDateString(dateTime, formatType);
        }
    }
    
    // returns dd_ordinal MMMM yy
    public static String getSlightlyLessUserFriendlyDateString(LocalDateTime dateTime) {
        return dateTime.format(getUserFriendlyFormatter("d.MY"));
    }
    
    // returns date in one of user friendly formats
    public static String getSlightlyLessUserFriendlyDateString (LocalDateTime dateTime, String formatType) {
        return dateTime.format(getUserFriendlyFormatter(formatType));
    }
    
    // formatter for dd_ordinal MMMM yy
    public static DateTimeFormatter getUserFriendlyFormatter() {
        return getUserFriendlyFormatter("d.MY");
    }
    
    // Set of user friendly setFormatters denoted by format string
    public static DateTimeFormatter getUserFriendlyFormatter(String formatType) {
        
        DateTimeFormatterBuilder builder = new DateTimeFormatterBuilder();
        
        String[] formatTypeSplit = formatType.split(",");
        String[] modifiers = {"_",".","!"};
        String currentModifier = "";
        Boolean modifierAttached = false;
        
        
        for (int i = 0; i < formatTypeSplit.length; i++) {
            
            for (String modifier : modifiers) {
                if (formatTypeSplit[i].lastIndexOf(modifier) == formatTypeSplit[i].length() - 1) {
                    formatTypeSplit[i] = formatTypeSplit[i].substring(0, formatTypeSplit[i].length() - 1);
                    
                    currentModifier = modifier;
                    modifierAttached = true;
                    break;
                }
            }
            
            switch  (formatTypeSplit[i]) {
                case "day+":
                    builder.appendText(ChronoField.DAY_OF_MONTH, DAYS_LOOKUP);
                    break;
                case "day-":
                    builder.appendText(ChronoField.DAY_OF_MONTH, COMMIE_DAYS_LOOKUP);
                    break;
                case "day":
                    builder.appendPattern("dd");
                    break;
                case "month":
                    builder.appendPattern("MMM");
                    break;
                case "MONTH":
                    builder.appendPattern("MMMM");
                    break;
                case "year":
                    builder.appendPattern("yy");
                    break;
                case "YEAR":
                    builder.appendPattern("yyyy");
                    break;
                case "hour":
                    builder.appendPattern("hh");
                    break;
                case "minute":
                    builder.appendPattern("mm");
                    break;
                case "ampm":
                    builder.appendPattern("a");
                    break;
                case "HOUR":
                    builder.appendPattern("HH");
                    break;
                case "long time":
                    builder.appendPattern("HH':'mm':'ss");
                    break;
                case "short time":
                    builder.appendPattern("HH':'mm");
                    break;
                case "weekday":
                    builder.appendPattern("E");
                    break;
                case ":":
                    builder.appendLiteral(":");
                    break;
                case "long date":
                    builder.appendText(ChronoField.DAY_OF_MONTH, DAYS_LOOKUP);
                    builder.appendPattern("' 'MMMM' 'yyyy");
                    break;
                case "short date":
                    builder.appendPattern("dd'.'MM'.'yyyy");
                    break;
                default:
                    if (formatTypeSplit[i].matches("'.*'")) {
                        builder.appendLiteral(formatTypeSplit[i].substring(1,formatTypeSplit[i].length()-1));
                    }
            }
            
            if (modifierAttached) {
                switch (currentModifier) {
                    case "_":
                        builder.appendLiteral(" ");
                        break;
                    case ".":
                        builder.appendLiteral(".");
                        break;
                }
                modifierAttached = false;
            }
        }
        
        return builder.toFormatter();
    }

    public static Date unmarshal(String dateString, DateFormat dateFormat, Logger logger) {
        try {
            return dateFormat.parse(dateString);
        } catch (Exception ex) {
            if (logger != null) {
                logger.log(Level.WARNING, String.format("Failed to parse string %s as date", dateString), ex);
            }
            return null;
        }
    }

    public static Date unmarshal(String dateString, List<DateFormat> dateFormats, Logger logger) {
        Date date = null;
        if (dateFormats != null) {
            for (DateFormat dateFormat : dateFormats) {
                try {
                    date = dateFormat.parse(dateString);
                } catch (Exception ex) {
                }
            }
        }
        if (date == null && logger != null) {
            logger.log(Level.WARNING, String.format("Failed to parse string %s as date", dateString));
        }
        return date;
    }

    public static LocalDate unmarshalLocalDate(String dateString, List<DateTimeFormatter> dateFormats, Logger logger) {
            try {
                return LocalDate.parse(dateString);

            } catch (Exception ex) {
                for (DateTimeFormatter dateFormatter : dateFormats) {
                    try {
                        return LocalDate.parse(dateString, dateFormatter);
                    } catch (Exception ex2) {
                    }
                }
            }

            logger.log(Level.WARNING, String.format("Failed to parse string %s as date", dateString));
            return null;
    }

    public static LocalTime unmarshallLocalTime(String timeString, List<DateTimeFormatter> timeFormats, Logger logger) {
        try {
            return LocalTime.parse(timeString);

        } catch (Exception ex) {
            for (DateTimeFormatter timeFormatter : timeFormats) {
                try {
                    return LocalTime.parse(timeString, timeFormatter);
                } catch (Exception ex2) {
                }
            }
        }

        logger.log(Level.WARNING, String.format("Failed to parse string %s as time", timeString));
        return null;
    }

    public static String marshal(Date date, DateFormat dateFormat, Logger logger) {
        try {
            return dateFormat.format(date);
        } catch (Exception ex) {
            if (logger != null) {
                logger.log(Level.WARNING, String.format("Failed to format date %s", date), ex);
            }
            return null;
        }
    }

    public static String marshalLocalDate(LocalDate localDate, DateTimeFormatter dateFormat, Logger logger) {
        try {
            return dateFormat.format(localDate);
        } catch (Exception ex) {
            if (logger != null) {
                logger.log(Level.WARNING, String.format("Failed to format date %s", localDate), ex);
            }
        }
        return null;
    }

    public static String marshalLocalTime(LocalTime localTime, DateTimeFormatter timeFormat, Logger logger) {
        try {
            return timeFormat.format(localTime);
        } catch (Exception ex) {
            if (logger != null) {
                logger.log(Level.WARNING, String.format("Failed to format time %s", localTime), ex);
            }
        }
        return null;
    }


    
}
