package com.remsdaq.osapplication.utilities;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtilities {
	
	public static File getResourceAsFile(String resourceName) {
		File file = null;
		try {
			file = new File(FileUtilities.class.getClassLoader().getResource(resourceName).getFile());
		} catch (Exception ex) {
			Logger.getLogger(FileUtilities.class.getName()).log(Level.SEVERE, "", ex);
		}
		return file;
	}
	
	public static String getResourceAsString(String resourceName, String encoding) {
		String string = null;
		try {
			string = new String(Files.readAllBytes(
					Paths.get(FileUtilities.class.getClassLoader().getResource(resourceName).toURI())), encoding);
		} catch (Exception ex) {
			Logger.getLogger(FileUtilities.class.getName()).log(Level.SEVERE, "", ex);
		}
		return string;
	}
	
	public static URL getResourceAsUrl(String resourceName) {
		URL url = null;
		try {
			url = Thread.currentThread().getContextClassLoader().getResource(resourceName);
		} catch (Exception ex) {
			Logger.getLogger(FileUtilities.class.getName()).log(Level.SEVERE, "", ex);
		}
		return url;
	}
	
	public static void appendStringToFile(String path, String content) throws IOException {
		File f = new File(path);
		try (FileWriter fw = new FileWriter(f)) {
			fw.append(content);
		}
	}
	
	public static void writeStringToFile(String path, String content) throws IOException {
		File f = new File(path);
		try (FileWriter fw = new FileWriter(f)) {
			fw.write(content);
		}
	}
	
	public static String readFileToString(String path) throws IOException {
		List<String> fileStrings = Files.readAllLines(Paths.get(path));
		String fileString = "";
		for (String line : fileStrings){
			fileString += line + "\n";
		}
		return fileString;
	}
	
	public static File getFileFromResources(Class clazz, String path) {
		ClassLoader classLoader = clazz.getClassLoader();
		return new File(classLoader.getResource(path).getFile());
	}
}
