package com.remsdaq.osapplication.utilities;

import com.remsdaq.osapplication.entities.mait.MaitSchema;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MaitUtilities {

    private static final Pattern SCHEMA_VERSION_PATTERN = Pattern.compile("schema\\s*=\\s*\"([^\"]*)\"", Pattern.CASE_INSENSITIVE);
    private static final int SCHEMA_VERSION_PATTERN_GROUP = 1;

    private MaitUtilities() {
        throw new AssertionError();
    }

    public static String getSchemaVersion(String xml) throws Exception {
        final Matcher matcher = SCHEMA_VERSION_PATTERN.matcher(xml);
        if (matcher.find()) {
            return matcher.group(SCHEMA_VERSION_PATTERN_GROUP);
        } else {
            throw new Exception(xml);
        }
    }

    public static MaitSchema getMaitSchema(String xml) throws Exception {
        return MaitSchema.getMaitSchema(getSchemaVersion(xml));
    }

}