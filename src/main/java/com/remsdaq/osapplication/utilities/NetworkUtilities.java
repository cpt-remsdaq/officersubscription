package com.remsdaq.osapplication.utilities;

public class NetworkUtilities {

    public static Boolean isValidIPAddress(String ipAddress) {
        Boolean valid = false;

        Parameters parameters = new Parameters(ipAddress, '.');
        if ((parameters.getSize() == 4)
                && (parameters.getIntParam(0) < 255)
                && (parameters.getIntParam(0) >= 0)
                && (parameters.getIntParam(1) < 255)
                && (parameters.getIntParam(1) >= 0)
                && (parameters.getIntParam(2) < 255)
                && (parameters.getIntParam(2) >= 0)
                && (parameters.getIntParam(3) < 255)
                && (parameters.getIntParam(3) >= 0)) {
            valid = true;
        }
        return valid;
    }
}
