/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities;

import com.remsdaq.osapplication.gui.main_application.PolygonCheckBox;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author cpt
 */
public class NodeUtilities {

	public static ArrayList<Node> getAllNodes(Parent root) {
		ArrayList<Node> Descenedents = new ArrayList<>();
		root.getChildrenUnmodifiable().stream().forEach((Node N) -> {
			if (!Descenedents.contains(N)) {
				Descenedents.add(N);
			}
			if (N instanceof Parent) {
				Descenedents.addAll(getAllNodes((Parent) N));
			}
		});
		return Descenedents;
	}

	public static void setAllNodesEditable(Parent root, Boolean b) {
		setAllNodesEditable(getAllNodes(root), b);
	}

	public static void setAllNodesEditable(List<Node> nl, Boolean b) {
		for (Node n : nl) {
			setNodeEditable(n, b);
		}
	}

	private static void setNodeEditable(Node n, Boolean b) {
		if (n.getClass() == TextField.class) {
			TextField textObj = (TextField) n;
			textObj.setEditable(b);
		} else if (n.getClass() == TextArea.class) {
			TextArea textObj = (TextArea) n;
			textObj.setEditable(b);
		} else if (n.getClass() == ComboBox.class) {
			ComboBox comboObj = (ComboBox) n;
			comboObj.setEditable(b);
		} else if (n.getClass() == ChoiceBox.class) {
			ChoiceBox choiceObj = (ChoiceBox) n;
		}
	}

	public static void clearAllNodes(Parent root) {
		clearAllNodes(getAllNodes(root));
	}

	public static void clearAllNodes(List<Node> nl) {
		for (Node n : nl) {
			clearNode(n);
		}
	}

	private static void clearNode(Node n) {
		if (n.getClass() == TextField.class) {
			TextField textObj = (TextField) n;
			textObj.clear();
		} else if (n.getClass() == TextArea.class) {
			TextArea textObj = (TextArea) n;
			textObj.clear();
		}
	}

	public static void setAllNodesDisabled(Parent root, Boolean b) {
		setAllNodesDisabled(getAllNodes(root), b);
	}

	public static void setAllNodesDisabled(List<Node> nl, Boolean b) {
		for (Node n : nl) {
			setNodeDisabled(n, b);
		}
	}

	private static void setNodeDisabled(Node n, Boolean b) {
		for (Disableable nodeType : Disableable.values()) {
			if (n.getClass() == nodeType.getClazz()) {
				n.setDisable(b);
			}
		}
	}

	public static void switchPanes(Pane newPane, Pane oldPane) {
		ObservableList oldPaneChildren = oldPane.getChildren();
		ObservableList newPaneChildren = newPane.getChildren();
		ArrayList<Node> oldNodes = NodeUtilities.getAllNodes(oldPane);
		ArrayList<Node> newNodes = NodeUtilities.getAllNodes(newPane);
		for (Node n : oldNodes) {
			setNodeDisabled(n, true);
			setNodeEditable(n, false);
		}
		for (Node n : newNodes) {
			setNodeDisabled(n, false);
			setNodeEditable(n, true);
		}
	}

	enum Disableable {
		BUTTON(Button.class),
		POLYGON_CHECK_BOX(PolygonCheckBox.class),
		WEB_VIEW(WebView.class),
		COMBO_BOX(ComboBox.class),
		CHOICE_BOX(ChoiceBox.class);

		private Class clazz;

		Disableable(Class clazz) {
			this.clazz = clazz;
		}

		public Class getClazz() {
			return this.clazz;
		}
	}
}
