package com.remsdaq.osapplication.utilities;

import java.util.Vector;

/**
 *
 * @author ita
 */
public class Parameters {

    private Vector params = new Vector(5, 5);

    /**
     * Creates a new instance of Tokens
     */
    public Parameters() {
    }

    public Parameters(String strparams, char delimiter) {
        this(strparams, delimiter, true);
    }

    public Parameters(String strparams, char delimiter, boolean bTrim) {
        boolean last = false;
        int i = 0;
        StringBuffer param = new StringBuffer();
        while (!last) {
            if (getDelimitedParam(i, strparams, param, delimiter, bTrim) == i) {
                if (param == null) {
                    params.addElement("");
                } else {
                    params.addElement(param.toString());
                }
                i++;
            } else {
                last = true;
            }
        }
    }

    public Parameters(String strparams, int maxparams) {
        boolean last = false;
        int i = 0;
        StringBuffer param = new StringBuffer();
        while (!last && (i < maxparams)) {
            if (getWhiteDelimitedParam(i, strparams, param) == i) {
                params.addElement(param.toString());
                i++;
            } else {
                last = true;
            }
        }
    }

    public int getSize() {
        return params.size();
    }

    public String getParam(int i) {
        String str = new String();
        if (i < params.size()) {
            str = (String) params.get(i);
        }
        return str;
    }

    public Integer getIntParam(int i) {
        Integer param;
        if (i < params.size()) {
            param = Integer.parseInt(params.get(i).toString());
            return param;
        }
        return null;
    }

    public Double getDoubleParam(int i) {
        Double param;
        if (i > params.size()) {
            param = Double.parseDouble(params.get(i).toString());
            return param;
        }
        return null;
    }

    public boolean isNumericParam(int i) {
        boolean result = true;
        String strval = getParam(i);
        try {
            int x = Integer.parseInt(strval);
        } catch (NumberFormatException numException) {
            result = false;
        }
        return result;
    }

    public boolean isBlankParam(int i) {
        boolean result = false;
        String strval = getParam(i);
        if (strval.trim().length() < 1) {
            result = true;
        }
        return result;
    }

    private int getDelimitedParam(int index, String params, StringBuffer retnparam, char delim) {
        return this.getDelimitedParam(index, params, retnparam, delim, true);
    }

    private int getDelimitedParam(int index, String params, StringBuffer retnparam, char delim, boolean bTrim) {
        int i, cnt, from;
        String strParam;
        cnt = -1;
        i = 0;
        from = 0;
        retnparam.setLength(0);
        if (params.length() > 0) {
            while (++cnt <= index) {
                i = params.indexOf(delim, from);
                if (cnt == index) {
                    if (i >= from) {
                        strParam = params.substring(from, i);
                        if (bTrim) {
                            strParam = strParam.trim();
                        }
                        retnparam.append(strParam);
                    } else {
                        strParam = params.substring(from);
                        if (bTrim) {
                            strParam = strParam.trim();
                        }
                        retnparam.append(strParam);
                    }
                    break;
                } else {

                    if ((i >= from) && ((i + 1) <= params.length())) {
                        from = ++i;
                    } else {
                        // param not found
                        cnt = -1;
                        break;
                    }
                }
            }
        }
        return cnt;
    }

    private int getWhiteDelimitedParam(int index, String params, StringBuffer retnparam) {
        int i, ix, cnt, from, len;
        boolean delimfound;
        cnt = -1;
        i = 0;
        from = 0;
        retnparam.setLength(0);
        if ((len = params.length()) > 0) {
            while (++cnt <= index) {
                ix = from;
                while (ix < len) {
                    if (java.lang.Character.isWhitespace(params.charAt(ix))) {
                        break;
                    } else {
                        ix++;
                    }
                }
                if (ix >= len) {
                    i = -1;
                } else {
                    i = ix;
                }
                if (cnt == index) {
                    if (i >= from) {
                        retnparam.append(params.substring(from, i).trim());
                    } else {
                        retnparam.append(params.substring(from).trim());
                    }
                    break;
                } else {
                    if (i > from) {
                        from = ++i;
                        while (from < len) {
                            delimfound = false;
                            if (java.lang.Character.isWhitespace(params.charAt(from))) {
                                from++;
                            } else {
                                break;
                            }
                        }
                    } else {
                        // param not found
                        cnt = -1;
                        break;
                    }
                }
            }
        }
        return cnt;
    }

}
