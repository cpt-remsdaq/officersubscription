package com.remsdaq.osapplication.utilities;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Joseph on 08/04/2014.
 * Taken from Resque
 */
public class PasswordUtilities {

    private static final Logger log = Logger.getLogger(PasswordUtilities.class.getName());

    public static String hashPassword(String password){
        String encodedPasswordHash = "";
        if (password != null && !password.isEmpty()) {
            try {
                MessageDigest md = java.security.MessageDigest.getInstance("MD5");
                md.update(password.getBytes("UTF-8"));

                byte[] passwordDigest = md.digest();
                encodedPasswordHash = new sun.misc.BASE64Encoder().encode(passwordDigest);
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
                log.log(Level.SEVERE, "", e);
            }
        }
        return encodedPasswordHash;
    }

    private String convertMD5ToString(byte[] passwordDigest) {
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < passwordDigest.length; i++) {
            if ((0xff & passwordDigest[i]) < 0x10) {
                hexString.append("0"
                        + Integer.toHexString((0xFF & passwordDigest[i])));
            } else {
                hexString.append(Integer.toHexString(0xFF & passwordDigest[i]));
            }
        }
        return hexString.toString();
    }

    /**
     * URL's have three control characters that base64 uses (+ / =) we need to encode these into url characters
     *
     * @param base64Password
     * @return
     */
    public static String urlEncodeBase64(String base64Password) {
        String convertedPassword = base64Password;
        if (base64Password != null && !base64Password.isEmpty()) {
            try {
                convertedPassword = convertedPassword.replaceAll("\\+", "%2B");
                convertedPassword = convertedPassword.replaceAll("/", "%2F");
                convertedPassword = convertedPassword.replaceAll("=", "%3D");
            } catch (Exception e) {
                log.log(Level.SEVERE, "", e);
            }
        }
        return convertedPassword;
    }

    public static boolean isValidMD5(String s) {
        return s.matches("[a-fA-F0-9]{32}");
    }
}
