
package com.remsdaq.osapplication.utilities;

/**
 *
 * @author cpt
 */
public class RegexUtilities {
    
    // RegexUtilities for input validation

    // Really weak matching.
    // Just makes sure there aren't any smiley faces the string if I'm honest.
    public static final String PHONE_REGEX = "^\\+?[0-9]{0,13}$";

    // Matches the emails of people who aren't weirdos.
    // Matches only uppercase strings.
    // The 63 char domain extension is properly overkill but apparently some people use them
    public static final String EMAIL_REGEX = "^\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,63}\\b";

    // Matches pretty much all (current) UK postcodes, with and without space.
    // Matches only uppercase strings.
    public static final String UK_POSTCODE_REGEX = "(((^[BEGLMNS][1-9]\\d?)|(^W[2-9])|(^(A[BL]|B[ABDHLNRST]|C[ABFHMORTVW]|D[ADEGHLNTY]|E[HNX]|F[KY]|G[LUY]|H[ADGPRSUX]|I[GMPV]|JE|K[ATWY]|L[ADELNSU]|M[EKL]|N[EGNPRW]|O[LX]|P[AEHLOR]|R[GHM]|S[AEGKL-PRSTWY]|T[ADFNQRSW]|UB|W[ADFNRSV]|YO|ZE)\\d\\d?)|(^W1[A-HJKSTUW0-9])|(((^WC[1-2])|(^EC[1-4])|(^SW1))[ABEHMNPRVWXY]))(\\s*)?([0-9][ABD-HJLNP-UW-Z]{2}))|(^GIR\\s?0AA)";
    
}
