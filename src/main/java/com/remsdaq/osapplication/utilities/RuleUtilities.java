package com.remsdaq.osapplication.utilities;

public class RuleUtilities {
    public static boolean isBetween(Comparable comparitor, Comparable[] range) {
        return (comparitor != null)
                && (range != null)
                && (range.length == 2)
                && (min(range).compareTo(comparitor) <= 0)
                && (max(range).compareTo(comparitor) >= 0)
                ;
    }

    public static boolean isGreaterThan(Comparable comparitor, Comparable other) {
        return (comparitor != null)
                && (other != null)
                && (comparitor.compareTo(other) > 0);
    }

    public static boolean isSmallerThan(Comparable comparitor, Comparable other) {
        return (comparitor != null)
                && (other != null)
                && (comparitor.compareTo(other) < 0);
    }

    public static Comparable max(Comparable[] range) {
        return (range != null)
                && (range.length == 2)
                && range[0].compareTo(range[1]) >= 0
                ? range[0] : range[1];
    }

    public static Comparable min(Comparable[] range) {
        return (range != null)
                && (range.length == 2)
                && (range[0].compareTo(range[1]) <= 0)
                ? range[0] : range[1];
    }
}
