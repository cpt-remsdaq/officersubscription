/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities;

import java.util.Random;

/**
 *
 * @author cpt
 */
public abstract class StringUtilities {
    
    public static String enumerate(String s, int n) {
        return s + " " + n;
    }
    
    public static String capitalise(String string) {
        string = string.toLowerCase();
        return string.substring(0, 1).toUpperCase() + string.substring(1);
    }
    
    public static String capitaliseFirstWord(String string) {
        return string.substring(0,1).toUpperCase() + string.substring(1);
    }
    
    public static String capitalizeEveryWorld(String string) {
        String[] splitArray = string.split("\\s+");
        String outString = "";
        for (String split : splitArray) {
            outString+= capitalise(split) + " ";
        }
        return outString;
    }

    public static String createRandomString(int stringLength) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!Â£$%^&*()1234567890_-+=|<,>.?/{[}]:;@~#";
        Random rng = new Random();
        char[] text = new char[8];
        for (int i = 0; i < 8; i++) {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        String randomString = new String(text);
        return randomString;
    }

    public static Boolean isEmptyNullOrBlank(String string){
        return string == null || string.isEmpty() || string == "";
    }

    public static Boolean isAllEmptyNullOrBlank(String[] stringArray) {
        Boolean out = true;

        for (String string : stringArray) {
            if (!isEmptyNullOrBlank(string)) {
                out = false;
            }
        }

        return out;
    }

    public static String intoHexString(int i) {
        return Integer.toHexString(i);
    }
    
    public static String intoHexString(int i, int iMinSize) {
        return padString(Integer.toHexString(i),iMinSize, '0', true);
    }

    public static String padString(String string, int length, char paddingChar, boolean leading) {

        int numberCharsToAdd;
        if (string == null) {
            string = "";
        }
        numberCharsToAdd = length - string.length();
        for (int i = 0; i < numberCharsToAdd; i++) {
            if (leading) {
                string = string + paddingChar;
            } else {
                string = paddingChar + string;
            }
        }

        return string;
    }
}
