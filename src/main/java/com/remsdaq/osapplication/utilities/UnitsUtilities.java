package com.remsdaq.osapplication.utilities;

public class UnitsUtilities {


    public enum Unit {

        // AREA
        METRE_SQUARED(1.0),
        KILOMETER_SQUARED(1e-6),
        MILE_SQUARED(3.86102e-7),
        ACRE(0.000247105),
        HECTARE(0.0001)
        ;

        private double value;

        Unit(double value) {
            this.value = value;
        }

        public double getValue() {
            return value;
        }
    }

    public double convertToDifferentUnit(double val, Unit currentUnit, Unit desiredUnit) {
        return val*desiredUnit.value/currentUnit.value;
    }

    public double convertToDifferentUnit(int val, Unit currentUnit, Unit desiredUnit) {
        return val*desiredUnit.value/currentUnit.value;
    }
}
