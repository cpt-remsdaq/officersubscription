/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities;

import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.entities.polygons.PolygonGeometry;
import com.remsdaq.osapplication.entities.polygons.SimplePolygon;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;

import java.util.ArrayList;
import java.util.regex.Pattern;

import static com.remsdaq.osapplication.utilities.WKTUtilities.GeometryType.INVALID;

/**
 * @author cpt
 */
public abstract class WKTUtilities {
	
	private static final String SINGLE_HEAD = "POLYGON ";
	private static final String MULTIPLE_HEAD = "MULTIPOLYGON (";
	private static final String LEFT_BRACKET = "(";
	private static final String RIGHT_BRACKET = ")";
	private static final String VERTEX_SEPARATOR_REGEX = "\\s*,\\s*";
	
	public static Boolean areBracketsBalanced(String WKTString) {
		final char LEFT_BRACKET = '(';
		final char RIGHT_BRACKET = ')';
		
		int leftCounter = 0;
		int rightCounter = 0;
		for (int i = 0; i < WKTString.length(); i++) {
			if (WKTString.charAt(i) == LEFT_BRACKET) {
				leftCounter++;
			} else if (WKTString.charAt(i) == RIGHT_BRACKET) {
				rightCounter++;
			}
		}
		return leftCounter == rightCounter;
	}
	
	public static boolean areStrictlyEquivalent(String firstWKT, String secondWKT) throws WellKnownTextException {
		String firstWKTNormalised = encode(decode(firstWKT));
		String secondWKTNormalised = encode(decode(secondWKT));
		return (firstWKTNormalised.equals(secondWKTNormalised));
	}
	
	/**
	 * Returns a MD array of the wkt geometry from a given wkt string
	 *
	 * @param WKTString
	 *
	 * @return
	 *
	 * @throws Exception
	 */
	public static PolygonGeometry decode(String WKTString) throws WellKnownTextException {
		GeometryType polyType = evaluate(WKTString);
		
		// if string is in wkt format
		if (polyType != INVALID) {
			// remove prefix
			WKTString = WKTString.replaceAll("^([\\s*\\w*]*[\\s*\\(]*)", "");
			// remove suffix
			WKTString = WKTString.replaceAll("([\\s*\\)]*)$", "");
		}
		
		PolygonGeometry multipolygon = new PolygonGeometry();
		
		switch (polyType) {
			case INVALID:
				throw new WellKnownTextException("Cannot decode string as it is not in wkt format: " + WKTString + ".");
			case SINGLE_SIMPLE:
				multipolygon = extractSimplePolygonFromWKT(WKTString);
				break;
			case SINGLE_COMPLEX:
				multipolygon = extractComplexPolygonFromWKT(WKTString);
				break;
			case MULTIPLE_SIMPLE:
			case MULTIPLE_COMPLEX:
				multipolygon = extractMultipolygonFromWKT(WKTString);
				break;
			default:
				throw new WellKnownTextException("String did not evaluate to be in wkt format: " + WKTString + ".");
		}
		return multipolygon;
	}
	
	public static String encode(PolygonGeometry multiPolygon) {
		String WKTString = "";
		GeometryType polyType = evaluate(multiPolygon);
		
		if (polyType == GeometryType.SINGLE_SIMPLE || polyType == GeometryType.SINGLE_COMPLEX)
			WKTString += SINGLE_HEAD;
		else if (polyType == GeometryType.MULTIPLE_SIMPLE || polyType == GeometryType.MULTIPLE_COMPLEX)
			WKTString += MULTIPLE_HEAD;
		
		for (int complexPolygon = 0; complexPolygon < multiPolygon.complexPolygonCount(); complexPolygon++) {
			WKTString += encodeComplexPolygon(multiPolygon, complexPolygon);
		}
		
		if (polyType == GeometryType.MULTIPLE_SIMPLE || polyType == GeometryType.MULTIPLE_COMPLEX)
			WKTString += RIGHT_BRACKET;
		return WKTString;
	}
	
	public static GeometryType evaluate(PolygonGeometry multiPolygon) {
		Boolean single;
		Boolean simple;
		
		if (multiPolygon == null) {
			single = true;
			simple = true;
		} else {
			single = multiPolygon.isSingle();
			simple = multiPolygon.isSimple();
		}
		
		if (single && simple)
			return GeometryType.SINGLE_SIMPLE;
		else if (single && !simple)
			return GeometryType.SINGLE_COMPLEX;
		else if (!single && simple)
			return GeometryType.MULTIPLE_SIMPLE;
		else if (!single && !simple)
			return GeometryType.MULTIPLE_COMPLEX;
		else
			return INVALID;
	}
	
	public static GeometryType evaluate(String WKTString) {
		Boolean isWKT = isWKT(WKTString);
		Boolean isSingle = isSingle(WKTString);
		Boolean isSimple = isSimple(WKTString);
		
		if (isWKT) {
			if (isSingle && isSimple)
				return GeometryType.SINGLE_SIMPLE;
			else if (isSingle && !isSimple)
				return GeometryType.SINGLE_COMPLEX;
			else if (!isSingle && isSimple)
				return GeometryType.MULTIPLE_SIMPLE;
			else if (!isSingle && !isSimple)
				return GeometryType.MULTIPLE_COMPLEX;
		}
		return INVALID;
	}
	
	public static PolygonGeometry extractComplexPolygonFromWKT(String WKTString) {
		
		// Outer multipolygon container
		PolygonGeometry multipolygon = new PolygonGeometry();
		// inner complex polygon container
		ArrayList<SimplePolygon> complexPolygon = new ArrayList<>();
		
		// Split into each individual polygon string
		String[] simplePolygonStrings = WKTString.split("(\\s*\\)\\s*,?\\s*\\(\\s*)");
		
		// for eeach individual polygon, add it to the complex polygon arraylist
		for (String simplePolygonString : simplePolygonStrings) {
			// inner simple polygon container
			SimplePolygon simplePolygon = extractSinglePolygonFromWKT(simplePolygonString);
			complexPolygon.add(simplePolygon);
		}
		
		multipolygon.addComplexPolygon(complexPolygon);
		
		return multipolygon;
	}
	
	public static PolygonGeometry extractMultipolygonFromWKT(String WKTString) {
		// Outer multipolygon container
		PolygonGeometry multipolygon = new PolygonGeometry();
		
		// Split wkt into each complex polygon
		//		String[] complexPolygonStrings = WKTString.split("(\\s*\\){2}\\s*,?\\s*\\({2}\\s*)");
		String[] complexPolygonStrings = WKTString.split("((\\s*\\)\\s*){2}\\s*,?\\s*(\\s*\\(\\s*){2})");
		
		for (String complexPolygonString : complexPolygonStrings) {
			// Split into each individual polygon string
			String[] simplePolygonStrings = complexPolygonString.split("(\\s*\\)\\s*,?\\s*\\(\\s*)");
			
			ArrayList<SimplePolygon> complexPolygon = new ArrayList<>();
			for (String simplePolygonString : simplePolygonStrings) {
				SimplePolygon simplePolygon = extractSinglePolygonFromWKT(simplePolygonString);
				complexPolygon.add(simplePolygon);
			}
			multipolygon.addComplexPolygon(complexPolygon);
		}
		return multipolygon;
	}
	
	public static PolygonGeometry extractSimplePolygonFromWKT(String WKTString) {
		PolygonGeometry multipolygon = new PolygonGeometry();
		ArrayList<SimplePolygon> complexPolygon = new ArrayList<>();
		SimplePolygon simplePolygon = extractSinglePolygonFromWKT(WKTString);
		complexPolygon.add(simplePolygon);
		multipolygon.addComplexPolygon(complexPolygon);
		return multipolygon;
	}
	
	public static SimplePolygon extractSinglePolygonFromWKT(String WKTString) {
		SimplePolygon singlePolygon = new SimplePolygon();
		String[] WKTCoordinatePairs = WKTString.split(VERTEX_SEPARATOR_REGEX);
		
		for (int i = 0; i < WKTCoordinatePairs.length; i++) {
			String[] coordinateStrings = WKTCoordinatePairs[i].split("\\s+");
			double lng = Double.parseDouble(coordinateStrings[0]);
			double lat = Double.parseDouble(coordinateStrings[1]);
			LatLng latLng = new LatLng(lng, lat);
			
			singlePolygon.addVertex(latLng);
		}
		return singlePolygon;
	}
	
	public static Boolean isSimple(String WKTString) {
		String isComplexString = "^.*[^\\)](\\),\\()[^\\(].*";
		Boolean isComplex = Pattern.compile(isComplexString).matcher(WKTString).matches();
		return !isComplex;
	}
	
	public static Boolean isSingle(String WKTString) {
		String isSingleString = "^(\\s*POLYGON).*";
		Boolean isSingle = Pattern.compile(isSingleString).matcher(WKTString).matches();
		return isSingle;
	}
	
	public static Boolean isWKT(String WKTString) {
		String isWKTString = "(\\s*POLYGON|\\s*MULTIPOLYGON)(\\s*\\(){2,3}[\\-0-9\\.\\sEe,\\(\\)]*(\\s*\\)){2,3}\\s*";
		Boolean isWKT = Pattern.compile(isWKTString).matcher(WKTString).matches();
		return isWKT && areBracketsBalanced(WKTString); // Regex is fucky so count brackets
	}
	
	private static String encodeComplexPolygon(PolygonGeometry multiPolygon, int complexPolygon) {
		String WKTString = LEFT_BRACKET;
		
		for (int simplePolygon = 0; simplePolygon < multiPolygon.simplePolygonCount(complexPolygon); simplePolygon++) {
			WKTString += encodeSimplePolygon(multiPolygon, complexPolygon, simplePolygon);
		}
		
		WKTString += getSimplePolygonClose(complexPolygon, multiPolygon.complexPolygonCount());
		return WKTString;
	}
	
	private static String encodeSimplePolygon(PolygonGeometry multiPolygon, int complexPolygon, int simplePolygon) {
		String WKTString = LEFT_BRACKET;
		WKTString += multiPolygon.getSimplePolygon(complexPolygon, simplePolygon).toString();
		
		WKTString += getSimplePolygonClose(simplePolygon, multiPolygon.simplePolygonCount(complexPolygon));
		return WKTString;
	}
	
	private static String getSimplePolygonClose(int simplePolygon, int i) {
		if (simplePolygon == i - 1)
			return RIGHT_BRACKET;
		else
			return "),";
	}
	
	enum GeometryType {
		SINGLE_SIMPLE,
		MULTIPLE_SIMPLE,
		SINGLE_COMPLEX,
		MULTIPLE_COMPLEX,
		INVALID
	}
}