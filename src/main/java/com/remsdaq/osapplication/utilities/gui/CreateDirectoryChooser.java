/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template folder, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities.gui;

import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 * @author cpt
 */
public class CreateDirectoryChooser {

	private Stage stage;
	private String title;
	private String initialDirectory;
	private File directory;

	public CreateDirectoryChooser(Stage stage) {
		this.stage = stage;
	}

	public File create() {
		DirectoryChooser directoryChooser = new DirectoryChooser();

		if (title != null) {
			directoryChooser.setTitle(title);
		} else {
			directoryChooser.setTitle("Select Directory");
		}

		if (initialDirectory != null) {
			directoryChooser.setInitialDirectory(new File(initialDirectory));
		} else {
			directoryChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
		}

		if (stage != null) {
			directory = directoryChooser.showDialog(stage);
		}

		return getDirectory();
	}

	public File getDirectory() {
		return directory;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public void setInitialDirectory(String initialDirectory) {
		this.initialDirectory = initialDirectory;
	}
}
