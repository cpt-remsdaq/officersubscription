/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities.gui;

import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

/**
 *
 * @author cpt
 */
public class CreateFileChooser {
    
    private Stage stage;
    private String title;
    private String initialDirectory;
    private String[] extensionFilters;
    private File file;
    private Boolean save = false;
    
    public CreateFileChooser(Stage stage) {
        this.stage = stage;
    }
    
    public File create() {
        FileChooser fileChooser = new FileChooser();
        
        if (title != null) {
            fileChooser.setTitle(title);
        } else {
            fileChooser.setTitle("Select File");
        }
        
        if (initialDirectory != null) {
            fileChooser.setInitialDirectory(new File(initialDirectory));
        } else {
            fileChooser.setInitialDirectory(new File(System.getProperty("user.dir")));
        }
        
        if (extensionFilters != null) {
            for (String filter : extensionFilters) {
                String infoFilter = filter.replace(";", " files, ");
                infoFilter = infoFilter.replace("*","");
                infoFilter += " files";
                FileChooser.ExtensionFilter filters = new FileChooser.ExtensionFilter(infoFilter,filter);
                fileChooser.getExtensionFilters().add(filters);
            }
        }
        
        if (stage != null) {
            if (save) {
                file = fileChooser.showSaveDialog(stage);
            } else {   
                file = fileChooser.showOpenDialog(stage);
            }
        }
        
        return getFile();
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setInitialDirectory(String initialDirectory) {
        this.initialDirectory = initialDirectory;
    }
    
    public void setExtensionFilter(String[] extensionFilters) {
        this.extensionFilters = extensionFilters;
    }
    
    public void setSave(Boolean save) {
        this.save = save;
    }
    
    private File getFile() {
        return file;
    }
}
