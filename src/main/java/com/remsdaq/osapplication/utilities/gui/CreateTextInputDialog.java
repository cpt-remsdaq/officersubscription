/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities.gui;

import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;

import java.util.Optional;

/**
 *
 * @author cpt
 */
public class CreateTextInputDialog {
    
    private Stage stage;
    private String title;
    private String text;
    private String headerText;
    private Boolean large = false;
    
    public CreateTextInputDialog(Stage stage) {
        this.stage = stage;
    }

    public CreateTextInputDialog() {
        this.stage = new Stage();
    }
    
    public String create() {
        
        TextInputDialog textInputDialog = new TextInputDialog();
        textInputDialog.initOwner(stage);
        textInputDialog.setTitle((this.title != null) ? this.title : "Text Input");
        textInputDialog.setContentText((this.text != null) ? this.text : "");
        textInputDialog.setHeaderText((this.headerText != null) ? this.headerText : "");
        
        if (this.large) {
            textInputDialog.getEditor().setMinWidth(250);
        }
        
        Optional<String> optionalString;
        optionalString = textInputDialog.showAndWait();
        
        this.text = optionalString.isPresent() ? optionalString.get() : null;
        
        return getText();
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setHeaderText(String headerText) {
        this.headerText = headerText;
    }
    
    public void setLarge(Boolean large) {
        this.large = large;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    private String getText() {
        return this.text;
    }
}
