/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities.gui;

import java.awt.Frame;
import javax.swing.JOptionPane;

public class CreateYesNoOptionDialog  {

    public static Boolean create(String messageString, String titleString) {
        int userChoice = JOptionPane.showConfirmDialog(
                new Frame(),
                messageString,
                titleString,
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);

        return (userChoice == 0);

    }
    
}
