package com.remsdaq.osapplication.utilities.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;

/**
 * Generates a GridPane object with standard configuration
 * 
 * @author cpt
 */
public class GenerateGrid extends GridPane {

    GenerateGrid(GridPane createdGrid) {
        createdGrid.setAlignment(Pos.CENTER);
        createdGrid.setHgap(10);
        createdGrid.setVgap(10);
        createdGrid.setPadding(new Insets(25, 25, 25, 25));
        createdGrid.autosize();
    }
}
