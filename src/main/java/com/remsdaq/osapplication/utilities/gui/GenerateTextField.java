package com.remsdaq.osapplication.utilities.gui;

import javafx.scene.control.TextField;

/**
 * Generates a TextField object and sets its text value and column count (width)
 * 
 * @author cpt
 */
public class GenerateTextField extends TextField{

    GenerateTextField(String fieldString) {
        this.setText(fieldString);
    }

    GenerateTextField(String fieldString, int colCount) {
        this.setText(fieldString);
        this.setPrefColumnCount(colCount);
        this.setText(fieldString);
    }
}
