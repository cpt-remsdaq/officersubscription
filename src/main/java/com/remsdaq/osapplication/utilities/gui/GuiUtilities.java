package com.remsdaq.osapplication.utilities.gui;

import com.remsdaq.osapplication.utilities.ColourUtilities;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class GuiUtilities {

    // Allow a textfield to be populated by user whilst in focused on another node
    // Helpful for when you want to search a table while focused inside the table
    public static void typeInFieldExternally(KeyEvent event, TextField textField) {

        KeyCode keyCode = event.getCode();

        String newText = textField.getText() + "" + keyCode.toString();

        if (keyCode.isLetterKey()) {
            textField.setText(newText);
        }

        if (keyCode.isDigitKey()) {
            textField.setText(textField.getText() + ""
                    + keyCode.toString().substring(keyCode.toString().length()-1, keyCode.toString().length()));
        }

        if (keyCode.isWhitespaceKey()) {
            textField.setText(textField.getText() + " ");
        }

        if (keyCode == KeyCode.BACK_SPACE || keyCode == KeyCode.DELETE) {

            if (!event.isControlDown()) {
                if (textField.getText().length() > 0) {
                    textField.setText(textField.getText().substring(0, textField.getText().length()-1));
                }
            } else {
                if (textField.getText().length() > 0) {
                    if (textField.getText().contains(" ")) {
                        textField.setText(textField.getText().substring(0,textField.getText().lastIndexOf(" ")));
                    } else {
                        textField.setText("");
                    }
                }
            }
        }
    }

    public static Node appendStyle(Node node, String appendString) {
        node.setStyle(node.getStyle() + appendString);
//        node.setStyle((appendString + node.getStyle()));
        return node;
    }

    public static Node setFontColourToContrastBackground(Node node, String background) {
        // Set background to user defined colour
        appendStyle(node, "-fx-background-color: " + background + "; ");
        // Check if luminance of background requires BLACK or WHITE text for highest colour contrast, and set to best value
        appendStyle(node, "-fx-text-fill: " + (ColourUtilities.isCloserToBlackThanWhite(background) ? "#FFFFFF" : "#000000") + "; ");
        return node;

    }

}
