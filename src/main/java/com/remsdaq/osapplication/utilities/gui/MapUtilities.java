/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.remsdaq.osapplication.utilities.gui;

import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.polygons.SimplePolygon;
import javafx.scene.web.WebEngine;

import java.util.ArrayList;

/**
 * @author cpt
 */
public abstract class MapUtilities {
	
	public static final double EARTH_POLAR_RADIUS = 6356752.3;
	public static final double EARTH_EQUATORIAL_RADIUS = 6378137.0;
	public static ArrayList<int[]> mapDrawHistory = new ArrayList<>();
	private static Config config = new Config();
	private static Double defaultMapLocationLatitude;
	private static Double defaultMapLocationLongitude;
	private static int defaultMapZoom;
	
	public MapUtilities() {
		MapUtilities.defaultMapZoom = Integer.parseInt(config.getProperty("default_map_zoom"));
		MapUtilities.defaultMapLocationLongitude = Double.parseDouble(config.getProperty("default_map_location_longitude"));
		MapUtilities.defaultMapLocationLatitude = Double.parseDouble(config.getProperty("default_map_location_latitude"));
	}
	
	public static Polygon SimplifyPolygon(int currentComplexPolygon, int currentSimplePolygon, Polygon polygon) {
		Config config = new Config();
		int maxPointsOnMap = Integer.parseInt(config.getProperty("maximum_points_shown_on_map"));
		SimplePolygon simplePolygon = new SimplePolygon();
		
		
		double distanceThreshold = Double.parseDouble(config.getProperty("distance_threshold"));
		LatLng startVertex = polygon.getGeometry().getFirstVertex(currentComplexPolygon, currentSimplePolygon);
		do {
			simplePolygon.addVertex(startVertex);
			int numberOfVerticesInSimplePolygon = polygon.getGeometry().simplePolygonVertexCount(currentComplexPolygon, currentSimplePolygon);
			for (int i = 1; i < numberOfVerticesInSimplePolygon - 1; i++) {
				LatLng currentVertex = polygon.getGeometry().getVertex(currentComplexPolygon, currentSimplePolygon, i);
				if (currentVertex.getAbsoluteDistance(startVertex) > distanceThreshold) {
					simplePolygon.addVertex(currentVertex);
				}
				startVertex = currentVertex;
			}
			LatLng endVertex = polygon.getGeometry().getLastVertex(currentComplexPolygon, currentSimplePolygon);
			simplePolygon.addVertex(endVertex);
			distanceThreshold = distanceThreshold * 2;
		} while (simplePolygon.vertexNumber() >= maxPointsOnMap);
		
		polygon.getGeometry().setSimplePolygon(currentComplexPolygon, currentSimplePolygon, simplePolygon);
		return polygon;
	}
	
	public static void clearPolygonMap(WebEngine engine) {
		engine.executeScript("linkLayer.destroyFeatures();");
		try {
			engine.executeScript("markers.clearMarkers();");
		} catch (Exception e) {
		}
	}
	
	public static String createLastMarker(int currentComplexPolygon, int currentSimplePolygon, Polygon polygon) {
		
		String polygonString = "";
		
		if (polygon.getGeometry().getGeometry().get(currentComplexPolygon) != null
		    && !polygon.getGeometry().getGeometry().isEmpty()
		    && polygon.getGeometry().getGeometry().get(currentComplexPolygon).get(currentSimplePolygon) != null
		    && !polygon.getGeometry().getGeometry().get(currentComplexPolygon).get(currentSimplePolygon).isEmpty()
		    && polygon.getGeometry().simplePolygonVertexCount(currentComplexPolygon, currentSimplePolygon) > 0) {
			LatLng latLng = polygon.getGeometry().getLastVertex(currentComplexPolygon, currentSimplePolygon);
			polygonString += "markers.clearMarkers();";
			polygonString += "markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat("
			                 + latLng.getLng() + "," + latLng.getLat()
			                 + ").transform(new OpenLayers.Projection(WGS84), map.getProjectionObject())));";
		}
		return polygonString;
	}
	
	public static String createPolygonSectionString(LatLng latLng) {
		String polygonString =
				"originLonLat = new OpenLayers.LonLat(" + latLng.getLng() + "," + latLng.getLat() + ")"
				+ ".transform(new OpenLayers.Projection(WGS84), map.getProjectionObject());\n"
				+ "origin = new OpenLayers.Geometry.Point(originLonLat.lon, originLonLat.lat);"
				+ "polyline.push(new OpenLayers.Geometry.Point(origin.x,origin.y));\n";
		return polygonString;
	}
	
	public static String createPolygonString(int currentComplexPolygon, int currentSimplePolygon, Polygon originalPoly, Boolean closeBool) {
		String fullPolygonString = "";
		
		for (int i = 0; i < originalPoly.getGeometry().simplePolygonVertexCount(currentComplexPolygon, currentSimplePolygon); i++) {
			LatLng latLng = originalPoly.getGeometry().getVertex(currentComplexPolygon, currentSimplePolygon, i);
			
			String polygonString = createPolygonSectionString(latLng);
			fullPolygonString += polygonString;
		}
		if (closeBool == true) {
			LatLng latLng = originalPoly.getGeometry().getVertex(currentComplexPolygon, currentSimplePolygon, 0);
			String routeString = createPolygonSectionString(latLng);
			fullPolygonString += routeString;
		}
		return fullPolygonString;
	}
	
	public static void drawGeometry(WebEngine engine, Polygon originalPoly, Boolean closeBool, Boolean lastMarkerBool, Boolean displayOnlyBool) {
		
		clearPolygonMap(engine);
		//engine.executeScript("createPolygonLayersAndControls();");
		for (int currentComplexPolygon = 0; currentComplexPolygon < originalPoly.getGeometry().complexPolygonCount(); currentComplexPolygon++) {
			for (int currentSimplePolygon = 0; currentSimplePolygon < originalPoly.getGeometry().simplePolygonCount(
					currentComplexPolygon); currentSimplePolygon++) {
				if (currentComplexPolygon == originalPoly.getGeometry().complexPolygonCount() - 1
				    && currentSimplePolygon == originalPoly.getGeometry().simplePolygonCount(currentComplexPolygon) - 1) {
					drawPolygon(currentComplexPolygon, currentSimplePolygon, engine, originalPoly, closeBool, lastMarkerBool, displayOnlyBool);
				} else {
					drawPolygon(currentComplexPolygon, currentSimplePolygon, engine, originalPoly, true, lastMarkerBool, displayOnlyBool);
				}
			}
		}
		if (closeBool) {
			setZoom(engine, originalPoly);
		}
	}
	
	public static void drawGeometry(WebEngine engine, Polygon originalPoly, Boolean displayOnlyBool) {
		MapUtilities.drawGeometry(engine, originalPoly, true, false, displayOnlyBool);
	}
	
	public static void drawPolygon(int currentComplexPolygon, int currentSimplePolygon, WebEngine engine, Polygon originalPoly, Boolean closeBool,
	                               Boolean lastMarkerBool, Boolean displayOnlyBool) {
		Config config = new Config();
		int maxPointsOnMap = Integer.parseInt(config.getProperty("maximum_points_shown_on_map"));
		
		Polygon simplifiedPoly = new Polygon();
		simplifiedPoly.setGeometry(originalPoly.getGeometry());
		
		if (originalPoly.getGeometry().simplePolygonVertexCount(currentComplexPolygon, currentSimplePolygon) > maxPointsOnMap && closeBool == true) {
			simplifiedPoly = SimplifyPolygon(currentComplexPolygon, currentSimplePolygon, simplifiedPoly);
			simplifiedPoly.calculateArea();
		}
		
		String fullPolygonString = createPolygonString(currentComplexPolygon, currentSimplePolygon, simplifiedPoly, closeBool);
		if (lastMarkerBool && !closeBool) {
			String markerString = createLastMarker(currentComplexPolygon, currentSimplePolygon, simplifiedPoly);
			fullPolygonString += markerString;
		}
		
		engine.executeScript("var shape;");
		engine.executeScript("function drawShape() {\n"
		                     + "var originLonLat;\n"
		                     + "var origin;\n"
		                     + "var polyline = [];\n"
		                     + fullPolygonString
		                     + "shape = new OpenLayers.Feature.Vector();\n"
		                     + "shape.style = {"
		                     + "strokeColor:\"" + config.getProperty((currentSimplePolygon == 0) ? "route_colour" : "hole_colour") + "\", "
		                     + "fillColor:\"" + config.getProperty((currentSimplePolygon == 0) ? "route_fill_colour" : "hole_fill_colour") + "\", "
		                     + "strokeWidth:" + config.getProperty((currentSimplePolygon == 0) ? "route_line_width" : "hole_line_width") + ", "
		                     + "strokeOpacity:" + config.getProperty((currentSimplePolygon == 0) ? "route_line_opacity" : "hole_line_opacity") + ", "
		                     + "fillOpacity:" + config.getProperty((currentSimplePolygon == 0) ? "route_fill_opacity" : "hole_fill_opacity") + ", "
		                     + "strokeDashstyle: \"" + config.getProperty((currentSimplePolygon == 0) ? "route_line_style" : "hole_line_style")
		                     + "\"};\n"
		
		                     + ((!displayOnlyBool) ? "shape.geometry = new OpenLayers.Geometry.LineString(polyline);\n" : "shape.geometry = new OpenLayers.Geometry.LinearRing(polyline);\n")
				                     //            + "shape.geometry = new OpenLayers.Geometry.LineString(polyline);\n"
		
		                     + "linkLayer.addFeatures([shape]);\n"
		                     + "};");
		engine.executeScript("drawShape();");
	}
	
	public static String getPolygonString(int currentComplexPolygon, int currentSimplePolygon, WebEngine engine, Polygon originalPoly,
	                                      Boolean closeBool, boolean lastMarkerBool) {
		Config config = new Config();
		int maxPointsOnMap = Integer.parseInt(config.getProperty("maximum_points_shown_on_map"));
		
		Polygon simplifiedPoly = new Polygon();
		simplifiedPoly.setGeometry(originalPoly.getGeometry());
		
		if (originalPoly.getGeometry().simplePolygonVertexCount(currentComplexPolygon, currentSimplePolygon) > maxPointsOnMap && closeBool == true) {
			simplifiedPoly = SimplifyPolygon(currentComplexPolygon, currentSimplePolygon, simplifiedPoly);
			simplifiedPoly.calculateArea();
		}
		
		String fullPolygonString = createPolygonString(currentComplexPolygon, currentSimplePolygon, simplifiedPoly, closeBool);
		if (lastMarkerBool && !closeBool) {
			String markerString = createLastMarker(currentComplexPolygon, currentSimplePolygon, simplifiedPoly);
			fullPolygonString += markerString;
		}
		
		return fullPolygonString;
	}
	
	public static void removeFirebug(WebEngine engine) {
		try {
			engine.executeScript("var frame = document.getElementById('FirebugUI');frame.parentNode.removeChild(frame);");
		} catch (Exception e) {
			System.out.println("FirebugUI : " + e.getMessage());
		}
	}
	
	public static void setZoom(WebEngine engine, Polygon originalPoly) {
		LatLng firstLatLng = originalPoly.getGeometry().getFirstVertex(0, 0);
		Double smallestLng = firstLatLng.getLng();
		Double smallestLat = firstLatLng.getLat();
		Double biggestLng = firstLatLng.getLng();
		Double biggestLat = firstLatLng.getLat();
		
		for (int complexPolygon = 0; complexPolygon < originalPoly.getGeometry().complexPolygonCount(); complexPolygon++) {
			for (int simplePolygon = 0; simplePolygon < originalPoly.getGeometry().simplePolygonCount(complexPolygon); simplePolygon++) {
				for (int vertex = 0; vertex < originalPoly.getGeometry().simplePolygonVertexCount(complexPolygon, simplePolygon); vertex++) {
					LatLng latLng = originalPoly.getGeometry().getVertex(complexPolygon, simplePolygon, vertex);
					smallestLng = recalculateSmallestLng(smallestLng, latLng);
					biggestLng = recalculateBiggestLng(biggestLng, latLng);
					smallestLat = recalculateSmallestLat(smallestLat, latLng);
					biggestLat = recalculateBiggestLat(biggestLat, latLng);
				}
			}
		}
		
		engine.executeScript("zoomToExtent(" + smallestLng + "," + smallestLat + "," + biggestLng + "," + biggestLat + ")");
		
		engine.executeScript("setZoom(getZoom() - 1);");
	}
	
	public static void zoomOut(WebEngine engine) {
		engine.executeScript("zoomToExtent(0,0,0,0)");
		engine.executeScript("panTo((" + defaultMapLocationLongitude + "), " + defaultMapLocationLatitude + ");");
		engine.executeScript("setZoom(getZoom() + " + defaultMapZoom + " - 18);");
	}
	
	public static void zoomToDefault(WebEngine engine) {
		//        engine.executeScript("zoomToExtent(0,0,0,0)");
		//
		//        String zoomString = "zoomLonLat = new OpenLayers.LonLat(" + DEFAULT_MAP_LONGITUDE + "," + DEFAULT_MAP_LATITUDE + ")"
		//                + ".transform(new OpenLayers.Projection(WGS84), map.getProjectionObject());\n";
		//
		//        engine.executeScript("var zoomLonLat;\n " + zoomString);
		//
		//        engine.executeScript("panTo(zoomLonLat);");
		//        engine.executeScript("java.log(zoomLonLat);");
		//
		//        engine.executeScript("setZoom(getZoom() + " + DEFAULT_MAP_ZOOM);
	}
	
	private static Double recalculateBiggestLat(Double biggestLat, LatLng latLng) {
		int latMaxComp = Double.compare(latLng.getLat(), biggestLat);
		if (latMaxComp > 0)
			biggestLat = latLng.getLat();
		return biggestLat;
	}
	
	private static Double recalculateSmallestLat(Double smallestLat, LatLng latLng) {
		int latMinComp = Double.compare(latLng.getLat(), smallestLat);
		if (latMinComp < 0)
			smallestLat = latLng.getLat();
		return smallestLat;
	}
	
	private static Double recalculateBiggestLng(Double biggestLng, LatLng latLng) {
		int lngMaxComp = Double.compare(latLng.getLng(), biggestLng);
		if (lngMaxComp > 0)
			biggestLng = latLng.getLng();
		return biggestLng;
	}
	
	private static Double recalculateSmallestLng(Double smallestLng, LatLng latLng) {
		int lngMinComp = Double.compare(latLng.getLng(), smallestLng);
		if (lngMinComp < 0)
			smallestLng = latLng.getLng();
		return smallestLng;
	}
}