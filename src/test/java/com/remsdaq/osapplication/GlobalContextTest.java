package com.remsdaq.osapplication;

import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.hibernate.Hibernate;
import com.remsdaq.osapplication.database.xml.XmlDataConnection;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

public class GlobalContextTest {
	
	private final String XML_DATA_CONNECTION_CONFIG_PATH = "testConfigs/xmlDataConnectionConfig.properties";
	private final String DEFAULT_CONFIG_PATH = "configs/config.properties";
	
	@Before
	public void resetSingleton() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
		Field instance = GlobalContext.class.getDeclaredField("instance");
		instance.setAccessible(true);
		instance.set(null, null);
	}
	
	@Before
	public void setUp() {
		GlobalContext gc = GlobalContext.getInstance();
	}
	
	@Test
	public void setXmlDataConnectionTest() {
		GlobalContext.getInstance().setDatabaseConnection(new XmlDataConnection());
		assertEquals(XmlDataConnection.class,GlobalContext.getInstance().getDatabaseConnection().getClass());
	}
	
	@Ignore // Keep ignore unless hibernate returns null when trying to connect. Takes long time to connect.
	@Test
	public void setHibernateConnectionTest() {
		GlobalContext.getInstance().setDatabaseConnection(new Hibernate());
		assertEquals(Hibernate.class, GlobalContext.getInstance().getDatabaseConnection().getClass());
	}
	
	@Test
	public void setConfigFilePathTest() {
		GlobalContext.getInstance().setConfig(new Config());
		assertEquals(DEFAULT_CONFIG_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
		
		GlobalContext.getInstance().setConfig(new Config(XML_DATA_CONNECTION_CONFIG_PATH));
		assertEquals(XML_DATA_CONNECTION_CONFIG_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
	}
	
	@Test
	public void subscriberShouldReturnDataPathFromNonDefaultConfig() {
		GlobalContext gc = GlobalContext.getInstance();
		gc.setConfig(new Config(XML_DATA_CONNECTION_CONFIG_PATH));
		Subscriber testSub = new Subscriber();
		assertEquals("xmlData/test/subscriber-test.xml", testSub.getXmlDataPath());
	}
	
	@Test
	public void subscriberShouldReturnDataCollectionPathFromNonDefaultConfig() {
		GlobalContext gc = GlobalContext.getInstance();
		gc.setConfig(new Config(XML_DATA_CONNECTION_CONFIG_PATH));
		Subscriber testSub = new Subscriber();
		assertEquals("xmlData/test/subscriber-collection-test.xml", testSub.getXmlDataCollectionPath());
	}
}