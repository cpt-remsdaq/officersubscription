package com.remsdaq.osapplication.database.xml;

import com.remsdaq.osapplication.utilities.entities.polygonTags.PolygonTagTestValues;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonTestValues;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberTestValues;

import java.util.Collections;

import static com.remsdaq.osapplication.utilities.StringUtilities.enumerate;

public class PolygonXmlDataTestUtilities {
	
	private static final String XML_HEADER ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
	
	public static String generateXmlPolygons(int[] ns, int[] ss, int[] ts) {
		String xmlPolygonsString = XML_HEADER + "<Polygons>\n";
		for (int n : ns) {
			xmlPolygonsString+= generateXmlPolygonString(n, 1, ss, ts) + "\n";
		}
		xmlPolygonsString += "</Polygons>";
		return xmlPolygonsString;
	}
	
	public static String generateXmlPolygons(int[] ns) {
		return generateXmlPolygons(ns, new int[] {}, new int[] {});
	}
	
	public static String generateXmlPolygon(int n, int[] ss, int[] ts) {
		String xmlSubscriberString = XML_HEADER + generateXmlPolygonString(n, 0, ss, ts);
		return xmlSubscriberString;
	}
	
	public static String generateXmlPolygon(int n) {
		return generateXmlPolygon(n, new int[] {}, new int[] {});
	}
	
	private static String generateXmlPolygonString(int n, int level, int[] subscribers, int[] tags) {
		String xmlLevelLeadingSpaces = generateLeadingSpaces(level);
		
		String xmlPolygonString = xmlLevelLeadingSpaces + "<Polygon ID=\"" + enumerate(PolygonTestValues.ID, n) + "\">\n"
				+ xmlLevelLeadingSpaces + "    <Name>" + enumerate(PolygonTestValues.NAME, n) + "</Name>\n"
				+ xmlLevelLeadingSpaces + "    <Description>" + enumerate(PolygonTestValues.DESCRIPTION, n) + "</Description>\n"
				+ xmlLevelLeadingSpaces + "    <WellKnownText>" + generateWktGeometry(n) + "</WellKnownText>\n";
		
		xmlPolygonString += generateSubscriberString(subscribers, xmlLevelLeadingSpaces);
		
		xmlPolygonString += generateTagString(tags, xmlLevelLeadingSpaces);
		
		xmlPolygonString += xmlLevelLeadingSpaces + "</Polygon>";
		return xmlPolygonString;
	}
	
	private static String generateSubscriberString(int[] subscribers, String xmlLevelLeadingSpaces) {
		String xmlSubscriberString = "";
		if (subscribers.length > 0) {
			xmlSubscriberString += xmlLevelLeadingSpaces + "    <Subscribers>\n";
			for (int s : subscribers) {
				xmlSubscriberString += xmlLevelLeadingSpaces + "        <Subscriber>" + enumerate(SubscriberTestValues.ID, s) + "</Subscriber>\n";
			}
			xmlSubscriberString += xmlLevelLeadingSpaces + "    </Subscribers>\n";
		}
		return xmlSubscriberString;
	}
	
	private static String generateTagString(int[] tags, String xmlLevelLeadingSpaces) {
		String xmlTagString = "";
		if (tags.length>0) {
			xmlTagString += xmlLevelLeadingSpaces + "    <PolygonTags>\n";
			for (int t: tags) {
				xmlTagString += xmlLevelLeadingSpaces + "        <PolygonTag>" + enumerate(PolygonTagTestValues.NAME, t) + "</PolygonTag>\n";
			}
			xmlTagString += xmlLevelLeadingSpaces + "    </PolygonTags>\n";
		}
		return xmlTagString;
	}
	
	private static String generateLeadingSpaces(int level) {
		return String.join("", Collections.nCopies(4 * level, " "));
	}
	
	static String generateWktGeometry(int n) {
		return "POLYGON ((0.0 0.0, 0.0 " + n + ".0, " + n + ".0 " + n + ".0, " + n + ".0 0.0))";
	}
	
}
