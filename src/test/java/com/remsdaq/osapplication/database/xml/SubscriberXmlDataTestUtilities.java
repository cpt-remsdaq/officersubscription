package com.remsdaq.osapplication.database.xml;

import com.remsdaq.osapplication.utilities.entities.polygons.PolygonTestValues;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberTestValues;

import java.util.Collections;

import static com.remsdaq.osapplication.utilities.StringUtilities.enumerate;
import static com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberTestValues.*;

public class SubscriberXmlDataTestUtilities {
	
	private static final String XML_HEADER ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
	
	public static String generateXmlSubscribers(int[] ns, int[] ps) {
		String xmlSubscribersString = XML_HEADER + "<Subscribers>\n";
		for (int n : ns) {
			xmlSubscribersString+= generateXmlSubscriberString(n,1,ps) + "\n";
		}
		xmlSubscribersString += "</Subscribers>";
		return xmlSubscribersString;
	}
	
	public static String generateXmlSubscribers(int[] ns) {
		return generateXmlSubscribers(ns, new int[] {});
	}
	
	public static String generateXmlSubscriber(int n, int[] ps) {
		String xmlSubscriberString = XML_HEADER +  generateXmlSubscriberString(n, 0, ps);
		return xmlSubscriberString;
	}
	
	public static String generateXmlSubscriber(int n) {
		return generateXmlSubscriber(n, new int[] {});
	}
	
	private static String generateXmlSubscriberString(int n, int level, int[] polygons) {
		String xmlLevelLeadingSpaces = generateLeadingSpaces(level);
		
		String xmlSubscriberString =
				xmlLevelLeadingSpaces + "<Subscriber ID=\"" + enumerate(SubscriberTestValues.ID, n) + "\">\n"
				+ xmlLevelLeadingSpaces + "    <FirstName>" + enumerate(SubscriberTestValues.FIRST_NAME, n) + "</FirstName>\n"
				+ xmlLevelLeadingSpaces + "    <LastName>" + enumerate(SubscriberTestValues.LAST_NAME, n) + "</LastName>\n"
				+ xmlLevelLeadingSpaces + "    <Email>" + enumerate(SubscriberTestValues.EMAIL, n) + "</Email>\n"
				+ xmlLevelLeadingSpaces + "    <EmailPriority>" + EMAIL_PRIORITY + n + "</EmailPriority>\n"
				+ xmlLevelLeadingSpaces + "    <Mobile>" + enumerate(SubscriberTestValues.MOBILE, n) + "</Mobile>\n"
				+ xmlLevelLeadingSpaces + "    <MobilePriority>" + MOBILE_PRIORITY + n  + "</MobilePriority>\n"
				+ xmlLevelLeadingSpaces + "    <Pager>" + enumerate(SubscriberTestValues.PAGER, n) + "</Pager>\n"
				+ xmlLevelLeadingSpaces + "    <PagerPriority>" + PAGER_PRIORITY + n  + "</PagerPriority>\n";
		
		xmlSubscriberString += getString(polygons, xmlLevelLeadingSpaces);
		
		xmlSubscriberString += xmlLevelLeadingSpaces + "</Subscriber>";
		return xmlSubscriberString;
	}
	
	private static String getString(int[] polygons, String xmlLevelLeadingSpaces) {
		String xmlPolygonString = "";
		if (polygons.length > 0) {
			xmlPolygonString += xmlLevelLeadingSpaces + "    <Polygons>\n";
			for (int p : polygons) {
					xmlPolygonString += xmlLevelLeadingSpaces + "        <Polygon>" + enumerate(PolygonTestValues.ID, p) + "</Polygon>\n";
				}
				xmlPolygonString += xmlLevelLeadingSpaces + "    </Polygons>\n";
			}
		return xmlPolygonString;
	}
	
	private static String generateLeadingSpaces(int level) {
		return String.join("", Collections.nCopies(4 * level, " "));
	}
	
}
