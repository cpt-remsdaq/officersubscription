package com.remsdaq.osapplication.database.xml;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.database.DatabaseConnection;
import com.remsdaq.osapplication.database.xml.entities.polygonTags.PolygonTagCollection;
import com.remsdaq.osapplication.database.xml.entities.polygons.PolygonCollection;
import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberCollection;
import com.remsdaq.osapplication.entities.XmlDataEntity;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.utilities.FileUtilities;
import com.remsdaq.osapplication.utilities.entities.polygonTags.PolygonTagTestUtilities;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonTestUtilities;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberTestUtilities;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class XmlDataConnectionTest {
	
	private final String XML_SUBSCRIBER_STRING = SubscriberXmlDataTestUtilities.generateXmlSubscriber(1, new int[]{1, 2});
	private final String XML_SUBSCRIBER_COLLECTION_STRING = SubscriberXmlDataTestUtilities.generateXmlSubscribers(new int[]{1, 2}, new int[]{1, 2});
	private final String XML_POLYGON_STRING = PolygonXmlDataTestUtilities.generateXmlPolygon(1, new int[]{1, 2}, new int[]{1, 2});
	private final String XML_POLYGON_COLLECTION_STRING = PolygonXmlDataTestUtilities.generateXmlPolygons(new int[]{1, 2}, new int[]{1, 2},
	                                                                                                     new int[]{1, 2});
	
	private final String CONFIG_FILE_PATH = "testConfigs/xmlDataConnectionConfig.properties";
	private final Set<Subscriber> subscribers = new HashSet<>();
	private final Set<Polygon> polygons = new HashSet<>();
	private final Set<PolygonTag> polygonTags = new HashSet<>();
	
	
	private SubscriberCollection subCol;
	private Subscriber sub1 = new Subscriber();
	private Subscriber sub2 = new Subscriber();
	
	private PolygonCollection polCol;
	private Polygon pol1 = new Polygon();
	private Polygon pol2 = new Polygon();
	
	private PolygonTagCollection tagCol;
	private PolygonTag tag1 = new PolygonTag();
	private PolygonTag tag2 = new PolygonTag();
	
	@BeforeEach
	public void setTestDataFilePaths() {
		GlobalContext.getInstance().setConfig(new Config(CONFIG_FILE_PATH));
	}
	
	@Before
	public void setUp() throws IOException, WellKnownTextException {
		GlobalContext.getInstance().setConfig(new Config(CONFIG_FILE_PATH));
		
		sub1 = SubscriberTestUtilities.generateSubscriber(1, new int[]{1, 2});
		sub2 = SubscriberTestUtilities.generateSubscriber(2, new int[]{1, 2});
		subscribers.add(sub1);
		subscribers.add(sub2);
		
		pol1 = PolygonTestUtilities.generatePolygon(1, new int[]{1, 2}, new int[]{1, 2});
		pol2 = PolygonTestUtilities.generatePolygon(2, new int[]{1, 2}, new int[]{1, 2});
		polygons.add(pol1);
		polygons.add(pol2);
		
		tag1 = PolygonTagTestUtilities.generatePolygonTag(1);
		tag2 = PolygonTagTestUtilities.generatePolygonTag(2);
		polygonTags.add(tag1);
		polygonTags.add(tag2);
		
		subCol = new SubscriberCollection();
		List<Subscriber> subSet = new ArrayList<>();
		subCol.setCollection(subSet);
		
		writeRequiredFiles();
	}
	
	@Ignore
	@Test
	public void testTheFuckingTest() {
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		System.out.println(XML_SUBSCRIBER_STRING + "\n" + "\n");
		System.out.println(XML_SUBSCRIBER_COLLECTION_STRING + "\n" + "\n");
		System.out.println(XML_POLYGON_STRING + "\n" + "\n");
		System.out.println(XML_POLYGON_COLLECTION_STRING + "\n" + "\n");
		Collection selectedSubscribers = xmlDataConnection.selectAll(new Subscriber());
	}
	
	@Test
	public void shouldSelectSingleSubscriber() {
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		Subscriber selectedSubscriber = (Subscriber) xmlDataConnection.select(new Subscriber(), "id 1");
		SubscriberTestUtilities.assertSubscribersAreEqual(sub1, selectedSubscriber);
	}
	
	@Test
	public void shouldSelectSinglePolygon() {
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		Polygon selectedPolygon = (Polygon) xmlDataConnection.select(new Polygon(), "id 2");
		PolygonTestUtilities.assertPolygonsAreEqual(pol2, selectedPolygon);
	}
	
	@Test
	public void selectAllShouldReturnCorrectNumberOfSubs() {
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		Collection selectedCollection = xmlDataConnection.selectAll(new Subscriber());
		assertEquals(2, selectedCollection.size());
	}
	
	@Test
	public void shouldSelectAllTwice() {
		for (int i = 0; i < 2; i++) {
			DatabaseConnection xmlDataConnection = new XmlDataConnection();
			Collection selectedCollection = xmlDataConnection.selectAll(new Subscriber());
			assertEquals(2, selectedCollection.size());
		}
	}
	
	@Test
	public void selectAllShouldReturnCorrectNumberOfPols() {
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		Collection selectedCollection = xmlDataConnection.selectAll(new Polygon());
		
		assertEquals(2, selectedCollection.size());
	}
	
	@Test
	public void shouldSelectAllSubscribers() throws IOException {
		writeXmlToAppropriateDataFile(new Subscriber(), XML_SUBSCRIBER_COLLECTION_STRING);
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		Collection selectedCollection = xmlDataConnection.selectAll(new Subscriber());
		
		for (Subscriber sub : subscribers)
			System.out.println("Expected:\n" + sub);
		
		for (Subscriber sub : (List<Subscriber>) selectedCollection)
			System.out.println("Tested:\n" + sub);
		
		assertSubscriberCollectionsAreEquivalent(subscribers, selectedCollection);
	}
	
	@Test
	public void shouldSelectAllPolygons() throws IOException {
		writeXmlToAppropriateDataFile(new Polygon(), XML_POLYGON_COLLECTION_STRING);
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		Collection selectedCollection = xmlDataConnection.selectAll(new Polygon());
		
		for (Polygon pol : polygons)
			System.out.println(pol);
		
		assertPolygonCollectionsAreEquivalent(polygons, selectedCollection);
	}
	
	@Test
	public void shouldDeleteSingleSubscriber() throws IOException {
		writeXmlToAppropriateDataFile(new Subscriber(), XML_SUBSCRIBER_COLLECTION_STRING);
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		xmlDataConnection.delete(sub1);
		
		Collection selectedCollection = xmlDataConnection.selectAll(new Subscriber());
		assertEquals(1, selectedCollection.size());
	}
	
	@Test
	public void shouldInsertSingleSubscriber() throws IOException {
		writeXmlToAppropriateDataFile(new Subscriber(), XML_SUBSCRIBER_COLLECTION_STRING);
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		xmlDataConnection.insert(new Subscriber());
		
		Collection selectedCollection = xmlDataConnection.selectAll(new Subscriber());
		assertEquals(3, selectedCollection.size());
	}
	
	@Test
	public void shouldUpdateSingleSubscriber() throws IOException {
		writeXmlToAppropriateDataFile(new Subscriber(), XML_SUBSCRIBER_COLLECTION_STRING);
		DatabaseConnection xmlDataConnection = new XmlDataConnection();
		Subscriber updateSub = sub1;
		String new_name = "new name";
		updateSub.setFirstName(new_name);
		xmlDataConnection.update(updateSub);
		
		Collection selectedCollection = xmlDataConnection.selectAll(new Subscriber());
		assertTrue("Updated subscriber is not contained within the selected collection of subscribers",
		           isSubscriberContainedWithinCollection(selectedCollection, updateSub));
		
		SubscriberCollection subCol = new SubscriberCollection();
		subCol.setCollection((List<Subscriber>) selectedCollection);
		assertTrue(
				"The updated subscriber is contained within the collection of selected subscribers,\nbut is not identical to the control.",
				areSubscribersEquivalent(updateSub, subCol.selectFromCollection(updateSub)));
	}
	
	private void writeRequiredFiles() throws IOException {
		FileUtilities.writeStringToFile(new Subscriber().getXmlDataPath(), XML_SUBSCRIBER_STRING);
		FileUtilities.writeStringToFile(new Polygon().getXmlDataPath(), XML_POLYGON_STRING);
		writeXmlToAppropriateDataFile(new Subscriber(), XML_SUBSCRIBER_COLLECTION_STRING);
		writeXmlToAppropriateDataFile(new Polygon(), XML_POLYGON_COLLECTION_STRING);
	}
	
	private void writeXmlToAppropriateDataFile(XmlDataEntity entity, String xmlString) throws IOException {
		File xmlFile = new File(entity.getXmlDataCollectionPath());
		FileUtilities.writeStringToFile(xmlFile.getPath(), xmlString);
	}
	
	private void assertSubscriberCollectionsAreEquivalent(Collection<Subscriber> controlCollection,
	                                                      Collection<Subscriber> resultCollection) {
		for (Subscriber sub : resultCollection) {
			assertEquals("", true, isSubscriberContainedWithinCollection(controlCollection, sub));
		}
	}
	
	private Boolean isSubscriberContainedWithinCollection(Collection<Subscriber> controlCollection, Subscriber result) {
		for (Subscriber control : controlCollection) {
			if (areSubscribersEquivalent(control, result)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean areSubscribersEquivalent(Subscriber control, Subscriber result) {
		boolean isContained = (control.getID().equals(result.getID())
		                       && control.getFirstName().equals(result.getFirstName())
		                       && control.getLastName().equals(result.getLastName())
		                       && control.getEmail().equals(result.getEmail())
		                       && control.getEmailPriority().equals(result.getEmailPriority())
		                       && control.getMobile().equals(result.getMobile())
		                       && control.getMobilePriority().equals(result.getMobilePriority())
		                       && control.getPager().equals(result.getPager())
		                       && control.getPagerPriority().equals(result.getPagerPriority()));
		return isContained;
	}
	
	private void assertPolygonCollectionsAreEquivalent(Collection<Polygon> controlCollection,
	                                                   Collection<Polygon> resultCollection) {
		for (Polygon pol : resultCollection) {
			assertEquals(true, isPolygonContainedWithinCollection(controlCollection, pol));
		}
	}
	
	private Boolean isPolygonContainedWithinCollection(Collection<Polygon> controlCollection, Polygon result) {
		for (Polygon control : controlCollection) {
			if (arePolygonsEquivalent(control, result)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean arePolygonsEquivalent(Polygon control, Polygon result) {
		boolean isContained = (control.getID().equals(result.getID())
		                       && control.getName().equals(result.getName())
		                       && control.getDescription().equals(result.getDescription())
		                       && control.getWKTGeometry().equals(result.getWKTGeometry()));
		return isContained;
	}
	
	private void assertPolygonTagCollectionsAreEquivalent(Collection<PolygonTag> controlCollection, Collection<PolygonTag> resultCollection) {
		for (PolygonTag tag : resultCollection) {
			assertEquals(true, isPolygonTagContainedWithinCollection(controlCollection, tag));
		}
	}
	
	private Boolean isPolygonTagContainedWithinCollection(Collection<PolygonTag> controlCollection, PolygonTag result) {
		for (PolygonTag control : controlCollection) {
			if (arePolygonTagsEquivalent(control, result)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean arePolygonTagsEquivalent(PolygonTag control, PolygonTag result) {
		boolean isContained = (control.getName().equals(result.getName()));
		return isContained;
	}
}
