package com.remsdaq.osapplication.database.xml.entities.polygonTags;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.examples.CountingNodeTester;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.custommonkey.xmlunit.XMLAssert.assertNodeTestPasses;
import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;

public class PolygonTagToXmlTest {
	
	final String TAG_ID = "sub ID";
	final String TAG_NAME = "tag name";
	final private String EMPTY_CONTROL_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
	                                         + "<PolygonTag Name=\"\"/>";
	
	final private String CONTROL_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
	                                   + "<PolygonTag Name=\"TAG NAME\">\n"
	                                   + "    <Polygons>\n"
	                                   + "        <Polygon>pol ID</Polygon>\n"
	                                   + "        <Polygon>pol ID</Polygon>\n"
	                                   + "    </Polygons>\n"
	                                   + "</PolygonTag>";
	
	PolygonTag tag = new PolygonTag();
	
	String POL_ID = "pol ID";
	String polName = "pol name";
	String polDescription = "pol description";
	String polWKT = "POLYGON ((1.0 1.0, 0.0 1.0, 1.0 0.0, 0.0 0.0))";
	Polygon pol = new Polygon();
	Set<Polygon> polSet = new HashSet<>();
	Set<PolygonTag> tagSet = new HashSet<>();
	String tagXml;
	private Polygon pol2 = new Polygon();
	private String emptyXml = "";
	
	@Before
	public void setUp() throws JAXBException, WellKnownTextException {
		
		XMLUnit.setIgnoreWhitespace(true);
		
		populateTag(tag);
		populatePolygon(pol);
		populatePolygon(pol2);
		tag.setName(TAG_NAME);
		
		polSet.add(pol2);
		polSet.add(pol);
		tagSet.add(tag);
		
		tag.setPolygons(polSet);
		pol.setPolygonTags(tagSet);
		tag.setPolygons(polSet);
		
		tagXml = PolygonTagToXml.convertToString(tag);
		
		emptyXml = PolygonTagToXml.convertToString(new PolygonTag());
	}
	
	private void populateTag(PolygonTag tag) {
		tag.setID(TAG_ID);
		tag.setName(TAG_NAME);
	}
	
	private void populatePolygon(Polygon pol) {
		pol.setID(POL_ID);
		pol.setName(polName);
		pol.setDescription(polDescription);
		pol.setWKTGeometry(polWKT);
	}
	
	@Test
	public void shouldReturnXmlStringIdenticalToControl() throws IOException, SAXException {
		assertXMLEqual("Comparing converted subscriber xml to control xml.", CONTROL_XML, tagXml);
	}
	
	@Test
	public void testCountingNodeTester() throws IOException, SAXException {
		CountingNodeTester countingNodeTester = new CountingNodeTester(7);
		assertNodeTestPasses(tagXml, countingNodeTester, Node.TEXT_NODE);
	}
	
	@Test
	public void shouldReturnDryXmlString() throws IOException, SAXException {
		System.out.println(emptyXml);
		assertXMLEqual("Comparing unpopulated subscriber xml string to control xmlstring.", EMPTY_CONTROL_XML,
		               emptyXml);
	}
	
	@Test
	public void shouldReturnPolygonTagCollectionXml() throws JAXBException {
		PolygonTagCollection tagCol = new PolygonTagCollection();
		tagCol.addToCollection(tag);
		tagCol.addToCollection(new PolygonTag());
		String tagColXml = PolygonTagToXml.convertCollectionToString(tagCol);
		System.out.println(tagColXml);
	}
}