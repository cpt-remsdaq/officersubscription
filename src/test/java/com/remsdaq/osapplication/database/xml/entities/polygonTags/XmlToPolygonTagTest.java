package com.remsdaq.osapplication.database.xml.entities.polygonTags;

import com.remsdaq.osapplication.database.xml.entities.polygonTags.XmlToPolygonTag;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class XmlToPolygonTagTest {
	
	private static final String TAG_NAME = "TAG NAME";
	private final String TAG_ID = "poly id";
	private final String POL_TAG_NAME_1 = "tag 1";
	private final String POL_TAG_NAME_2 = "tag 2";
	private final PolygonTag POL_TAG_1 = new PolygonTag(POL_TAG_NAME_1);
	private final PolygonTag POL_TAG_2 = new PolygonTag(POL_TAG_NAME_2);
	private final String TAG_XML
			= "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			  + "<PolygonTag Name=\"" + TAG_NAME + "\"/>";
	private PolygonTag controlTag = new PolygonTag();
	private PolygonTag emptyConvertedTag;
	private PolygonTag convertedTag;
	private String EMPTY_TAG_XML
			= "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			  + "<PolygonTag Name=\"\"/>";
	
	@Before
	public void setUp() throws JAXBException, UnsupportedEncodingException {
		emptyConvertedTag = XmlToPolygonTag.convertFromString(EMPTY_TAG_XML);
		
		Set<PolygonTag> tagSet = new HashSet<>();
		tagSet.add(POL_TAG_1);
		tagSet.add(POL_TAG_2);
		
		controlTag.setID(TAG_ID);
		controlTag.setName(TAG_NAME);
		
		convertedTag = XmlToPolygonTag.convertFromString(TAG_XML);
	}
	
	@Test
	public void shouldReturnEmptyPolygonTag() {
		assertPolygonTagEmpty(emptyConvertedTag);
	}
	
	private void assertPolygonTagEmpty(PolygonTag polygonTag) {
		//		assertNull(polygonTag.getID());
		assertEquals("", polygonTag.getName());
	}
	
	@Test
	public void populatedPolygonsShouldBeEqual() {
		assertPolygonsAreEqual(controlTag, convertedTag);
	}
	
	private void assertPolygonsAreEqual(PolygonTag controlTag, PolygonTag convertedTag) {
		//		assertEquals(controlTag.getID(), convertedTag.getID());
		assertEquals(controlTag.getName(), convertedTag.getName());
	}
}