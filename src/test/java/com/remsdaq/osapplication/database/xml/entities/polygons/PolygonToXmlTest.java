package com.remsdaq.osapplication.database.xml.entities.polygons;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonTestUtilities;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonXmlTestUtilities;
import org.custommonkey.xmlunit.examples.CountingNodeTester;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static com.remsdaq.osapplication.utilities.XMLTestUtilities.assertXMLEqualIgnoringElementOrder;
import static org.custommonkey.xmlunit.XMLAssert.assertNodeTestPasses;

public class PolygonToXmlTest {
	
	private final String CONTROL_XML = PolygonXmlTestUtilities.generateSinglePolygonXml(1,
	                                                                                    new int[]{10, 20},
	                                                                                    new int[]{100, 200});
	
	private final String EMPTY_CONTROL_XML =
			"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			+ "<Polygon/>";
	private final int EXPECTED_NUM_NODES = 19;
	private String emptyPolyXml;
	private Polygon emptyControlPol;
	private String polyXml;
	private Polygon controlPol;
	
	@Before
	public void setUp() throws WellKnownTextException, JAXBException {
		
		emptyControlPol = new Polygon();
		emptyPolyXml = PolygonToXml.convertToString(emptyControlPol);
		
		controlPol = PolygonTestUtilities.generatePolygon(1, new int[]{10, 20}, new int[]{100, 200});
		polyXml = PolygonToXml.convertToString(controlPol);
	}
	
	@Test
	public void shouldReturnXmlStringIdenticalToControl() throws IOException,
	                                                             SAXException {
		System.out.println(CONTROL_XML);
		System.out.println(polyXml);
		assertXMLEqualIgnoringElementOrder("Comparing populated subscriber xml string to control xml string.",
		                                   CONTROL_XML, polyXml);
	}
	
	@Test
	public void testCountingNodeTester() throws IOException, SAXException {
		CountingNodeTester countingNodeTester = new CountingNodeTester(EXPECTED_NUM_NODES);
		assertNodeTestPasses(polyXml, countingNodeTester, Node.TEXT_NODE);
	}
	
	@Test
	public void shouldReturnDryXmlString() throws IOException, SAXException {
		assertXMLEqualIgnoringElementOrder("Comparing unpopulated subscriber xml string to control xml string.",
		                                   EMPTY_CONTROL_XML,
		                                   emptyPolyXml);
	}
	
	private Subscriber createSubscriber(String sub_id) {
		Subscriber sub = new Subscriber();
		sub.setID(sub_id);
		return sub;
	}
	
}