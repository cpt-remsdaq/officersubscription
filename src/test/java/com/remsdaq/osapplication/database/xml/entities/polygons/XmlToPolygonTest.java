package com.remsdaq.osapplication.database.xml.entities.polygons;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.utilities.entities.polygonTags.PolygonTagXmlTestUtilities;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonTestUtilities;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonXmlTestUtilities;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberXmlTestUtilities;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class XmlToPolygonTest {
	
	private final String EMPTY_POL_XML
			= "<Polygon>\n"
			  + "    <Subscribers/>\n"
			  + "</Polygon>";
	
	private final String POL_XML = PolygonXmlTestUtilities.generateSinglePolygonXml(1, new int[]{10, 20},
	                                                                                new int[]{100, 200});
	private Polygon emptyControlPol;
	private Polygon emptyConvertedPol;
	private Polygon controlPol;
	private Polygon convertedPol;
	
	@Before
	public void setUp() throws JAXBException, UnsupportedEncodingException, WellKnownTextException {
		emptyControlPol = new Polygon();
		emptyConvertedPol = XmlToPolygon.convertFromString(EMPTY_POL_XML);
		
		controlPol = PolygonTestUtilities.generatePolygon(1, new int[]{10, 20}, new int[]{100, 200});
		convertedPol = XmlToPolygon.convertFromString(POL_XML);
	}
	
	@Test
	public void shouldReturnPolygonWithCorrectSubscribers() throws JAXBException, UnsupportedEncodingException {
		String polXml = PolygonXmlTestUtilities.generateSinglePolygonXml(1, new int[]{10, 20}, new int[]{0});
		String subXml = SubscriberXmlTestUtilities.generateMultipleSubscribersXml(new int[]{10, 20}, new int[]{1});
		
		Polygon convertedPolWithSubs = XmlToPolygon.convertFromStringWitihSubscribers(polXml, subXml);
		
		assertEquals(controlPol.getSubscribers().size(), convertedPolWithSubs.getSubscribers().size());
		assertSubscribersAreEqual(controlPol, convertedPolWithSubs);
	}
	
	private void assertSubscribersAreEqual(Polygon controlPoly, Polygon convertedPoly) {
		String[] convertedTags = convertedPoly.getSubscriberStringArray();
		Arrays.sort(convertedTags);
		String[] controlTags = controlPoly.getSubscriberStringArray();
		Arrays.sort(controlTags);
		assertTrue(Arrays.equals(convertedTags, controlTags));
	}
	
	@Test
	public void shouldReturnPolygonWithCorrectSubscribersAndPolygonTags() throws JAXBException, UnsupportedEncodingException {
		String polXml = PolygonXmlTestUtilities.generateSinglePolygonXml(1, new int[]{10, 20}, new int[]{100, 200});
		String subXml = SubscriberXmlTestUtilities.generateMultipleSubscribersXml(new int[]{10, 20}, new int[]{1});
		String tagXml = PolygonTagXmlTestUtilities.generateMultiplePolygonTagsXml(new int[]{100, 200}, new int[]{1});
		
		System.out.println(polXml);
		System.out.println(subXml);
		System.out.println(tagXml);
		
		Polygon convertedPolWithSubsAndTags = XmlToPolygon.convertFromStringWithSubscribersAndPolygonTags(polXml, subXml, tagXml);
		
		System.out.println(convertedPolWithSubsAndTags);
	}
	
	@Test
	public void shouldReturnEmptyPolygon() {
		System.out.println(emptyConvertedPol);
		assertPolygonEmpty(emptyConvertedPol);
	}
	
	private void assertPolygonEmpty(Polygon convertedPoly) {
		assertNull(convertedPoly.getID());
		assertNull(convertedPoly.getName());
		assertNull(convertedPoly.getDescription());
		assertNull(convertedPoly.getWKTGeometry());
		assertEquals(0, convertedPoly.getSubscriberStringArray().length);
	}
	
	@Test
	public void populatedPolygonsShouldBeEqual() {
		assertPolygonsAreEqual(controlPol, convertedPol);
	}
	
	private void assertPolygonsAreEqual(Polygon controlPoly, Polygon convertedPoly) {
		assertEquals(controlPoly.getID(), convertedPoly.getID());
		assertEquals(controlPoly.getName(), convertedPoly.getName());
		assertEquals(controlPoly.getDescription(), convertedPoly.getDescription());
		assertEquals(controlPoly.getWKTGeometry(), convertedPoly.getWKTGeometry());
	}
	
	@Ignore
	@Test
	public void populatedTagsShouldBeEqual() {
//		assertTagsAreEqual(controlPol, convertedPol);
		assertEquals(controlPol, convertedPol);
	}
	
	private void assertTagsAreEqual(Polygon controlPoly, Polygon convertedPoly) {
		String[] controlTags = convertTagsToStringArray(controlPoly.getPolygonTags());
		String[] convertedTags = convertTagsToStringArray(convertedPoly.getPolygonTags());
		Arrays.sort(controlTags);
		Arrays.sort(convertedTags);
		assertTrue(Arrays.equals(controlTags, convertedTags));
	}
	
	private String[] convertTagsToStringArray(Set<PolygonTag> tags) {
		if (tags != null) {
			String[] tagStrings = new String[tags.size()];
			int i = 0;
			for (PolygonTag tag : tags) {
				tagStrings[i] = tag.getName();
				i++;
			}
			return tagStrings;
		}
		return new String[0];
	}
	
}