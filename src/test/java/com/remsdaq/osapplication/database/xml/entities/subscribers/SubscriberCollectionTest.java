package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.database.xml.entities.subscribers.SubscriberCollection;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SubscriberCollectionTest {
	
	private final Subscriber sub1 = new Subscriber();
	private final String sub1Id = "subscriber 1";
	private final String sub2Id = "subscriber 2";
	private final Subscriber sub = new Subscriber();
	private final Subscriber sub2 = new Subscriber();
	private final SubscriberCollection subCol = new SubscriberCollection();
	private final String subId = "id";
	
	@Before
	public void setUp() {
		sub.setID(subId);
		sub1.setID(sub1Id);
		sub2.setID(sub2Id);
	}
	
	@Test
	public void shouldAddSubscriberToEmptyCollection() {
		SubscriberCollection subCol = new SubscriberCollection();
		subCol.addToCollection(sub);
		
		assertEquals(1, subCol.getCollection().size());
	}
	
	@Test
	public void shouldRemoveSubscriberFromCollection() {
		subCol.addToCollection(sub1);
		subCol.addToCollection(sub2);
		
		subCol.removeFromCollection(sub1);
		assertEquals(1, subCol.getCollection().size());
	}
	
	@Test
	public void shouldSelectCorrectSubscriber() {
		subCol.addToCollection(sub);
		subCol.addToCollection(sub1);
		subCol.addToCollection(sub2);
		
		Subscriber selectedSub = subCol.selectFromCollection(sub);
		assertEquals(subId, selectedSub.getID());
	}
}