package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.utilities.XMLTestUtilities;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.examples.CountingNodeTester;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;

import static org.custommonkey.xmlunit.XMLAssert.assertNodeTestPasses;

public class SubscriberCollectionToXmlTest {
	
	private final String ID = "id";
	private final String FIRST_NAME = "first name";
	private final String LAST_NAME = "last name";
	private final String EMAIL = "email";
	private final String MOBILE = "mobile";
	private final String PAGER = "pager";
	private final int EMAIL_PRIORITY = 1;
	private final int MOBILE_PRIORITY = 2;
	private final int PAGER_PRIORITY = 3;
	
	private final String POLY_1_ID = "poly 1";
	private final String POLY_2_ID = "poly 2";
	
	private final int EXPECTED_NODE_COUNT_SINGLE_SUBSCRIBER = 25;
	private final int EXPECTED_NODE_COUNT_MULTIPLE_SUBSCRIBERS = 49;
	
	private final String XML_CONTROL_MULTIPLE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
	                                            + "<Subscribers>\n"
	                                            + "    <Subscriber ID=\"id 1\">\n"
	                                            + "        <FirstName>first name 1</FirstName>\n"
	                                            + "        <LastName>last name 1</LastName>\n"
	                                            + "        <Email>email 1</Email>\n"
	                                            + "        <EmailPriority>1</EmailPriority>\n"
	                                            + "        <Mobile>mobile 1</Mobile>\n"
	                                            + "        <MobilePriority>2</MobilePriority>\n"
	                                            + "        <Pager>pager 1</Pager>\n"
	                                            + "        <PagerPriority>3</PagerPriority>\n"
	                                            + "        <Polygons>\n"
	                                            + "            <Polygon>poly 1</Polygon>\n"
	                                            + "            <Polygon>poly 2</Polygon>\n"
	                                            + "        </Polygons>\n"
	                                            + "    </Subscriber>\n"
	                                            + "    <Subscriber ID=\"id 2\">\n"
	                                            + "        <FirstName>first name 2</FirstName>\n"
	                                            + "        <LastName>last name 2</LastName>\n"
	                                            + "        <Email>email 2</Email>\n"
	                                            + "        <EmailPriority>1</EmailPriority>\n"
	                                            + "        <Mobile>mobile 2</Mobile>\n"
	                                            + "        <MobilePriority>2</MobilePriority>\n"
	                                            + "        <Pager>pager 2</Pager>\n"
	                                            + "        <PagerPriority>3</PagerPriority>\n"
	                                            + "        <Polygons>\n"
	                                            + "            <Polygon>poly 1</Polygon>\n"
	                                            + "            <Polygon>poly 2</Polygon>\n"
	                                            + "        </Polygons>\n"
	                                            + "    </Subscriber>\n"
	                                            + "</Subscribers>";
	
	private final String XML_CONTROL_SINGLE = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
	                                          + "<Subscribers>\n"
	                                          + "    <Subscriber ID=\"" + ID + "\">\n"
	                                          + "        <FirstName>" + FIRST_NAME + "</FirstName>\n"
	                                          + "        <LastName>" + LAST_NAME + "</LastName>\n"
	                                          + "        <Email>" + EMAIL + "</Email>\n"
	                                          + "        <EmailPriority>" + EMAIL_PRIORITY + "</EmailPriority>\n"
	                                          + "        <Mobile>" + MOBILE + "</Mobile>\n"
	                                          + "        <MobilePriority>" + MOBILE_PRIORITY + "</MobilePriority>\n"
	                                          + "        <Pager>" + PAGER + "</Pager>\n"
	                                          + "        <PagerPriority>" + PAGER_PRIORITY + "</PagerPriority>\n"
	                                          + "        <Polygons>\n"
	                                          + "            <Polygon>" + POLY_1_ID + "</Polygon>\n"
	                                          + "            <Polygon>" + POLY_2_ID + "</Polygon>\n"
	                                          + "        </Polygons>\n"
	                                          + "    </Subscriber>\n"
	                                          + "</Subscribers>\n";
	private String xmlStringSingle;
	private String xmlStringMultiple;
	
	@Before
	public void setUp() throws JAXBException {
		XMLUnit.setIgnoreComments(Boolean.TRUE);
		XMLUnit.setIgnoreWhitespace(Boolean.TRUE);
		XMLUnit.setNormalizeWhitespace(Boolean.TRUE);
		XMLUnit.setIgnoreDiffBetweenTextAndCDATA(Boolean.TRUE);
		XMLUnit.setIgnoreAttributeOrder(Boolean.TRUE);
		XMLUnit.setNormalize(Boolean.TRUE);
		
		Subscriber sub = createSub(0);
		
		Subscriber sub1 = createSub(1);
		Subscriber sub2 = createSub(2);
		
		SubscriberCollection subColl = new SubscriberCollection();
		subColl.addToCollection(sub);
		
		SubscriberCollection subColl2 = new SubscriberCollection();
		subColl2.addToCollection(sub1);
		subColl2.addToCollection(sub2);
		
		xmlStringSingle = SubscriberCollectionToXml.convertToString(subColl);
		xmlStringMultiple = SubscriberCollectionToXml.convertToString(subColl2);
	}
	
	private Subscriber createSub(int number) {
		Subscriber subscriber = new Subscriber();
		String numberSuffix;
		if (number == 0)
			numberSuffix = "";
		else
			numberSuffix = " " + number;
		
		subscriber.setID(ID + numberSuffix);
		subscriber.setFirstName(FIRST_NAME + numberSuffix);
		subscriber.setLastName(LAST_NAME + numberSuffix);
		subscriber.setEmail(EMAIL + numberSuffix);
		subscriber.setEmailPriority(EMAIL_PRIORITY);
		subscriber.setMobile(MOBILE + numberSuffix);
		subscriber.setMobilePriority(MOBILE_PRIORITY);
		subscriber.setPager(PAGER + numberSuffix);
		subscriber.setPagerPriority(PAGER_PRIORITY);
		subscriber.setPolygonStringArray(new String[]{POLY_1_ID, POLY_2_ID});
		return subscriber;
	}
	
	@Test
	public void shouldReturnXmlStringIdenticalToControl_Single() throws IOException, SAXException {
		XMLTestUtilities.assertXMLEqualIgnoringElementOrder(XML_CONTROL_SINGLE, xmlStringSingle);
	}
	
	@Test
	public void testCountingNodeTester_Single() throws IOException, SAXException {
		CountingNodeTester countingNodeTester = new CountingNodeTester(EXPECTED_NODE_COUNT_SINGLE_SUBSCRIBER);
		assertNodeTestPasses(xmlStringSingle, countingNodeTester, Node.TEXT_NODE);
	}
	
	@Test
	public void shouldReturnXmlStringIdenticalToControl_Multiple() throws IOException, SAXException {
		XMLTestUtilities.assertXMLEqualIgnoringElementOrder(XML_CONTROL_MULTIPLE, xmlStringMultiple);
	}
	
	@Test
	public void testCountingNodetester_Multiple() throws IOException, SAXException {
		CountingNodeTester countingNodeTester = new CountingNodeTester(EXPECTED_NODE_COUNT_MULTIPLE_SUBSCRIBERS);
		assertNodeTestPasses(xmlStringMultiple, countingNodeTester, Node.TEXT_NODE);
	}
	
}