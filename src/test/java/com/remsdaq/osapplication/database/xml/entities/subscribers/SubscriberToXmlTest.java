package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import org.custommonkey.xmlunit.XMLUnit;
import org.custommonkey.xmlunit.examples.CountingNodeTester;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import static org.custommonkey.xmlunit.XMLAssert.assertNodeTestPasses;
import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;

public class SubscriberToXmlTest {
	
	final String SUB_ID = "sub ID";
	final String SUB_FIRST_NAME = "first name";
	final String SUB_LAST_NAME = "last name";
	final String EMAIL = "EMAIL address";
	final int SUB_EMAIL_PRIORITY = 1;
	final String MOBILE = "MOBILE number";
	final int SUB_MOBILE_PRIORITY = 2;
	final String PAGER = "PAGER number";
	final int SUB_PAGER_PRIORITY = 3;
	final String TAG_NAME = "tag name";
	final private String EMPTY_CONTROL_XML =
			"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			+ "<Subscriber ID=\"\">\n"
			+ "    <FirstName></FirstName>\n"
			+ "    <LastName></LastName>\n"
			+ "    <Email></Email>\n"
			+ "    <EmailPriority>0</EmailPriority>\n"
			+ "    <Mobile></Mobile>\n"
			+ "    <MobilePriority>0</MobilePriority>\n"
			+ "    <Pager></Pager>\n"
			+ "    <PagerPriority>0</PagerPriority>\n"
			+ "    <Polygons/>\n"
			+ "</Subscriber>";
	Subscriber sub = new Subscriber();
	Set<Subscriber> subSet = new HashSet<>();
	String POL_ID = "pol ID";
	final private String CONTROL_XML =
			"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			+ "<Subscriber ID=\"" + SUB_ID + "\">\n"
			+ "    <FirstName>" + SUB_FIRST_NAME + "</FirstName>\n"
			+ "    <LastName>" + SUB_LAST_NAME + "</LastName>\n"
			+ "    <Email>" + EMAIL + "</Email>\n"
			+ "    <EmailPriority>" + SUB_EMAIL_PRIORITY + "</EmailPriority>\n"
			+ "    <Mobile>" + MOBILE + "</Mobile>\n"
			+ "    <MobilePriority>" + SUB_MOBILE_PRIORITY + "</MobilePriority>\n"
			+ "    <Pager>" + PAGER + "</Pager>\n"
			+ "    <PagerPriority>" + SUB_PAGER_PRIORITY + "</PagerPriority>\n"
			+ "    <Polygons>\n"
			+ "        <Polygon>" + POL_ID + "</Polygon>\n"
			+ "        <Polygon>" + POL_ID + "</Polygon>\n"
			+ "    </Polygons>"
			+ "</Subscriber>";
	String polName = "pol name";
	String polDescription = "pol description";
	String polWKT = "POLYGON ((1.0 1.0, 0.0 1.0, 1.0 0.0, 0.0 0.0))";
	Polygon pol = new Polygon();
	Set<Polygon> polSet = new HashSet<>();
	PolygonTag tag = new PolygonTag();
	Set<PolygonTag> tagSet = new HashSet<>();
	String subXml;
	private Polygon pol2 = new Polygon();
	private String emptySubXml = "";
	
	@Before
	public void setUp() throws JAXBException, WellKnownTextException {
		
		XMLUnit.setIgnoreWhitespace(true);
		
		populateSubscriber(sub);
		
		populatePolygon(pol);
		
		populatePolygon(pol2);
		
		tag.setName(TAG_NAME);
		
		polSet.add(pol2);
		polSet.add(pol);
		subSet.add(sub);
		tagSet.add(tag);
		
		sub.setPolygons(polSet);
		pol.setSubscribers(subSet);
		pol.setPolygonTags(tagSet);
		tag.setPolygons(polSet);
		
		subXml = SubscriberToXml.convertToString(sub);
		
		emptySubXml = SubscriberToXml.convertToString(new Subscriber());
	}
	
	private void populateSubscriber(Subscriber sub) {
		sub.setID(SUB_ID);
		sub.setFirstName(SUB_FIRST_NAME);
		sub.setLastName(SUB_LAST_NAME);
		sub.setEmail(EMAIL);
		sub.setEmailPriority(SUB_EMAIL_PRIORITY);
		sub.setMobile(MOBILE);
		sub.setMobilePriority(SUB_MOBILE_PRIORITY);
		sub.setPager(PAGER);
		sub.setPagerPriority(SUB_PAGER_PRIORITY);
	}
	
	private void populatePolygon(Polygon pol) {
		pol.setID(POL_ID);
		pol.setName(polName);
		pol.setDescription(polDescription);
		pol.setWKTGeometry(polWKT);
	}
	
	@Test
	public void shouldReturnXmlStringIdenticalToControl() throws IOException, SAXException {
		System.out.println(subXml);
		assertXMLEqual("Comparing converted subscriber xml to control xml.", CONTROL_XML, subXml);
	}
	
	@Test
	public void testCountingNodeTester() throws IOException, SAXException {
		CountingNodeTester countingNodeTester = new CountingNodeTester(23);
		assertNodeTestPasses(subXml, countingNodeTester, Node.TEXT_NODE);
	}
	
	@Test
	public void shouldReturnDryXmlString() throws IOException, SAXException {
		System.out.println(emptySubXml);
		assertXMLEqual("Comparing unpopulated subscriber xml string to control xmlstring.", EMPTY_CONTROL_XML,
		               emptySubXml);
	}
	
	@Test
	public void shouldReturnSubscriberCollectionXml() throws JAXBException {
		SubscriberCollection subCol = new SubscriberCollection();
		subCol.addToCollection(sub);
		subCol.addToCollection(new Subscriber());
		String subColXml = SubscriberToXml.convertCollectionToString(subCol);
		System.out.println(subColXml);
	}
}