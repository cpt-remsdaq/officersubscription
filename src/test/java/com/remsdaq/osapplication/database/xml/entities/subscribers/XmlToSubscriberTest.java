package com.remsdaq.osapplication.database.xml.entities.subscribers;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberTestUtilities;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberXmlTestUtilities;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.bind.JAXBException;
import java.io.UnsupportedEncodingException;
import java.util.Set;

public class XmlToSubscriberTest {
	
	private final String EMPTY_SUB_XML =
			"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			+ "<Subscriber ID=\"\">\n"
			+ "    <FirstName></FirstName>\n"
			+ "    <LastName></LastName>\n"
			+ "    <Email></Email>\n"
			+ "    <EmailPriority>0</EmailPriority>\n"
			+ "    <Mobile></Mobile>\n"
			+ "    <MobilePriority>0</MobilePriority>\n"
			+ "    <Pager></Pager>\n"
			+ "    <PagerPriority>0</PagerPriority>\n"
			+ "</Subscriber>";
	
	private final String SUB_XML = SubscriberXmlTestUtilities.generateSingleSubscriberXmlFragment(1,
	                                                                                              new int[]{10, 20}, 0);
	
	private Subscriber emptyControlSub;
	private Subscriber emptyConvertedSub;
	
	private Subscriber controlSub;
	private Subscriber convertedSub;
	
	private Set<Polygon> polSet;
	
	public XmlToSubscriberTest() {
	}
	
	@Before
	public void setUp() throws JAXBException, UnsupportedEncodingException {
		emptyControlSub = new Subscriber();
		emptyConvertedSub = XmlToSubscriber.convertFromString(EMPTY_SUB_XML);
		
		controlSub = SubscriberTestUtilities.generateSubscriber(1, new int[]{10,20});
		convertedSub = XmlToSubscriber.convertFromString(SUB_XML);
	}
	
	@Test
	public void shouldReturnEmptySubscriber() {
		SubscriberTestUtilities.assertSubscribersAreEqual(emptyControlSub, emptyConvertedSub);
	}
	
	@Test
	public void populatedSubscribersShouldBeEqual() {
		SubscriberTestUtilities.assertSubscribersAreEqual(controlSub, convertedSub);
	}
	
	@Ignore
	@Test
	public void populatedSubscribersSubscriptionsShouldBeEqual() {
		SubscriberTestUtilities.assertSubscriptionsAreEqual(controlSub, convertedSub);
	}
	
	@Ignore
	@Test
	public void shouldBe2SubscriberSubscriptions() {
		System.out.println("CONTROL: " + controlSub + "\n");
		System.out.println("CONVERTED: " + convertedSub + "\n");
		
		System.out.println(convertedSub.getPolygonString());
		SubscriberTestUtilities.assertSubscriptionsAreOfSameSize(controlSub, convertedSub);
	}
}