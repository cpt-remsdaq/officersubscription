package com.remsdaq.osapplication.entities;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.EntityIdComparator;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonTestUtilities;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberTestUtilities;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class EntityIdComparatorTest {
	
	@Test
	public void entitySetShouldEqualItself() {
		EntityIdComparator comparator = new EntityIdComparator();
		
		Set<Subscriber> subSet1 = SubscriberTestUtilities.generateSubscribers(new int[]{1, 2, 3});
		
		assertTrue("Subscriber set should equal itself", comparator.areEntitySetsEqual(subSet1,subSet1));
	}
	
	@Test
	public void entitySetShouldNotEqualNull() {
		EntityIdComparator comparator = new EntityIdComparator();
		
		Set<Subscriber> subSet1 = SubscriberTestUtilities.generateSubscribers(new int[] {1, 2, 3});
		Set<Subscriber> subSet2 = null;
		
		assertFalse("Subscriber set should not equal null.", comparator.areEntitySetsEqual(subSet1, subSet2));
		assertFalse("Null should not equal subscriber set.", comparator.areEntitySetsEqual(subSet2, subSet1));
		
		Set<Subscriber> subSet3 = new HashSet<>();
		
		subSet3.add(null);
		
		assertFalse("Subscriber set should not equal set containing null.", comparator.areEntitySetsEqual(subSet1, subSet3));
		assertFalse("Set containin null should not equal subscriber  set.", comparator.areEntitySetsEqual(subSet3, subSet1));
	}
	
	@Test
	public void allEntitiesShouldBeInComparisonSet() throws WellKnownTextException {
		EntityIdComparator comparator = new EntityIdComparator();
		
		Set<Subscriber> subSet1 = SubscriberTestUtilities.generateSubscribers(new int[]{1, 2, 3});
		Set<Subscriber> subSet2 = SubscriberTestUtilities.generateSubscribers(new int[]{1, 2, 3});
		
		assertTrue("All subscribers from set 1 should be contained in set 2 and vice versa.", comparator.areEntitySetsEqual(subSet1, subSet2));
		
		Set<Polygon> polSet1 = PolygonTestUtilities.generatePolygons(new int[]{1, 2, 3});
		Set<Polygon> polSet2 = PolygonTestUtilities.generatePolygons(new int[]{1, 2, 3});
		
		assertTrue("All polygons from set 1 should be contained in set 2 and vice versa.", comparator.areEntitySetsEqual(polSet1, polSet2));
	}
	
	@Test
	public void comparisonSetShouldNotContainAllEntities() {
		EntityIdComparator comparator = new EntityIdComparator();
		
		Set<Subscriber> subSet1 = SubscriberTestUtilities.generateSubscribers(new int[]{1, 2, 3, 4});
		Set<Subscriber> subSet2 = SubscriberTestUtilities.generateSubscribers(new int[]{1, 2, 3});
		
		assertFalse("One subscriber from set 1 should not be contained in set 2.", comparator.areEntitySetsEqual(subSet1, subSet2));
		assertFalse("One subscriber from set 1 should not be contained in set 2.", comparator.areEntitySetsEqual(subSet2, subSet1));
	}
	
	@Test
	public void comparisonSetShouldNotContainAllEntitiesDueToDuplicates() {
		EntityIdComparator comparator = new EntityIdComparator();
		
		Set<Subscriber> subSet1 = SubscriberTestUtilities.generateSubscribers(new int[]{1,1,2,3});
		Set<Subscriber> subSet2 = SubscriberTestUtilities.generateSubscribers(new int[]{1,2,3,4});
		
		assertFalse(comparator.areEntitySetsEqual(subSet1,subSet2));
		assertFalse(comparator.areEntitySetsEqual(subSet2,subSet1));
		
		Set<Subscriber> subSet3 = SubscriberTestUtilities.generateSubscribers(new int[]{1,2,2,4});
		
		assertFalse(comparator.areEntitySetsEqual(subSet1,subSet3));
		assertFalse(comparator.areEntitySetsEqual(subSet3,subSet1));
	}
}