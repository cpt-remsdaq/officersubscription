package com.remsdaq.osapplication.entities;

import com.remsdaq.osapplication.GlobalContext;
import com.remsdaq.osapplication.configs.Config;
import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class XmlDataEntityTest {
	
	private final String CONFIG_FILE_PATH = "testConfigs/xmlDataConnectionConfig.properties";
	private final Subscriber testSub = new Subscriber();
	private final Polygon testPol = new Polygon();
	private final PolygonTag testTag = new PolygonTag();
	
	private final String SUBSCRIBER_XML_DATA_PATH = "xmlData/test/subscriber-test.xml";
	private final String SUBSCRIBER_XML_DATA_COLLECTION_PATH = "xmlData/test/subscriber-collection-test.xml";
	private final String POLYGON_XML_DATA_PATH = "xmlData/test/polygon-test.xml";
	private final String POLYGON_XML_DATA_COLLECTION_PATH = "xmlData/test/polygon-collection-test.xml";
	private final String POLYGON_TAG_XML_DATA_PATH = "xmlData/test/polygon-tag-test.xml";
	private final String POLYGON_TAG_XML_DATA_COLLECTION_PATH = "xmlData/test/polygon-tag-collection-test.xml";
	
	
	@Before
	public void setUp() {
		GlobalContext.getInstance().setConfig(new Config(CONFIG_FILE_PATH));
	}
	
	@Test
	public void subscriberShouldReturnCorrectXmlDataPath() {
		assertEquals("Instantiated Subscriber data path is being retreived from separate instance of global context",
		             SUBSCRIBER_XML_DATA_PATH, testSub.getXmlDataPath());
		assertEquals("New Subscriber data path is being retreived from separate instance of global constext", SUBSCRIBER_XML_DATA_PATH,
		             new Subscriber().getXmlDataPath());
		assertEquals(CONFIG_FILE_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
	}
	
	@Test
	public void subscriberShouldReturnCorrectXmlDataCollectionPath() {
		Subscriber testSub = new Subscriber();
		assertEquals("Instantiated Subscriber data path is being retreived from separate instance of global context",
		             SUBSCRIBER_XML_DATA_COLLECTION_PATH, testSub.getXmlDataCollectionPath());
		assertEquals("New Subscriber data path is being retreived from separate instance of global constext",
		             SUBSCRIBER_XML_DATA_COLLECTION_PATH, new Subscriber().getXmlDataCollectionPath());
		assertEquals(CONFIG_FILE_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
	}
	
	@Test
	public void polygonShouldReturnCorrectXmlDataPath() {
		assertEquals("Instantiated Polygon data path is being retreived from separate instance of global context",
		             POLYGON_XML_DATA_PATH, testPol.getXmlDataPath());
		assertEquals("New Polygon data path is being retreived from separate instance of global constext", POLYGON_XML_DATA_PATH,
		             new Polygon().getXmlDataPath());
		assertEquals(CONFIG_FILE_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
	}
	
	@Test
	public void polygonShouldReturnCorrectXmlDataCollectionPath() {
		Subscriber testSub = new Subscriber();
		assertEquals("Instantiated Polygon data path is being retreived from separate instance of global context",
		             POLYGON_XML_DATA_COLLECTION_PATH, testPol.getXmlDataCollectionPath());
		assertEquals("New Polygon data path is being retreived from separate instance of global constext",
		             POLYGON_XML_DATA_COLLECTION_PATH, new Polygon().getXmlDataCollectionPath());
		assertEquals(CONFIG_FILE_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
	}
	
	@Test
	public void polygonTagShouldReturnCorrectXmlDataPath() {
		assertEquals("Instantiated PolygonTag data path is being retreived from separate instance of global context",
		             POLYGON_TAG_XML_DATA_PATH, testTag.getXmlDataPath());
		assertEquals("New PolygonTag data path is being retreived from separate instance of global constext", POLYGON_TAG_XML_DATA_PATH,
		             new PolygonTag().getXmlDataPath());
		assertEquals(CONFIG_FILE_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
	}
	
	@Test
	public void polygonTagShouldReturnCorrectXmlDataCollectionPath() {
		Subscriber testSub = new Subscriber();
		assertEquals("Instantiated PolygonTag data path is being retreived from separate instance of global context",
		             POLYGON_TAG_XML_DATA_COLLECTION_PATH, testTag.getXmlDataCollectionPath());
		assertEquals("New PolygonTag data path is being retreived from separate instance of global constext",
		             POLYGON_TAG_XML_DATA_COLLECTION_PATH, new PolygonTag().getXmlDataCollectionPath());
		assertEquals(CONFIG_FILE_PATH, GlobalContext.getInstance().getConfig().getConfigFilePath());
	}
}