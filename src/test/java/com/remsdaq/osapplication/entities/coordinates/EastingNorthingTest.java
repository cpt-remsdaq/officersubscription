package com.remsdaq.osapplication.entities.coordinates;

import org.junit.Assert;
import org.junit.Test;

public class EastingNorthingTest {
	
	private final double DELTA = 0.0001;
	EastingNorthing en = new EastingNorthing(119396L, 129968L);
	
	@Test
	public void testConversionToLatLng() {
		Assert.assertEquals(51, en.toLatLng().getLat(), DELTA);
        Assert.assertEquals(-6, en.toLatLng().getLng(), DELTA);
	}
	
}