package com.remsdaq.osapplication.entities.coordinates;

import math.geom2d.Point2D;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static java.lang.Math.sqrt;
import static org.junit.Assert.assertEquals;

public class LatLngTest {
	
	private final double LNG1 = 0;
	private final double LAT1 = 0;
	private final double LNG2 = -1;
	private final double LAT2 = -1;
	private final double DELTA = 1E-7;
	private final double DISTANCE = sqrt(Math.pow((LAT2 - LAT1), 2) + Math.pow((LNG2 - LNG1), 2));
	private LatLng latLng1;
	private LatLng latLng2;
	private Point2D point2D;
	
	
	@Before
	public void setUp() {
		latLng1 = new LatLng(LNG1, LAT1);
		latLng2 = new LatLng(LNG2, LAT2);
		
		point2D = new Point2D(LNG1, LAT1);
	}
	
	@Test
	public void shouldReturnCorrectLatitude() {
		assertEquals("getLat() didn't return the correct latitude", LAT1, latLng1.getLat(), DELTA);
	}
	
	@Test
	public void shouldReturnCorrectLongitude() {
		assertEquals("getLng() didn't return the correct longitude", LNG1, latLng1.getLng(), DELTA);
	}
	
	@Test
	public void shouldReturnCorrectSignedDistance() {
		assertEquals("getSignedDistance() returned the wrong signed distance. ", DISTANCE, latLng2.getSignedDistance(latLng1), DELTA);
	}
	
	@Test
	public void shouldReturnCorrectAbsoluteDistance() {
		assertEquals("getAbsoluteDistance() returned the wrong distance", DISTANCE, latLng2.getAbsoluteDistance(latLng1), DELTA);
	}
	
	@Test
	public void shouldReturnCorrectClass() {
		// I know this is silly but it isn't using the default point2d so I just want to make sure
		assertEquals("getPoint2D() returned the wrong Point2D class.", Point2D.class, latLng1.getPoint2D().getClass());
	}
	
	@Test
	public void shouldReturnCorrectPoint2D() {
		assertEquals("getPoint2D() didn't return the matching LatLng", point2D, latLng1.getPoint2D());
	}
	
	@Test
	public void shouldReturnCorrectCoordinates() {
		assertEquals("getPoint2D() didn't return a LatLng with the correct longitude", LNG1, latLng1.getPoint2D().x(), DELTA);
		assertEquals("getPoint2D() didn't return a LatLng with the correct latitude", LAT1, latLng1.getPoint2D().y(), DELTA);
	}
	
	@Test
	public void testConversionToEastingNorthing() {
		LatLng latLng = new LatLng(-6, 51);
		EastingNorthing eastNorth = latLng.toEastingNorthing();
		Assert.assertEquals(119396, (long) eastNorth.getEast());
		Assert.assertEquals(129968, (long) eastNorth.getNorth());
	}
}