package com.remsdaq.osapplication.entities.incidents;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class IncidentBuilderTest {
	
	List<IncidentType> types = new ArrayList<>();
	IncidentType type = new IncidentType();
	
	@Before
	public void setUp() {
		type.setLevel1Description("Fire");
		type.setLevel2Description("Postbox");
	}
	
	@Test
	public void shouldBuildIncident() {
		Incident incident = new IncidentBuilder(123456L)
				.type(type)
				.dateTime(LocalDateTime.now())
				.build();
	}
}