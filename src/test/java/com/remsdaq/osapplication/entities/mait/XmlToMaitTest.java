package com.remsdaq.osapplication.entities.mait;

import com.remsdaq.osapplication.entities.mait.entities.messages.IncidentCreationMait;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class XmlToMaitTest {

	@Test
	public void init() throws IOException {
		
		String filePath = "C:\\Users\\cpt\\Documents\\Codes\\Testing\\CreateIncidentTest.XML";
		
		// Get xml as stream of strings
		List<String> strings = Files.readAllLines(Paths.get(filePath));
		String validXml = strings.stream().collect(Collectors.joining());
		
		XmlToMait toMait = new XmlToMait();
		IncidentCreationMait mait = (IncidentCreationMait) toMait.getMait(validXml);
		
		System.out.println(mait);
		System.out.println(mait.getSummary());
	}
	
}