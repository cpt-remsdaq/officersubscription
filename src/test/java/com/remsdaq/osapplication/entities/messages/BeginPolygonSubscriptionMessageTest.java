package com.remsdaq.osapplication.entities.messages;

import com.remsdaq.osapplication.entities.messages.polygonSubscription.BeginPolygonSubscriptionMessage;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BeginPolygonSubscriptionMessageTest {
	
	@Test
	public void testInitialAreaMessage() {
		Polygon poly = new Polygon();
		poly.setID("poly id");
		poly.setName("poly name");
		
		assertEquals("You are now receiving updates from polygon area 'poly name'", new BeginPolygonSubscriptionMessage(poly).buildMessage());
	}
	
}