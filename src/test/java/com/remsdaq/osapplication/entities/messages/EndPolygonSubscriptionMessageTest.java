package com.remsdaq.osapplication.entities.messages;

import com.remsdaq.osapplication.entities.messages.polygonSubscription.EndPolygonSubscriptionMessage;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EndPolygonSubscriptionMessageTest {

	@Test
	public void testFinalAreaMessage() {
		Polygon poly = new Polygon();
		poly.setID("poly id");
		poly.setName("poly name");
		
		assertEquals("You are NO LONGER receiving updates from polygon area 'poly name'", new EndPolygonSubscriptionMessage(poly).buildMessage());
	}
	
}