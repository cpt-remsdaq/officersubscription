//package com.remsdaq.osapplication.entities.messages;
//
//import com.remsdaq.osapplication.entities.coordinates.EastingNorthing;
//import com.remsdaq.osapplication.resqueInteractors.builders.ResqueIncidentBuilder;
//import com.remsdaq.osapplication.entities.resqueentities.*;
//import org.junit.Before;
//import org.junit.Test;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import static com.remsdaq.osapplication.entities.resqueentities.Incident.STATUS_MOBILISED;
//import static org.junit.Assert.assertEquals;
//
//public class IncidentMessageTest {
//
//	private final String EXPECTED_MESSAGE =
//			"Incident No:        123456\n"
//			+ "Incident Type:      Fire - Postbox\n"
//			+ "Postcode:           SW1A 2AA\n"
//			+ "Map Ref:            123456 654321\n"
//			+ "Resource:           CPT123\n"
//			+ "Resource:           CL01\n";
//
//	private final String EXPECTED_VERBOSE_MESSAGE =
//			"Incident No:        123456\n"
//			+ "Incident Type:      Fire - Postbox\n"
//			+ "Postcode:           SW1A 2AA\n"
//			+ "Map Ref:            123456 654321\n"
//			+ "Resource:           CPT123\n"
//			+ "Resource:           CL01\n"
//			+ "Incident Status:    STATUS_MOBILISED\n"
//			+ "Incident Priority:  1\n"
//			+ "No Of Calls:        1\n"
//			+ "Address:            10 Downing Street, Westminster, London\n"
//			+ "Command Person:     CPT123\n"
//			+ "Command Vehicle:    CL01\n"
//			+ "Description:        Cris de coeur.\n";
//
//	private final int STATUS = STATUS_MOBILISED;
//	private final int NUMBER_OF_CALLS = 1;
//	private final String ADDRESS_STRING = "10 Downing Street\nWestminster\nLondon";
//	private final String DESCRIPTION = "Cris de coeur.";
//	private final EastingNorthing MAP_REF = new EastingNorthing(123456L, 654321L);
//	private final Priority PRIORITY = new Priority(1L);
//	private IncidentType type = new IncidentType();
//	private Incident incident;
//	private Address address = new Address();
//	private List<Resource> resources = new ArrayList<>();
//	private IncidentMessage message;
//	private Person commandPerson = new Person();
//	private Vehicle commandVehicle = new Vehicle();
//
//	@Before
//	public void setUp() {
//		type.setId(52L);
//		type.setLevel1Description("Fire");
//		type.setLevel1DescriptionCode("F");
//		type.setLevel2Description("Postbox");
//		type.setDescription(
//				"Fires involving the contents and or structure of freestanding post boxes, excluding those attached to or incorporated with a building.");
//		type.setPriority(new Priority(4L));
//
//		address.setPostcode("SW1A 2AA");
//
//		commandPerson.setCallSign("CPT123");
//		commandVehicle.setCallSign("CL01");
//
//		resources.add(commandPerson);
//		resources.add(commandVehicle);
//	}
//
//	@Test
//	public void shouldBuildMessageBasedOnIncident() {
//		incident = new ResqueIncidentBuilder(123456L)
//				.addType(type)
//				.tempTransientAddress(address)
//				.resources(resources)
//				.build();
//
//		IncidentMessage message = new IncidentMessage(incident);
//		System.out.println(message.buildMessage());
//		assertEquals(EXPECTED_MESSAGE, message.buildMessage());
//	}
//
//	@Test
//
//	public void shouldBuildVerboseMessageBasedOnIncident() {
//		incident = new ResqueIncidentBuilder(123456L)
//				.addType(type)
//				.status(STATUS)
//				.priority(PRIORITY)
//				.tempTransientAddress(address)
//				.addressString(ADDRESS_STRING)
//				.commandPerson(398475L)
//				.commandVehicle(23487L)
//				.resources(resources)
//				.description(DESCRIPTION)
//				.build();
//
//		IncidentMessage message = new IncidentMessage(incident);
//		System.out.println(message.buildMessageVerbose());
//		assertEquals(EXPECTED_VERBOSE_MESSAGE, message.buildMessageVerbose());
//	}
//
//}