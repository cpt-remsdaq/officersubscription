//package com.remsdaq.osapplication.entities.messages.incidentSubscription;
//
//import com.remsdaq.osapplication.entities.resqueentities.Incident;
//import com.remsdaq.osapplication.utilities.entities.resqueEntities.ResqueIncidentTestUtilities;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//import static org.junit.Assert.assertEquals;
//
//public class BeginIncidentSubscriptionMessageTest {
//
//	private static Incident incident = new Incident();
//
//	private String normalMessage =
//			"You are now receiving incident updates for...\n"
//			+ "\n"
//			+ "Incident No:        123456\n"
//			+ "Incident Type:      Fire - Postbox\n"
//			+ "Postcode:           SW1A 2AA\n"
//			+ "Map Ref:            123456 654321\n"
//			+ "Resource:           CPT123\n"
//			+ "Resource:           CL01\n";
//
//	private String verboseMessage =
//			"You are now receiving incident updates for...\n"
//			+ "\n"
//			+ "Incident No:        123456\n"
//			+ "Incident Type:      Fire - Postbox\n"
//			+ "Postcode:           SW1A 2AA\n"
//			+ "Map Ref:            123456 654321\n"
//			+ "Resource:           CPT123\n"
//			+ "Resource:           CL01\n"
//			+ "Incident Status:    STATUS_MOBILISED\n"
//			+ "Incident Priority:  1\n"
//			+ "No Of Calls:        1\n"
//			+ "Address:            10 Downing Street, Westminster, London\n"
//			+ "Command Person:     CPT123\n"
//			+ "Command Vehicle:    CL01\n"
//			+ "Description:        Cris de coeur.\n";
//
//	@BeforeClass
//	public static void setUp() {
//		incident = ResqueIncidentTestUtilities.getIncident();
//	}
//
//	@Test
//	public void testNormalMessage() {
//		System.out.println(new BeginIncidentSubscriptionMessage(incident).buildMessage());
//		assertEquals(normalMessage, new BeginIncidentSubscriptionMessage(incident).buildMessage());
//	}
//
//	@Test
//	public void testVerboseMessage() {
//		System.out.println(new BeginIncidentSubscriptionMessage(incident).buildMessageVerbose());
//		assertEquals(verboseMessage, new BeginIncidentSubscriptionMessage(incident).buildMessageVerbose());
//	}
//
//}