package com.remsdaq.osapplication.entities.polygons;

import com.remsdaq.osapplication.entities.Entity;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.utilities.entities.polygons.PolygonTestUtilities;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class PolygonTest {
	
	
	private final int[] numArray = {1, 2, 3};
	private final int[] otherNumArray = {1, 2};
	private Polygon polygon;
	private Polygon identicalPolygon;
	private Polygon differentPolygon;
	private Set<Polygon> polSet = new HashSet<>();
	private Subscriber sub1 = new Subscriber();
	private Set<Subscriber> subSet = new HashSet<>();
	
	@Before
    public void setUp() throws WellKnownTextException {
		polygon = PolygonTestUtilities.generatePolygon(1, numArray, numArray);
		identicalPolygon = PolygonTestUtilities.generatePolygon(1, numArray, numArray);
		differentPolygon = PolygonTestUtilities.generatePolygon(1, numArray, otherNumArray);
    }
    
    @Test
    public void hashCodeOfAPolygonShouldBeConstant() {
    	assertEquals(polygon.hashCode(), polygon.hashCode());
    }
    
    @Test
    public void hashCodesShouldBeIdentical() {
        assertEquals(polygon.hashCode(), identicalPolygon.hashCode());
    }
    
    @Test
	public void shouldAllowBidirectionalObjectsWithNoError() {
    	try {
		    polSet.add(polygon);
		    subSet.add(sub1);
		    sub1.setPolygons(polSet);
		    polygon.setSubscribers(subSet);
		    
		    assertTrue(polygon.equals(polygon));
		    assertEquals(polygon.hashCode(), polygon.hashCode());
	    } catch (Exception e) {
    		fail("Bidirectional objects cause hashCode() error: " + e.getMessage());
	    }
    }
    
    @Test
	public void equalsShouldReturnTrueForIdenticalPolygonID() {
    	assertTrue(polygon.equals(polygon.getID()));
    }
    @Test
    public void equalsShouldReturnFalseForNonIdenticalPolygon() {
		assertFalse(polygon.equals(differentPolygon));
    }
    
    @Test
	public void shouldReturnPolygonAsEntityType() {
		assertEquals(Entity.EntityType.POLYGON, polygon.getEntityType());
    }
    
    @Test
	public void shouldReturnArrayOfSubscriberIds() {
		String[] idArray = polygon.getSubscriberStringArray();
	    for (int i = 0; i < 3; i++){
	        assertEquals("id " + numArray[i], idArray[i]);
	    }
    }
    
    @Test
	public void shouldReturnStringOfSubscriberIds() {
		assertEquals("id 1, id 2, id 3", polygon.getSubscriberString());
    }
	
    @Test
    public void shouldReturnStringOfTagIds() {
		assertEquals("NAME 1, NAME 2, NAME 3", polygon.getPolygonTagString());
    }
}