package com.remsdaq.osapplication.entities.polygons;

import com.remsdaq.osapplication.entities.coordinates.LatLng;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class SimplePolygonTest {

    private final LatLng latLng1 = new LatLng(-0.5, -0.5);
    private final LatLng latLng2 = new LatLng(-0.5, 0.5);
    private final LatLng latLng3 = new LatLng(0.5,0.5);
    private final LatLng latLng4 = new LatLng(0.5,-0.5);
    private final LatLng centroid = new LatLng(0,0);
    private final double DELTA = 1E-6;

    @Before
    public void setUp() {

    }

    @Test
    public void shouldReturnCorrectCentroid() {

        // 2 _______ 3
        //  |       |
        //  |   x   |
        //  |_______|
        // 1         4


        SimplePolygon simplePolygon = new SimplePolygon();
        simplePolygon.addVertex(latLng1);
        simplePolygon.addVertex(latLng2);
        simplePolygon.addVertex(latLng3);
        simplePolygon.addVertex(latLng4);

        assertEquals(centroid.getLat(),simplePolygon.centroid().getLat(), DELTA);
        assertEquals(centroid.getLng(),simplePolygon.centroid().getLng(), DELTA);

    }

    @Test
    public void shouldReturnNullCentroid() {
        SimplePolygon simplePolygon = new SimplePolygon();
        assertEquals(null,simplePolygon.centroid());
    }

    @Test
    public void getLatLng() {
        SimplePolygon simplePolygon = new SimplePolygon();
        simplePolygon.addVertex(latLng1);
        simplePolygon.addVertex(latLng2);
        simplePolygon.addVertex(latLng3);
        simplePolygon.addVertex(latLng4);

        assertEquals(latLng1.getLat(),simplePolygon.getLatLng(0).getLat(), DELTA);
        assertEquals(latLng1.getLng(),simplePolygon.getLatLng(0).getLng(), DELTA);
        assertEquals(latLng2.getLat(),simplePolygon.getLatLng(1).getLat(), DELTA);
        assertEquals(latLng2.getLng(),simplePolygon.getLatLng(1).getLng(), DELTA);
        assertEquals(latLng3.getLat(),simplePolygon.getLatLng(2).getLat(), DELTA);
        assertEquals(latLng3.getLng(),simplePolygon.getLatLng(2).getLng(), DELTA);
        assertEquals(latLng4.getLat(),simplePolygon.getLatLng(3).getLat(), DELTA);
        assertEquals(latLng4.getLng(),simplePolygon.getLatLng(3).getLng(), DELTA);

    }

    @Test
    public void latLngs() {
    }

    @Test
    public void addVertex() {
    }

    @Test
    public void containsLatLng() {
    }

    @Test
    public void isIntersectedBy() {
    }
}