package com.remsdaq.osapplication.entities.subscribers;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class SubscriberBuilderTest {
	
	private final String ID = "id";
	private final String FIRST_NAME = "first name";
	private final String LAST_NAME = "last name";
	private final String EMAIL = "email";
	private final int EMAIL_PRIORITY = 1;
	private final String MOBILE = "mobile";
	private final int MOBILE_PRIORITY = 2;
	private final String PAGER = "pager";
	private final int PAGER_PRIORITY = 3;
	
	private final Subscriber emptyExpectedSub = new Subscriber();
	private final Subscriber emptyBuiltSub = new SubscriberBuilder(ID).build();
	
	private final Subscriber expectedSub = new Subscriber();
	private final SubscriberBuilder builder = new SubscriberBuilder(ID);
	private Subscriber builtSub;
	
	@Before
	public void initialSetUp() {
		
		emptyExpectedSub.setID(ID);
		
		expectedSub.setID(ID);
		expectedSub.setFirstName(FIRST_NAME);
		expectedSub.setLastName(LAST_NAME);
		expectedSub.setEmail(EMAIL);
		expectedSub.setEmailPriority(EMAIL_PRIORITY);
		expectedSub.setMobile(MOBILE);
		expectedSub.setMobilePriority(MOBILE_PRIORITY);
		expectedSub.setPager(PAGER);
		expectedSub.setPagerPriority(PAGER_PRIORITY);
		
		builder.firstName(FIRST_NAME)
		       .lastName(LAST_NAME)
		       .email(EMAIL, EMAIL_PRIORITY)
		       .mobile(MOBILE, MOBILE_PRIORITY)
		       .pager(PAGER, PAGER_PRIORITY);
	}
	
	@Test
	public void emptyBuiltShouldEqualEmptyExpectedSubscriber() {
		assertEquals(emptyExpectedSub, emptyBuiltSub);
	}
	
	@Test
	public void builtShouldEqualExceptedSubscriber() {
		builtSub = builder.build();
		assertEquals(expectedSub, builtSub);
		
	}
	
	@Test
	public void builtShouldEqualExceptedSubscriberWithPolygons() {
		Polygon polygon = new Polygon();
		polygon.setID(ID);
		Set<Polygon> polygonSet = new HashSet<>();
		polygonSet.add(polygon);
		
		expectedSub.setPolygons(polygonSet);
		builtSub = builder.setPolygons(polygonSet).build();
		
		assertEquals(expectedSub, builtSub);
	}
	
}