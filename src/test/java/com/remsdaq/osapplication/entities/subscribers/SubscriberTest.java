package com.remsdaq.osapplication.entities.subscribers;

import com.remsdaq.osapplication.entities.Entity;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import com.remsdaq.osapplication.utilities.entities.subscribers.SubscriberTestUtilities;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class SubscriberTest {
	private final String CONFIG_FILE_PATH = "testConfigs/xmlDataConnectionConfig.properties";
	private final int[] numArray = {1, 2, 3};
	private final int[] otherNumArray = {1, 2};
	private Subscriber subscriber;
	private Subscriber identicalSubscriber;
	private Subscriber differentSubscriber;
	private Set<Subscriber> subSet = new HashSet<>();
	private Polygon pol1 = new Polygon();
	private Set<Polygon> polSet = new HashSet<>();
	
	@Before
	public void setUp() {
		subscriber = SubscriberTestUtilities.generateSubscriber(1, numArray);
		identicalSubscriber = SubscriberTestUtilities.generateSubscriber(1, numArray);
		differentSubscriber = SubscriberTestUtilities.generateSubscriber(1, otherNumArray);
	}
	
	@Test
	public void hashCodeOfAPolygonShouldBeConstant() {
		assertEquals(subscriber.hashCode(), subscriber.hashCode());
	}
	
	@Test
	public void hashCodesShouldBeIdentical() {
		assertEquals(subscriber.hashCode(), identicalSubscriber.hashCode());
	}
	
	@Test
	public void shouldAllowBidirectionalObjectsWithNoError() {
		try {
			subSet.add(subscriber);
			polSet.add(pol1);
			pol1.setSubscribers(subSet);
			subscriber.setPolygons(polSet);
			
			assertTrue(subscriber.equals(subscriber));
			assertEquals(subscriber.hashCode(), subscriber.hashCode());
		} catch (Exception e) {
			fail("Bidirectional objects cause hashCode() error: " + e.getMessage());
		}
	}
	
	@Test
	public void equalsShouldReturnTrueForIdenticalPolygonID() {
		assertTrue(subscriber.equals(subscriber.getID()));
	}
	
	@Test
	public void equalsShouldReturnFalseForNonIdenticalPolygon() {
		assertFalse(subscriber.equals(differentSubscriber));
	}
	
	@Test
	public void shouldReturnSubscriberAsEntityType() {
		assertEquals(Entity.EntityType.SUBSCRIBER, subscriber.getEntityType());
	}
	
	@Test
	public void shouldReturnArrayOfSubscriberIds() {
		String[] idArray = subscriber.getPolygonStringArray();
		for (int i = 0; i < 3; i++) {
			assertEquals("id " + numArray[i], idArray[i]);
		}
	}
	
	@Test
	public void shouldReturnStringOfSubscriberIds() {
		assertEquals("id 1, id 2, id 3", subscriber.getPolygonString());
	}
}