package com.remsdaq.osapplication.entities.subscribers.contacts;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmailAddressTest {
	
	private static final int PRIORITY = 1;
	private final String ADDRESS = "email@address.co.uk";
	private final EmailAddress email = new EmailAddress(ADDRESS);
	private final EmailAddress emailWithPriority = new EmailAddress(ADDRESS, PRIORITY);
	
	@Test
	public void newEmailShouldHaveEmailType() {
		assertEquals(EmailAddress.Type.EMAIL,email.getType());
		assertEquals(EmailAddress.Type.EMAIL,emailWithPriority.getType());
	}
	
	@Test
	public void newEmailShouldHaveCorrectAddress() {
		assertEquals(ADDRESS, email.getValue());
	}
	
	@Test
	public void newEmailShouldHaveCorrectAddressAndPriority() {
		assertEquals(ADDRESS, emailWithPriority.getValue());
		assertEquals(PRIORITY,emailWithPriority.getContactPriority());
	}
	
}