package com.remsdaq.osapplication.entities.subscribers.contacts;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PagerNumberTest {
	
	private static final int PRIORITY = 1;
	private final String NUMBER = "09876543210";
	private final PagerNumber pager = new PagerNumber(NUMBER);
	private final PagerNumber pagerWithPriority = new PagerNumber(NUMBER, PRIORITY);
	
	@Test
	public void newPagerShouldHavePagerType() {
		assertEquals(EmailAddress.Type.PAGER, pager.getType());
		assertEquals(EmailAddress.Type.PAGER, pagerWithPriority.getType());
	}
	
	@Test
	public void newPagerShouldHaveCorrectNumber() {
		assertEquals(NUMBER, pager.getValue());
	}
	
	@Test
	public void newPagerShouldHaveCorrectNumberAndPriority() {
		assertEquals(NUMBER, pagerWithPriority.getValue());
		assertEquals(PRIORITY, pagerWithPriority.getContactPriority());
	}
	
}