package com.remsdaq.osapplication.entities.subscribers.contacts;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PhoneNumberTest {
	
	private static final int PRIORITY = 1;
	private final String NUMBER = "01234567890";
	private final PhoneNumber phone = new PhoneNumber(NUMBER);
	private final PhoneNumber phoneWithPriority = new PhoneNumber(NUMBER, PRIORITY);
	
	@Test
	public void newPhoneNumberShouldHavePhoneType() {
		assertEquals(EmailAddress.Type.PHONE, phone.getType());
		assertEquals(EmailAddress.Type.PHONE,phoneWithPriority.getType());
	}
	
	@Test
	public void newPhoneNumberShouldHaveCorrectNumber() {
		assertEquals(NUMBER, phone.getValue());
	}
	
	@Test
	public void newPhoneNumberShouldHaveCorrectNumberAndPriority() {
		assertEquals(NUMBER, phoneWithPriority.getValue());
		assertEquals(PRIORITY,phoneWithPriority.getContactPriority());
	}
	
}