package com.remsdaq.osapplication.gui.controllers;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.testfx.framework.junit.ApplicationTest;

public class MainControllerTest extends ApplicationTest{

    @Override public void start(Stage stage) {
        Parent sceneRoot = new MainController().outerVBox;
        Scene scene = new Scene(sceneRoot, 100, 100);
        stage.setScene(scene);
        stage.show();
    }

}