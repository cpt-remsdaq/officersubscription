package com.remsdaq.osapplication.rules;

import com.remsdaq.osapplication.entities.coordinates.LatLng;
import com.remsdaq.osapplication.entities.incidents.Incident;
import com.remsdaq.osapplication.entities.incidents.IncidentType;
import com.remsdaq.osapplication.entities.incidents.Priority;
import com.remsdaq.osapplication.entities.resources.Person;
import com.remsdaq.osapplication.entities.resources.Resource;
import com.remsdaq.osapplication.entities.resources.Status;
import com.remsdaq.osapplication.entities.resources.Vehicle;
import com.remsdaq.osapplication.utilities.RuleUtilities;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

import static com.remsdaq.osapplication.entities.resources.Status.StatusClass.INC_RELATED;
import static com.remsdaq.osapplication.entities.resources.Status.StatusClass.NOT_AVAILABLE;
import static com.remsdaq.osapplication.rules.ConditionalFunctionInstance.*;
import static junit.framework.TestCase.assertTrue;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class ConditionalFunctionInstanceTest {

    private final Incident emptyIncident = new Incident();

    private final long LONG_ID = 123L;
    private final long OTHER_LONG_ID = 321L;
    private final long WONG_ID = 1L;
    private final String DESCRIPTION = "some description goes here!";
    private final String IN_DESCRIPTION_1 = "goes";
    private final String IN_DESCRIPTION_2 = "description";
    private final String NOT_IN_DESCRIPTION = "apples";
    private final long BOAT_FIRE_ID = 35L;
    private final long MANIACLE_FARM_ANIMAL_ID = 25L;
    private final LocalDate DATE = LocalDate.of(2018, 8, 22);
    private final LocalDate WRONG_DATE = LocalDate.of(2018, 8, 28);
    private final LocalTime TIME = LocalTime.of(9, 50, 50);
    private final LocalTime WRONG_TIME = LocalTime.of(16, 20, 41);
    private final long PRIORITY = 1L;
    private final Integer NUMBER_OF_CALLS
            = 3;
    private IncidentType boatFire = new IncidentType();
    private IncidentType maniacleFarmAnimal = new IncidentType();
    private Resource person = new Person(LONG_ID);
    private Resource vehicle = new Vehicle(OTHER_LONG_ID);
    private Incident incident = new Incident();

    @Before
    public void setUp() throws Exception {
        boatFire.setLevel1Description("Fire");
        boatFire.setLevel2Description("Boat");
        boatFire.setDescription("Fires involving recreational water borne craft. Barges, yachts, fishing boats.");
        boatFire.setLevel1DescriptionCode("F");
        boatFire.setId(BOAT_FIRE_ID);
        boatFire.setPriority(new Priority(3L));

        maniacleFarmAnimal.setLevel1Description("False alarm due to Apparatus");
        maniacleFarmAnimal.setLevel2Description("Animal");
        maniacleFarmAnimal.setDescription("Other");
        maniacleFarmAnimal.setId(MANIACLE_FARM_ANIMAL_ID);

        person.setFirstName("first name");
        person.setLastName("last name");
        person.setCallSign("person call sign");
        person.setIncident(incident);
        person.setStatus(new Status(INC_RELATED));
        person.setPagerNumber("pager number");
        person.setLocation("10 Downing Street, London");
        person.setLatLng(new LatLng(51.5034, 0.1276));

        vehicle.setCallSign("vehicle call sign");
        vehicle.setPagerNumber("01234567890");
        vehicle.setLocation("10 Downing Street, London");
        vehicle.setLatLng(new LatLng(51.5034, 0.1276));
        vehicle.setLocked(false);
        vehicle.setStatus(new Status(NOT_AVAILABLE));
        vehicle.setIncident(incident);

        incident.setId(LONG_ID);
        incident.setBatchId(LONG_ID);
        incident.setDescription(DESCRIPTION);
        incident.setTypes(Arrays.asList(new IncidentType[]{boatFire, maniacleFarmAnimal}));
        incident.setDateTime(DATE.atTime(TIME));
        incident.setStopTime(DATE.atTime(TIME));
        incident.setPriority(PRIORITY);
        incident.setNumberOfCalls(NUMBER_OF_CALLS);
        incident.setResources(Arrays.asList(new Resource[]{person, vehicle}));
    }

    @Test
    public void incidentNumberOfCallsBetweenTest() {
        Integer emptyNumberOfCalls = _INCIDENT_NUMBER_OF_CALLS_BETWEEN.apply(emptyIncident, new Integer[]{NUMBER_OF_CALLS - 1, NUMBER_OF_CALLS + 1});
        Integer bothGreater = _INCIDENT_NUMBER_OF_CALLS_BETWEEN.apply(incident, new Integer[]{NUMBER_OF_CALLS + 1, NUMBER_OF_CALLS + 2});
        Integer bothSmaller = _INCIDENT_NUMBER_OF_CALLS_BETWEEN.apply(incident, new Integer[]{NUMBER_OF_CALLS - 2, NUMBER_OF_CALLS - 1});
        Integer wrongOrder = _INCIDENT_NUMBER_OF_CALLS_BETWEEN.apply(incident, new Integer[]{NUMBER_OF_CALLS + 1, NUMBER_OF_CALLS - 1});
        Integer withinRange = _INCIDENT_NUMBER_OF_CALLS_BETWEEN.apply(incident, new Integer[]{NUMBER_OF_CALLS - 1, NUMBER_OF_CALLS + 1});

        assertNull(emptyNumberOfCalls);
        assertNull(bothGreater);
        assertNull(bothSmaller);
        assertEquals(NUMBER_OF_CALLS,wrongOrder);
        assertEquals(NUMBER_OF_CALLS,withinRange);
    }

    @Test
    public void incidentNumberOfCallsEqualsTest() {
        Integer emptyCalls = _INCIDENT_NUMBER_OF_CALLS_EQUALS.apply(emptyIncident, NUMBER_OF_CALLS);
        Integer notEqual = _INCIDENT_NUMBER_OF_CALLS_EQUALS.apply(incident, NUMBER_OF_CALLS + 1);
        Integer equal = _INCIDENT_NUMBER_OF_CALLS_EQUALS.apply(incident, NUMBER_OF_CALLS);

        assertNull(emptyCalls);
        assertNull(notEqual);
        assertEquals(NUMBER_OF_CALLS, equal);
    }

    @Test
    public void incidentPrioritySmallerThanTest() {
        Long emptyPriority = _INCIDENT_PRIORITY_SMALLER_THAN.apply(emptyIncident, PRIORITY);
        Long greaterThan = _INCIDENT_PRIORITY_SMALLER_THAN.apply(incident, PRIORITY - 1);
        Long equalTo = _INCIDENT_PRIORITY_SMALLER_THAN.apply(incident, PRIORITY);
        Long smallerThan = _INCIDENT_PRIORITY_SMALLER_THAN.apply(incident, PRIORITY + 1);

        assertNull(emptyPriority);
        assertNull(greaterThan);
        assertNull(equalTo);
        assertEquals(PRIORITY, (long) smallerThan);
    }

    @Test
    public void incidentPriorityGreaterThanTest() {
        Long emptyPriority = _INCIDENT_PRIORITY_GREATER_THAN.apply(emptyIncident, PRIORITY);
        Long lowerThan = _INCIDENT_PRIORITY_GREATER_THAN.apply(incident, PRIORITY + 1);
        Long equalTo = _INCIDENT_PRIORITY_GREATER_THAN.apply(incident, PRIORITY);
        Long greaterThan = _INCIDENT_PRIORITY_GREATER_THAN.apply(incident, PRIORITY - 1);

        assertNull(emptyPriority);
        assertNull(lowerThan);
        assertNull(equalTo);
        assertEquals(PRIORITY, (long) greaterThan);
    }

    @Test
    public void incidentPriorityBetweenTest() {
        Long emptyPriority = _INCIDENT_PRIORITY_BETWEEN.apply(emptyIncident, new Long[]{PRIORITY + -1, PRIORITY + 1});
        Long higherThanBoth = _INCIDENT_PRIORITY_BETWEEN.apply(incident, new Long[]{PRIORITY - 2, PRIORITY - 1});
        Long lowerThanBoth = _INCIDENT_PRIORITY_BETWEEN.apply(incident, new Long[]{PRIORITY + 1, PRIORITY + 2});
        Long wrongWayRound = _INCIDENT_PRIORITY_BETWEEN.apply(incident, new Long[]{PRIORITY + 1, PRIORITY - 1});
        Long insideRange = _INCIDENT_PRIORITY_BETWEEN.apply(incident, new Long[]{PRIORITY - 1, PRIORITY + 1});

        assertNull(emptyPriority);
        assertNull(higherThanBoth);
        assertNull(lowerThanBoth);
        assertEquals(PRIORITY, (long) wrongWayRound);
        assertEquals(PRIORITY, (long) insideRange);
    }

    @Test
    public void incidentPriorityEqualsTest() {
        Long emptyPriority = _INCIDENT_PRIORITY_EQUALS.apply(emptyIncident, PRIORITY);
        Long wrongPriority = _INCIDENT_PRIORITY_EQUALS.apply(incident, PRIORITY + 1);
        Long rightPriority = _INCIDENT_PRIORITY_EQUALS.apply(incident, PRIORITY);

        assertNull(emptyPriority);
        assertNull(wrongPriority);
        assertEquals(PRIORITY, (long) rightPriority);
    }

    @Test
    public void allResourcesFromListContainedTest() {
        List<Resource> empty = _INCIDENT_CONTAINS_ALL_RESOURCES_WITH_IDS.apply(emptyIncident, new Long[]{person.getId(), vehicle.getId()});
        List<Resource> oneWrongResource = _INCIDENT_CONTAINS_ALL_RESOURCES_WITH_IDS.apply(incident, new Long[]{person.getId(), WONG_ID});
        List<Resource> bothRightResources = _INCIDENT_CONTAINS_ALL_RESOURCES_WITH_IDS.apply(incident, new Long[]{person.getId(), vehicle.getId()});

        assertNull(empty);
        assertNull(oneWrongResource);
        assertThat(Arrays.asList(person, vehicle), containsInAnyOrder(bothRightResources.toArray()));
    }

    @Test
    public void anyResourceFromListContainedTest() {
        List<Resource> empty = _INCIDENT_CONTAINS_ANY_RESOURCE_WITH_ID.apply(emptyIncident, new Long[]{person.getId(), vehicle.getId()});
        List<Resource> noRightResources = _INCIDENT_CONTAINS_ANY_RESOURCE_WITH_ID.apply(incident, new Long[]{WONG_ID});
        List<Resource> oneRightResource = _INCIDENT_CONTAINS_ANY_RESOURCE_WITH_ID.apply(incident, new Long[]{person.getId(), WONG_ID});
        List<Resource> twoRightResources = _INCIDENT_CONTAINS_ANY_RESOURCE_WITH_ID.apply(incident, new Long[]{person.getId(), vehicle.getId()});

        assertNull(empty);
        assertNull(noRightResources);
        assertEquals(Arrays.asList(new Resource[]{person}), oneRightResource);
        assertEquals(Arrays.asList(new Resource[]{person, vehicle}), twoRightResources);
    }

    @Test
    public void incidentContainsResourceWithIdTest() {
        Resource empty = _INCIDENT_CONTAINS_RESOURCE_WITH_ID.apply(emptyIncident, person.getId());
        Resource wrongId = _INCIDENT_CONTAINS_RESOURCE_WITH_ID.apply(incident, WONG_ID);
        Resource rightId = _INCIDENT_CONTAINS_RESOURCE_WITH_ID.apply(incident, person.getId());

        assertNull(empty);
        assertNull(wrongId);
        assertEquals(person, rightId);
    }

    @Test
    public void noResourcesAtIncidentTest() {
        boolean incidentWithNoResources = _NO_RESOURCES_AT_INCIDENT.apply(emptyIncident, null);
        boolean incidentWithResources = _NO_RESOURCES_AT_INCIDENT.apply(incident, null);

        assertTrue(incidentWithNoResources);
        assertFalse(incidentWithResources);
    }

    @Test
    public void stopTimeBetweenTest() {
        LocalTime emptyTime = _STOP_TIME_BETWEEN.apply(emptyIncident, new LocalTime[]{TIME.minusHours(1), TIME.plusHours(1)});
        LocalTime bothBefore = _STOP_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.minusHours(2), TIME.minusHours(1)});
        LocalTime bothAfter = _STOP_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.plusHours(1), TIME.plusHours(2)});
        LocalTime wrongWayRound = _STOP_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.plusHours(1), TIME.minusHours(1)});
        LocalTime justRight = _STOP_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.minusHours(1), TIME.plusHours(1)});

        assertNull(emptyTime);
        assertNull(bothBefore);
        assertNull(bothAfter);
        assertThat(wrongWayRound, is(TIME));
        assertThat(justRight, is(TIME));
    }

    @Test
    public void stopTimeEqualsTest() {
        LocalTime emptyTime = _STOP_TIME_EQUALS.apply(emptyIncident, TIME);
        LocalTime wrongTime = _STOP_TIME_EQUALS.apply(incident, WRONG_TIME);
        LocalTime rightTime = _STOP_TIME_EQUALS.apply(incident, TIME);

        assertNull(emptyTime);
        assertNull(wrongTime);
        assertThat(rightTime, is(TIME));
    }

    @Test
    public void incidentTimeAfterTest() {
        LocalTime emptyTime = _INCIDENT_TIME_AFTER.apply(emptyIncident, TIME.minusHours(1));
        LocalTime timeBefore = _INCIDENT_TIME_AFTER.apply(incident, TIME.plusHours(1));
        LocalTime timeEqual = _INCIDENT_TIME_AFTER.apply(incident, TIME);
        LocalTime timeAfter = _INCIDENT_TIME_AFTER.apply(incident, TIME.minusHours(1));

        assertNull(emptyTime);
        assertNull(timeBefore);
        assertNull(timeEqual);
        assertEquals(TIME, timeAfter);
    }

    @Test
    public void incidentTimBeforeTest() {
        LocalTime emptyTime = _INCIDENT_TIME_BEFORE.apply(emptyIncident, TIME.plusHours(1));
        LocalTime timeAfter = _INCIDENT_TIME_BEFORE.apply(incident, TIME.minusHours(1));
        LocalTime timeEqual = _INCIDENT_TIME_BEFORE.apply(incident, TIME);
        LocalTime timeBefore = _INCIDENT_TIME_BEFORE.apply(incident, TIME.plusHours(1));

        assertNull(emptyTime);
        assertNull(timeAfter);
        assertNull(timeEqual);
        assertEquals(TIME, timeBefore);
    }

    @Test
    public void incidentTimeBetweenTest() {
        LocalTime emptyTime = _INCIDENT_TIME_BETWEEN.apply(emptyIncident, new LocalTime[]{TIME.minusHours(1), TIME.plusHours(1)});
        LocalTime bothTimesBefore = _INCIDENT_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.plusHours(1), TIME.plusHours(2)});
        LocalTime bothTimesAfter = _INCIDENT_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.minusHours(2), TIME.minusHours(1)});
        LocalTime wrongWayAround = _INCIDENT_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.plusHours(1), TIME.minusHours(2)});
        LocalTime justRight = _INCIDENT_TIME_BETWEEN.apply(incident, new LocalTime[]{TIME.minusHours(1), TIME.plusHours(1)});

        assertNull(emptyTime);
        assertNull(bothTimesBefore);
        assertNull(bothTimesAfter);
        assertEquals(TIME, wrongWayAround);
        assertEquals(TIME, justRight);
    }

    @Test
    public void incidentTimeEqualsTest() {
        LocalTime emptyTime = _INCIDENT_TIME_EQUALS.apply(emptyIncident, TIME);
        LocalTime wrongTime = _INCIDENT_TIME_EQUALS.apply(incident, TIME.plusHours(1));
        LocalTime rightTime = _INCIDENT_TIME_EQUALS.apply(incident, TIME);

        assertNull(emptyTime);
        assertNull(wrongTime);
        assertEquals(TIME, rightTime);
    }

    @Test
    public void incidentDateAfterTest() {
        LocalDate emptyDate = _INCIDENT_DATE_AFTER.apply(emptyIncident, DATE);
        LocalDate dateBefore = _INCIDENT_DATE_AFTER.apply(incident, DATE.plusDays(1));
        LocalDate dateMatching = _INCIDENT_DATE_AFTER.apply(incident, DATE);
        LocalDate dateAfter = _INCIDENT_DATE_AFTER.apply(incident, DATE.minusDays(1));

        assertNull(emptyDate);
        assertNull(dateBefore);
        assertNull(dateMatching);
        assertThat(dateAfter, is(DATE));
    }

    @Test
    public void incidentDateBeforeTest() {
        LocalDate emptyDate = _INCIDENT_DATE_BEFORE.apply(emptyIncident, DATE);
        LocalDate dateAfter = _INCIDENT_DATE_BEFORE.apply(incident, DATE.minusDays(1));
        LocalDate dateMatching = _INCIDENT_DATE_BEFORE.apply(incident, DATE);
        LocalDate dateBefore = _INCIDENT_DATE_BEFORE.apply(incident, DATE.plusDays(1));

        assertNull(emptyDate);
        assertNull(dateAfter);
        assertNull(dateMatching);
        assertThat(dateBefore, is(DATE));
    }

    @Test
    public void incidentDateBetweenTest() {
        LocalDate emptyDate = _INCIDENT_DATE_BETWEEN.apply(emptyIncident, new LocalDate[]{DATE.minusDays(1), DATE.plusDays(1)});
        LocalDate bothDatesAfter = _INCIDENT_DATE_BETWEEN.apply(incident, new LocalDate[]{DATE.plusDays(1), DATE.plusDays(3)});
        LocalDate bothDatesBefore = _INCIDENT_DATE_BETWEEN.apply(incident, new LocalDate[]{DATE.minusDays(3), DATE.minusDays(1)});
        LocalDate datesBackwards = _INCIDENT_DATE_BETWEEN.apply(incident, new LocalDate[]{DATE.plusDays(1), DATE.minusDays(1)});
        LocalDate datesCorrect = _INCIDENT_DATE_BETWEEN.apply(incident, new LocalDate[]{DATE.minusDays(1), DATE.plusDays(1)});

        assertNull(emptyDate);
        assertNull(bothDatesAfter);
        assertNull(bothDatesBefore);
        assertTrue(datesBackwards.equals(datesCorrect));
        assertTrue(DATE.equals(datesCorrect));


    }

    @Test
    public void incidentDateEqualsTest() {
        LocalDate emptyDate = _INCIDENT_DATE_EQUALS.apply(emptyIncident, DATE);
        LocalDate wrongDate = _INCIDENT_DATE_EQUALS.apply(incident, WRONG_DATE);
        LocalDate rightDate = _INCIDENT_DATE_EQUALS.apply(incident, DATE);

        assertNull(emptyDate);
        assertNull(wrongDate);
        assertEquals(DATE, rightDate);
    }

    @Test
    public void incidentContainsAnyTypeWithIdTest() {
        List<IncidentType> noTypes = _INCIDENT_CONTAINS_ANY_TYPE_WITH_ID.apply(emptyIncident, new Long[]{BOAT_FIRE_ID, MANIACLE_FARM_ANIMAL_ID});
        List<IncidentType> allIdsWrong = _INCIDENT_CONTAINS_ANY_TYPE_WITH_ID.apply(incident, new Long[]{WONG_ID, WONG_ID});
        List<IncidentType> oneIdRight = _INCIDENT_CONTAINS_ANY_TYPE_WITH_ID.apply(incident, new Long[]{BOAT_FIRE_ID, WONG_ID});

        assertNull(noTypes);
        assertNull(allIdsWrong);
        assertEquals(Arrays.asList(boatFire), oneIdRight);
    }

    @Test
    public void incidentContainsAllTypesWithIdTest() {
        List<IncidentType> noTypes = _INCIDENT_CONTAINS_ALL_TYPES_WITH_ID.apply(emptyIncident, new Long[]{BOAT_FIRE_ID, MANIACLE_FARM_ANIMAL_ID});
        List<IncidentType> oneIdWrong = _INCIDENT_CONTAINS_ALL_TYPES_WITH_ID.apply(incident, new Long[]{MANIACLE_FARM_ANIMAL_ID, WONG_ID});
        List<IncidentType> bothIdsRight = _INCIDENT_CONTAINS_ALL_TYPES_WITH_ID.apply(incident, new Long[]{MANIACLE_FARM_ANIMAL_ID, BOAT_FIRE_ID});

        assertNull(noTypes);
        assertNull(oneIdWrong);
        assertThat(Arrays.asList(new IncidentType[]{boatFire, maniacleFarmAnimal}), containsInAnyOrder(bothIdsRight.toArray()));
    }

    @Test
    public void incidentContainsTypeWithIdTest() {
        IncidentType noTypes = _INCIDENT_CONTAINS_TYPE_WITH_ID.apply(emptyIncident, MANIACLE_FARM_ANIMAL_ID);
        IncidentType wrongType = _INCIDENT_CONTAINS_TYPE_WITH_ID.apply(incident, WONG_ID);
        IncidentType rightType = _INCIDENT_CONTAINS_TYPE_WITH_ID.apply(incident, MANIACLE_FARM_ANIMAL_ID);

        assertNull(noTypes);
        assertNull(wrongType);
        assertEquals(maniacleFarmAnimal, rightType);
    }

    @Test
    public void incidentDescriptionChangedFromTest() {
        String change = " some change";

        String emptyDescription = _INCIDENT_DESCRIPTION_CHANGED_FROM.apply(emptyIncident, DESCRIPTION);
        String noChange = _INCIDENT_DESCRIPTION_CHANGED_FROM.apply(incident, DESCRIPTION);
        String withChange = _INCIDENT_DESCRIPTION_CHANGED_FROM.apply(incident, DESCRIPTION + change);

        assertNull(emptyDescription);
        assertNull(noChange);
        assertEquals(DESCRIPTION, withChange);
    }

    @Test
    public void incidentDescriptionContainsTest() {
        List<String> matches = Arrays.asList(new String[]{IN_DESCRIPTION_1, IN_DESCRIPTION_2});

        List<String> emptyDescription = _INCIDENT_DESCRIPTION_CONTAINS.apply(emptyIncident, new String[]{IN_DESCRIPTION_1, IN_DESCRIPTION_2, NOT_IN_DESCRIPTION});
        List<String> noMatches = _INCIDENT_DESCRIPTION_CONTAINS.apply(incident, new String[]{NOT_IN_DESCRIPTION});
        List<String> withMatches = _INCIDENT_DESCRIPTION_CONTAINS.apply(incident, new String[]{IN_DESCRIPTION_1, IN_DESCRIPTION_2, NOT_IN_DESCRIPTION});

        assertNull(emptyDescription);
        assertNull(noMatches);
        assertEquals(matches, withMatches);
    }

    @Test
    public void incidentBatchIdEqualsTest() {
        Long emptyId = _INCIDENT_BATCH_ID_EQUALS.apply(emptyIncident, LONG_ID);
        Long wrongId = _INCIDENT_BATCH_ID_EQUALS.apply(incident, WONG_ID);
        Long rightId = _INCIDENT_BATCH_ID_EQUALS.apply(incident, LONG_ID);

        assertNull(emptyId);
        ;
        assertNull(wrongId);
        assertEquals(LONG_ID, (long) rightId);
    }

    @Test
    public void incidentIdEqualsTest() {
        Long emptyId = _INCIDENT_ID_EQUALS.apply(emptyIncident, LONG_ID);
        Long wrongId = _INCIDENT_ID_EQUALS.apply(incident, WONG_ID);
        Long rightId = _INCIDENT_ID_EQUALS.apply(incident, LONG_ID);

        assertNull(emptyId);
        assertNull(wrongId);
        assertEquals(LONG_ID, (long) rightId);
    }

    @Test
    public void isBetweenTest() {
        assertFalse(RuleUtilities.isBetween(1,new Integer[] {2,3}));
        assertTrue(RuleUtilities.isBetween(2,new Integer[] {1,3}));
        assertTrue(RuleUtilities.isBetween(2,new Integer[] {3,1}));
        assertTrue(RuleUtilities.isBetween(2.1, new Double[] {3.1,1.1}));
        assertTrue(RuleUtilities.isBetween(DATE, new LocalDate[]{DATE.plusDays(1), DATE.minusDays(1)}));
        assertTrue(RuleUtilities.isBetween(TIME, new LocalTime[]{TIME.plusHours(1), TIME.minusHours(1)}));
    }
}