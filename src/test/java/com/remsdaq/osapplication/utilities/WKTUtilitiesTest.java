package com.remsdaq.osapplication.utilities;

import com.remsdaq.osapplication.entities.polygons.PolygonGeometry;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class WKTUtilitiesTest {

    private static final String SIMPLE_SINGLE_POLYGON = "POLYGON ((0.0 0.0, 1.0 0.0, 0.0 1.0, 1.0 1.0))";
    private static final String NON_EQUIVALENT_SIMPLE_SINGLE_POLYGON = "POLYGON ((0.0 0.1, 1.0 0.0, 0.0 1.0, 1.0 1.0))";
    
    private static final String COMPLEX_SINGLE_POLYGON = "POLYGON ((0.0 0.0, 1.0 0.1, 0.1 1.0, 1.0 1.0),(0.6 0.6, 0.4 0.6, 0.6 0.4, 4.0 4.0))";
    private static final String SIMPLE_MULTIPLE_POLYGON =
            "MULTIPOLYGON (((0.0 0.0, 1.0 0.0, 0.0 1.0, 1.0 1.0)),((0.6 0.6, 0.4 0.6, 0.6 0.4, 4.0 4.0)))";
    private static final String COMPLEX_MULTIPLE_POLYGON =
            "MULTIPOLYGON (((0.0 0.0, 1.0 0.1, 0.1 1.0, 1.0 1.0)," +
            "(0.6 0.6, 0.4 0.6, 0.6 0.4, 4.0 4.0))," +
            "((0.6 0.6, 0.4 0.6, 0.6 0.4, 4.0 4.0)))";

    private static final String SIMPLE_SINGLE_POLYGON_WITH_EXTRA_SPACES = " POLYGON ( (  0.0  0.0 , 1.0  0.0 , 0.0  1.0 , 1.0  1.0 ) ) ";
    private static final String SIMPLE_SINGLE_POLYGON_WITH_FEWER_SPACES = "POLYGON((0.0 0.0,1.0 0.0,0.0 1.0,1.0 1.0))";
    private static final String COMPLEX_MULTIPLE_POLYGON_WITH_EXTRA_SPACES = " MULTIPOLYGON ( ( ( 0.0  0.0 , 1.0  0.1 , 0.1  1.0 , 1.0  1.0 ) ," +
            " ( 0.6  0.6 , 0.4  0.6 , 0.6  0.4 , 4.0  4.0 ) ) ," +
            " ( ( 0.6  0.6 , 0.4  0.6 , 0.6  0.4 , 4.0  4.0 ) ) ) ";
    private static final String COMPLEX_MULTIPLE_POLYGON_WITH_FEWER_SPACES = "MULTIPOLYGON(((0.0 0.0,1.0 0.1,0.1 1.0,1.0 1.0)," +
            "(0.6 0.6,0.4 0.6,0.6 0.4,4.0 4.0))," +
            "((0.6 0.6,0.4 0.6,0.6 0.4,4.0 4.0)))";

    private final String NON_WKT_STRING = "This is not a WKT string";
    private final String EMPTY_STRING = "";
    private final String WKT_STRING_WITH_MISMATCHED_BRACKETS_1 = "POLYGON ((0.0 0.0, 1.0 0.0, 0.0 1.0, 1.0 1.0)";
    private final String WKT_STRING_WITH_MISMATCHED_BRACKETS_2 = "POLYGON ((0.0 0.0, 1.0 0.0, 0.0 1.0, 1.0 1.0)))";
    private final String WKT_STRING_WITH_MISSING_COMMA = "POLYGON ((0.0 0.0 1.0 0.0, 0.0 1.0, 1.0 1.0))";

    private PolygonGeometry SIMPLE_SINGLE_GEOMETRY = WKTUtilities.decode(SIMPLE_SINGLE_POLYGON);
    private PolygonGeometry COMPLEX_SINGLE_GEOMETRY = WKTUtilities.decode(COMPLEX_SINGLE_POLYGON);
    private PolygonGeometry SIMPLE_MULTIPLE_GEOMETRY = WKTUtilities.decode(SIMPLE_MULTIPLE_POLYGON);
    private PolygonGeometry COMPLEX_MULTIPLE_GEOMETRY = WKTUtilities.decode(COMPLEX_MULTIPLE_POLYGON);

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    public WKTUtilitiesTest() throws WellKnownTextException {
    }

    @Before
    public void setUp() {

    }

    @Test
    public void shouldEncodeSimpleSinglePolygon() throws WellKnownTextException {
        PolygonGeometry pg = WKTUtilities.decode(SIMPLE_SINGLE_POLYGON);
        assertEquals("Failed to properly encode the simple single polygon", SIMPLE_SINGLE_POLYGON, WKTUtilities.encode(pg));
    }

    @Test
    public void shouldEncodeSimpleMultiplePolygon() throws WellKnownTextException {
        PolygonGeometry pg = WKTUtilities.decode(SIMPLE_MULTIPLE_POLYGON);
        assertEquals("Failed to properly encode the simple multiple polygon", SIMPLE_MULTIPLE_POLYGON, WKTUtilities.encode(pg));
    }

    @Test
    public void shouldEncodeComplexSinglePolygon() throws WellKnownTextException {
        PolygonGeometry pg = WKTUtilities.decode(COMPLEX_SINGLE_POLYGON);
        assertEquals("Failed to properly encode the complex single polygon", COMPLEX_SINGLE_POLYGON, WKTUtilities.encode(pg));
    }

    @Test
    public void shouldEncodeComplexMultiplePolygon() throws WellKnownTextException {
        PolygonGeometry pg = WKTUtilities.decode(COMPLEX_MULTIPLE_POLYGON);
        assertEquals("Failed to properly encode the complex multiple polygon", COMPLEX_MULTIPLE_POLYGON, WKTUtilities.encode(pg));
    }

    @Test
    public void shouldThrowExceptionWhenDecodingNonWKTString() throws WellKnownTextException {

        exception.expect(WellKnownTextException.class);
        WKTUtilities.decode(NON_WKT_STRING);
    }

    @Test
    public void shouldEvaluateToWKT() {
        assertTrue("Simple single polygon WKT did not evaluate to be a WKT String",WKTUtilities.isWKT(SIMPLE_SINGLE_POLYGON));
        assertTrue("Simple multiple polygon WKT did not evaluate to be a WKT String",WKTUtilities.isWKT(SIMPLE_MULTIPLE_POLYGON));
        assertTrue("Complex single polygon WKT did not evaluate to be a WKT String",WKTUtilities.isWKT(COMPLEX_SINGLE_POLYGON));
        assertTrue("Complex multiple polygon WKT did not evaluate to be a WKT String",WKTUtilities.isWKT(COMPLEX_MULTIPLE_POLYGON));

        assertTrue("Simple single polygon WKT with extra spaces did not evaluate to be a WKT String",WKTUtilities.isWKT(SIMPLE_SINGLE_POLYGON_WITH_EXTRA_SPACES));
        assertTrue("Simple single polygon WKT with fewer spaces did not evaluate to be a WKT String",WKTUtilities.isWKT(SIMPLE_SINGLE_POLYGON_WITH_FEWER_SPACES));
        assertTrue("Complex multiple polygon WKT with extra spaces did not evaluate to be a WKT String",WKTUtilities.isWKT(COMPLEX_MULTIPLE_POLYGON_WITH_EXTRA_SPACES));
        assertTrue("Complex multiple polygon WKT  with fewer spacesdid not evaluate to be a WKT String",WKTUtilities.isWKT(COMPLEX_MULTIPLE_POLYGON_WITH_FEWER_SPACES));
    }

    @Test
    public void shouldNotEvaluateToWKT() {
        assertFalse("Non WKT string evaluated to be WKT", WKTUtilities.isWKT(NON_WKT_STRING));
        assertFalse("Empty string evaluated to be WKT", WKTUtilities.isWKT(EMPTY_STRING));
        assertFalse("String with mismatched brackets evaluated to be WKT", WKTUtilities.isWKT(WKT_STRING_WITH_MISMATCHED_BRACKETS_1));
        assertFalse("String with mismatched brackets evaluated to be WKT", WKTUtilities.isWKT(WKT_STRING_WITH_MISMATCHED_BRACKETS_2));
//        assertFalse("String with missing comma evaluated to be WKT", WKTUtilities.isWKT(WKT_STRING_WITH_MISSING_COMMA));
    }

    @Test
    public void shouldReturnBracketsAreBalanced() {
        String balancedBracketsString = "())()(())(()())())()(()()(()())(";
        assertTrue(WKTUtilities.areBracketsBalanced(balancedBracketsString));
        assertTrue(WKTUtilities.areBracketsBalanced(EMPTY_STRING));
    }

    @Test
    public void shouldReturnBracketsAreNotBalanced() {
        String unbalancedBracketsString = "())()(())(()())((()())()(()())(";
        assertFalse(WKTUtilities.areBracketsBalanced(unbalancedBracketsString));
    }

    @Test
    public void shouldReturnIsSingle() {
        assertTrue(WKTUtilities.isSingle(SIMPLE_SINGLE_POLYGON));
        assertTrue(WKTUtilities.isSingle(COMPLEX_SINGLE_POLYGON));
    }

    @Test
    public void shouldReturnIsNotSingle() {
        assertFalse(WKTUtilities.isSingle(SIMPLE_MULTIPLE_POLYGON));
        assertFalse(WKTUtilities.isSingle(COMPLEX_MULTIPLE_POLYGON));
    }

    @Test
    public void shouldReturnIsSimple() {
        assertTrue(WKTUtilities.isSimple(SIMPLE_SINGLE_POLYGON));
        assertTrue(WKTUtilities.isSimple(SIMPLE_MULTIPLE_POLYGON));
    }

    @Test
    public void shouldReturnIsNotSimple() {
        assertFalse(WKTUtilities.isSimple(COMPLEX_SINGLE_POLYGON));
        assertFalse(WKTUtilities.isSimple(COMPLEX_MULTIPLE_POLYGON));
    }

    @Test
    public void shouldEvaluateToSimpleSinglePolygon() {
        assertEquals("WKT String did not evaluate as simple and single", WKTUtilities.GeometryType.SINGLE_SIMPLE, WKTUtilities.evaluate(SIMPLE_SINGLE_POLYGON));
        assertEquals("Geometry did not evaluate as simple and single",WKTUtilities.GeometryType.SINGLE_SIMPLE,WKTUtilities.evaluate(SIMPLE_SINGLE_GEOMETRY));
    }

    @Test
    public void shouldEvaluateToComplexSinglePolygon() {
        assertEquals("WKT String did not evaluate as complex and single",WKTUtilities.GeometryType.SINGLE_COMPLEX,WKTUtilities.evaluate(COMPLEX_SINGLE_POLYGON));
        assertEquals("Geometry did not evaluate as complex and single",WKTUtilities.GeometryType.SINGLE_COMPLEX,WKTUtilities.evaluate(COMPLEX_SINGLE_GEOMETRY));
    }

    @Test
    public void shouldEvaluateToSimpleMultiplePolygon() {
        assertEquals("WKT String did not evaluate as simple and multiple",WKTUtilities.GeometryType.MULTIPLE_SIMPLE,WKTUtilities.evaluate(SIMPLE_MULTIPLE_POLYGON));
        assertEquals("Geometry did not evaluate as simple and multiple",WKTUtilities.GeometryType.MULTIPLE_SIMPLE,WKTUtilities.evaluate(SIMPLE_MULTIPLE_GEOMETRY));
    }

    @Test
    public void shouldEvaluateToComplexMultiplePolygon() {
        assertEquals("WKT String did not evaluate as complex and multiple",WKTUtilities.GeometryType.MULTIPLE_COMPLEX,WKTUtilities.evaluate(COMPLEX_MULTIPLE_POLYGON));
        assertEquals("Geometry did not evaluate as complex and multiple",WKTUtilities.GeometryType.MULTIPLE_COMPLEX,WKTUtilities.evaluate(COMPLEX_MULTIPLE_GEOMETRY));
    }

    @Test
    public void shouldEvaluateToUnknown() {
        assertEquals("WKT String did not evaluate as complex and multiple",WKTUtilities.GeometryType.INVALID,WKTUtilities.evaluate(NON_WKT_STRING));
    }

    @Test
    public void shouldSayWKTStringsAreEquivalent() throws WellKnownTextException {
        assertTrue(WKTUtilities.areStrictlyEquivalent(SIMPLE_SINGLE_POLYGON, SIMPLE_SINGLE_POLYGON_WITH_EXTRA_SPACES));
        assertTrue(WKTUtilities.areStrictlyEquivalent(SIMPLE_SINGLE_POLYGON, SIMPLE_SINGLE_POLYGON_WITH_FEWER_SPACES));
        assertTrue(WKTUtilities.areStrictlyEquivalent(COMPLEX_MULTIPLE_POLYGON, COMPLEX_MULTIPLE_POLYGON_WITH_EXTRA_SPACES));
        assertTrue(WKTUtilities.areStrictlyEquivalent(COMPLEX_MULTIPLE_POLYGON, COMPLEX_MULTIPLE_POLYGON_WITH_FEWER_SPACES));
    }
    
    @Test
	public void shouldSayWKTStringsAreNotEquivalent() throws  WellKnownTextException {
    	assertFalse("Two un-equivalent WKT strings evaluated to be equivalent.",WKTUtilities.areStrictlyEquivalent(SIMPLE_SINGLE_POLYGON, COMPLEX_MULTIPLE_POLYGON));
    }

}