package com.remsdaq.osapplication.utilities;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.ElementNameAndTextQualifier;
import org.custommonkey.xmlunit.XMLAssert;
import org.xml.sax.SAXException;

import java.io.IOException;

public class XMLTestUtilities {

	public static void assertXMLEqualIgnoringElementOrder(String control,
	                                                      String toTest) throws IOException, SAXException {
		Diff diff = new Diff(control, toTest);
		diff.overrideElementQualifier(new ElementNameAndTextQualifier());
		XMLAssert.assertXMLEqual(diff, true);
	}
	
	public static void assertXMLEqualIgnoringElementOrder(String message, String control, String toTest) throws IOException, SAXException {
		Diff diff = new Diff(control, toTest);
		diff.overrideElementQualifier(new ElementNameAndTextQualifier());
		XMLAssert.assertXMLEqual(message, diff, true);
	}
	
	public static void assertXML_Not_EqualIgnoringElementOrder(String control, String toTest) throws IOException, SAXException {
	Diff diff = new Diff(control, toTest);
	diff.overrideElementQualifier(new ElementNameAndTextQualifier());
	XMLAssert.assertXMLEqual(diff, false);
	}
	
}
