package com.remsdaq.osapplication.utilities.entities.incidents;

import com.remsdaq.osapplication.entities.addresses.Address;
import com.remsdaq.osapplication.entities.incidents.Incident;
import com.remsdaq.osapplication.entities.incidents.IncidentBuilder;
import com.remsdaq.osapplication.entities.incidents.IncidentType;
import com.remsdaq.osapplication.entities.incidents.Priority;
import com.remsdaq.osapplication.entities.messages.IncidentMessage;
import com.remsdaq.osapplication.entities.coordinates.EastingNorthing;
import com.remsdaq.osapplication.entities.resources.Person;
import com.remsdaq.osapplication.entities.resources.Resource;
import com.remsdaq.osapplication.entities.resources.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.remsdaq.osapplication.entities.incidents.Incident.Status.STATUS_MOBILISED;

public class IncidentTestUtilities {
	
	private static final Incident.Status STATUS = STATUS_MOBILISED;
	private static final int NUMBER_OF_CALLS = 1;
	private static final String ADDRESS_STRING = "10 Downing Street\nWestminster\nLondon";
	private static final String DESCRIPTION = "Cris de coeur.";
	private static final EastingNorthing MAP_REF = new EastingNorthing(123456L, 654321L);
	private static final Long PRIORITY = 1L;
	private static IncidentType type = new IncidentType();
	private static Address address = new Address();
	private static List<Resource> resources = new ArrayList<>();
	private static IncidentMessage message;
	private static Person commandPerson = new Person(19283L);
	private static Vehicle commandVehicle = new Vehicle(102938L);
	
	private static Incident incident;
	
	public static Incident getIncident() {
		type.setId(52L);
		type.setLevel1Description("Fire");
		type.setLevel1DescriptionCode("F");
		type.setLevel2Description("Postbox");
		type.setDescription(
				"Fires involving the contents and or structure of freestanding post boxes, excluding those attached to or incorporated with a building.");
		type.setPriority(new Priority(4L));
		
		address.setPostcode("SW1A 2AA");
		
		commandPerson.setCallSign("CPT123");
		commandVehicle.setCallSign("CL01");
		
		resources = new ArrayList<>();
		resources.add(commandPerson);
		resources.add(commandVehicle);
		
		incident = new IncidentBuilder(123456L)
				.type(type)
				.status(STATUS)
				.priority(PRIORITY)
				.numberOfCalls(NUMBER_OF_CALLS)
				.transientAddress(address)
				.mapRef(MAP_REF)
				.addressString(ADDRESS_STRING)
				.commandPerson(commandPerson)
				.commandVehicle(commandVehicle)
				.resources(resources)
				.description(DESCRIPTION)
				.build();
		
		return incident;
	}
	
}
