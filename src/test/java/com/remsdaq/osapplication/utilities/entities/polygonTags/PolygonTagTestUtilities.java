package com.remsdaq.osapplication.utilities.entities.polygonTags;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class PolygonTagTestUtilities {
	
	private static final String ID = "id";
	private static final String NAME = "name";
	
	public static PolygonTag generatePolygonTag(int n) {
		PolygonTag tag = new PolygonTag();
		tag.setID(ID + " " + n);
		tag.setName(NAME + " " + n);
		return tag;
	}
	
	public static Set<PolygonTag> generatePolygonTags(int[] ns) {
		Set<PolygonTag> tagSet = new HashSet<>();
		for (int n : ns) {
			tagSet.add(generatePolygonTag(n));
		}
		return tagSet;
	}
	
	public static String generateSinglePolygonTagXmlFragment(int tagNum, int[] polNums, int level) {
		String lead = generateLeadingSpaces(level);
		String xmlFragment =
				lead + "<PolygonTag ID=\"" + enumerate(ID, tagNum) + "\">\n"
				+ lead + "    <Name>" + enumerate(NAME, tagNum) + "</Name>\n";
		
		if (polNums != null)
			xmlFragment += generateSubscriberXml(polNums, level);
		xmlFragment += lead + "</PolygonTag>\n";
		
		return xmlFragment;
	}
	
	public static String enumerate(String s, int n) {
		return s + " " + n;
	}
	
	private static String generateLeadingSpaces(int level) {
		return String.join("", Collections.nCopies(4 * level, " "));
	}
	
	private static String generateSubscriberXml(int[] subNums, int level) {
		String lead = generateLeadingSpaces(level);
		
		String xmlFragment =
				lead + "    <Subscribers>\n"
				+ generateSubscriberXmlFragment(subNums, level)
				+ lead + "    </Subscribers>\n";
		return xmlFragment;
	}
	
	private static String generateSubscriberXmlFragment(int[] subNums, int level) {
		String lead = generateLeadingSpaces(level);
		
		String xmlFragment = "";
		for (int n : subNums) {
			xmlFragment += lead + "        <Subscriber>" + enumerate(ID, n) + "</Subscriber>\n";
		}
		return xmlFragment;
	}
	
}
