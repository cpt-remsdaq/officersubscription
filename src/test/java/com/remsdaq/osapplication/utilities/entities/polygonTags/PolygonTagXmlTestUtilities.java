package com.remsdaq.osapplication.utilities.entities.polygonTags;

import java.util.Collections;

public class PolygonTagXmlTestUtilities {
	
	private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
	
	public static String generateMultiplePolygonTagsXml(int[] subNums, int[] polNums) {
		String xml = XML_HEADER + "<PolygonTags>\n";
		for (int n : subNums) {
			xml += generateSinglePolygonTagXmlFragment(n, polNums, 1);
		}
		xml += "</PolygonTags>";
		return xml;
	}
	
	public static String generateSinglePolygonTagXmlFragment(int subNum, int[] polyNums, int level) {
		String lead = generateLeadingSpaces(level);
		String xmlFragment =
				lead + "<PolygonTag Name=\"" + enumerate(PolygonTagTestValues.NAME, subNum) + "\"/>\n";
		return xmlFragment;
	}
	
	private static String generateLeadingSpaces(int level) {
		return String.join("", Collections.nCopies(4 * level, " "));
	}
	
	public static String enumerate(String s, int n) {
		return s + " " + n;
	}
	
}
