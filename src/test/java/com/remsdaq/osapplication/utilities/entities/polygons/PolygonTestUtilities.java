package com.remsdaq.osapplication.utilities.entities.polygons;

import com.remsdaq.osapplication.entities.polygonTags.PolygonTag;
import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;
import com.remsdaq.osapplication.exceptions.WellKnownTextException;

import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PolygonTestUtilities {
	
	public static void assertPolygonsAreEqual(Polygon controlPolygon, Polygon testPolygon) {
		assertEquals(controlPolygon.getID(), testPolygon.getID());
		assertEquals(controlPolygon.getName(), testPolygon.getName());
		assertEquals(controlPolygon.getDescription(), testPolygon.getDescription());
		assertEquals(controlPolygon.getWKTGeometry(), testPolygon.getWKTGeometry());
	}
	
	public static void assertSubscribersAreEqual(Polygon controlSubscriber, Polygon testSubscriber) {
		String[] test = testSubscriber.getSubscriberStringArray();
		Arrays.sort(test);
		String[] control = controlSubscriber.getSubscriberStringArray();
		Arrays.sort(control);
		assertTrue(Arrays.equals(control, test));
	}
	
	public static void assertSubscribersAreOfSameSize(Polygon controlSubscriber, Polygon testSubscriber) {
		assertEquals(controlSubscriber.getSubscribers().size(), testSubscriber.getSubscribers().size());
	}
	
	public static Polygon generatePolygon(int n, int[] subNums, int[] tagNums) throws WellKnownTextException {
		Polygon pol = generatePolygon(n);
		
		Set<Subscriber> subSet = generateSubscriberSetWithIDs(subNums);
		pol.setSubscribers(subSet);
		
		Set<PolygonTag> tagSet = generatePolygonTagSetWithIDs(tagNums);
		pol.setPolygonTags(tagSet);
		return pol;
	}
	
	private static Set<Subscriber> generateSubscriberSetWithIDs(int[] subNums) {
		Set<Subscriber> subSet = new LinkedHashSet<>();
		for (int s : subNums) {
			subSet.add(generateSubscriberWithID(s));
		}
		return subSet;
	}
	
	private static Set<PolygonTag> generatePolygonTagSetWithIDs(int[] tagNums) {
		Set<PolygonTag> tagSet = new LinkedHashSet<>();
		for (int t : tagNums) {
			tagSet.add(new PolygonTag(PolygonXmlTestUtilities.enumerate(PolygonTestValues.NAME, t)));
		}
		return tagSet;
	}
	
	private static Subscriber generateSubscriberWithID(int s) {
		Subscriber sub = new Subscriber();
		sub.setID(PolygonXmlTestUtilities.enumerate(PolygonTestValues.ID, s));
		return sub;
	}
	
	public static Set<Polygon> generatePolygons(int[] ns) throws WellKnownTextException {
		Set<Polygon> polList = new HashSet<>();
		
		for (int n : ns) {
			polList.add(generatePolygon(n));
		}
		return polList;
	}
	
	public static Polygon generatePolygon(int n) {
		Polygon pol = new Polygon();
		
		pol.setID(PolygonTestValues.ID + " " + n);
		pol.setName(PolygonTestValues.NAME + " " + n);
		pol.setDescription(PolygonTestValues.DESCRIPTION + " " + n);
		pol.setWKTGeometry(PolygonXmlTestUtilities.generateWktGeometry(n));
		return pol;
	}
	
}
