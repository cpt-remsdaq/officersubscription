package com.remsdaq.osapplication.utilities.entities.polygons;

public class PolygonTestValues {
	
	public static final String ID = "id";
	public static final String NAME = "name";
	public static final String DESCRIPTION = "description";
	
}
