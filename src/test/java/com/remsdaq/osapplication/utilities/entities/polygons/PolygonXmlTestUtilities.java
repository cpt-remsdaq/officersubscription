package com.remsdaq.osapplication.utilities.entities.polygons;

import java.util.Collections;

public class PolygonXmlTestUtilities {
	
	private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
	
	public static String enumerate(String s, int n) {
		return s + " " + n;
	}
	
	public static String generateMultiplePolygonsXml(int[] polyNums, int[] subNums, int[] tagNums) {
		
		String xml = XML_HEADER + "<Polygons>\n";
		for (int n : polyNums) {
			xml += generateSinglePolygonXmlFragment(n, subNums, tagNums, 1);
		}
		xml += "</Polygons>";
		return xml;
	}
	
	public static String generateSinglePolygonXml(int polyNum, int[] subNums, int[] tagNums) {
		String xml =
				XML_HEADER
				+ generateSinglePolygonXmlFragment(polyNum, subNums, tagNums, 0);
		return xml;
	}
	
	public static String generateSinglePolygonXmlFragment(int polyNum, int[] subNums, int[] tagNums, int level) {
		String lead = generateLeadingSpaces(level);
		String xmlFragment =
				lead + "<Polygon ID=\"" + enumerate(PolygonTestValues.ID, polyNum) + "\">\n"
				+ lead + "    <Name>" + enumerate(PolygonTestValues.NAME, polyNum) + "</Name>\n"
				+ lead + "    <Description>" + enumerate(PolygonTestValues.DESCRIPTION, polyNum) + "</Description>\n"
				+ lead + "    <WellKnownText>" + generateWktGeometry(polyNum) + "</WellKnownText>\n";
		
		if (subNums != null)
			xmlFragment += generateSubscriberXml(subNums, level);
		if (tagNums != null)
			xmlFragment += generatePolygonTagXml(tagNums, level);
		xmlFragment += lead + "</Polygon>\n";
		
		return xmlFragment;
	}
	
	static String generateSubscriberXml(int[] subNums, int level) {
		String lead = generateLeadingSpaces(level);
		
		String xmlFragment =
				lead + "    <Subscribers>\n"
				+ generateSubscriberXmlFragment(subNums, level)
				+ lead + "    </Subscribers>\n";
		return xmlFragment;
	}
	
	static String generatePolygonTagXml(int[] tagNums, int level) {
		String lead = generateLeadingSpaces(level);
		
		String xmlFragment =
				lead + "    <Tags>\n"
				+ generatePolygonTagXmlFragment(tagNums, level)
				+ lead + "    </Tags>\n";
		return xmlFragment;
	}
	
	private static String generateSubscriberXmlFragment(int[] subNums, int level) {
		String lead = generateLeadingSpaces(level);
		
		String xmlFragment = "";
		for (int n : subNums) {
			xmlFragment += lead + "        <Subscriber>" + enumerate(PolygonTestValues.ID, n) + "</Subscriber>\n";
		}
		return xmlFragment;
	}
	
	private static String generatePolygonTagXmlFragment(int[] tagNums, int level) {
		String lead = generateLeadingSpaces(level);
		String xmlFragment = "";
		for (int n : tagNums) {
			xmlFragment += lead + "        <Tag>" + enumerate(PolygonTestValues.NAME.toUpperCase(), n) + "</Tag>\n";
		}
		return xmlFragment;
	}
	
	private static String generateLeadingSpaces(int level) {
		return String.join("", Collections.nCopies(4 * level, " "));
	}
	
	static String generateWktGeometry(int n) {
		return "POLYGON ((0.0 0.0, 0.0 " + n + ".0, " + n + ".0 " + n + ".0, " + n + ".0 0.0))";
	}
}
