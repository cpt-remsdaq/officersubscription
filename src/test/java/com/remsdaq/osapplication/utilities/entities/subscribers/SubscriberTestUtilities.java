package com.remsdaq.osapplication.utilities.entities.subscribers;

import com.remsdaq.osapplication.entities.polygons.Polygon;
import com.remsdaq.osapplication.entities.subscribers.Subscriber;

import java.util.*;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SubscriberTestUtilities {
	
	public static void assertSubscribersAreEqual(Subscriber controlSubscriber, Subscriber testSubscriber) {
		assertEquals(controlSubscriber.getID(), testSubscriber.getID());
		assertEquals(controlSubscriber.getFirstName(), testSubscriber.getFirstName());
		assertEquals(controlSubscriber.getLastName(), testSubscriber.getLastName());
		assertEquals(controlSubscriber.getEmail(), testSubscriber.getEmail());
		assertEquals(controlSubscriber.getEmailPriority(), testSubscriber.getEmailPriority());
		assertEquals(controlSubscriber.getMobile(), testSubscriber.getMobile());
		assertEquals(controlSubscriber.getMobilePriority(), testSubscriber.getMobilePriority());
		assertEquals(controlSubscriber.getPager(), testSubscriber.getPager());
		assertEquals(controlSubscriber.getPagerPriority(), testSubscriber.getPagerPriority());
	}
	
	public static void assertSubscriptionsAreEqual(Subscriber controlSubscriber, Subscriber testSubscriber) {
		System.out.println(testSubscriber.getPolygons().size());
		System.out.println(controlSubscriber.getPolygons().size());
		
		String[] test = testSubscriber.getPolygonStringArray();
		Arrays.sort(test);
		String[] control = controlSubscriber.getPolygonStringArray();
		Arrays.sort(control);
		System.out.println("CONTROL: " + Arrays.toString(control) + "\nTEST: " + Arrays.toString(test));
		assertTrue(Arrays.equals(control, test));
	}
	
	public static void assertSubscriptionsAreOfSameSize(Subscriber controlSubscriber, Subscriber testSubscriber) {
		assertEquals(controlSubscriber.getPolygons().size(), testSubscriber.getPolygons().size());
	}
	
	public static String enumerate(String s, int n) {
		return s + " " + n;
	}
	
	public static String enumerate(int s, int n) {
		return s + "" + n;
	}
	
	public static Subscriber generateSubscriber(int n, int[] polNums) {
		Subscriber sub = generateSubscriber(n);
		
		Set<Polygon> polSet = generatePolygonSetWithIDs(polNums);
		sub.setPolygons(polSet);
		
		return sub;
	}
	
	private static Set<Polygon> generatePolygonSetWithIDs(int[] polNums) {
		Set<Polygon> polSet = new LinkedHashSet<>();
		for (int p : polNums) {
			polSet.add(generatePolygonWithID(p));
		}
		return polSet;
	}
	
	private static Polygon generatePolygonWithID(int p) {
		Polygon pol = new Polygon();
		pol.setID(enumerate(SubscriberTestValues.ID, p));
		return pol;
	}
	
	public static Set<Subscriber> generateSubscribers(int[] ns) {
		Set<Subscriber> subList = new HashSet<>();
		
		for (int n : ns) {
			subList.add(generateSubscriber(n));
		}
		return subList;
	}
	
	public static Subscriber generateSubscriber(int n) {
		Subscriber sub = new Subscriber();
		
		String emailPriority = SubscriberTestValues.EMAIL_PRIORITY + "" + n;
		String mobilePriority = SubscriberTestValues.MOBILE_PRIORITY + "" + n;
		String pagerPriority = SubscriberTestValues.PAGER_PRIORITY + "" + n;
		
		sub.setID(SubscriberTestValues.ID + " " + n);
		sub.setFirstName(SubscriberTestValues.FIRST_NAME + " " + n);
		sub.setLastName(SubscriberTestValues.LAST_NAME + " " + n);
		sub.setEmail(SubscriberTestValues.EMAIL + " " + n);
		sub.setMobile(SubscriberTestValues.MOBILE + " " + n);
		sub.setPager(SubscriberTestValues.PAGER + " " + n);
		sub.setEmailPriority(Integer.parseInt(emailPriority));
		sub.setMobilePriority(Integer.parseInt(mobilePriority));
		sub.setPagerPriority(Integer.parseInt(pagerPriority));
		return sub;
	}
	
}
