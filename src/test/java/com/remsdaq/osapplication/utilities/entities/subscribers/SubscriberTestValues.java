package com.remsdaq.osapplication.utilities.entities.subscribers;

public class SubscriberTestValues {
	
	public static final String ID = "id";
	public static final String FIRST_NAME = "first name";
	public static final String LAST_NAME = "last name";
	public static final String EMAIL = "email";
	public static final String MOBILE = "12345";
	public static final String PAGER = "67890";
	public static final int EMAIL_PRIORITY = 1;
	public static final int MOBILE_PRIORITY = 2;
	public static final int PAGER_PRIORITY = 3;
	
}
