package com.remsdaq.osapplication.utilities.entities.subscribers;

import java.util.Collections;

public class SubscriberXmlTestUtilities {
	
	private static final String XML_HEADER = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n";
	
	public static String generateMultipleSubscribersXml(int[] subNums, int[] polNums) {
		String xml = XML_HEADER + "<Subscribers>\n";
		for (int n : subNums) {
			xml += generateSingleSubscriberXmlFragment(n, polNums, 1);
		}
		xml += "</Subscribers>";
		return xml;
	}
	
	public static String generateSingleSubscriberXmlFragment(int subNum, int[] polyNums, int level) {
		String lead = generateLeadingSpaces(level);
		String xmlFragment =
				lead + "<Subscriber ID =\"" + enumerate(SubscriberTestValues.ID, subNum) + "\">\n"
				+ lead + "    <FirstName>" + enumerate(SubscriberTestValues.FIRST_NAME, subNum) + "</FirstName>\n"
				+ lead + "    <LastName>" + enumerate(SubscriberTestValues.LAST_NAME, subNum) + "</LastName>\n"
				+ lead + "    <Email>" + enumerate(SubscriberTestValues.EMAIL, subNum) + "</Email>\n"
				+ lead + "    <EmailPriority>" + enumerate(SubscriberTestValues.EMAIL_PRIORITY, subNum) + "</EmailPriority>\n"
				+ lead + "    <Mobile>" + enumerate(SubscriberTestValues.MOBILE, subNum) + "</Mobile>\n"
				+ lead + "    <MobilePriority>" + enumerate(SubscriberTestValues.MOBILE_PRIORITY, subNum) + "</MobilePriority>\n"
				+ lead + "    <Pager>" + enumerate(SubscriberTestValues.PAGER, subNum) + "</Pager>\n"
				+ lead + "    <PagerPriority>" + enumerate(SubscriberTestValues.PAGER_PRIORITY, subNum) + "</PagerPriority>\n";
		
		if (polyNums != null)
			xmlFragment += generatePolygonXml(polyNums, level);
		xmlFragment += lead + "</Subscriber>\n";
		return xmlFragment;
	}
	
	private static String generateLeadingSpaces(int level) {
		return String.join("", Collections.nCopies(4 * level, " "));
	}
	
	public static String enumerate(String s, int n) {
		return s + " " + n;
	}
	
	public static String enumerate(int s, int n) {
		return s + "" + n;
	}
	
	private static String generatePolygonXml(int[] polyNums, int level) {
		String lead = generateLeadingSpaces(level);
		String xmlFragment = lead + "    <Polygons>\n"
		                     + generatePolygonXmlFragment(polyNums, level)
		                     + lead + "    </Polygons>\n";
		return xmlFragment;
	}
	
	private static String generatePolygonXmlFragment(int[] polyNums, int level) {
		String lead = generateLeadingSpaces(level);
		String xmlFragment = "";
		for (int n : polyNums) {
			xmlFragment += lead + "        <Polygon>" + enumerate(SubscriberTestValues.ID, n) + "</Polygon>\n";
		}
		return xmlFragment;
	}
	
	
}
